%Set default plot parameters
set(0,'DefaultAxesFontSize',8)
set(0,'DefaultLineLinewidth',4)
set(0, 'DefaultAxesFontWeight','normal')
set(0, 'DefaultAxesLineWidth',4)
set(0, 'DefaultFigureColor','w')
set(0, 'DefaultTextFontSize',8)
set(0, 'DefaultTextFontWeight','normal')

d1 = logspace(-5,0,100);
d2 = logspace(-5,0,100);
h1 = 1;
h2 = 1;
C1 = .005;
C2 = .001;
E0 = .05;
E1 = -.01;
E2 = .01;
beta = [0];
alpha = [1];
hf = figure('color','w')
clf
p = 1;
k = 1;
Ed = []
for i = 1:length(d1)
    for j = 1:length(d2)
        E3 = -beta(p)*min(E1,E2)+min(E1,E2);
        Ed(i,j) = (C1^h1*C2^h2*E0 + d1(i).^h1*C2^h2*E1 + d2(j).^h2*C1^h1*E2 + alpha(k)*d1(i)^h1*d2(j)^h2*E3)/(C1^h1*C2^h2 + d1(i)^h1*C2^h2 + d2(j)^h2*C1^h1 + alpha(k)*d1(i)^h1*d2(j)^h2);
    end
end
%Downloaded from http://www.kennethmoreland.com/color-maps/
map = diverging_map(0:.001:1,[.23,.299,.745],[.706,.016,.15]);
[X,Y] = meshgrid(d1,d2);
C = contour(X,Y,Ed,20);
clf
hold on
surf(X,Y,Ed,'lines','None')
colormap(map)
set(gca,'xscale','log','yscale','log','linewidth',2.,'xticklabels',[],'yticklabels',[],'zticklabels',[],'xtick',logspace(-5,-1,5),'ytick',logspace(-5,-1,5))
grid off
axis([min(d1),max(d1),min(d2),max(d2),-.015,max(Ed(:))])
view([67,46])
xlabel('log([Drug 1])')
ylabel('log([Drug 2])')
zlabel('DIP')
hold on
plot3(d1,min(d2)*ones(100,1),Ed(1,:),'color',[.7,.7,.7])
plot3(min(d1)*ones(100,1),d2,Ed(:,1),'k')
scatter3(min(d1),min(d2),max(Ed(:)),150,'r','filled')
scatter3(max(d1),min(d2),Ed(1,end),150,'r','filled')
scatter3(min(d1),max(d2),Ed(end,1),150,'r','filled')
scatter3(max(d1),max(d2),min(Ed(:)),150,'r','filled')
[~,m_idx] = min((d2-C2).^2);
scatter3(C2,min(d2),Ed(1,m_idx),150,'r','filled')
[~,m_idx] = min((d1-C1).^2);
scatter3(min(d2),C1,Ed(m_idx,1),150,'r','filled')

s = contourdata(C); 
for i = 1:length(s)
    cont = [];
    for j = 1:length(s(i).xdata)
            cont(j) = (C1^h1*C2^h2*E0 + s(i).xdata(j).^h1*C2^h2*E1 + s(i).ydata(j).^h2*C1^h1*E2 + alpha(k)*s(i).xdata(j)^h1*s(i).ydata(j)^h2*E3)/(C1^h1*C2^h2 + s(i).xdata(j)^h1*C2^h2 + s(i).ydata(j)^h2*C1^h1 + alpha(k)*s(i).xdata(j)^h1*s(i).ydata(j)^h2);                    
    end
    %plot3(s(i).xdata,s(i).ydata,cont,'color',map(i,:),'linewidth',1)
    plot3(s(i).ydata,s(i).xdata,cont,'k','linewidth',1)
end
set(gca,'ZGrid','on')
h(1) = line([min(d1),min(d1)],[min(d2),max(d2)],[0,0])
h(2) = line([min(d1),max(d1)],[min(d2),min(d2)],[0,0])
h(3) = line([max(d1),max(d1)],[min(d2),max(d2)],[0,0])
h(4) = line([min(d1),max(d1)],[max(d2),max(d2)],[0,0])
for i=1:4
    h(i).Color=[0,0,0];
end
xlabel([])
ylabel([])
zlabel([])
saveas(hf,'2D_hill.png')


h = figure('color','w')
figure(h)
clf
hold on
plot(d1,Ed(1,:),'color',[.7,.7,.7])
plot(d2,Ed(:,1),'k')
scatter(min(d1),max(Ed(:)),150,'r','filled')
scatter(max(d1),Ed(1,end),150,'r','filled')
scatter(max(d2),Ed(end,1),150,'r','filled')
[~,m_idx] = min((d2-C2).^2);
scatter(C2,Ed(1,m_idx),150,'r','filled')
[~,m_idx] = min((d1-C1).^2);
scatter(C1,Ed(m_idx,1),150,'r','filled')
plot([min(d1),max(d1)],[0,0],'k')
set(gca,'xscale','log','linewidth',2.,'xticklabels',[],'yticklabels',[],'xtick',logspace(-5,-1,5))
saveas(h,'1D_hill.png')



%%%3D cube
nx = 20;
ny = 20;
nz = 20;
d1 = logspace(-5,0,nx);
d2 = logspace(-5,0,ny);
d3 = logspace(-5,0,nz);
h1 = 1;
h2 = 1;
h3 = 1;
C1 = .005;
C2 = .001;
C3 = .003;
E0 = .05;
E1 = -.01;
E2 = .01;
E3 = -.025;
beta = [0];
alpha12=1;alpha21=1;alpha13=1;alpha31=1;alpha23=1;alpha32=1;alpha123=1;alpha213=1;alpha312=1;
gamma12=1;gamma21=1;gamma13=1;gamma31=1;gamma23=1;gamma32=1;gamma123=1;gamma213=1;gamma312=1;
E12 = min(E1,E2);
E23 = min(E2,E3);
E13 = min(E1,E3);
E123 = min([E12,E13,E23]);

[X,Y,Z]=meshgrid(d1,d2,d3);

r1=E0*100;
r2=E0*100;
r3=E0*100;
V = zeros(nx,ny,nz);
for i = 1:length(d1)
    for j = 1:length(d2)
        for k = 1:length(d3)
            V(i,j,k) = threeDeg(d1(i),d2(j),d3(k),h1,h2,h3,C1,C2,C3,E0,E1,E2,E3,E12,E13,E23,E123,r1,r2,r3,alpha12,alpha21,alpha13,alpha31,alpha23,alpha32,alpha123,alpha213,alpha312,gamma12,gamma21,gamma13,gamma31,gamma23,gamma32,gamma123,gamma213,gamma312);
        end
    end
end


hf = figure('color','w')
%Downloaded from http://www.kennethmoreland.com/color-maps/
map = diverging_map(0:.001:1,[.23,.299,.745],[.706,.016,.15]);
colormap(map)
Sz = logspace(-5,-1.8,5);
Sy = [];
Sx = [];
cvals = linspace(E0,E123,20)
h = contourslice(X,Y,Z,V,Sx,Sy,Sz,cvals)
set(gca,'xscale','log','zscale','log','yscale','log','linewidth',2.,'xticklabels',[],'yticklabels',[],'zticklabels',[],'xtick',logspace(-5,-1,5),'ytick',logspace(-5,-1,5),'ztick',logspace(-5,-1,5))
for i=1:length(h)
    h(i).LineWidth=2;
end
c = colorbar
view(60,10)
box on
c.TickLabels = [];
saveas(hf,'3D_contourslice.png')




hf = figure('color','w','units','normalized','position',[.1,.1,.8,.8])
subplot(1,2,2)
%Downloaded from http://www.kennethmoreland.com/color-maps/
map = diverging_map(0:.001:1,[.23,.299,.745],[.706,.016,.15]);
colormap(map)
Sz = logspace(-5,-1.8,5);
Sy = [];
Sx = [];
cvals = linspace(E0,E123,20)
h = contourslice(X,Y,Z,V,Sx,Sy,Sz,cvals)
set(gca,'xscale','log','zscale','log','yscale','log','linewidth',2.,'xticklabels',[],'yticklabels',[],'zticklabels',[],'xtick',logspace(-5,-1,5),'ytick',logspace(-5,-1,5),'ztick',logspace(-5,-1,5))
set(gca,'fontsize',20)
for i=1:length(h)
    h(i).LineWidth=2;
end
c = colorbar
view(60,10)
box on
c.TickLabels = [];
xlabel('log(drug1)')
ylabel('log(drug2)')
zlabel('log(drug3)')

subplot(1,2,1)
hold on
plot(squeeze(X(1,:,1)),squeeze(V(1,:,1)),'k')
plot(squeeze(Y(:,1,1)),squeeze(V(:,1,1)),'m')
plot(squeeze(Z(1,1,:)),squeeze(V(1,1,:)),'c')
set(gca,'xscale','log','linewidth',2.,'xticklabels',[],'yticklabels',[],'xtick',logspace(-5,-1,5),'fontsize',20)
xlabel('log(drug)')
ylabel('Drug effect')
legend('drug1','drug2','drug3')
plot([xlim


