%Assuming proliferation doesn't matter + hill synergy + 3Drugs
syms r1 r1_1 r2 r2_1 r3 r3_1 positive
syms d1 d2 d3 real
syms a12 a21 a13 a31 a23 a32 a123 a213 a312 real
syms g12 g21 g13 g31 g23 g32 g123 g213 g312 real
syms E0 E1 E2 E3 E12 E13 E23 E123 real
syms C1 C2 C3 h1 h2 h3 positive
r1_1 = C1^h1*r1;
r2_1 = C2^h2*r2;
r3_1 = C3^h3*r3;
T = [-r1*d1^h1-r2*d2^h2-r3*d3^h3,r1_1                                                       ,r2_1                                                       ,r3_1                                                       ,0                                              ,0                                              ,0                                              ,0;
    r1*d1^h1                    ,-r1_1-r2^g12*(a12*d2)^(g12*h2)-r3^g13*(a13*d3)^(g13*h3)    ,0                                                          ,0                                                          ,r2_1^g12                                       ,r3_1^g13                                       ,0                                              ,0;
    r2*d2^h2                    ,0                                                          ,-r2_1-r1^g21*(a21*d1)^(g21*h1)-r3^g23*(a23*d3)^(g23*h3)    ,0                                                          ,r1_1^g21                                       ,0                                              ,r3_1^g23                                       ,0;
    r3*d3^h3                    ,0                                                          ,0                                                          ,-r3_1-r2^g32*(a32*d2)^(g32*h2)-r1^g31*(a31*d1)^(g31*h1)    ,0                                              ,r1_1^g31                                       ,r2_1^g32                                       ,0;
    0                           ,r2^g12*(a12*d2)^(g12*h2)                                   ,r1^g21*(a21*d1)^(g21*h1)                                   ,0                                                          ,-r2_1^g12-r1_1^g21-r3^g312*(a312*d3)^(g312*h3) ,0                                              ,0                                              ,r3_1^g312;
    0                           ,r3^g13*(a13*d3)^(g13*h3)                                   ,0                                                          ,r1^g31*(a31*d1)^(g31*h1)                                   ,0                                              ,-r3_1^g13-r1_1^g31-r2^g213*(a213*d2)^(g213*h2) ,0                                              ,r2_1^g213;
    0                           ,0                                                          ,r3^g23*(a23*d3)^(g23*h3)                                   ,r2^g32*(a32*d2)^(g32*h2)                                   ,0                                              ,0                                              ,-r2_1^g32-r3_1^g23-r1^g123*(a123*d1)^(g123*h1) ,r1_1^g123;
    1                           ,1                                                          ,1                                                          ,1                                                          ,1                                              ,1                                              ,1                                              ,1];
Eqn = [E0,E1,E2,E3,E12,E13,E23,E123] * inv(T)*[0;0;0;0;0;0;0;1];
eq1 = simplify(Eqn);
eq2 = combine(eq1);





fileID = fopen('syn3D.py','w');
nbytes = fprintf(fileID,'%s',char(eq1))
fclose(fileID)