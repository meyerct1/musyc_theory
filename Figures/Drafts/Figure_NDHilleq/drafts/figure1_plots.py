#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 27 10:17:03 2018

@author: xnmeyer
"""
import pandas as pd
import numpy as np
from plotting_surfaces import plot_surface
fit_params = {}
fit_params['min_conc_d1'] = np.power(10.,-10)
fit_params['min_conc_d2'] = np.power(10.,-10)
fit_params['max_conc_d1'] = np.power(10.,-5)
fit_params['max_conc_d2'] = np.power(10.,-5)
fit_params['h1'] = 1.
fit_params['h2'] = 1.
fit_params['log_C1'] = -7.
fit_params['log_C2'] = -7.5
fit_params['E0'] = .05
fit_params['E1'] = .005
fit_params['E2'] = .01
fit_params['E3'] = -.05
fit_params['log_alpha1']=.5
fit_params['log_alpha2']=.5
fit_params['r1']=100.*fit_params['E0']
fit_params['r2']=100.*fit_params['E0']
fit_params['drug1_name']='d1'
fit_params['drug2_name']='d2'
fit_params['expt'] = 'cartoon'
fit_params = pd.DataFrame([fit_params])
plot_surface(fit_params,metric_name="Drug Effect",fname='fig1a')



####################################################################
#####DSD with different alpha beta
####################################################################
###Import Packages
####################################################################
import pandas as pd
import numpy as np
import matplotlib.pylab as plt
#from matplotlib.patches import FancyArrowPatch
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

fit_params['E2'] = .015
fig = plt.figure(figsize=(3.5,3.5))
ax = plt.subplot(111)
#1D hill equation
def hill(d,E0,E1,C,h,alpha):
    return E1+(E0-E1)*(C**h)/(C**h+(alpha*d)**h)

alpha = [1,-1]
beta = [.25,-.4]
t = []
x = np.logspace(np.log10(fit_params['min_conc_d1']/10),np.log10(fit_params['max_conc_d2']),50)
t.append(hill(x,fit_params['E0'].values[0],fit_params['E1'].values[0],np.power(10.,fit_params['log_C1'].values[0]),fit_params['h1'].values[0],alpha=1))
for a in alpha:
    for b in beta:
        E3 = -(fit_params['E0']-fit_params['E1'].values[0])*b+fit_params['E1']
        t.append(hill(x,fit_params['E2'].values[0],E3.values[0],np.power(10.,fit_params['log_C1'].values[0]),fit_params['h1'].values[0],alpha=np.power(10.,a)))

plt.xlim(-1,1)
rng = np.max(t)-np.min(t)
pad = .2
plt.ylim(-rng-pad*rng,rng+pad*rng)
from sklearn.preprocessing import MinMaxScaler
log_x = np.log10(x)
mm = MinMaxScaler(feature_range=(pad,1-pad))
x1 = mm.fit_transform(log_x.reshape(-1,1)).reshape(len(log_x))
x2 = x1-1
plt.plot(x1,t[0]+abs(np.min(t))+pad*rng/2,color='k',linestyle='-',linewidth=3)
plt.plot(x2,t[0]+abs(np.min(t))+pad*rng/2,color='k',linestyle='-',linewidth=3)
plt.plot(x1,t[0]-abs(np.max(t))-pad*rng/2,color='k',linestyle='-',linewidth=3)
plt.plot(x2,t[0]-abs(np.max(t))-pad*rng/2,color='k',linestyle='-',linewidth=3)    

plt.plot(x1,t[1]+abs(np.min(t))+pad*rng/2,color='b',linestyle='-',linewidth=3)
plt.plot(x2,t[3]+abs(np.min(t))+pad*rng/2,color='b',linestyle='-',linewidth=3)
plt.plot(x1,t[2]-abs(np.max(t))-pad*rng/2,color='b',linestyle='-',linewidth=3)
plt.plot(x2,t[4]-abs(np.max(t))-pad*rng/2,color='b',linestyle='-',linewidth=3)    

ax.axhline(y=0, color='k')
ax.axvline(x=0, color='k')
ax.spines['top'].set_visible(False)
ax.spines['bottom'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.spines['left'].set_visible(False)
ax.set_xticks([])
ax.set_yticks([])

ax.axhline(linewidth=1.7, color="black")
ax.axvline(linewidth=1.7, color="black")
from matplotlib.transforms import BlendedGenericTransform
ax.text(0, 1.05, r'$\beta$', transform=BlendedGenericTransform(ax.transData, ax.transAxes), ha='center')
ax.text(1.03, 0, r'$\alpha$', transform=BlendedGenericTransform(ax.transAxes, ax.transData), va='center')
ax.annotate('',xy=(0,0),xytext=(0,plt.ylim()[1]+.005),arrowprops=dict(arrowstyle="<|-"))
ax.annotate('',xy=(0,0),xytext=(plt.xlim()[1]+.06,0),arrowprops=dict(arrowstyle="<|-"))
plt.savefig('fig1b.pdf',pad_inches=0.,format='pdf')
#################################################################
#Now plot data from initial BRAF MEK experiment
df = pd.read_csv('../rnai+drug_dose_response_surfaces/07-02-2018/MasterResults.csv')
path = '../rnai+drug_dose_response_surfaces/07-02-2018/'
zmin = df['E3_obs'].min()
zmax = df['E0'].max()
if np.abs(zmin) > np.abs(zmax): zmax = np.abs(zmin)
else: zmin = -np.abs(zmax)
zlim = (zmin,zmax)
for i in df.index:
    plot_surface(pd.DataFrame([df.loc[i]]),data_pts=df['expt'].loc[i],zlim=(-.03,.03),fname=df['drug1_name'].loc[i]+'_'+df['drug2_name'].loc[i],path=path,zero_conc=1)


#################################################################
#DSDs
#####################################
#Function to plot the DSD for RNAi+Drug
############################################################
###Import packages
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
from adjustText import adjust_text
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 3}
rc('font', **font)
rc('axes',**axes)
from matplotlib.patches import Patch
from matplotlib.lines import Line2D
legend_elements = [Line2D([0], [0], color='r', lw=8, label='siRNA_CON'),
                   Line2D([0], [0], color='b', lw=8, label='siRNA_BRAF'),
                   Line2D([0], [0], color=(128./255.,0.,128./255.,1.), lw=8, label='siRNA_MKK7'),
                   Line2D([0], [0], color='k', lw=8, label='siRNA_TOX'),
                   Line2D([0], [0], marker='o', color='w', label='PLX4720', markerfacecolor=(128./255.,128./255.,128./255.,1.), markersize=12),
                   Line2D([0], [0], marker='D', color='w', label='TRAMETINIB', markerfacecolor=(128./255.,128./255.,128./255.,1.), markersize=12)
                   ]
                   

T = pd.read_csv('../rnai+drug_dose_response_surfaces/07-02-2018/MasterResults.csv')
d1_colors = {'sirna_con':'r','sirna_braf':'b','sirna_mek':(128./255.,0.,128./255.,1.),'sirna_tox':'k'}
d2_shapes = {'plx4720':'o','trametinib':'D'}
plt.figure(figsize=(7.5,2.5))
ax = []
ax.append(plt.subplot2grid((2,3),(0,0)))
ax.append(plt.subplot2grid((2,3),(1,0)))
ax.append(plt.subplot2grid((2,3),(0,1),rowspan=2))
ax.append(plt.subplot2grid((2,3),(0,2),rowspan=2))
plt.subplots_adjust(wspace=.4,hspace=.4)
plt.sca(ax[0])
sub_T = T[T['drug1_name']=='plx4720']
sub_T.sort_values('beta_obs',inplace=True)
sub_T.reset_index(inplace=True)
for i in sub_T.index:
    plt.bar(i,sub_T['beta_obs'].loc[i],color=d1_colors[sub_T['drug2_name'].loc[i]])
    plt.errorbar(i,sub_T['beta_obs'].loc[i],yerr=sub_T['beta_obs_std'].loc[i],color='k')

plt.title('PLX4720')
ax[0].set_xticklabels([])
ax[0].set_ylabel('$\\beta_{obs}$')
ax[0].set_ylim((T['beta_obs'].min()-.2,T['beta_obs'].max()+.2))
plt.plot(plt.xlim(),[0,0],'--',color='k')


plt.sca(ax[1])
sub_T = T[T['drug1_name']=='trametinib']
sub_T.sort_values('beta_obs',inplace=True)
sub_T.reset_index(inplace=True)
for i in sub_T.index:
    plt.bar(i,sub_T['beta_obs'].loc[i],color=d1_colors[sub_T['drug2_name'].loc[i]])
    plt.errorbar(i,sub_T['beta_obs'].loc[i],yerr=sub_T['beta_obs_std'].loc[i],color='k')

plt.title('Trametinib')
ax[1].set_xticklabels([])
ax[1].set_ylim((T['beta_obs'].min()-.2,T['beta_obs'].max()+.2))
plt.plot(plt.xlim(),[0,0],'--',color='k')


plt.sca(ax[2])
text = []
for i in T.index:
    plt.scatter(T['log_alpha1'].loc[i],T['log_alpha2'].loc[i],s=100,zorder=0,c=d1_colors[T['drug2_name'].loc[i]],marker=d2_shapes[T['drug1_name'].loc[i]])
    plt.errorbar(T['log_alpha1'].loc[i],T['log_alpha2'].loc[i],xerr=T['log_alpha1_std'].loc[i],yerr=T['log_alpha2_std'].loc[i],color='k')
x_lim = plt.xlim()
y_lim = plt.ylim()
plt.plot([0,0],y_lim,color='k',linestyle='--')
plt.plot(x_lim,[0,0],color='k',linestyle='--')
plt.xlabel('log($\\alpha_1$)\n(Drug -> RNAi)')
plt.ylabel('log($\\alpha_2$)\n(RNAi -> Drug)')

ax[3].legend(handles=legend_elements, loc='left')
ax[3].set_frame_on(False)
ax[3].set_xticks([])
ax[3].set_yticks([])

plt.tight_layout()
plt.savefig('fig1d.pdf',pad_inches=0.,format='pdf')

    
