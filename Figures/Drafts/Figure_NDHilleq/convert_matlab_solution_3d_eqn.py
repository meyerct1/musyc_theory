#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 10 10:06:03 2018

@author: xnmeyer
"""

#Convert matlab output 3D equation to python code
str1 = 'def Edrug3D_hill(d1,d2,d3,E0,E1,E2,E3,E12,E13,E23,E123,C1,C2,C3,h1,h2,h3,r1,r2,r3,a12,a21,a13,a31,a23,a32,a123,a213,a312,g12,g21,g13,g31,g23,g32,g123,g213,g312):\n\t return '
fid = open('3d-eqn.txt')
eqn = fid.read()
fid.close()
#eqn = 'd1+d2+d3+E0+E1+E2+E3+E12+E13+E23+E123+C1+C2+C3+h1+h2+h3+r1+r2+r3+a12+a21+a13+a31+a23+a32+a123+a213+a312+g12+g21+g13+g31+g23+g32+g123+g213+g312'
eqn = eqn.replace('^','**')
eqnstr = str1+eqn
fid = open('threeDHillFun.py','w')
fid.write(eqnstr)
fid.close()


from threeDHillFun import Edrug3D_hill
import numpy as np
import pandas as pd

noise = 0.001
N = 7
rep = 3
E0=1.
E1=0.
E2=0.
E3=0.
E12=-.25 
E13=-.25
E23=-.25
E123=-.5
h1=1.
h2=1.
h3=1.
C1=10e-5
C2=10e-5
C3=10e-5
r1=100.
r2=100.
r3=100.
a12=1.
a21=1.
a13=1.
a31=1.
a23=1.
a32=1.
a123=1.
a213=1.
a312=1.
g12=1.
g21=1.
g13=1.
g31=1.
g23=1.
g32=1.
g123=1.
g213=1.
g312=1.
d1 = []
d2 = []
d3 = []
dip = []
for r in range(rep):
    DD1,DD2,DD3 = np.meshgrid(np.concatenate(([0],np.logspace(-10,0,N))),np.concatenate(([0],np.logspace(-10,0,N))),np.concatenate(([0],np.logspace(-10,0,N))))
    d1_tmp = DD1.reshape((-1,))
    d2_tmp = DD2.reshape((-1,))
    d3_tmp = DD3.reshape((-1,))
    dip_tmp = Edrug3D_hill(d1_tmp,d2_tmp,d3_tmp,E0,E1,E2,E3,E12,E13,E23,E123,C1,C2,C3,h1,h2,h3,r1,r2,r3,a12,a21,a13,a31,a23,a32,a123,a213,a312,g12,g21,g13,g31,g23,g32,g123,g213,g312)
    dip_tmp = np.random.randn(len(dip_tmp))*noise+dip_tmp
    d1 = np.concatenate((d1,d1_tmp))
    d2 = np.concatenate((d2,d2_tmp))
    d3 = np.concatenate((d3,d3_tmp))
    dip = np.concatenate((dip,dip_tmp))
df = pd.DataFrame([])
df['drug1.conc']=d1
df['drug2.conc']=d2
df['drug3.conc']=d3
df['drug1.units']='M'
df['drug2.units']='M'
df['drug3.units']='M'
df['drug1']='d1_'
df['drug2']='d2_'
df['drug3']='d3_'
df['effect']=dip
dip_sd=np.ones(len(dip))*noise*(2*1.96)
df['effect.95ci'] = dip_sd
df['sample']='cell_line'
df.to_csv('TestData-3D.csv',index=False)



import scipy.optimize
hill_fun = lambda (d1,d2,d3),E0,E1,E2,E3,E12,E13,E23,E123,C1,C2,C3,h1,h2,h3,a12,a21,a13,a31,a23,a32,a123,a213,a312,g12,g21,g13,g31,g23,g32,g123,g213,g312: Edrug3D_hill(d1,d2,d3,E0,E1,E2,E3,E12,E13,E23,E123,C1,C2,C3,h1,h2,h3,100.,100.,100.,a12,a21,a13,a31,a23,a32,a123,a213,a312,g12,g21,g13,g31,g23,g32,g123,g213,g312)
st =             [E0,E1,E2,E3,E12,E13,E23,E123,C1,C2,C3,h1,h2,h3,a12,a21,a13,a31,a23,a32,a123,a213,a312,g12,g21,g13,g31,g23,g32,g123,g213,g312]
#Begin fitting    
popt, pcov = scipy.optimize.curve_fit(hill_fun,(d1,d2,d3),dip,sigma=dip_sd,p0=st,maxfev=100000,absolute_sigma=True)
perr = np.sqrt(np.diag(pcov))
print('Testing for co-linearities in non-linear optimization....')
pcor = np.matmul(np.matmul(np.linalg.inv(np.diagflat(np.sqrt(np.diag(pcov)))),pcov),np.linalg.inv(np.diagflat(np.sqrt(np.diag(pcov)))))
T['mx_p_corr'] = np.max(np.abs(pcor-np.diagflat(np.diag(pcor))))


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from mpl_toolkits.mplot3d import axes3d, Axes3D #<-- Note the capitalization! 

plt.figure()
ax = plt.subplot(111,projection='3d')
d1c = d1.copy();d2c=d2.copy();d3c=d3.copy()
d1c[d1c==0]=-11;d2c[d2c==0]=-11;d3c[d3c==0]=-11
ax.scatter(np.log10(d1c),np.log10(d2c),np.log10(d3c),c=dip)

plt.figure()
ax = plt.subplot(111,projection='3d')
d1c = d1.copy();d2c=d2.copy();d3c=d3.copy()
d1c[d1c==0]=-11;d2c[d2c==0]=-11;d3c[d3c==0]=-11
ax.scatter(np.log10(d1c),np.log10(d2c),np.log10(d3c),c=hill_fun((d1,d2,d3),*popt))



