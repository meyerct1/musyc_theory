#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  8 13:43:32 2018

@author: xnmeyer
"""
import numpy as np
import pandas as pd
from SynergyCalculator.NDHillFun import Edrug2D_NDB


noise = .01
data_mat = [5,7,10,15,25]
mx_dose = np.logspace(0,4,5)
num_replicates = 2
df = pd.DataFrame()
cnt = 0
for N in data_mat:
        for e1,log_alpha in enumerate([-2,-1,0,1,2]):
            for e2,beta in enumerate([-.5,-.25,0,.25,.5]):
                for e3,h in enumerate([.5,1.,5.]):
                    for e4,mxd in enumerate(mx_dose):
                        for _ in range(num_replicates):
                            df_tmp = pd.DataFrame()
                            E0=1.
                            E1=0.3333
                            E2=0.3333
                            E3=-beta*(E0-min(E1,E2))+min(E1,E2) 
                            h1=h
                            h2=h
                            C1=10e-5
                            C2=10e-5
                            r1=100.*E0
                            r2=100.*E0
                            alpha1=10**log_alpha
                            alpha2=10**log_alpha
                            gamma1=1.
                            gamma2=1.
                            DD1,DD2 = np.meshgrid(np.concatenate(([0],np.logspace(-10,np.log10(mxd*C1),N))),np.concatenate(([0],np.logspace(-10,np.log10(mxd*C2),N))))
                            d1 = DD1.reshape((-1,))
                            d2 = DD2.reshape((-1,))
                            dip = Edrug2D_NDB((d1,d2),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2)
                            dip = np.random.randn(len(dip))*noise+dip
                            dip_sd = np.ones(len(dip))*noise
                            df_tmp['drug1.conc']=d1
                            df_tmp['drug2.conc']=d2
                            df_tmp['effect.95ci'] =dip_sd
                            df_tmp['effect'] = dip
                            df_tmp['drug2'] = 'd2'
                            df_tmp['drug1'] = 'd1'
                            df_tmp['drug1.units'] = 'M'
                            df_tmp['drug2.units'] = 'M'
                            df_tmp['expt.date'] = '00-00-0000'
                            df_tmp['sample'] = 'NumData-' +str(N)+'_beta-'+str(e1)+'_alpha-'+str(e2)+'_h-'+str(e3)+'_mxdose-'+str(e4)
                            df = df.append(df_tmp,ignore_index=True)
                    cnt+=1
    

df.to_csv('testing_dataset.csv',index=False)
        


