input = "/home/xnmeyer/Desktop/spheres/";
output = "/home/xnmeyer/Desktop/spheres/resized/";


function action(input, output, filename) {
		if (endsWith(filename, ".png")) {
			print(filename);
	        open(input + filename);
	        run("Canvas Size...", "width=800 height=700 position=Center");
	        saveAs("Jpeg", output + filename);
			close();
		}
}

setBatchMode(true); 
list = getFileList(input);
for (i = 0; i < list.length; i++)
        action(input, output, list[i]);
setBatchMode(false);
