#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May 29 09:06:25 2019

@author: meyerct6
"""

#Code to create plots for conflation figure 
import pandas as pd
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
#Calculate other synergymetrics using calcOtherSynergy function in each folder
import os
import numpy as np

##############################################################################
##############################################################################
#Begin with Malaria combination analysis
i = 'cokol_antifungal'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T.reset_index(drop=True,inplace=True)
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/'+i
T['model_level']=1
for k in T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]




from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
import os

        
#T[T['drugunique']=='paclitaxel_mk-2206_KPL1]
sub_T = T.loc[0]
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
sub_T['save_direc'] = os.getcwd()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:] = 0
popt3 = [sub_T['E0'],sub_T['E1'],sub_T['E2'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C1'],10**sub_T['log_C2'],10**sub_T['log_h1'],10**sub_T['log_h2'],10**sub_T['log_alpha1'],10**sub_T['log_alpha2']]
DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     


T['log_alpha_mean'] = (T['log_alpha1']+T['log_alpha2'])/2.
from scipy.stats import ttest_ind 
key = 'log_alpha_mean'
ttest_ind(T.loc[ind,key],T.loc[ind1,key])


#Select out the important conditions
key = 'loewe'
mask = []
sub_T = T[~T[key+'_fit_ec50'].isna()]
sub_T = sub_T.reset_index(drop=True)
plotViolin(sub_T,mask,key,ylim,xticklabs)





sub_T= sub_T[(sub_T['loewe_fit_ec50']>0)&(sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta_obs']<0.)&(sub_T['beta']<0.)]



#List of Synthetic lethal interactions
t = ['lat_ter',    'lat_tac',    'dyc_lat',    'sta_tac',    'fen_lat',    'rad_ter',    'hal_lat',    'lit_tac',    'fen_sta',    'dyc_sta',    'rap_ter',    'ben_pen',    'hal_sta',    'aba_wor',    'hyg_myr',    'myr_rad',    'ben_rad',    'lit_rad',    'rad_rap',    'qnn_rad',    'myr_tac',    'lit_sta',    'hyg_rad',    'lat_rap',    'hyg_sta',    'lit_rap',    'lat_tun',    'clo_rad',    'hyg_rap',    'sta_wor',    'myr_qnn',    'aba_lit',    'ben_lat',    'ben_cal',    'hyg_lat',    'rad_sta',    'lat_rad',    'cyc_rad']
ind = []
for i in t:
    d1 = i.split('_')[1]
    d2 = i.split('_')[0]
    tmp = T[(T['drug1_name']==d1)&(T['drug2_name']==d2)].index
    if len(tmp)>0:
        ind.append(T[(T['drug1_name']==d1)&(T['drug2_name']==d2)].index[0])
 