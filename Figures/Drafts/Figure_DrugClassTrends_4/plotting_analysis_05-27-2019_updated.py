#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 27 10:04:55 2019

@author: meyerct6
"""

#Code to create plots for conflation figure 
import pandas as pd
from conflation_plots import plotViolin
from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.calcOtherSynergyMetrics import loewe,Edrug2D_NDB_hill

import os
import numpy as np
import seaborn as sns
import numpy as np
#Plotting functions for the conflation figure:
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
import scipy.stats as st
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

##############################################################################
##############################################################################
#Read in the data
#Run from the MuSyC_Theory/Figures/Figure_DrugClassTrends_4/ folder
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10.**T['log_C1'];T['C2']=10.**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)

###############################################################################
###############################################################################
drgs_mech = pd.read_csv('drug_classes.csv')
drgs_mech = drgs_mech.sort_values('class')
drgs_mech.reset_index(inplace=True,drop=True)
drgs_mech = drgs_mech.loc[20:]
drgs_mech.reset_index(inplace=True,drop=True)
drgs_mech = drgs_mech.loc[~np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003','l778123','pd325901'])].append(drgs_mech.loc[np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003','l778123','pd325901'])],ignore_index=True)
drgs = list(drgs_mech['drug_name'])
ylim=None
key = 'beta'
def d_mat_Fun(key):
    d_mat = -np.ones((len(drgs),len(drgs)))
    for e1 in range(len(drgs)):
        for e2 in range(len(drgs))[::-1]:
            msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
            if sum(msk)<3 or e1==e2:
                d_mat[e1,e2]=-1
            else:
                sub_T= T.loc[msk]
                d_mat[e1,e2] = sub_T[key].median()
    d_mat[d_mat==-1]=np.nan
    return d_mat

d_mat = d_mat_Fun('loewe_fit_ec50')
#d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
#d_mat[d_mat==-1]=np.nan
plt.figure(facecolor='w',figsize=(4,2))
ax = []
ax.append(plt.subplot2grid((10,5),(0,0),rowspan=10,colspan=5))
plt.subplots_adjust(hspace=0)
plt.sca(ax[0])
g1 = sns.heatmap(pd.DataFrame(d_mat,index=drgs,columns=drgs),linewidths=0.1,cmap=cm.cool)
plt.xticks(np.arange(len(drgs))+.5,drgs)
plt.yticks(np.arange(len(drgs))+.5,drgs)

d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan



def d_Fun(key):
    d_mat = []
    combo=[]
    for e1 in range(len(drgs)):
        for e2 in np.arange(e1,len(drgs)):
            msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
            if sum(msk)>3 and e1!=e2:
                sub_T= T.loc[msk]
                d_mat.append(sub_T[key].median())
                combo.append(drgs[e1]+'_'+drgs[e2])
    return d_mat,combo

plt.figure()
d_mat,combo=d_Fun('hsa_fit_ec50')
combo=[combo[i] for i in np.argsort(d_mat)]
d_mat=np.sort(d_mat);
plt.bar(range(len(d_mat)),d_mat)
plt.bar(combo.index('lapatinib_mrk-003'),d_mat[combo.index('lapatinib_mrk-003')])
plt.xticks(range(len(d_mat)),combo,rotation=90)
plt.tight_layout()

drg1 = list(drgs_mech['drug_name'].loc[0:11])
drg2 = list(drgs_mech['drug_name'].loc[11:])
sub_T = T.loc[~T[key].isna()] 
msk1 = (np.in1d(sub_T['drug1_name'],drg1) & (np.in1d(sub_T['drug2_name'],drg1)))  
msk2 = (np.in1d(sub_T['drug1_name'],drg2) & (np.in1d(sub_T['drug2_name'],drg2))) 
ttest_ind(sub_T[key].loc[msk1],sub_T[key].loc[msk2])

tgr = d_mat[0:11,0:11].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[:,11:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]
ttest_ind(tgr,tyl)

d_mat = d_mat_Fun('beta')
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan
tgr = d_mat[0:11,0:11].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]
tyl = d_mat[:,11:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]
ttest_ind(tgr,tyl)



lst1 = ['dasatinib','lapatinib','mrk-003']
lst2 = ['beta','loewe_fit_ec50','bliss_fit_ec50','hsa_fit_ec50']
plt.figure()
ax = []
for e,m in enumerate(lst2):
    ax.append(plt.subplot(5,1,e+1))
    cnt = 0
    to_plt = []
    to_plt.append(np.array(T.loc[~T.loc[:,m].isna(),m]))
    for e1,l1 in enumerate(lst1):
        for l2 in lst1[e1:]:
            if l1!=l2:
                msk = ((T['drug1_name']==l2) | (T['drug2_name']==l2)) & ((T['drug1_name']==l1) | (T['drug2_name']==l1))
                tmp = np.array(T.loc[msk,m])
                tmp = tmp[~np.isnan(tmp)]
                to_plt.append(tmp)
                cnt = cnt + 1
    print m
    for e1,l1 in enumerate(lst1):
        print ttest_ind(to_plt[0],to_plt[e1+1])
    plt.boxplot(to_plt,positions=np.arange(cnt+1),showfliers=False)
    












































#Figure D
#Show HSA masks kinase combination trends
#Read in the drug mechanism data from drug_classes.csv
from scipy.stats import f_oneway
drgs_mech = pd.read_csv('drug_classes.csv')
key = 'beta'
mat = np.ones((len(drgs_mech['class'].unique()),len(drgs_mech['class'].unique())))
for v1,d1 in enumerate(drgs_mech['class'].unique()):
    for v2,d2 in enumerate(drgs_mech['class'].unique()):
        dcl1=list(drgs_mech[drgs_mech['class']==d1]['drug_name'])
        dcl2=list(drgs_mech[drgs_mech['class']==d2]['drug_name'])
        drgs = dcl1+dcl2
        d_mat = -np.ones((len(drgs),len(drgs)))
        for e1 in range(len(drgs)):
            for e2 in range(len(drgs)):
                msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
                if sum(msk)<3 or e1==e2:
                    d_mat[e1,e2]=-1
                else:
                    sub_T= T.loc[msk]
                    d_mat[e1,e2] = sub_T[key].median()
        d_mat[d_mat==-1]=np.nan
        d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
        d_mat[d_mat==-1]=np.nan
        
        tgr = d_mat[0:len(dcl1),0:len(dcl1)].reshape((-1,))
        tgr = tgr[~np.isnan(tgr)]
        
        tyl = d_mat[len(dcl1):,len(dcl1):].reshape((-1,))
        tyl = tyl[~np.isnan(tyl)]
               
        tbl = d_mat[0:len(dcl1),len(dcl1):].reshape((-1,))
        tbl = tbl[~np.isnan(tbl)]

        _,mat[v1,v2] = f_oneway(tgr,tyl,tbl)
        

plt.figure(facecolor='w',figsize=(4,2))
ax = []
ax.append(plt.subplot2grid((10,5),(0,0),rowspan=10,colspan=5))
plt.subplots_adjust(hspace=0)
plt.sca(ax[0])
g1 = sns.heatmap(pd.DataFrame(np.log10(mat),index=drgs_mech['class'].unique(),columns=drgs_mech['class'].unique()),linewidths=.05)


d1 = 'Kinase'
d2 = 'Hormone'
dcl1=list(drgs_mech[drgs_mech['class']==d1]['drug_name'])
dcl2=list(drgs_mech[drgs_mech['class']==d2]['drug_name'])
drgs = dcl1+dcl2
d_mat = -np.ones((len(drgs),len(drgs)))
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<3 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[0:len(dcl1),0:len(dcl1)].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[len(dcl1):,len(dcl1):].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]
       
tbl = d_mat[0:len(dcl1),len(dcl1):].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

_,mat[v1,v2] = f_oneway(tgr,tyl,tbl)


plt.figure()
p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)




drgs_mech = pd.read_csv('drug_classes.csv')
drgs_mech = drgs_mech.sort_values('class')
drgs_mech.reset_index(inplace=True,drop=True)
drgs_mech = drgs_mech.loc[20:]
drgs_mech.reset_index(inplace=True,drop=True)
drgs_mech = drgs_mech.loc[~np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003'])].append(drgs_mech.loc[np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003'])],ignore_index=True)
drgs = list(drgs_mech['drug_name'])
ylim=None
key = 'beta'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan
plt.figure(facecolor='w',figsize=(4,2))
ax = []
ax.append(plt.subplot2grid((10,5),(0,0),rowspan=10,colspan=5))
plt.subplots_adjust(hspace=0)
plt.sca(ax[0])
g1 = sns.heatmap(pd.DataFrame(d_mat,index=drgs,columns=drgs),linewidths=0.2)
plt.xticks(range(len(drgs)),drgs)
plt.yticks(range(len(drgs)),drgs)






drgs_mech = pd.read_csv('drug_classes.csv')
drgs_mech = drgs_mech.sort_values('class')
drgs_mech.reset_index(inplace=True,drop=True)
drgs_mech = drgs_mech.loc[20:]
drgs_mech.reset_index(inplace=True,drop=True)
drgs_mech = drgs_mech.loc[~np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003'])].append(drgs_mech.loc[np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003'])],ignore_index=True)
drgs = list(drgs_mech['drug_name'])
ylim=None
key = 'log_alpha'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk1 = (T['drug1_name']==drgs[e1]) & (T['drug2_name']==drgs[e2])
        msk2 = (T['drug2_name']==drgs[e1]) & (T['drug1_name']==drgs[e2])
        if sum(msk1)>sum(msk2):
            sub_T = T.loc[msk1]
            d_mat[e1,e2] = sub_T['log_alpha1'].median()
            d_mat[e2,e1] = sub_T['log_alpha2'].median()
        else:
            sub_T = T.loc[msk2]
            d_mat[e2,e1] = sub_T['log_alpha1'].median()
            d_mat[e1,e2] = sub_T['log_alpha2'].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan


plt.figure(facecolor='w',figsize=(4,2))
ax = []
ax.append(plt.subplot2grid((10,5),(0,0),rowspan=10,colspan=5))
plt.subplots_adjust(hspace=0)
plt.sca(ax[0])
g1 = sns.heatmap(pd.DataFrame(d_mat,index=drgs,columns=drgs),linewidths=0.2)
plt.xticks(range(len(drgs)),drgs)
plt.yticks(range(len(drgs)),drgs)



tgr = d_mat[0:13,0:13].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[:,13:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]


drgs_mech = pd.read_csv('drug_classes.csv')
drgs_mech = drgs_mech.sort_values('class')
drgs_mech.reset_index(inplace=True,drop=True)
drgs_mech = drgs_mech.loc[20:]
drgs_mech.reset_index(inplace=True,drop=True)
drgs_mech = drgs_mech.loc[~np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003'])].append(drgs_mech.loc[np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003'])],ignore_index=True)
drgs = list(drgs_mech['drug_name'])
ylim=None
key = 'loewe_fit_ec50'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan
plt.figure(facecolor='w',figsize=(4,2))
ax = []
ax.append(plt.subplot2grid((10,5),(0,0),rowspan=10,colspan=5))
plt.subplots_adjust(hspace=0)
plt.sca(ax[0])
g1 = sns.heatmap(pd.DataFrame(d_mat,index=drgs,columns=drgs),linewidths=0.2)
plt.xticks(range(len(drgs)),drgs)
plt.yticks(range(len(drgs)),drgs)


tgr = d_mat[0:13,0:13].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[:,13:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]


ttest_ind(tgr,tyl)













x = [0,len(drgs)]
for m in drgs_mech['class'].unique():
    x1 = x[1]
    x2 = sum(drgs_mech['class']==m)
    y1 
    y2
    
    
drgs_mech.loc[np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003'])].append(drgs_mech.loc[~np.in1d(drgs_mech['drug_name'],['mk-8669','mk-2206','dasatinib','lapatinib','mrk-003'])],ignore_index=True)

    
    






ylim=None
key = 'beta'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

#tgr = d_mat[1:5,1:5].reshape((-1,))
#tgr = tgr[~np.isnan(tgr)]
#
#tyl = d_mat[0:5,5:].reshape((-1,))
#tyl = tyl[~np.isnan(tyl)]
#
#tbl = d_mat[5:,5:].reshape((-1,))
#tbl = tbl[~np.isnan(tbl)]

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[5:9,5:9].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

#tbl = d_mat[5:,5:].reshape((-1,))
#tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)















#create interaction map
cnt = 0
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
            cnt+=1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T['beta'].median() #sum(sub_T['beta_obs']>.05)/float(sum(msk))
    tmp = drgs_mech[drgs_mech['drug_name']==drgs[e1]]['subclass'].values[0]        
    if tmp == 'Cell Cycle':
        cols.append('green')
    elif tmp == 'PI3K_AKT':
        cols.append('yellow')
    elif tmp =='Multi-kinase':
        cols.append('blue')
    elif tmp=='MAPK':
        cols.append('purple')
    elif tmp=='AMPK':
        cols.append('black')
            
d_mat[d_mat==-1]=np.nan
plt.figure(facecolor='w',figsize=(4,2))
ax = []
ax.append(plt.subplot2grid((10,5),(0,0),rowspan=9,colspan=5))
ax.append(plt.subplot2grid((10,5),(9,0),colspan=4))
plt.subplots_adjust(hspace=0)
plt.sca(ax[0])
g1 = sns.heatmap(pd.DataFrame(d_mat,index=drgs,columns=drgs),linewidths=.25,vmin=-.17,vmax=.55)
plt.xticks([])
plt.sca(ax[1])
ax[1].scatter(range(len(drgs)),np.zeros(len(drgs)),c=cols,marker='s',s=35,edgecolor='face')
ax[1].axis('off')
plt.savefig('merck_promiscuous_synergizers.pdf', bbox_inches = 'tight',pad_inches = 0)

###############################################################################
###############################################################################


#Box plot of different classes.
plt.figure(facecolor='w',figsize=(6,1.7))
ax = []
ax.append(plt.subplot2grid((8,4),(0,0),rowspan=6))
ax.append(plt.subplot2grid((8,4),(0,1),rowspan=6))
#ax.append(plt.subplot2grid((8,4),(0,2),rowspan=6))
ax.append([])
ax.append(plt.subplot2grid((8,4),(0,2),rowspan=6))
ax.append(plt.subplot2grid((8,4),(0,3),rowspan=6))

ax.append(plt.subplot2grid((8,4),(6,0),colspan=4,rowspan=2))

plt.sca(ax[0])
ylim=None
key = 'beta'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

#tgr = d_mat[1:5,1:5].reshape((-1,))
#tgr = tgr[~np.isnan(tgr)]
#
#tyl = d_mat[0:5,5:].reshape((-1,))
#tyl = tyl[~np.isnan(tyl)]
#
#tbl = d_mat[5:,5:].reshape((-1,))
#tbl = tbl[~np.isnan(tbl)]

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[5:9,5:9].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

#tbl = d_mat[5:,5:].reshape((-1,))
#tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)

plt.sca(ax[1])
ylim=None
key = 'log_alpha'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)-1):
    for e2 in np.arange(e1+1,len(drgs)):
        msk1 = (T['drug1_name']==drgs[e1]) & (T['drug2_name']==drgs[e2])
        msk2 = (T['drug2_name']==drgs[e1]) & (T['drug1_name']==drgs[e2])
        if sum(msk1)>sum(msk2):
            sub_T = T.loc[msk1]
            d_mat[e1,e2] = sub_T['log_alpha1'].median()
            d_mat[e2,e1] = sub_T['log_alpha2'].median()
        else:
            sub_T = T.loc[msk2]
            d_mat[e2,e1] = sub_T['log_alpha1'].median()
            d_mat[e1,e2] = sub_T['log_alpha2'].median()
            
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)

##Loewe
#plt.sca(ax[2])
#ylim=None
#key = 'loewe_fit_ec50'
#d_mat = -np.ones((len(drgs),len(drgs)))
#cols = []
#for e1 in range(len(drgs)):
#    for e2 in range(len(drgs)):
#        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
#        if sum(msk)<5 or e1==e2:
#            d_mat[e1,e2]=-1
#        else:
#            sub_T= T.loc[msk]
#            d_mat[e1,e2] = sub_T[key].median()
#d_mat[d_mat==-1]=np.nan
#d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
#d_mat[d_mat==-1]=np.nan
#
#tgr = d_mat[1:5,1:5].reshape((-1,))
#tgr = tgr[~np.isnan(tgr)]
#
#tyl = d_mat[0:5,5:].reshape((-1,))
#tyl = tyl[~np.isnan(tyl)]
#
#tbl = d_mat[5:,5:].reshape((-1,))
#tbl = tbl[~np.isnan(tbl)]
#
#p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)
#
#cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
#for e,bp in enumerate(p1['boxes']):
#    plt.setp(bp,edgecolor='k')
#    plt.setp(bp,facecolor=cols[e])
#for e,bp in enumerate(p1['medians']):
#    plt.setp(bp,color='r',linewidth=2)
#for e,bp in enumerate(p1['whiskers']):    
#    plt.setp(bp,color='k')
#for e,bp in enumerate(p1['caps']):
#    plt.setp(bp,color='k')
#
#plt.xlim((.25,3.75))
#if ylim is not None:
#    plt.ylim(ylim)
#plt.xticks([])
#from scipy.stats import ttest_ind
#print 'Loewe'
#print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
#print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
#print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
##plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
#import matplotlib.ticker as ticker
#ax[2].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))

#Bliss
plt.sca(ax[3])
ylim=None
key = 'bliss_fit_ec50'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Bliss'
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
import matplotlib.ticker as ticker

ax[3].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))

#HSA
plt.sca(ax[4])
ylim=None
key = 'hsa_fit_ec50'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'HSA'
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
import matplotlib.ticker as ticker

ax[4].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))

ax[-1].spines['right'].set_visible(False)
ax[-1].spines['top'].set_visible(False)
ax[-1].spines['bottom'].set_visible(False)
ax[-1].spines['left'].set_visible(False)
ax[-1].set_xticks([])
ax[-1].set_yticks([])
ax[-1].scatter(.15,.85,s=50,marker='s',c='green')
ax[-1].scatter(.5,.85,s=50,marker='s',c='yellow')
ax[-1].scatter(.85,.85,s=50,marker='s',c='blue')
ax[-1].text(.15,.65,'Cell Cycle\n+\nCell Cycle',ha='center',va='top')
ax[-1].text(.5,.65,'Cell Cycle\n+\nAKT/MAPK/Mult-Kinase',ha='center',va='top')
ax[-1].text(.85,.65,'AKT/MAPK/Multi-Kinase\n+\nAKT/MAPK/Multi-Kinase',ha='center',va='top')
ax[-1].set_ylim((0,1))
ax[-1].set_xlim((0,1))
ax[0].set_xlabel(r'$\beta$')
ax[1].set_xlabel(r'log($\alpha$)')
ax[4].set_xlabel(r'HSA @ EC50')
ax[3].set_xlabel(r'Bliss @ EC50')
#ax[2].set_xlabel(r'Loewe @ EC50')

plt.tight_layout()
plt.subplots_adjust(hspace=.0,wspace=.7)
plt.savefig('merck_box_differences.pdf', bbox_inches = 'tight',pad_inches = 0)
