#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu May 30 09:03:26 2019

@author: meyerct6
"""


#Code to create plots for conflation figure 
import pandas as pd
from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.calcOtherSynergyMetrics import loewe,Edrug2D_NDB_hill

import os
import numpy as np
import seaborn as sns
import numpy as np
#Plotting functions for the conflation figure:
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
import scipy.stats as st
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

##############################################################################
##############################################################################
#Read in the data
#Run from the MuSyC_Theory/Figures/Figure_DrugClassTrends_4/ folder
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10.**T['log_C1'];T['C2']=10.**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)



x1 = T['bliss_fit_ec50']
x2 = T['loewe_fit_ec50']
x3 = T['hsa_fit_ec50']
plt.figure()
plt.subplot(131)
plt.hist(x1[~np.isnan(x1)],bins=50)
plt.title('bliss')
plt.subplot(132)
plt.hist(x2[~np.isnan(x2)],bins=50)
plt.title('loewe')
plt.subplot(133)
plt.hist(x3[~np.isnan(x3)],bins=50)
plt.title('hsa')



plt.figure()
msk = [[],[],[],[],[]]
msk[0] = (x1>0)&(x2>0)&(x3>0)
msk[1] = (x1<0)&(x2<0)&(x3<0)
msk[2] = ((x1>0)&(x3<0))|((x1<0)&(x3>0))
msk[3] = ((x2>0)&(x3<0))|((x2<0)&(x3>0))
msk[4] = ((x1>0)&(x2<0))|((x1<0)&(x2>0))
for i in range(5):
    plt.boxplot(T['log_alpha2'].loc[msk[i]].values,showfliers=False,positions=[i])
    
plt.xlim(-1,5)