#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 27 10:04:55 2019

@author: meyerct6
"""

#Code to create plots for conflation figure 
import pandas as pd
from conflation_plots import plotViolin
from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.calcOtherSynergyMetrics import loewe,Edrug2D_NDB_hill

import os
import numpy as np
import seaborn as sns
import numpy as np
#Plotting functions for the conflation figure:
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
import scipy.stats as st
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

##############################################################################
##############################################################################
#Read in the data
#Run from the MuSyC_Theory/Figures/Figure_DrugClassTrends_4/ folder
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10.**T['log_C1'];T['C2']=10.**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)

#from SynergyCalculator.calcOtherSynergyMetrics import loewe
#from SynergyCalculator.NDHillFun import Edrug2D_NDB
#for i in T.index:
#    E_ec50 = Edrug2D_NDB((T.loc[i,'C1'],T.loc[i,'C2']),T.loc[i,'E0'],T.loc[i,'E1'],T.loc[i,'E2'],T.loc[i,'E3'],T.loc[i,'r1'],T.loc[i,'r2'],T.loc[i,'C1'],T.loc[i,'C2'],10**T.loc[i,'log_h1'],10**T.loc[i,'log_h2'],10**T.loc[i,'log_alpha1'],10**T.loc[i,'log_alpha2'])
#    T.loc[i,'loewe_fit_ec50'] = -np.log10(loewe(T.loc[i,'C1'], T.loc[i,'C2'], E_ec50, T.loc[i,'E0'], T.loc[i,'E1'], T.loc[i,'E2'], T.loc[i,'h1'], T.loc[i,'h2'], 10**T.loc[i,'log_C1'], 10**T.loc[i,'log_C2']))
#    

#key = ['drug1_name','drug1_units','drug2_name','drug2_units','drugunique','expt','expt_date','fit_method','metric_name','sample','save_direc','selected_fit_alg','start_time']
#for k in T.columns:
#    if k not in key and not k.endswith('_ci'):
#        T[k] = T[k].astype('float64')
for k in T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
     
###############################################################################
###############################################################################
#Figure D
#Show HSA masks kinase combination trends
#Read in the drug mechanism data from drug_classes.csv
drgs_mech = pd.read_csv('drug_classes.csv')
drgs_mech = drgs_mech[np.in1d(drgs_mech['class'],['Kinase'])]
drgs = drgs_mech.sort_values('subclass')['drug_name'].values

#create interaction map
cnt = 0
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
            cnt+=1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T['beta'].median() #sum(sub_T['beta_obs']>.05)/float(sum(msk))
    tmp = drgs_mech[drgs_mech['drug_name']==drgs[e1]]['subclass'].values[0]        
    if tmp == 'Cell Cycle':
        cols.append('green')
    elif tmp == 'PI3K_AKT':
        cols.append('yellow')
    elif tmp =='Multi-kinase':
        cols.append('blue')
    elif tmp=='MAPK':
        cols.append('purple')
    elif tmp=='AMPK':
        cols.append('black')
            
d_mat[d_mat==-1]=np.nan
plt.figure(facecolor='w',figsize=(4,2))
ax = []
ax.append(plt.subplot2grid((10,5),(0,0),rowspan=9,colspan=5))
ax.append(plt.subplot2grid((10,5),(9,0),colspan=4))
plt.subplots_adjust(hspace=0)
plt.sca(ax[0])
g1 = sns.heatmap(pd.DataFrame(d_mat,index=drgs,columns=drgs),linewidths=.25,vmin=-.17,vmax=.55)
plt.xticks([])
plt.sca(ax[1])
ax[1].scatter(range(len(drgs)),np.zeros(len(drgs)),c=cols,marker='s',s=35,edgecolor='face')
ax[1].axis('off')
plt.savefig('merck_promiscuous_synergizers.pdf', bbox_inches = 'tight',pad_inches = 0)

###############################################################################
###############################################################################


#Box plot of different classes.
plt.figure(facecolor='w',figsize=(6,1.7))
ax = []
ax.append(plt.subplot2grid((8,4),(0,0),rowspan=6))
ax.append(plt.subplot2grid((8,4),(0,1),rowspan=6))
#ax.append(plt.subplot2grid((8,4),(0,2),rowspan=6))
ax.append([])
ax.append(plt.subplot2grid((8,4),(0,2),rowspan=6))
ax.append(plt.subplot2grid((8,4),(0,3),rowspan=6))

ax.append(plt.subplot2grid((8,4),(6,0),colspan=4,rowspan=2))

plt.sca(ax[0])
ylim=None
key = 'beta'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)

plt.sca(ax[1])
ylim=None
key = 'log_alpha'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)-1):
    for e2 in np.arange(e1+1,len(drgs)):
        msk1 = (T['drug1_name']==drgs[e1]) & (T['drug2_name']==drgs[e2])
        msk2 = (T['drug2_name']==drgs[e1]) & (T['drug1_name']==drgs[e2])
        if sum(msk1)>sum(msk2):
            sub_T = T.loc[msk1]
            d_mat[e1,e2] = sub_T['log_alpha1'].median()
            d_mat[e2,e1] = sub_T['log_alpha2'].median()
        else:
            sub_T = T.loc[msk2]
            d_mat[e2,e1] = sub_T['log_alpha1'].median()
            d_mat[e1,e2] = sub_T['log_alpha2'].median()
            
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)

##Loewe
#plt.sca(ax[2])
#ylim=None
#key = 'loewe_fit_ec50'
#d_mat = -np.ones((len(drgs),len(drgs)))
#cols = []
#for e1 in range(len(drgs)):
#    for e2 in range(len(drgs)):
#        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
#        if sum(msk)<5 or e1==e2:
#            d_mat[e1,e2]=-1
#        else:
#            sub_T= T.loc[msk]
#            d_mat[e1,e2] = sub_T[key].median()
#d_mat[d_mat==-1]=np.nan
#d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
#d_mat[d_mat==-1]=np.nan
#
#tgr = d_mat[1:5,1:5].reshape((-1,))
#tgr = tgr[~np.isnan(tgr)]
#
#tyl = d_mat[0:5,5:].reshape((-1,))
#tyl = tyl[~np.isnan(tyl)]
#
#tbl = d_mat[5:,5:].reshape((-1,))
#tbl = tbl[~np.isnan(tbl)]
#
#p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)
#
#cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
#for e,bp in enumerate(p1['boxes']):
#    plt.setp(bp,edgecolor='k')
#    plt.setp(bp,facecolor=cols[e])
#for e,bp in enumerate(p1['medians']):
#    plt.setp(bp,color='r',linewidth=2)
#for e,bp in enumerate(p1['whiskers']):    
#    plt.setp(bp,color='k')
#for e,bp in enumerate(p1['caps']):
#    plt.setp(bp,color='k')
#
#plt.xlim((.25,3.75))
#if ylim is not None:
#    plt.ylim(ylim)
#plt.xticks([])
#from scipy.stats import ttest_ind
#print 'Loewe'
#print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
#print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
#print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
##plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
#import matplotlib.ticker as ticker
#ax[2].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))

#Bliss
plt.sca(ax[3])
ylim=None
key = 'bliss_fit_ec50'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Bliss'
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
import matplotlib.ticker as ticker

ax[3].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))

#HSA
plt.sca(ax[4])
ylim=None
key = 'hsa_fit_ec50'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'HSA'
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
import matplotlib.ticker as ticker

ax[4].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))

ax[-1].spines['right'].set_visible(False)
ax[-1].spines['top'].set_visible(False)
ax[-1].spines['bottom'].set_visible(False)
ax[-1].spines['left'].set_visible(False)
ax[-1].set_xticks([])
ax[-1].set_yticks([])
ax[-1].scatter(.15,.85,s=50,marker='s',c='green')
ax[-1].scatter(.5,.85,s=50,marker='s',c='yellow')
ax[-1].scatter(.85,.85,s=50,marker='s',c='blue')
ax[-1].text(.15,.65,'Cell Cycle\n+\nCell Cycle',ha='center',va='top')
ax[-1].text(.5,.65,'Cell Cycle\n+\nAKT/MAPK/Mult-Kinase',ha='center',va='top')
ax[-1].text(.85,.65,'AKT/MAPK/Multi-Kinase\n+\nAKT/MAPK/Multi-Kinase',ha='center',va='top')
ax[-1].set_ylim((0,1))
ax[-1].set_xlim((0,1))
ax[0].set_xlabel(r'$\beta$')
ax[1].set_xlabel(r'log($\alpha$)')
ax[4].set_xlabel(r'HSA @ EC50')
ax[3].set_xlabel(r'Bliss @ EC50')
#ax[2].set_xlabel(r'Loewe @ EC50')

plt.tight_layout()
plt.subplots_adjust(hspace=.0,wspace=.7)
plt.savefig('merck_box_differences.pdf', bbox_inches = 'tight',pad_inches = 0)
