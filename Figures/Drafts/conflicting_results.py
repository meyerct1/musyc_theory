#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jun 18 06:42:10 2019

@author: meyerct6
"""

#Code to create plots for conflation figure 
import pandas as pd
from conflation_plots import plotViolin
from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data

import os
import sys
sys.path.insert(0,'../Figure_Sham_5/')
from pythontools.synergy import synergy_tools
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from matplotlib.collections import PolyCollection
from matplotlib.patches import Rectangle
from matplotlib.colors import Normalize,colorConverter
from matplotlib.colorbar import ColorbarBase
import matplotlib.cm as cm
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
###############################################################################
###############################################################################
#Panels B,C
##############################################################################
##############################################################################
#Begin with Cancer combination analysis
#Run from the MuSyC_Theory/Figures/Figure_Conflate_3/ folder
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)
T['geo_h'] = np.sqrt(T['h1']*T['h2'])

for k in T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
        
        
#Select out the important conditions
key = 'loewe'
mask = []
#sub_T = sub_T[(sub_T['E1']<.1)&(sub_T['E2']<.1)&(sub_T['E3']<.1)]
#Correct the hill bias for Loewe
#T = T[~T[key+'_fit_ec50'].isna()]

from SynergyCalculator.NDHillFun import Edrug2D_NDB
#
from SynergyCalculator.calcOtherSynergyMetrics import loewe
#
def get_loewe_bias(h1, h2, d1, d2, C1, C2, E0, E1, E2, E3=None, alpha=0.):
    """
    Calculates Loewe for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False,r1=100.)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False,r1=100.)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    l = -np.log10(synergy_tools.loewe(d1, d2, E, E0, E1, E2, h1, h2, C1, C2))
    return np.nanmedian(l)
#
T['loewe_fit_ec50_fix'] = np.nan
T['loewe_fit_ec10_fix'] = np.nan
T['loewe_fit_ec25_fix'] = np.nan

from SynergyCalculator.calcOtherSynergyMetrics import hill_1D_inv
for ind in T.index:
    print ind
    h1,h2,d1,d2,C1,C2,E0,E1,E2,E3,alpha1,alpha2 = T.loc[ind,'h1'],T.loc[ind,'h2'],T.loc[ind,'C1'],T.loc[ind,'C2'],T.loc[ind,'C1'],T.loc[ind,'C2'],T.loc[ind,'E0'],T.loc[ind,'E1'],T.loc[ind,'E2'],T.loc[ind,'E3'],10**T.loc[ind,'log_alpha1'],10**T.loc[ind,'log_alpha2']
    E_ec50 = Edrug2D_NDB((C1,C2),E0,E1,E2,E3,100.,100.,C1,C2,h1,h2,alpha1,alpha2)
    T.loc[ind,'loewe_fit_ec50_fix'] = -np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2)) - get_loewe_bias(h1, h2, d1, d2, C1, C2, E0, E1, E2,E3=E3)
#    EC10_1 = hill_1D_inv(E0-(E0-E1)*.1,E0,E1,h1,C1) #concentration for this effect
#    EC10_2 = hill_1D_inv(E0-(E0-E2)*.1,E0,E2,h2,C2) #concentration for this effect
#    E_ec10 = Edrug2D_NDB((EC10_1,EC10_2),E0,E1,E2,E3,100.,100.,C1,C2,h1,h2,alpha1,alpha2)
#    T.loc[ind,'loewe_fit_ec10_fix'] = -np.log10(loewe(EC10_1, EC10_1, E_ec10, E0, E1, E2, h1, h2, C1, C2)) - get_loewe_bias(h1, h2, EC10_1, EC10_2, C1, C2, E0, E1, E2,E3=E3)
#    EC25_1 = hill_1D_inv(E0-(E0-E1)*.25,E0,E1,h1,C1)
#    EC25_2 = hill_1D_inv(E0-(E0-E2)*.25,E0,E2,h2,C2)
#    E_ec25 = Edrug2D_NDB((EC25_1,EC25_2),E0,E1,E2,E3,100.,100.,C1,C2,h1,h2,alpha1,alpha2)
#    T.loc[ind,'loewe_fit_ec25_fix'] = -np.log10(loewe(EC25_1, EC25_2, E_ec50, E0, E1, E2, h1, h2, C1, C2)) - get_loewe_bias(h1, h2, EC25_1, EC25_2, C1, C2, E0, E1, E2,E3=E3)

T['loewe_fit_ec50_orig']=T['loewe_fit_ec50']
T['loewe_fit_ec50']=T['loewe_fit_ec50_fix']

from scipy import stats

msk = []
msk.append((T['loewe_fit_ec50']>0)&(T['bliss_fit_ec50']>0)&(T['hsa_fit_ec50']>0))
msk.append((T['loewe_fit_ec50']<0)&(T['bliss_fit_ec50']<0)&(T['hsa_fit_ec50']<0))
msk.append((T['hsa_fit_ec50']>0)&(T['bliss_fit_ec50']<0))
plt.figure(figsize=(7,2))
ax=[]
for e,m in enumerate(msk):
    x = T['log_alpha1'].loc[m]
    y = T['log_alpha2'].loc[m]
    z = T['beta'].loc[m]
    xyz = np.vstack([x,y,z])
    kde = stats.gaussian_kde(xyz)
    density = kde(xyz)
    ax.append(plt.subplot(1,len(msk),e+1,projection='3d'))
    ax[-1].scatter3D(x,y,z,c=density,edgecolor=None,cmap=cm.plasma,s=50,alpha=.05)
    ax[-1].set_xlim(-4,4);ax[-1].set_ylim(-4,4);ax[-1].set_zlim(-2,2)
    ax[-1].plot([-4,4],[0,0],[0,0],'k');
    ax[-1].plot([0,0],[-4,4],[0,0],'k');
    ax[-1].plot([0,0],[0,0],[-2,2],'k');
    ax[-1].view_init(30,55)
    ax[-1].set_xticks([-4,0,4])
    ax[-1].set_yticks([-4,0,4])
    ax[-1].set_zticks([-2,0,2])


ax = []
plt.figure(figsize=(3.5,5))
for e in range(3):
    ax.append(plt.subplot(3,1,e+1))
for e,m in enumerate(msk):
    x = T['log_alpha1'].loc[m].values
    y = T['log_alpha2'].loc[m].values
    z = T['beta'].loc[m].values
    ax[0].boxplot(x,positions=[e],showfliers=False)
    ax[1].boxplot(y,positions=[e],showfliers=False)
    ax[2].boxplot(z,positions=[e],showfliers=False)

for e in range(3):
    ax[e].set_xlim(-.5,2.5)
    
ax[2].set_ylim(-2,2)

plt.figure(figsize=(3.5,3))
ax = plt.subplot(111)
for e,m in enumerate(msk):
    x = T['log_alpha1'].loc[m].values
    y = T['log_alpha2'].loc[m].values
    z = T['beta'].loc[m].values
    per_x1 = sum((x>0.)&(y>0.))/float(len(x))
    per_x2 = sum(((x<0.)&(y>0.)) | ((x>0.)&(y<0.)))/float(len(y))
    per_z = sum(z>0.)/float(len(z))
    plt.bar([0+3*e,1+3*e],[per_x1,per_z],color='b')
    plt.bar([0+3*e],[per_x2],bottom=[per_x1],color='purple')
    plt.bar([0+3*e,1+3*e],[1-(per_x2+per_x1),1-per_z],bottom=[per_x1+per_x2,per_z],color='r')

plt.xticks([.5,3.5,6.5])
ax.set_xticklabels(['Syn by all','Ant by all','Syn by Loewe\nAnt by Bliss'])
plt.ylabel('Percent')
plt.ylim(-.5,1.1)
plt.yticks([0,.5,1.])
ax.add_patch(plt.Rectangle((.5,-.45),.55,.15,color='b'))
ax.add_patch(plt.Rectangle((3,-.45),.55,.15,color='purple'))
ax.add_patch(plt.Rectangle((5.5,-.45),.55,.15,color='r'))
ax.spines['right'].set_color('none')
ax.spines['bottom'].set_position('zero')
ax.spines['top'].set_color('none')
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
plt.text(0,1.1,r'$\alpha$')
plt.text(1,1.1,r'$\beta$')




