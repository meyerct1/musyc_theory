#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 14 14:54:53 2018

@author: xnmeyer
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from mpl_toolkits.mplot3d import axes3d, Axes3D #<-- Note the capitalization! 
import matplotlib as mpl

def Edrug2D_NDB_hill(d,E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2):
    d1 = d[0]
    d2 = d[1]
    Ed =    (E2*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2**(gamma1 + 1)*(C1**h1*r1)**gamma2 + 
             E1*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*(C2**h2*r2)**gamma1 + 
             C1**h1*E0*r1**(gamma2 + 1)*(alpha2*d1)**(gamma2*h1)*(C2**h2*r2)**gamma1 + 
             C2**h2*E0*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2)*(C1**h1*r1)**gamma2 + 
             E2*d1**h1*r1**(gamma2 + 1)*r2**gamma1*(C1**h1)**gamma2*(alpha1*d2)**(gamma1*h2) + 
             E1*d2**h2*r1**gamma2*r2**(gamma1 + 1)*(C2**h2)**gamma1*(alpha2*d1)**(gamma2*h1) + 
             E3*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r1**gamma2*r2**(gamma1 + 1)*(alpha2*d1)**(gamma2*h1) + 
             E3*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*r2**gamma1*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*C2**h2*E0*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + C1**h1*C2**h2*E0*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             C2**h2*E3*d1**h1*r1*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*E3*d2**h2*r1**(gamma2 + 1)*r2*(alpha2*d1)**(gamma2*h1) + 
             C2**h2*E1*d1**h1*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + 
             C2**h2*E1*d1**h1*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             C1**h1*E2*d2**h2*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + 
             C1**h1*E2*d2**h2*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2)/ \
            (C1**h1*r1**(gamma2 + 1)*(alpha2*d1)**(gamma2*h1)*(C2**h2*r2)**gamma1 + 
             C2**h2*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2)*(C1**h1*r1)**gamma2 + 
             alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2**(gamma1 + 1)*(C1**h1*r1)**gamma2 + 
             alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*(C2**h2*r2)**gamma1 + 
             alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r1**gamma2*r2**(gamma1 + 1)*(alpha2*d1)**(gamma2*h1) + 
             alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*r2**gamma1*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*C2**h2*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + C1**h1*C2**h2*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             C2**h2*d1**h1*r1*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*d2**h2*r1**(gamma2 + 1)*r2*(alpha2*d1)**(gamma2*h1) +
             C1**h1*d2**h2*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 +
             C1**h1*d2**h2*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             C2**h2*d1**h1*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + 
             C2**h2*d1**h1*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             d1**h1*r1**(gamma2 + 1)*r2**gamma1*(C1**h1)**gamma2*(alpha1*d2)**(gamma1*h2) + 
             d2**h2*r1**gamma2*r2**(gamma1 + 1)*(C2**h2)**gamma1*(alpha2*d1)**(gamma2*h1))
    return Ed


noise = .0
N = 50
rep = 1
E0=1.
E1=0.
E2=0.
E3=0.
C1=1.
C2=1.
r1=100.
r2=100.
alpha1=0.
alpha2=0.
gamma1=1.
gamma2=1.
DD1,DD2 = np.meshgrid(np.concatenate(([0],np.linspace(1e-10,1,N))),np.concatenate(([0],np.linspace(1e-10,1,N))))
d1_tmp = DD1.reshape((-1,))
d2_tmp = DD2.reshape((-1,))
fig = plt.figure(figsize=(3.5,1))
fig.patch.set_facecolor('w')  
ax= []
for e,h in enumerate([1,.5,2]):
    h1=h
    h2=h
    dip = Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2).reshape(DD1.shape)
    ax.append(plt.subplot2grid((1,3),(0,e)))
    plt.sca(ax[-1])
    pc = plt.pcolor(DD1, DD2, dip, cmap='PRGn', vmin=0, vmax=1,rasterized=True)
    CS = plt.contour(DD1,DD2,dip,levels=np.linspace(0.1,1,10),colors='k')
    manual_loc=[]
    for p in CS.collections:
        if len(p.get_paths())!=0:
            tmp = p.get_paths()[0].vertices
            tmp_idx = np.argmin((tmp[:,0]-tmp[:,1])**2)
            manual_loc.append((tmp[tmp_idx,0],tmp[tmp_idx,1]))
    cs = plt.clabel(CS, inline=1, fontsize=8,manual=manual_loc)
    plt.title(r'$h_1$=$h_2$=%.1f,'%h + '\n' + r'$\alpha_1$=$\alpha_2$=0',fontsize=8)
    plt.xticks([])
    plt.yticks([])
    if e==0:
        plt.ylabel('[drug 1]')
        plt.xlabel('[drug 2]')
    
plt.subplots_adjust(wspace=0.1,hspace=0)
    
fig.subplots_adjust(right=0.9)
cax = fig.add_axes([0.92, 0.1, 0.02, .8])
norm = mpl.colors.Normalize(vmin=0, vmax=1)
cb1 = mpl.colorbar.ColorbarBase(cax, cmap=cm.PRGn,
                                norm=norm,
                                orientation='vertical',
                                ticks = [0,.5,1])
cb1.set_label('Drug Effect')


plt.savefig('Isobles.pdf',bbox_inches = 'tight',pad_inches = 0)
