#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
#Code to calculate other synergy parameters for metrics which results in a hill bias
"""
#Code to calculate loewe, combination index, schindler to show hill bias
import numpy as np
import pandas as pd
from SynergyCalculator.calcOtherSynergyMetrics import loewe,combination_index,schindler

fit_fil = 'oneil_anticancer/MasterResults_noGamma.csv'
dat_fil = 'oneil_anticancer/merck_perVia_10-29-2018.csv'
label = 'merck_synergy'

#File with MuSyC fits
T = pd.read_csv("../../../Data/" + fit_fil)
# Arbitrary filters to drugs with good enough fits
T = T.loc[(T['R2'] > 0.8)]
T = T.reset_index(drop=True)
#File with raw data
df = pd.read_csv("../../../Data/" + dat_fil)
# Filter out any weird datapoints, or points that aren't real combinations
df = df.loc[(df['drug1.conc'] > 0) & (df['drug2.conc'] > 0) & (df['effect'] >= 0) & (df['effect'] <= 1)]
df['schindler'] = np.nan
df['loewe'] = np.nan
df['ci'] = np.nan

df['h1'] = np.nan
df['h2'] = np.nan
df['E0'] = np.nan
df['E1'] = np.nan
df['E2'] = np.nan
df['C1'] = np.nan
df['C2'] = np.nan
df['drugunique'] = np.nan

df['drug1'] = [i.lower() for i in df['drug1']]
df['drug2'] = [i.lower() for i in df['drug2']]

prog = 0
count = 0
n_rows = T.shape[0]

#For each drug
for i in T.index:
    count += 100 # Print progress, counting up to 100
    if count/n_rows > prog:
        prog = count/n_rows
        print prog

    # Read drug parameters
    cline, drug1, drug2, h1, h2, E0, E1, E2, C1, C2 = T.loc[i, ['sample', 'drug1_name', 'drug2_name', 'h1', 'h2', 'E0', 'E1', 'E2', 'log_C1', 'log_C2']]
    C1 = np.power(10., C1)
    C2 = np.power(10., C2)
    
    # Subset the data to just combinations of those drugs
    df_sub = df.loc[(df['drug1']==drug1) & (df['drug2']==drug2) & (df['sample']==cline)]
    
    # Read the data
    d1c = df_sub['drug1.conc']
    d2c = df_sub['drug2.conc']
    E = df_sub['effect']
    
    # Calculate synergy and population dataframe
    df.loc[df_sub.index, "schindler"] = schindler(d1c, d2c, E, E0, E1, E2, h1, h2, C1, C2)
    df.loc[df_sub.index, "loewe"] = loewe(d1c, d2c, E, E0, E1, E2, h1, h2, C1, C2)
#    df.loc[df_sub.index, "ci"] = combination_index(d1,d2,dip)
    df.loc[df_sub.index, 'h1'] = h1
    df.loc[df_sub.index, 'h2'] = h2
    df.loc[df_sub.index, 'C1'] = C1
    df.loc[df_sub.index, 'C2'] = C2
    df.loc[df_sub.index, 'E0'] = E0
    df.loc[df_sub.index, 'E1'] = E1
    df.loc[df_sub.index, 'E2'] = E2
    df.loc[df_sub.index, 'drugunique'] = "%s_%s_%s"%(cline, drug1, drug2)


df.to_csv("merck_synergy_full.csv",index=None)

df = df.loc[~pd.isnull(df['schindler'])]
df = df.loc[~pd.isnull(df['loewe'])]

df.to_csv("merck_synergy.csv",index=None)

