import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
from adjustText import adjust_text
import scipy.stats as st
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

import matplotlib.gridspec as gridspec
import numpy as np
from scipy.stats import linregress, pearsonr, spearmanr
from pythontools.synergy import synergy_tools

def get_loewe_bias(h1, h2, d1, d2, ec50_1, ec50_2, E0, E1, E2, E3=None, alpha=1.):
    """
    Calculates Loewe for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    l = -np.log10(synergy_tools.loewe(d1, d2, E, E0, E1, E2, h1, h2, C1, C2))
    return np.nanmedian(l)
    
def moving_window_percentiles(df, xaxis, yaxis, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True):
    """
    Calculates moving window percentiles.
    The first window begins at df[xaxis].min()-window_size/2
    The final window ends at df[xaxis].max()+window_size/2
    The window step_size is calculated so that the result will have n_steps many values
    x values for each window are the midpoint of xmin,xmax of that window
    """
    dff = pd.DataFrame(df)[xaxis]
    if logx: dff = np.log(dff)
    XMIN, XMAX = dff.min()-window_size/2., dff.max()+window_size/2.
    step_size = (XMAX - XMIN - window_size) / (1.*(n_steps-1))
    w_df = pd.DataFrame(index = range(n_steps), columns=percentiles + ['xmin','xmax','xmid','n_rows'])
    
    for i in w_df.index:
        xmin = XMIN + step_size*i
        xmax = xmin + window_size
        xmid = xmin + window_size/2.
        if logx: w_df.loc[i,['xmin','xmax','xmid']] = np.exp(xmin), np.exp(xmax), np.exp(xmid)
        else: w_df.loc[i,['xmin','xmax','xmid']] = xmin, xmax, xmid
        sub_df = df.loc[(dff >= xmin) & (dff <= xmax)]
        w_df.loc[i,"n_rows"] = sub_df.shape[0]
        w_df.loc[i, percentiles] = sub_df[yaxis].quantile(percentiles)

    return w_df
        
def plot_quantile_bias(window_df, xaxis, ax, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True, alpha_lim=(0.2,0.8), color='gold', bias=None, xlim=None, ylim=None, plot_midline=True):
    """
    Plot the bias of df[yaxis] as a function of df['xmid']
    Assumes window_df has been returned as window_df=moving_window_percentiles(df, ...)
    """
    
    x = np.float64(window_df['xmid'])
    
    n_color_shades = len(percentiles)/2
    
    for i in range(n_color_shades):
        alpha = alpha_lim[0] + (alpha_lim[1] - alpha_lim[0])*i/(n_color_shades-1.)
        y1 = np.float64(window_df[percentiles[i+1]])
        y2 = np.float64(window_df[percentiles[i]])
        ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
        
        if i<n_color_shades-1 or len(percentiles)%2==1:
            y1 = np.float64(window_df[percentiles[-i-1]])
            y2 = np.float64(window_df[percentiles[-i-2]])
            ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
    if bias is not None: ax.plot(bias[0], bias[1],'b-',lw=2,zorder=1000,label='Predicted Bias')
    ax.plot([x.min(), x.max()], [0,0],'k--')
    if plot_midline: ax.plot(x, window_df[percentiles[n_color_shades]], 'r-',label='Median') # Draw the median, (assuming 0.5 is the middle percentile)
    
    if xlim is not None: ax.set_xlim(xlim)
    if ylim is not None: ax.set_ylim(ylim)
    if logx: ax.set_xscale('log')
    
    
#fit_fil = 'oneil_anticancer/MasterResults_merck.csv'
#dat_fil = 'oneil_anticancer/merck_perVia_10-29-2018.csv'
#label = 'merck_synergy'
#
#
#T = pd.read_csv("../../../Data/" + fit_fil)

columns = ['sample', 'drug1', 'drug1.conc', 'drug2', 'drug2.conc', 'effect', 'loewe', 'h1','h2','C1','C2','E0','E1','E2', 'drugunique']

# Read the data (or load a version that was already calculated)
if True:
    data = pd.read_csv("../../Data/oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation_allDoses.csv")
    df = pd.read_csv("../../Data/oneil_anticancer/MasterResults_noGamma.csv")
    df['drugunique']=df['sample']+'_'+df['drug1_name']+'_'+df['drug2_name']
    data = data.merge(df,on=['drugunique'])[columns]
    # data is a dataframe with Loewe synergy already calculated as a column, as well as columns for h1, h2, drug1.conc, drug2.conc, C1, C2, E0, E1, and E2
    #data['loewe'] = -np.log10(data['loewe'])
    data = data[~np.isnan(data['loewe'])]
    data.reset_index(drop=True,inplace=True)
    data['loewe_bias'] = np.nan
    data['hill_min'] = data[['h1','h2']].min(axis=1)
    data['hill_geom'] = np.sqrt(data['h1']*data['h2'])

    #data = data.loc[data.index[:10000]] # for testing purposes, use only the first x rows of the file
    prog = 0
    count = 0
    n_rows = data.shape[0]
    for i in data.index:
        count += 100 # count is used to show progess... it will count up to 100
        if count/n_rows > prog:
            prog = count/n_rows
            print prog
        h1, h2, C1, C2, E0, E1, E2, d1, d2 = data.loc[i,['h1','h2','C1','C2','E0','E1','E2','drug1.conc','drug2.conc']]
        data.loc[i,'loewe_bias'] = get_loewe_bias(h1, h2, d1, d2, C1, C2, E0, E1, E2)

    data = data.loc[~pd.isnull(data['loewe_bias'])]
    data['loewe_corrected'] = data['loewe'] - data['loewe_bias']
    data.to_csv("../../Data/oneil_anticancer/loewe_synergy_allDoses_corrected.csv")
    
else:
    data = pd.read_csv("../../Data/oneil_anticancer/loewe_synergy_allDoses_corrected.csv")


# Include only drugs with doses spanning ec50 (within the multiplicative factor of edge_buffer, e.g. dmin > ec50/buffer, and dmax < ec50*buffer
remove_expts = []
edge_buffer = 1.
prog = 0
count = 0
n_rows = len(data['drugunique'].unique())
for drugunique in data['drugunique'].unique():
    count += 100
    if count/n_rows > prog:
        prog = count/n_rows
        print prog
    sub_data = data.loc[data['drugunique']==drugunique]
    d1min = sub_data['drug1.conc'].min()
    d1max = sub_data['drug1.conc'].max()
    d2min = sub_data['drug2.conc'].min()
    d2max = sub_data['drug2.conc'].max()
    C1 = sub_data['C1'].median()
    C2 = sub_data['C2'].median()
    if (C1*edge_buffer < d1min) or (C1/edge_buffer > d1max) or (C2*edge_buffer < d2min) or (C2/edge_buffer > d2max): remove_expts.append(drugunique)
    
data2 = data.loc[~data['drugunique'].isin(remove_expts)]


#print "linregress ALL points %0.2e"%linregress(data2['hill_geom'],data2['loewe']).pvalue
#print "linregress ALL points corrected %0.2e"%linregress(data2['hill_geom'],data2['loewe_corrected']).pvalue


# Calculate an "average bias" as a function of h
# "average bias" is the median synergy, assuming a null MuSyC surface, across doses ranging from ec50/10 to ec50*10
xaxis = "hill_geom"
hmin = data2[xaxis].min()
hmax = data2[xaxis].max()
hmax = 33.
hrange = np.logspace(np.log10(hmin), np.log10(hmax), 50)
loewe_y = []

C1,C2 = 1e-4, 1e-4
d1min, d1max = C1/10., C1*10.
d2min, d2max = C2/10., C2*10.
d1 = np.logspace(np.log10(d1min), np.log10(d1max), 20)
d2 = np.logspace(np.log10(d2min), np.log10(d2max), 20)
DD1, DD2 = np.meshgrid(d1, d2)
E0, E1, E2, E3 = 1., 0., 0., 0.

for h in hrange:
    loewe_y.append(get_loewe_bias(h, h, DD1, DD2, C1, C2, E0, E1, E2)) # This is the average bias
    

# Calculate the moving windows of the scattered data
percentiles = [i/10. for i in range(1,10)]
n_steps=200
window_size=0.1 # This is in h-space

window_df = moving_window_percentiles(data2, xaxis, "loewe", percentiles = percentiles, n_steps=n_steps, window_size=window_size)
corrected_window_df = moving_window_percentiles(data2, xaxis, "loewe_corrected", percentiles = percentiles, n_steps=n_steps, window_size=window_size)

x = np.float64(window_df['xmid'])


xlim=(0.35,10)
ylim=(-1,1.2)
fig = plt.figure(figsize=(3.75,3),facecolor='w')
ax = []
ax.append(plt.subplot2grid((5,1),(0,0),rowspan=1))
ax.append(plt.subplot2grid((5,1),(1,0),rowspan=2))
ax.append(plt.subplot2grid((5,1),(3,0),rowspan=2))

# Plot the number of points within each window
ax[0].plot(x, window_df['n_rows'],lw=3,c='k')
ax[0].set_xscale('log')
ax[0].set_xticklabels([])
ax[0].set_xlim(xlim)
ax[0].set_yscale('log')
ax[0].set_ylabel("N-points")
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')
ax[0].set_yticks([1,100,10000])

# Plot the loewe bias
ax[1].scatter(data2[xaxis],data2['loewe'],alpha=0.05, s=.1,rasterized=True)
plot_quantile_bias(window_df, xaxis, ax[1], color='darkorange', percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=(hrange,loewe_y), xlim=xlim, ylim=ylim)
ax[1].set_xticklabels([])
ax[1].set_ylabel("-log10(Loewe)")
ax[1].text(2,1,'Loewe biased by hill slope',ha='center',va='center')
ax[1].set_yticks([-.5,0,.5])
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].xaxis.set_ticks_position('bottom')
ax[1].yaxis.set_ticks_position('left')

# Plot the corrected Loewe
ax[2].scatter(data2[xaxis],data2['loewe_corrected'],alpha=0.05, s=.1,rasterized=True)
plot_quantile_bias(corrected_window_df, xaxis, ax[2], color='darkorange', percentiles=percentiles, n_steps=n_steps, window_size=window_size, xlim=xlim, ylim=ylim)
ax[2].set_ylabel("-log10(Loewe)")
ax[2].set_xlabel(r"$\sqrt{h_1 h_2}$",labelpad=-10)
ax[2].text(2,1,'Loewe corrected for hill bias',ha='center',va='center')
ax[2].set_yticks([-.5,0,.5])
ax[2].spines['top'].set_visible(False)
ax[2].spines['right'].set_visible(False)
ax[2].xaxis.set_ticks_position('bottom')
ax[2].yaxis.set_ticks_position('left')

#plt.tight_layout()

plt.savefig("bias.pdf",bbox_inches = 'tight',pad_inches = 0)



#Find example where the bias matters
g = data2.sort_values('hill_geom')[['loewe','loewe_corrected','drugunique','hill_geom']].groupby(['drugunique']).median()

from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf,matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data

i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_noGamma_otherSynergyMetricsCalculation.csv')
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)
dat_fil = 'oneil_anticancer/merck_perVia_10-29-2018.csv'
T['barcode'] = T['sample']+'_'+T['drug1_name']+'_'+T['drug2_name']
T['loewe_fit_ec50_fix'] = np.nan

for ind in T.index:
    # Read drug parameters
    r1,r2,h1, h2, E0, E1, E2, C1, C2 = T.loc[ind, ['r1','r2','h1', 'h2', 'E0', 'E1', 'E2', 'C1', 'C2']]
    E_ec50 = Edrug2D_NDB_hill((C1,C2),E0,E1,E2,min(E1,E2),r1,r2,C1,C2,h1,h2,1,1,1,1)
    T.loc[ind,'loewe_fit_ec50_fix']=T.loc[ind,'loewe_fit_ec50']+np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2))


T['hill_geom'] = np.sqrt(T['h1']*T['h2'])

T[((T[['beta_obs','beta','log_alpha1','log_alpha2']]<0).all(axis=1) & (T['loewe_fit_ec50']>0) & ((T[['h1','h2','hill_geom']]<1.).all(axis=1)))][['beta_obs','beta','log_alpha1','log_alpha2','loewe_fit_ec50','hill_geom']]
    
sub_T = T[(T['sample']=='ZR751')&(T['drug1_name']=='topotecan')&(T['drug2_name']=='bez-235')].to_dict(orient='record')[0]
sub_T = T.loc[4571]
sub_T['to_save_plots'] = 1
sub_T['save_direc']  = '../../Data/oneil_anticancer'
expt        = "../../Data/"+dat_fil
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()

d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
title = 'Loewe @ EC50:%.2f'%sub_T['loewe_fit_ec50']+'\n'+r'$\sqrt{h2*h2}$'%sub_T['hill_geom']+'\n'+'Corrected Loewe:%.2f'%sub_T['loewe_fit_ec50_fix']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 

matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
