#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 11:53:43 2019

@author: xnmeyer
"""

#Code to create plots for conflation figure 
import pandas as pd
from conflation_plots import plotViolin
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.calcOtherSynergyMetrics import loewe,Edrug2D_NDB_hill

#Calculate other synergymetrics using calcOtherSynergy function in each folder
import os
import numpy as np
import seaborn as sns
import numpy as np
#Plotting functions for the conflation figure:
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
import scipy.stats as st
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

d = 'cokol_antifungal'
T = pd.read_csv('../../Data/' + d + '/MasterResults_noGamma_otherSynergyMetricsCalculation.csv')
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[~np.isnan(T['loewe_fit_ec50'])]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['avg_alpha'] = T[['log_alpha1','log_alpha2']].mean(axis=1)
T.reset_index(drop=True,inplace=True)
T['loewe_fit_ec50_fix'] = np.nan

for ind in T.index:
    # Read drug parameters
    r1,r2,h1, h2, E0, E1, E2, C1, C2 = T.loc[ind, ['r1','r2','h1', 'h2', 'E0', 'E1', 'E2', 'C1', 'C2']]
    E_ec50 = Edrug2D_NDB_hill((C1,C2),E0,E1,E2,min(E1,E2),r1,r2,C1,C2,h1,h2,1,1,1,1)
    T.loc[ind,'loewe_fit_ec50_fix']=T.loc[ind,'loewe_fit_ec50']+np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2))


plt.figure(facecolor='w',figsize=(2,2))
ax = plt.subplot(111)
p = []
ymin=0;ymax=0;

p.append(plt.violinplot(T['loewe_fit_ec50'],positions=[0],widths=.75,showextrema=False,showmedians=True,vert=True))
p.append(plt.violinplot(T['loewe_fit_ec50_fix'],positions=[1],widths=.75,showextrema=False,showmedians=True,vert=True))
plt.xlim(-.5,1.5)

for e,bp in enumerate(p):
    for pc in bp['bodies']:
        pc.set_facecolor('k')
        pc.set_edgecolor('r') 
        pc.set_linewidth(2)
        pc.set_alpha(.8)
    bp['cmedians'].set_color('r')







plt.ylim((-.5,len(mask)-.5))
if xlim is None:
    plt.xlim((xmin,xmax))
else:
    plt.xlim(xlim)
plt.yticks(range(len(mask)))
ax.set_yticklabels(yticklabs,ha='center',va='center')
ax.tick_params(axis='y', which='major', pad=20)

xmi,xmx=ax.get_xlim(); ymi,ymx=ax.get_ylim()
ax.fill([0,0,xmx,xmx],[ymi,ymx,ymx,ymi],color='yellow',alpha=.5,zorder=-10)
ax.fill([0,0,xmi,xmi],[ymi,ymx,ymx,ymi],color='blue',alpha=.5,zorder=-10)

ax.text(0,len(mask)-.25,'<--Ant   Syn-->',fontsize=8,ha='center',va='center')

plt.xlabel(key.upper() +' @ EC50',fontsize=8)
plt.tight_layout()
plt.savefig(key+'_fit_ec50_conflation.pdf', bbox_inches = 'tight',pad_inches = 0)
return ax

