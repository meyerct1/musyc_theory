#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jan  7 13:55:30 2019

@author: xnmeyer
"""
import numpy as np
#Plotting functions for the conflation figure:
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
from adjustText import adjust_text
import scipy.stats as st
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from mpl_toolkits.mplot3d import Axes3D


def plotBox(sub_T,mask,key,ylim,xticklabs):
    plt.figure(facecolor='w',figsize=(2.75,2.5))
    ax = plt.subplot(111)
    p = []
    ymin=0;ymax=0;
    for e in range(3):
        p.append(plt.boxplot(sub_T[key+'_fit_ec50'].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
        ymin = np.min((ymin,sub_T[key+'_fit_ec50'].loc[mask[e]].min()))
        ymax = np.max((ymax,sub_T[key+'_fit_ec50'].loc[mask[e]].max()))
    
    for e,bp in enumerate(p):
        plt.setp(bp['medians'],color='r',linewidth=2)
        plt.setp(bp['whiskers'],color='k')
        plt.setp(bp['caps'],color='k')
        plt.setp(bp['boxes'],edgecolor='k')
        
        for patch in bp['boxes']:
            patch.set(facecolor=[.25,.25,.25])    

    plt.xlim((-.5,len(mask)-.5))
    if ylim is None:
        plt.ylim((ymin,ymax))
    else:
        plt.ylim(ylim)
    plt.xticks(range(len(mask)))
    ax.set_xticklabels(['$\\alpha_1,\\alpha_2>0$',
                        '$\\alpha_1,\\alpha_2<0$',
                        '$\\alpha_1<0,\\alpha_2>0$',
                     ])
    
    ax.axhline(y=0.,color='k',linestyle='--',linewidth=3)
    ax.fill([ax.get_xlim()[0],ax.get_xlim()[1],ax.get_xlim()[1],ax.get_xlim()[0]],[0,0,ax.get_ylim()[1],ax.get_ylim()[1]],color='yellow',alpha=.5,zorder=-10)
    ax.fill([ax.get_xlim()[0],ax.get_xlim()[1],ax.get_xlim()[1],ax.get_xlim()[0]],[0,0,ax.get_ylim()[0],ax.get_ylim()[0]],color='b',alpha=.5,zorder=-10)
    
    ax.text(3.6,.05,'<--Ant    ' + key.upper()+'    Syn-->',     rotation=90,ha='left',va='center')
    plt.ylabel(key.upper() +' @ EC50',labelpad=0)
    plt.tight_layout()
    plt.savefig(key+'_fit_ec50_conflation.pdf', bbox_inches = 'tight',pad_inches = 0)
    return (ymin,ymax)





def plotViolin(sub_T,mask,key,xlim,yticklabs):
    plt.figure(facecolor='w',figsize=(2,2))
    ax = plt.subplot(111)
    p = []
    ymin=0;ymax=0;
    for e in range(len(mask)):
        p.append(plt.violinplot(sub_T[key+'_fit_ec50'].loc[mask[e]].values,positions=[e],widths=.75,showextrema=False,showmedians=True,vert=False))
        xmin = np.min((ymin,sub_T[key+'_fit_ec50'].loc[mask[e]].min()))
        xmax = np.max((ymax,sub_T[key+'_fit_ec50'].loc[mask[e]].max()))
    
    for e,bp in enumerate(p):
        for pc in bp['bodies']:
            pc.set_facecolor('k')
            pc.set_edgecolor('r') 
            pc.set_linewidth(2)
            pc.set_alpha(.8)
        bp['cmedians'].set_color('r')

    plt.ylim((-.5,len(mask)-.5))
    if xlim is None:
        plt.xlim((xmin,xmax))
    else:
        plt.xlim(xlim)
    plt.yticks(range(len(mask)))
    ax.set_yticklabels(yticklabs,ha='center',va='center')
    ax.tick_params(axis='y', which='major', pad=20)
    
    xmi,xmx=ax.get_xlim(); ymi,ymx=ax.get_ylim()
    ax.fill([0,0,xmx,xmx],[ymi,ymx,ymx,ymi],color='yellow',alpha=.5,zorder=-10)
    ax.fill([0,0,xmi,xmi],[ymi,ymx,ymx,ymi],color='blue',alpha=.5,zorder=-10)
    
    ax.text(0,len(mask)-.25,'<--Ant   Syn-->',fontsize=8,ha='center',va='center')
    
    plt.xlabel(key.upper() +' @ EC50',fontsize=8)
    plt.tight_layout()
    plt.savefig(key+'_fit_ec50_conflation.pdf', bbox_inches = 'tight',pad_inches = 0)
    return ax
