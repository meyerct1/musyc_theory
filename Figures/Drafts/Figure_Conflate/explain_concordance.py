#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Jan 14 07:09:41 2019

@author: xnmeyer
"""



import matplotlib.pyplot as plt
import numpy as np
#Code to create plots for conflation figure 
import pandas as pd
from conflation_plots import plotViolin
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.calcOtherSynergyMetrics import loewe,Edrug2D_NDB_hill

#Calculate other synergymetrics using calcOtherSynergy function in each folder
import os




ds = ['cokol_antifungal','oneil_anticancer','mott_antimalaria']
c = ['purple','green', 'blue']
keys = ['log_alpha1','log_alpha2','beta']
labkeys = [r'$log(\alpha_1)$',r'$log(\alpha_2)$',r'$\beta$']

list_T = []
for d in ds:
    T = pd.read_csv('../../Data/' + d + '/MasterResults_noGamma_otherSynergyMetricsCalculation.csv')
    T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
    T = T[~np.isinf(T['loewe_fit_ec50'])]
    T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
    if d=='mott_antimalarial':
        T = T[(T['E1']<.1)&(T['E2']<.1)&(T['E3']<.1)]
    T['avg_alpha'] = T[['log_alpha1','log_alpha2']].mean(axis=1)
    T.reset_index(drop=True,inplace=True)
    for ind in T.index:
        # Read drug parameters
        r1,r2,h1, h2, E0, E1, E2, C1, C2 = T.loc[ind, ['r1','r2','h1', 'h2', 'E0', 'E1', 'E2', 'C1', 'C2']]
        E_ec50 = Edrug2D_NDB_hill((C1,C2),E0,E1,E2,min(E1,E2),r1,r2,C1,C2,h1,h2,1,1,1,1)
        T.loc[ind,'loewe_fit_ec50']=T.loc[ind,'loewe_fit_ec50']+np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2))

    list_T.append(T.copy())
    ylim = (-2,2)







for v in range(3):
    T = list_T[v]
    mask = list_mask[v]

        ax.append(plt.subplot2grid((8,9),(0,3*v+e1),rowspan=7))
        plt.sca(ax[-1])
        ymin=0;ymax=0;
        p = []
        for e in range(len(mask)):
            if sum(mask[e])<10:
                plt.scatter(e,0,s=10,marker='*',c=c[e])
            else:
                p.append(plt.boxplot(T[k].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
      
        for e,bp in enumerate(p):
            plt.setp(bp['medians'],color='r',linewidth=2)
            plt.setp(bp['whiskers'],color='k')
            plt.setp(bp['caps'],color='k')
            plt.setp(bp['boxes'],edgecolor='k')
            
            for patch in bp['boxes']:
                patch.set(facecolor=c[e])    
                    
        plt.xlim((-.75,len(mask)-.25))
        if ylim is not None:
            (ymin,ymax)=ylim         
        plt.ylim((ymin,ymax))
        plt.xticks(range(len(mask)))
        ax[-1].set_xticklabels([])
        ax[-1].set_yticks([ymin,0,ymax])
        ax[-1].axhline(y=0.,color='k',linestyle='--',linewidth=1)
    #        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[1],ax[e1].get_ylim()[1]],color='r',alpha=.5,zorder=-10)
    #        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[0],ax[e1].get_ylim()[0]],color='b',alpha=.5,zorder=-10)
        
        plt.title(labkeys[e1])


plt.tight_layout()
plt.subplots_adjust(wspace=0.4, hspace=.2)
ax.append(plt.subplot2grid((8,6),(7,0),colspan=6))

ax[-1].spines['right'].set_visible(False)
ax[-1].spines['top'].set_visible(False)
ax[-1].spines['bottom'].set_visible(False)
ax[-1].spines['left'].set_visible(False)
ax[-1].set_xticks([])
ax[-1].set_yticks([])
ax[-1].scatter(.15,.75,s=50,marker='s',c='yellow')
ax[-1].scatter(.85,.75,s=50,marker='s',c='purple')
ax[-1].text(.15,.25,'Synergistic',ha='center',va='top')
ax[-1].text(.85,.25,'Conflicting',ha='center',va='top')
ax[-1].set_ylim((0,1))
ax[-1].set_xlim((0,1))




#First cokol compare hsa and loewe
list_mask=[]
mask = []
T = list_T[0]
mask.append((T['hsa_fit_ec50']>0)&(T['loewe_fit_ec50']>0))
mask.append((T['hsa_fit_ec50']<0)&(T['loewe_fit_ec50']<0))
mask.append(~mask[0] & ~mask[1])
list_mask.append(mask)
mask = []
T = list_T[1]
mask.append((T['hsa_fit_ec50']>0)&(T['bliss_fit_ec50']>0)&(T['loewe_fit_ec50']>0))
mask.append((T['hsa_fit_ec50']<0)&(T['bliss_fit_ec50']<0)&(T['loewe_fit_ec50']<0))
mask.append(~mask[0] & ~mask[1])
list_mask.append(mask)
mask = []
T = list_T[2]
mask.append((T['hsa_fit_ec50']>0)&(T['bliss_fit_ec50']>0)&(T['loewe_fit_ec50']>0))
mask.append((T['hsa_fit_ec50']<0)&(T['bliss_fit_ec50']<0)&(T['loewe_fit_ec50']<0))
mask.append(~mask[0] & ~mask[1])
list_mask.append(mask)





fig = plt.figure(facecolor='w')
ax = []
for v in range(3):
    T = list_T[v]
    mask = list_mask[v]
    ax.append(plt.subplot2grid((8,6),(0,2*v+e1),rowspan=7))
    plt.sca(ax[-1])
    ymin=0;ymax=0;
    p = []
    for e in range(len(mask)):
        ax[-1].scatter(T['log_alpha1'].loc[mask[e]].values,T['log_alpha2'].loc[mask[e]].values)

    
 
ax[-1].spines['right'].set_visible(False)
ax[-1].spines['top'].set_visible(False)
ax[-1].spines['bottom'].set_visible(False)
ax[-1].spines['left'].set_visible(False)
ax[-1].set_xticks([])
ax[-1].set_yticks([])
ax[-1].scatter(.15,.75,s=50,marker='s',c='yellow')
ax[-1].scatter(.85,.75,s=50,marker='s',c='purple')
ax[-1].text(.15,.25,'Synergistic',ha='center',va='top')
ax[-1].text(.85,.25,'Conflicting',ha='center',va='top')
ax[-1].set_ylim((0,1))
ax[-1].set_xlim((0,1))
    
    
    




#First cokol compare hsa and loewe
list_mask=[]
mask = []
T = list_T[0]
mask.append((T['hsa_fit_ec50']>0))
mask.append((T['loewe_fit_ec50']>0))
list_mask.append(mask)
mask = []
T = list_T[1]
mask.append((T['hsa_fit_ec50']>0))
mask.append((T['loewe_fit_ec50']>0))
mask.append((T['bliss_fit_ec50']>0))
list_mask.append(mask)
mask = []
T = list_T[2]
mask.append((T['hsa_fit_ec50']>0))
mask.append((T['loewe_fit_ec50']>0))
mask.append((T['bliss_fit_ec50']>0))
list_mask.append(mask)


plt.figure(facecolor='w',figsize=(7,2))
ax = []


for v in range(3):
    T = list_T[v]
    mask = list_mask[v]
    for e1,k in enumerate(keys):
        ax.append(plt.subplot2grid((8,9),(0,3*v+e1),rowspan=7))
        plt.sca(ax[-1])
        ymin=0;ymax=0;
        p = []
        for e in range(len(mask)):
            if sum(mask[e])<10:
                plt.scatter(e,0,s=10,marker='*',c=c[e])
            else:
                p.append(plt.boxplot(T[k].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
      
        for e,bp in enumerate(p):
            plt.setp(bp['medians'],color='r',linewidth=2)
            plt.setp(bp['whiskers'],color='k')
            plt.setp(bp['caps'],color='k')
            plt.setp(bp['boxes'],edgecolor='k')
            
            for patch in bp['boxes']:
                patch.set(facecolor=c[e])    
                    
        plt.xlim((-.75,len(mask)-.25))
        if ylim is not None:
            (ymin,ymax)=ylim         
        plt.ylim((ymin,ymax))
        plt.xticks(range(len(mask)))
        ax[-1].set_xticklabels([])
        ax[-1].set_yticks([ymin,0,ymax])
        ax[-1].axhline(y=0.,color='k',linestyle='--',linewidth=1)
    #        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[1],ax[e1].get_ylim()[1]],color='r',alpha=.5,zorder=-10)
    #        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[0],ax[e1].get_ylim()[0]],color='b',alpha=.5,zorder=-10)
        
        plt.title(labkeys[e1])


plt.tight_layout()
plt.subplots_adjust(wspace=0.4, hspace=.2)
ax.append(plt.subplot2grid((8,6),(7,0),colspan=6))

ax[-1].spines['right'].set_visible(False)
ax[-1].spines['top'].set_visible(False)
ax[-1].spines['bottom'].set_visible(False)
ax[-1].spines['left'].set_visible(False)
ax[-1].set_xticks([])
ax[-1].set_yticks([])
ax[-1].scatter(.15,.75,s=50,marker='s',c='yellow')
ax[-1].scatter(.85,.75,s=50,marker='s',c='purple')
ax[-1].text(.15,.25,'Synergistic',ha='center',va='top')
ax[-1].text(.85,.25,'Conflicting',ha='center',va='top')
ax[-1].set_ylim((0,1))
ax[-1].set_xlim((0,1))
    
    
    