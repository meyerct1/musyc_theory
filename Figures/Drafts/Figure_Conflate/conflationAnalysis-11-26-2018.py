#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 14:11:32 2018

@author: xnmeyer
"""

#Code to create plots for conflation figure 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
from adjustText import adjust_text
import scipy.stats as st
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from mpl_toolkits.mplot3d import Axes3D
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface,matplotlibDoseResponseSlices
from SynergyCalculator.gatherData import subset_data
#Calculate other synergymetrics using calcOtherSynergy function in each folder


##############################################################################
##############################################################################
#Begin with Merck Anti-cancer combination analysis
T = pd.read_csv('MasterResults_otherSynergyMetrics_merck.csv')
#Generate graphs
T = T[~T['loewe_fit_ec50'].isna()]
T = T.reset_index(drop=True)

def plotBox(T,key,ylim):
    plt.figure(facecolor='w',figsize=(2.75,2.5))
    ax = plt.subplot(111)
    p = []
    mask = []
    mask.append((T['log_alpha1']>0.)&(T['log_alpha2']>0.)&(T['beta']>0.))
    mask.append((T['log_alpha1']<0.)&(T['log_alpha2']<0.)&(T['beta']<0.))
    mask.append((T['log_alpha1']<0.)&(T['log_alpha2']<0.)&(T['beta']>0.))
    mask.append((T['log_alpha1']>0.)&(T['log_alpha2']>0.)&(T['beta']<0.))
    ymin=0;ymax=0;
    for e in range(4):
        p.append(plt.boxplot(T[key+'_fit_ec50'].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75))
        ymin = np.min((ymin,T[key+'_fit_ec50'].loc[mask[e]].min()))
        ymax = np.max((ymax,T[key+'_fit_ec50'].loc[mask[e]].max()))
    
    for e,bp in enumerate(p):
        plt.setp(bp['medians'],color='r',linewidth=2)
        plt.setp(bp['fliers'],color='k')
        plt.setp(bp['whiskers'],color='k')
        plt.setp(bp['caps'],color='k')
        plt.setp(bp['boxes'],edgecolor='k')
        
        for patch in bp['boxes']:
            patch.set(facecolor=[.25,.25,.25])    

    plt.xlim((-.5,3.5))
    if ylim is None:
        plt.ylim((ymin,ymax))
    else:
        plt.ylim(ylim)
    plt.xticks(range(4))
    ax.set_xticklabels(['$\\alpha_1,\\alpha_2>0$\n$\\beta>0$',
                        '$\\alpha_1,\\alpha_2<0$\n$\\beta<0$',
                        '$\\alpha_1,\\alpha_2<0$\n$\\beta>0$',
                        '$\\alpha_1,\\alpha_2>0$\n$\\beta<0$'
                     ])
    
    ax.axhline(y=0.,color='k',linestyle='--',linewidth=3)
    ax.fill([ax.get_xlim()[0],ax.get_xlim()[1],ax.get_xlim()[1],ax.get_xlim()[0]],[0,0,ax.get_ylim()[1],ax.get_ylim()[1]],color='yellow',alpha=.5,zorder=-10)
    ax.fill([ax.get_xlim()[0],ax.get_xlim()[1],ax.get_xlim()[1],ax.get_xlim()[0]],[0,0,ax.get_ylim()[0],ax.get_ylim()[0]],color='b',alpha=.5,zorder=-10)
    
    ax.text(3.6,.05,'<--Ant    ' + key.upper()+'    Syn-->',     rotation=90,ha='left',va='center')
    plt.ylabel(key.upper() +' @ EC50',labelpad=0)
    plt.tight_layout()
    plt.savefig(key+'_fit_ec50_conflation.pdf', bbox_inches = 'tight',pad_inches = 0)
    return (ymin,ymax)

key = 'hsa'
ylim = (-1, 1)
plotBox(T,key,ylim)

key = 'bliss'
ylim = (-0.68, 0.68)
plotBox(T,key,ylim)

key = 'loewe'
ylim = (-1.1,1.1)
plotBox(T,key,ylim)




#Find two surfaces which are very similar by HSA and different in beta and alpha
#Remove the samples which did not converge past initial_fit
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T.reset_index(drop=True,inplace=True)
from scipy.spatial.distance import pdist
from sklearn.preprocessing import normalize
mat2 = normalize(T[['log_alpha1','log_alpha2','beta_obs']])
for cl in T['sample'].unique():
    mask = T['sample']==cl  
    sub_T = T[mask]
    sub_T = sub_T.reset_index(drop=True)
    d1 = pdist(np.array(sub_T['hsa_fit_ec50']).reshape((-1,1)))
    d2 = pdist(mat2[mask])    
    diff = d2-d1
    sort_index = np.argsort(diff)
    bst10 = sort_index[-2:]    
    lktab = np.zeros((len(d1),2))
    cnt = 0
    for i in range(len(sub_T)):
        for j in np.arange(i+1,len(sub_T)):
            lktab[cnt,0]=i
            lktab[cnt,1]=j
            cnt = cnt + 1          
    bst10_ind = [lktab[ind,:] for ind in bst10]
    for i in range(2):
        if sub_T['hsa_fit_ec50'].loc[[bst10_ind[i][0],bst10_ind[i][1]]].max()<0.:
           if np.diff(np.sign(sub_T['beta'].loc[[bst10_ind[i][0],bst10_ind[i][1]]]))[0]==2:     
                print cl
                print i
                print sub_T[['log_alpha1','log_alpha2','beta_obs','hsa_fit_ec50','drug1_name','drug2_name','sample']].loc[[bst10_ind[i][0],bst10_ind[i][1]]]
          
            

#Select MDAMB436 (doxorubicin and mk-4827) and (l778123 and etoposide)            
#Find antagonistic by hsa synergistic by beta antagonistic by alpha
#MDAMB436
#      log_alpha1  log_alpha2  beta_obs  hsa_fit_ec50   drug1_name drug2_name    sample
#22.0    0.626728   -4.000000 -0.180228     -0.423390  doxorubicin    mk-4827  MDAMB436
#32.0   -0.168941    1.083052  0.159145     -0.428632      l778123  etoposide  MDAMB436

sub_T = T[(T['drug1_name']=='doxorubicin')&(T['drug2_name']=='mk-4827')&(T['sample']=='MDAMB436')]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
matplotlibDoseResponseSurface(T,d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.0), zero_conc=0,title=None,d1lim=(-3,-1.2),d2lim=(-2,-.1))
    
    
#Find antagonistic by hsa synergistic by beta antagonistic by alpha

sub_T = T[(T['drug1_name']=='l778123')&(T['drug2_name']=='etoposide')&(T['sample']=='MDAMB436')]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
matplotlibDoseResponseSurface(T,d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.0), zero_conc=0,title=None,d1lim=(-3,-1.2),d2lim=(-2,-.1))
    
            
##################################################################################
#Find antagonistic by hsa synergistic by beta antagonistic by alpha
sub_T = T[(T['hsa_fit_ec50']<0.)&(T['beta']>0.)&(T['beta_obs']>0.)&(T['log_alpha1']<0.)&(T['log_alpha2']<0)]
sub_T = sub_T[['hsa_fit_ec50','beta','beta_obs','log_alpha1','log_alpha2','drug1_name','drug2_name','sample']]
print sub_T.sort_values('hsa_fit_ec50')

sub_T = T[(T['drug1_name']=='vinblastine')&(T['drug2_name']=='azd1775')&(T['sample']=='DLD1')]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
matplotlibDoseResponseSurface(T,d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.0), zero_conc=0,title=None,d1lim=(-3,-1.2),d2lim=(-2,-.1))
    

################
#Show global trends in synergy can be understood by MuSyC
def plotBoxSyn(T,ylim):
    plt.figure(facecolor='w',figsize=(2.75,2.5))
    ax = []
    ax.append(plt.subplot2grid((8,3),(0,0),rowspan=7))
    ax.append(plt.subplot2grid((8,3),(0,1),rowspan=7))
    ax.append(plt.subplot2grid((8,3),(0,2),rowspan=7))
    ax.append(plt.subplot2grid((8,3),(7,0),colspan=3))
    
    mask = []
    mask.append((T['hsa_fit_ec50']>0.)&(T['bliss_fit_ec50']>0.)&(T['loewe_fit_ec50']>0.))
    mask.append((T['hsa_fit_ec50']<0.)&(T['bliss_fit_ec50']<0.)&(T['loewe_fit_ec50']<0.))
    mask.append((~mask[0])&(~mask[1]))
    keys = ['beta','log_alpha1','log_alpha2']
    labkeys = [r'$\beta$',r'$log(\alpha_1)$',r'$log(\alpha_2)$']
    c = ['yellow','b','green']
    for e1,k in enumerate(keys):
        ymin=0;ymax=0;
        plt.sca(ax[e1])
        p = []
        for e in range(3):
            p.append(plt.boxplot(T[k].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
            ymin = np.min((ymin,T[k].loc[mask[e]].min()))
            ymax = np.max((ymax,T[k].loc[mask[e]].max()))
        
        for e,bp in enumerate(p):
            plt.setp(bp['medians'],color='r',linewidth=2)
#            plt.setp(bp['fliers'],color='k')
            plt.setp(bp['whiskers'],color='k')
            plt.setp(bp['caps'],color='k')
            plt.setp(bp['boxes'],edgecolor='k')
            plt.setp(bp['boxes'],facecolor=c[e])
    
        plt.xlim((-.75,2.75))
        if ylim is not None:
            (ymin,ymax)=ylim[e1]            
        plt.ylim((ymin,ymax))
        plt.xticks(range(3))
        ax[e1].set_xticklabels([])
        ax[e1].set_yticks([ymin,0,ymax])
        ax[e1].axhline(y=0.,color='k',linestyle='--',linewidth=1)
#        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[1],ax[e1].get_ylim()[1]],color='r',alpha=.5,zorder=-10)
#        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[0],ax[e1].get_ylim()[0]],color='b',alpha=.5,zorder=-10)
        
        plt.title(labkeys[e1])
    
    plt.tight_layout()
    plt.subplots_adjust(wspace=0.4, hspace=.2)
    
    ax[-1].spines['right'].set_visible(False)
    ax[-1].spines['top'].set_visible(False)
    ax[-1].spines['bottom'].set_visible(False)
    ax[-1].spines['left'].set_visible(False)
    ax[-1].set_xticks([])
    ax[-1].set_yticks([])
    ax[-1].scatter(.15,.75,s=50,marker='s',c='yellow')
    ax[-1].scatter(.5,.75,s=50,marker='s',c='b')
    ax[-1].scatter(.85,.75,s=50,marker='s',c='green')
    ax[-1].text(.15,.25,'Synergistic',ha='center',va='top')
    ax[-1].text(.5,.25,'Antagonistic\nby HSA, Loewe, Bliss @ EC50',ha='center',va='top')
    ax[-1].text(.85,.25,'Conflicting',ha='center',va='top')
    ax[-1].set_ylim((0,1))
    ax[-1].set_xlim((0,1))
    plt.savefig('syn_trends.pdf', bbox_inches = 'tight',pad_inches = 0)
    return (ymin,ymax)

ylim=((-1,1),(-2,2),(-2,2))
plotBoxSyn(T,ylim)


