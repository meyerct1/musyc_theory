#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 12:56:42 2018

@author: xnmeyer
"""
from SynergyCalculator.gatherData import subset_data
import pandas as pd
import numpy as np
from SynergyCalculator.calcOtherSynergyMetrics import calcOtherSynergy
import os
import warnings
warnings.simplefilter("ignore")

T = pd.read_csv('MasterResults_yeast.csv')
E_fix = None
key = ['loewe_fit_ec50','loewe_fit_ec10','loewe_fit_ec25','loewe_fit_all','loewe_fit_perUnd','zip_fit','hsa_raw_all','hsa_fit_all','hsa_fit_ec50','bliss_fit_all','bliss_fit_ec50','zimmer_fit_a1','zimmer_fit_a2','ci_fit_all','ci_fit_ec50','schindler_fit_ec50','schindler_fit_all','loewe_raw_all','loewe_raw_perUnd','schindler_raw_ec50','zimmer_raw_a1','zimmer_raw_a2','bliss_raw_all','zip_raw','ci_raw','brd_raw','brd_fit']
for k in key:
    T[k]=np.nan
#ky = ['E0','E1','E2','E3']
#for k in ky:
#    T[k]=T[k]/100.
#T = T[(T['drug1_name']=='5-fu')&(T['drug2_name']=='pd325901')&(T['sample']=='A2058')]
#T = T.reset_index(drop=True)
#    
for ind in T.index:
    print ind
    sub_T       = T.loc[ind].to_dict()
    expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1'] = data['drug1'].str.lower()
    data['drug2'] = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
#    dip = dip/100.
#    dip_sd=dip_sd/100.
    
    try:
        x = calcOtherSynergy(sub_T,d1,d2,dip,dip_sd,percent=False,E_fix=E_fix,calc_brd_zip=False)
        for e,k in enumerate(key):
            T.loc[ind,k]=x[e]
    except:
        continue
       
        
T.to_csv('MasterResults_otherSynergyMetrics_merck.csv')


#
#T = pd.read_csv('MasterResults_otherSynergyMetrics_merck.csv')
#key = ['loewe_fit_ec50','loewe_fit_ec10','loewe_fit_ec25','hsa_fit_ec50']
#for k in key:
#    T[k]=np.nan
#
#from SynergyCalculator.calcOtherSynergyMetrics import get_params,Edrug2D_NDB_hill,loewe,hsa,hill_1D_inv,hill_1D
#expt        = '/home/xnmeyer/Desktop/merck_anticancer_combinations/merck_perVia_10-29-2018.csv'
#data        = pd.read_table(expt, delimiter=',')        
#data['drug1'] = data['drug1'].str.lower()
#data['drug2'] = data['drug2'].str.lower()
#data['sample'] = data['sample'].str.upper()
#    
#for ind in T.index:
#    print ind
#    sub_T       = T.loc[ind].to_dict()
#    drug1_name  = sub_T['drug1_name']
#    drug2_name  = sub_T['drug2_name']
#    sample      = sub_T['sample']
#
#    
#    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)   
#    E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, C1, C2, alpha1, alpha2,gamma1,gamma2, d1min, d1max, d2min, d2max, drug1_name, drug2_name, expt = get_params(pd.DataFrame([sub_T]))
#    C1 = C1/1000. if h1>20 else C1; d1 = d1/1000. if h1>20. else d1
#    C2 = C2/1000. if h2>20 else C2; d2 = d2/1000. if h2>20. else d2
#
#    E_ec50 = Edrug2D_NDB_hill((C1,C2),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
#    T.loc[ind,'loewe_fit_ec50'] = -np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2))
#    EC10_1 = hill_1D_inv(E0-(E0-E1)*.1,E0,E1,h1,C1)
#    EC10_2 = hill_1D_inv(E0-(E0-E2)*.1,E0,E2,h2,C2)
#    E_ec10 = Edrug2D_NDB_hill((EC10_1,EC10_2),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
#    T.loc[ind,'loewe_fit_ec10'] = -np.log10(loewe(EC10_1, EC10_2, E_ec10, E0, E1, E2, h1, h2, C1, C2))
#    EC25_1 = hill_1D_inv(E0-(E0-E1)*.25,E0,E1,h1,C1)
#    EC25_2 = hill_1D_inv(E0-(E0-E2)*.25,E0,E2,h2,C2)
#    E_ec25 = Edrug2D_NDB_hill((EC25_1,EC25_2),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
#    T.loc[ind,'loewe_fit_ec25'] = -np.log10(loewe(EC25_1, EC25_2, E_ec25, E0, E1, E2, h1, h2, C1, C2))    
#    T.loc[ind,'hsa_fit_ec50'] = hsa([E0-(E0-E1)*.5],[E0-(E0-E2)*.5],E_ec50)[0]
#    
#    