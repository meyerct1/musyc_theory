#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  9 10:47:18 2018

@author: xnmeyer
"""

########Show HSA is the same for synergy of potency and efficacy curves
import pandas as pd
import numpy as np
import matplotlib.pylab as plt
#from matplotlib.patches import FancyArrowPatch
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from plotting_surfaces import plot_surface
from plotting_surfaces import get_params
from plotting_surfaces import dip

fit_params = {}
fit_params['min_conc_d1'] = np.power(10.,-10)
fit_params['min_conc_d2'] = np.power(10.,-10)
fit_params['max_conc_d1'] = np.power(10.,-5)
fit_params['max_conc_d2'] = np.power(10.,-5)
fit_params['h1'] = 1.
fit_params['h2'] = 1.
fit_params['log_C1'] = -7.5
fit_params['log_C2'] = -7.5
fit_params['E0'] = .05
fit_params['E1'] = .005
fit_params['E2'] = .005
fit_params['E3'] = -.025
fit_params['log_alpha1']=0.
fit_params['log_alpha2']=0.
fit_params['r1']=100000.*fit_params['E0']
fit_params['r2']=100000.*fit_params['E0']
fit_params['drug1_name']='d1'
fit_params['drug2_name']='d2'
fit_params['expt'] = 'cartoon'
fit_params = pd.DataFrame([fit_params])
N=50
zero_conc=2
zlim=(-0.055,0.055)
plot_surface(fit_params,metric_name="Drug Effect",N=N,zero_conc=zero_conc,zlim=zlim)


e0, e1, e2, e3, h1, h2, r1, r1r, r2, r2r, ec50_1, ec50_2, alpha1, alpha2, d1_min, d1_max, d2_min, d2_max, drug1_name, drug2_name, expt = get_params(fit_params)
#ax.set_axis_off()
y = np.linspace(d1_min-zero_conc, d1_max ,N)
t = np.linspace(d2_min-zero_conc, d2_max ,N)
yy, tt = np.meshgrid(y, t)
zz = dip(yy,tt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
cnt = 0
for i in range(zz.shape[0]):
    for j in range(zz.shape[1]):
        cnt=cnt+zz[0,:][t==np.max([yy[i][j],tt[i][j]])]-zz[i][j]

init_int = cnt

fit_params['E3'] = .005

def HSA(alpha):
    fit_params['log_alpha1']=alpha
    fit_params['log_alpha2']=alpha
    e0, e1, e2, e3, h1, h2, r1, r1r, r2, r2r, ec50_1, ec50_2, alpha1, alpha2, d1_min, d1_max, d2_min, d2_max, drug1_name, drug2_name, expt = get_params(fit_params)
    #ax.set_axis_off()
    y = np.linspace(d1_min-zero_conc, d1_max ,N)
    t = np.linspace(d2_min-zero_conc, d2_max ,N)
    yy, tt = np.meshgrid(y, t)
    zz = dip(yy,tt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
    cnt = 0
    for i in range(zz.shape[0]):
        for j in range(zz.shape[1]):
            cnt=cnt+zz[0,:][t==np.max([yy[i][j],tt[i][j]])]-zz[i][j]
    return cnt-init_int

from scipy.optimize import root

alpha = root(HSA,5.).x[0]
fit_params['log_alpha1']=alpha
fit_params['log_alpha2']=alpha
plot_surface(fit_params,metric_name="Drug Effect",N=N,zero_conc=zero_conc,zlim=zlim)



N=50
zero_conc = 2
alpha = np.linspace(-5,5,N)
beta = np.linspace(-1,1,N)
aa, bb = np.meshgrid(alpha,beta)
hsa = np.zeros(aa.shape)
for en1,a in enumerate(alpha):
    print en1
    for en2,b in enumerate(beta):
        fit_params['E3']=fit_params['E1']-b*(fit_params['E0']-fit_params['E1'])
        fit_params['log_alpha1']=a
        fit_params['log_alpha2']=a
        e0, e1, e2, e3, h1, h2, r1, r1r, r2, r2r, ec50_1, ec50_2, alpha1, alpha2, d1_min, d1_max, d2_min, d2_max, drug1_name, drug2_name, expt = get_params(fit_params)
        y = np.linspace(d1_min-zero_conc, d1_max ,N)
        t = np.linspace(d2_min-zero_conc, d2_max ,N)
        yy, tt = np.meshgrid(y, t)
        zz = dip(yy,tt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
        cnt = 0
        for i in range(zz.shape[0]):
            for j in range(zz.shape[1]):
                cnt=cnt+zz[0,:][t==np.max([yy[i][j],tt[i][j]])]-zz[i][j]
        hsa[en1,en2]=cnt
        
        
fig = plt.figure(figsize=(5,5))
ax = plt.subplot(111)
vmin = np.min(hsa)
vmax = np.max(hsa)
if np.abs(vmin)<vmax:
    vmin=-np.abs(vmax)
else:
    vmax=np.abs(vmin)

cf = plt.contourf(aa,bb,hsa,20,cmap='PRGn',vmin=vmin,vmax=vmax)
cl = plt.contour(aa,bb,hsa,20,cmap='PRGn',vmin=vmin,vmax=vmax)
cb = plt.colorbar(cf)
cb.set_label('HSA')
from scipy.interpolate import interp1d

x = cl.collections[9].get_paths()[0].vertices[:,0]
y = cl.collections[9].get_paths()[0].vertices[:,1]
f1 = interp1d(x,y)
y1 = f1(0)
f2 = interp1d(y,x)
x1 = f2(0)

plt.scatter(0,y1,s=50,c='k',zorder=10)
plt.scatter(x1,0,s=50,c='k',zorder=10)

a = 0
b = y1
fit_params['E3']=fit_params['E1']-b*(fit_params['E0']-fit_params['E1'])
fit_params['log_alpha1']=a
fit_params['log_alpha2']=a
plot_surface(fit_params,metric_name="Drug Effect",N=N,zero_conc=zero_conc,elev=41, azim=7)



a = x1
b = 0
fit_params['E3']=fit_params['E1']-b*(fit_params['E0']-fit_params['E1'])
fit_params['log_alpha1']=a
fit_params['log_alpha2']=a
plot_surface(fit_params,metric_name="Drug Effect",N=N,zero_conc=zero_conc,elev=41, azim=7)



