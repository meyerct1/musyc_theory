set(0,'DefaultAxesFontSize',8)
set(0,'DefaultLineLinewidth',2)
set(0, 'DefaultAxesFontWeight','normal')
set(0, 'DefaultAxesLineWidth',2)
set(0, 'DefaultFigureColor','w')
set(0, 'DefaultTextFontSize',8)
set(0, 'DefaultTextFontWeight','normal')
set(0, 'defaultTextFontName','arial')
set(0, 'defaultAxesFontName','arial')


syms r1 r1_1 r2 r2_1 h1 h2 d1 d2 alpha1 alpha2 positive
syms E0 E1 E2 E3 real
syms C1 C2
r1 = r2;
d1 = C1; d2 = C2;
h1 = 1;  h2 = 1;
E0 = 1;  E1 = 0;  E2 = 0;
r1_1 = C1^h1*r1; r2_1 = C2^h2*r2;
T = [-(r1*d1^h1+r2*d2^h2),r1_1,r2_1,0;
    r1*d1^h1,-(r1_1+r2*(alpha1*d2)^h2),0,r2_1;
    r2*d2^h2,0,-(r2_1+r1*(alpha2*d1)^h1),r1_1;
    1,1,1,1];
Eqn = [E0,E1,E2,E3] * inv(T)*[0;0;0;1];
eq1 = simplify(Eqn);
eq2 = combine(eq1);
C1 = 1;C2 = 1;


[x,y,z] = sphere(50);
v = zeros(size(x));
for i=1:size(x,1)
    for j=1:size(x,2)
        alpha1 = x(i,j);
        alpha2 = y(i,j);
        E3 = -z(i,j);
        v(i,j)= 1/2-subs(eq2);
    end
end

fig = figure('Color','white','rend','painters');
fig.PaperUnits = 'inches';
fig.PaperPosition = [1,1,2.5,2.5];
ax2 = axes('Position',[0.1 0.1 0.3 0.3]);
ax1 = axes('Position',[0.05 0.05 0.9 0.9]);
axes(ax1)
surf(x,y,z,v,'FaceAlpha',1,'EdgeColor','none','FaceLighting','gouraud')
caxis([-max(v(:)),max(v(:))])
daspect([1 1 1])
view(75,21)
h = colorbar('East','AxisLocation','out');
h.Label.String = '<--Ant     HSA     Syn-->';
h.Ticks = [min(h.Ticks),0,max(h.Ticks)];
h.Position = [0.82,0.25,0.03,0.55];
h.Label.Position = [2.2,0,0];
axis('off')
axes(ax2)
mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
xlim([-1,1])
ylim([-1,1])
zlim([-1,1])
view(75,21)
axis('off')
text(0,-1.5,-1,'\alpha_1','fontsize',10,'fontweight','bold')
text(-1.8,-.2,-1,'\alpha_2','fontsize',10,'fontweight','bold')
text(-1.25,-1.25,-.3,'\beta','fontsize',10,'fontweight','bold')
ax2.Clipping='off';
tightfig
print('hsa_sphere_1','-dpng','-r0')


fig = figure('Color','white','rend','painters');
fig.PaperUnits = 'inches';
fig.PaperPosition = [1,1,2.5,2.5];
ax2 = axes('Position',[0.01 0.01 0.3 0.3]);
ax1 = axes('Position',[0.05 0.05 0.9 0.9]);
axes(ax1)
surf(x,y,z,v,'FaceAlpha',1,'EdgeColor','none','FaceLighting','gouraud')
caxis([-max(v(:)),max(v(:))])
daspect([1 1 1])
view(75+180,21)
h = colorbar('East','AxisLocation','out');
h.Label.String = '<--Ant     HSA     Syn-->';
h.Ticks = [min(h.Ticks),0,max(h.Ticks)];
h.Position = [0.82,0.25,0.03,0.55];
h.Label.Position = [2.2,0,0];
axis('off')
axes(ax2)
mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
xlim([-1,1])
ylim([-1,1])
zlim([-1,1])
view(75+180,21)
axis('off')
text(1.5,-1,-1,'\alpha_1','fontsize',10,'fontweight','bold')
text(-.2,.2,-1,'\alpha_2','fontsize',10,'fontweight','bold')
text(-1.1,-1.1,-.3,'\beta','fontsize',10,'fontweight','bold')
ax2.Clipping='off';
tightfig
print('hsa_sphere_2','-dpng','-r0')


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Now bliss
v = zeros(size(x));
for i=1:size(x,1)
    for j=1:size(x,2)
        alpha1 = x(i,j);
        alpha2 = y(i,j);
        E3 = -z(i,j);
        v(i,j)= 1/4-subs(eq2);
    end
end

fig = figure('Color','white','rend','painters');
fig.PaperUnits = 'inches';
fig.PaperPosition = [1,1,2.5,2.5];
ax2 = axes('Position',[0.1 0.1 0.3 0.3]);
ax1 = axes('Position',[0.05 0.05 0.9 0.9]);
axes(ax1)
surf(x,y,z,v,'FaceAlpha',1,'EdgeColor','none','FaceLighting','gouraud')
caxis([-max(v(:)),max(v(:))])
daspect([1 1 1])
view(75,21)
h = colorbar('East','AxisLocation','out');
h.Label.String = '<--Ant     Bliss     Syn-->';
h.Ticks = [min(h.Ticks),0,max(h.Ticks)];
h.Position = [0.82,0.25,0.03,0.55];
h.Label.Position = [2.2,0,0];
axis('off')
axes(ax2)
mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
xlim([-1,1])
ylim([-1,1])
zlim([-1,1])
view(75,21)
axis('off')
text(0,-1.5,-1,'\alpha_1','fontsize',10,'fontweight','bold')
text(-1.8,-.2,-1,'\alpha_2','fontsize',10,'fontweight','bold')
text(-1.25,-1.25,-.3,'\beta','fontsize',10,'fontweight','bold')
ax2.Clipping='off';
tightfig
print('bliss_sphere_1','-dpng','-r0')


fig = figure('Color','white','rend','painters');
fig.PaperUnits = 'inches';
fig.PaperPosition = [1,1,2.5,2.5];
ax2 = axes('Position',[0.01 0.01 0.3 0.3]);
ax1 = axes('Position',[0.05 0.05 0.9 0.9]);
axes(ax1)
surf(x,y,z,v,'FaceAlpha',1,'EdgeColor','none','FaceLighting','gouraud')
caxis([-max(v(:)),max(v(:))])
daspect([1 1 1])
view(75+180,21)
h = colorbar('East','AxisLocation','out');
h.Label.String = '<--Ant     Bliss     Syn-->';
h.Ticks = [min(h.Ticks),0,max(h.Ticks)];
h.Position = [0.82,0.25,0.03,0.55];
h.Label.Position = [2.2,0,0];
axis('off')
axes(ax2)
mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
xlim([-1,1])
ylim([-1,1])
zlim([-1,1])
view(75+180,21)
axis('off')
text(1.5,-1,-1,'\alpha_1','fontsize',10,'fontweight','bold')
text(-.2,.2,-1,'\alpha_2','fontsize',10,'fontweight','bold')
text(-1.1,-1.1,-.3,'\beta','fontsize',10,'fontweight','bold')
ax2.Clipping='off';
tightfig
print('bliss_sphere_2','-dpng','-r0')







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Now Loewe
%Assume h1=h2=1,C1=C2,E1=E2=0
v = zeros(size(x));
for i=1:size(x,1)
    for j=1:size(x,2)
        alpha1 = x(i,j);
        alpha2 = y(i,j);
        E3 = -z(i,j);
        ed = subs(eq2);
        v(i,j)=-log10(2/(1/ed-1));
    end
end

xs = zeros(10,10,10);
ys = zeros(size(xs));
zs = zeros(size(xs));
vs1 = zeros(size(xs));
for i=1:size(xs,1)
    [xt,yt,zt]=sphere(9);
    xt = xt*1.1;yt=yt*1.1;zt=zt*1.1;
    xs(:,:,i)= xt./i;
    ys(:,:,i)= yt./i;
    zs(:,:,i)= zt./i;
    for j=1:size(xs,1)
        for k=1:size(xs,1)
            alpha1 = xs(j,k,i);
            alpha2 = ys(j,k,i);
            E3 = -zs(j,k,i);
            ed = subs(eq2);
            vs1(j,k,i)=-log10(2/(1/ed-1));
        end
    end
end

%https://www.mathworks.com/matlabcentral/answers/93623-how-do-i-plot-the-line-of-intersection-between-two-surfaces
[x3, y3, z3] = meshgrid(linspace(-1,1,10));
vs2 = xs.^2+ys.^2+zs.^2;
[x2, y2] = meshgrid(linspace(-1,1,10));
z2 = sqrt(y2.^2+x2.^2);
f3 = vs1 - vs2;
% Interpolate the difference field on the explicitly defined surface.
f3s = interp3(xs, ys, zs, f3, x2, y2, z2);
% Find the contour where the difference (on the surface) is zero.
C = contours(x2, y2, f3s, [0 0]);
% Extract the x- and y-locations from the contour matrix C.
xL = C(1, 2:end);
yL = C(2, 2:end);
% Interpolate on the first surface to find z-locations for the intersection
% line.
zL = interp2(x2, y2, z2, xL, yL);
% Visualize the line.
line(xL,yL,zL,'Color','k','LineWidth',3);



fig = figure('Color','white','rend','painters');
fig.PaperUnits = 'inches';
fig.PaperPosition = [1,1,2.5,2.5];
ax2 = axes('Position',[0.1 0.1 0.3 0.3]);
ax1 = axes('Position',[0.05 0.05 0.9 0.9]);
axes(ax1)
hold on
[xp,yp]=meshgrid(-1.05:.05:1.05,-1.05:.05:1.05);
zp = zeros(size(xp,1));
surf(xp,yp,zp,'FaceAlpha',.5,'EdgeColor','none','FaceLighting','gouraud','FaceColor',[.5,.5,.5]);
surf(zp,yp,xp,'FaceAlpha',.5,'EdgeColor','none','FaceLighting','gouraud','FaceColor',[.5,.5,.5]);
surf(xp,zp,yp,'FaceAlpha',.5,'EdgeColor','none','FaceLighting','gouraud','FaceColor',[.5,.5,.5]);


surf(x,y,z,v,'FaceAlpha',.75,'EdgeColor','interp','FaceLighting','gouraud','FaceColor','interp')
caxis([-max(v(:)),max(v(:))])
daspect([1 1 1])
view(75,21)
h = colorbar('East','AxisLocation','out');
h.Label.String = '<--Ant     Loewe     Syn-->';
h.Ticks = [min(h.Ticks),0,max(h.Ticks)];
h.Position = [0.82,0.25,0.03,0.55];
h.Label.Position = [2.2,0,0];
xlim([-1,1])
ylim([-1,1])
zlim([-1,1])
axis('off')

p = patch(isosurface(xs,ys,zs,vs,0));

axes(ax2)
mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
xlim([-1,1])
ylim([-1,1])
zlim([-1,1])
view(75,21)
axis('off')
text(0,-1.5,-1,'\alpha_1','fontsize',10,'fontweight','bold')
text(-1.8,-.2,-1,'\alpha_2','fontsize',10,'fontweight','bold')
text(-1.25,-1.25,-.3,'\beta','fontsize',10,'fontweight','bold')
ax2.Clipping='off';
tightfig
print('loewe_sphere_1','-dpng','-r0')


fig = figure('Color','white','rend','painters');
fig.PaperUnits = 'inches';
fig.PaperPosition = [1,1,2.5,2.5];
ax2 = axes('Position',[0.01 0.01 0.3 0.3]);
ax1 = axes('Position',[0.05 0.05 0.9 0.9]);
axes(ax1)
surf(x,y,z,v,'FaceAlpha',1,'EdgeColor','none','FaceLighting','gouraud')
caxis([-max(v(:)),max(v(:))])
daspect([1 1 1])
view(75+180,21)
h = colorbar('East','AxisLocation','out');
h.Label.String = '<--Ant     Loewe     Syn-->';
h.Ticks = [min(h.Ticks),0,max(h.Ticks)];
h.Position = [0.82,0.25,0.03,0.55];
h.Label.Position = [2.2,0,0];
axis('off')
axes(ax2)
mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
xlim([-1,1])
ylim([-1,1])
zlim([-1,1])
view(75+180,21)
axis('off')
text(1.5,-1,-1,'\alpha_1','fontsize',10,'fontweight','bold')
text(-.2,.2,-1,'\alpha_2','fontsize',10,'fontweight','bold')
text(-1.1,-1.1,-.3,'\beta','fontsize',10,'fontweight','bold')
ax2.Clipping='off';
tightfig
print('loewe_sphere_2','-dpng','-r0')





























%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Now Loewe
%Assume h1=h2=1,C1=C2,E1=E2=0
figure()
[alpha1,alpha2,E3] = meshgrid(linspace(-.5,.5,100));
ed=(1 + 1 + alpha2 + alpha1 + E3.*alpha2 + E3.*alpha1 + E3.*alpha1.*alpha2 + E3.*alpha1.*alpha2)./(3 + 3 + alpha1 + 3*alpha2 + 3*alpha1 + alpha2 + alpha1.*alpha2 + alpha1.*alpha2);
vs1 =-log10(2/(1./ed-1));
vs2 = x3.^2+y3.^2+z3.^2;
f3 = vs2 - vs1;
C = contours(x2, y2, f3, [0 1]);
p1 = patch(isosurface(x3, y3, z3, vs1, 0), 'FaceColor', [0.5 1.0 0.5], 'EdgeColor', 'none');
p2 = patch(isosurface(x3, y3, z3, vs2, 1), 'FaceColor', [1.0 0.5 0.0], 'EdgeColor', 'none');
% Extract the x- and y-locations from the contour matrix C.
xL = C(1, 2:end);
yL = C(2, 2:end);
% Interpolate on the first surface to find z-locations for the intersection
% line.
zL = interp2(x2, y2, z2, xL, yL);
% Visualize the line.
line(xL,yL,zL,'Color','k','LineWidth',3);




figure()
[x3,y3,z3] = meshgrid(linspace(-.5,.5,10));
ed=(1 + 1 + y3 + x3 + z3.*y3 + z3.*x3 + z3.*x3.*y3 + z3.*x3.*y3)./(3 + 3 + x3 + 3*y3 + 3*x3 + y3 + x3.*y3 + x3.*y3);
f1 =-log10(2/(1./ed-1));
f2 = x3.^2+y3.^2+z3.^2;
[x2, y2] = meshgrid(linspace(-1,1,100));
z2 = 2*y2 - 6*x2.^3;
% Visualize the two surfaces.
p1 = isosurface(x3, y3, z3, f1, 0);
p2 = isosurface(x3, y3, z3, f2, .25);
patch(p1)
patch(p2)
view(3); camlight; axis vis3d;
clf; hold on
[i12, s12] = SurfaceIntersection(p1, p2);
S=s12; trisurf(S.faces, S.vertices(:,1),S.vertices(:,2),S.vertices(:,3),'EdgeColor', 'r', 'FaceColor', 'r');



% Find the difference field.
f3 = f1 - f2;
% Interpolate the difference field on the explicitly defined surface.
f3s = interp3(x3, y3, z3, f3, x2, y2, z2);
% Find the contour where the difference (on the surface) is zero.
C = contours(x2, y2, f3s, [0 0]);
% Extract the x- and y-locations from the contour matrix C.
xL = C(1, 2:end);
yL = C(2, 2:end);
% Interpolate on the first surface to find z-locations for the intersection
% line.
zL = interp2(x2, y2, z2, xL, yL);
% Visualize the line.
line(xL,yL,zL,'Color','k','LineWidth',3);






% Interpolate the difference field on the explicitly defined surface.
f3s = interp3(x3, y3, z3, f3, x2, y2, z2);
% Visualize the two surfaces.
view(3); camlight; axis vis3d;
% Find the contour where the difference (on the surface) is zero.
C = contours(x2, y2, f3s, [0 0]);
% Extract the x- and y-locations from the contour matrix C.
xL = C(1, 2:end);
yL = C(2, 2:end);
% Interpolate on the first surface to find z-locations for the intersection
% line.
zL = interp2(x2, y2, z2, xL, yL);
% Visualize the line.
line(xL,yL,zL,'Color','k','LineWidth',3);



fig = figure('Color','white','rend','painters');
fig.PaperUnits = 'inches';
fig.PaperPosition = [1,1,2.5,2.5];
ax2 = axes('Position',[0.1 0.1 0.3 0.3]);
ax1 = axes('Position',[0.05 0.05 0.9 0.9]);
axes(ax1)
hold on
[xp,yp]=meshgrid(-1.05:.05:1.05,-1.05:.05:1.05);
zp = zeros(size(xp,1));
surf(xp,yp,zp,'FaceAlpha',.5,'EdgeColor','none','FaceLighting','gouraud','FaceColor',[.5,.5,.5]);
surf(zp,yp,xp,'FaceAlpha',.5,'EdgeColor','none','FaceLighting','gouraud','FaceColor',[.5,.5,.5]);
surf(xp,zp,yp,'FaceAlpha',.5,'EdgeColor','none','FaceLighting','gouraud','FaceColor',[.5,.5,.5]);


surf(x,y,z,v,'FaceAlpha',.75,'EdgeColor','interp','FaceLighting','gouraud','FaceColor','interp')
caxis([-max(v(:)),max(v(:))])
daspect([1 1 1])
view(75,21)
h = colorbar('East','AxisLocation','out');
h.Label.String = '<--Ant     Loewe     Syn-->';
h.Ticks = [min(h.Ticks),0,max(h.Ticks)];
h.Position = [0.82,0.25,0.03,0.55];
h.Label.Position = [2.2,0,0];
xlim([-1,1])
ylim([-1,1])
zlim([-1,1])
axis('off')

p = patch(isosurface(xs,ys,zs,vs,0));

axes(ax2)
mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
xlim([-1,1])
ylim([-1,1])
zlim([-1,1])
view(75,21)
axis('off')
text(0,-1.5,-1,'\alpha_1','fontsize',10,'fontweight','bold')
text(-1.8,-.2,-1,'\alpha_2','fontsize',10,'fontweight','bold')
text(-1.25,-1.25,-.3,'\beta','fontsize',10,'fontweight','bold')
ax2.Clipping='off';
tightfig
print('loewe_sphere_1','-dpng','-r0')








