#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Code for caluculating drug synergy using MCMC fitting given a data frame resulting from one of the HTC experiments on ACCRE.
The dose response surface is fit in a multi-step optimization.
    1) The single dose response curves are fit to a hill equation.  If either fails, its parameters are estimated from the data
    2) Particle Swarm Optimization to estimate an initial fit for the the dose response surface
    3) Six different models are fit with differing amounts of granularity using MCMC optimization
    4) The MCMC optimization fit uses the Geweke Information Criterion to determine convergence
         -Note since the scale of the parameters is diverse, it is necessary to use an adaptive sampling technique
    5) The models are compared using the DIC which picks the minimum to choose the model

The results are returned as a dictionary T
Also includes surface plotting function called DosePlots_PLY.
Function is fit for models with and without the assumption of detail balance

Code commented and up to date 6.13.18
@author: Christian Meyer 
@email: christian.t.meyer@vanderbilt.edu
"""
##Import Packages/Install missing ones
import numpy as np
import pandas as pd
from uncertainties import ufloat
import pymc3 as pm
import plotly.graph_objs as go
from plotly.offline import  plot
#import matplotlib.pyplot as plt
import time
from scipy.integrate import dblquad
import os
import scipy.optimize
import scipy.stats
import resource
from math import pi
#PSO from James for fitting.  From github https://github.com/LoLab-VU/ParticleSwarmOptimization
#from simplepso.pso import PSO
import datetime

#####################################
#MCMC burn in and sample iteration counts
BURN = 1000000
SAMPLES = 1000000
PSO_PARTICLES=10000
PSO_ITER=100
######################################    

def pymc_fit(syn_Model,SAMPLES,BURN):
    with syn_Model:
        #Needed a metropolis sampling before the pm.sample() for it to work
        #It is possible that the NUTS algorithm can't converge because the
        #Scale of the parameters is quite different.
        #See https://github.com/pymc-devs/pymc3/issues/2272 for details
        step = pm.Metropolis()           
        trace = pm.sample(SAMPLES,step, tune=BURN, njobs=1)

    #Make sure everything converged by looking at the geweke criterion.
    #If a trace moves beyond 2 standard deviations it is considered not to converge
    converge = 1
    ge =pm.diagnostics.geweke(trace)
    for key, value in ge.iteritems():
        if not key.endswith('_'): #Not sure what the 'log_alpha1_interval__' parameter is but its convergence doesn't matter
            if (max(abs(value[:,1]))) > 2:
                converge = 0
    #Make sure everything converged by looking at the gelman rubin criterion.
    #As per Brooks, S. P., and A. Gelman. 1997. General Methods for Monitoring Convergence of Iterative Simulations. Journal of Computational and Graphical Statistics 7: 434–455.
    #Used a value of 1.2
#    converge = 1
#    gr = pm.gelman_rubin(trace)
#    for key,value in gr.iteritems():
#        if not key.endswith('_'): 
#            if value > 1.2:
#                converge = 0          
    return trace,converge
           
#Function to calculate the synergy using MCMC method sampling 
def SyngCalc(d1,d2,dip,dip_sd,drug1_name,drug2_name,to_plot,target_cell_line,expt,min_nest=0,max_nest=5,metric_name='DIP Rate',hill_orient=1,to_save=0,direc=os.getcwd(),E0_fix=1,Emx_fix=0): 
    #Store results in a dict
    T = {}
    #Set the random seed
    start_time = time.time()#keep track of the time
    np.random.seed(int(np.round(start_time))) #set a random seed so the mcmc fits are reproducible
    T['start_time']=time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(start_time))
    T['init_rndm_seed']=int(np.round(start_time))
    T['cell_line'] = target_cell_line
    T['expt'] = expt
    print('Fitting ' + drug1_name + ' ' + drug2_name + ' Cell line: '+ target_cell_line)

    ###########################################################################
    ###  Fit the dose response curve for the single drug cases.    #########
    #### Use pydrc library                                         #########
    #### If curve fitting fails or a drug is never tested at the   #########
    #### zero concentration, make these approximations:            #########
    #### E0=affect at min(conc).  Emax=affect at max(conc)         #########
    #### h = 1, EC50 = max(conc)/100                               #########
    ###########################################################################
    if max_nest<7:
        T = init_fit(T,d1,d2,dip,dip_sd,drug1_name,drug2_name,hill_orient=hill_orient)
    else:
        T = init_fit_fix(T,d1,d2,dip,dip_sd,drug1_name,drug2_name,E0_fix,Emx_fix)
        min_nest=max_nest
    ###########################################################################
    ###  Next estimate the dose response surface for 2D no detail  #########
    #### balance model (tier 5) using James Pino's PSO             #########
    #### The converged parameters are used to for priors in the    #########
    #### MCMC fitting next step                                    #########
    ###########################################################################    
    print('Starting Particle Swarm Optimization fitting')
    #Objective Function used for PSO fitting.
    #Use a gaussian to generate the log-likelihood of fit given the data

    if max_nest<=5:
        def likelihood(parms):
            dip_fit =  Edrug2D_NDB((d1,d2),parms[0],parms[1],parms[2],parms[3],parms[4],parms[5],10**parms[6],10**parms[7],parms[8],parms[9],10**parms[10],10**parms[11])
            log_likelihood =  sum(np.log(1/(dip_sd*np.sqrt(2*pi)))) - sum(((dip-dip_fit)/dip_sd)**2)/2
            if np.isnan(log_likelihood):
                log_likelihood = -10e10
            return -log_likelihood,
        
        #Starting point for the PSO optimization
        st=[T['tmp_E0'],T['tmp_E1'],T['tmp_E2'],T['tmp_E3'],100*T['tmp_E0'],100*T['tmp_E0'],T['tmp_log_C1'],T['tmp_log_C2'],T['tmp_h1'],T['tmp_h2'],0,0]
        #Declare the optimizer.  This optimization uses James Pinos PSO for python
        T['log_like_pso_start'] = likelihood(st)[0]
        optimizer = PSO(cost_function=likelihood,start=st,verbose=True)
        #Set the lower and upper bounds
        #The cost function is very sensitive to changes in h and c bounds
        #I have found <10 for h and log_c(1/2)_lower+2 to work
        #The sampling for alpha and EC50 are in log space
        lower=[T['tmp_E0']-2*T['tmp_E0_std'],T['tmp_E1']-2*T['tmp_E1_std'],T['tmp_E2']-2*T['tmp_E2_std'],T['tmp_E3']-2*T['tmp_E3_std'],
                             10*T['tmp_E0'],10*T['tmp_E0'],T['log_c1_lower']+2,T['log_c2_lower']+2,0.1,0.1,T['alpha_lower'],T['alpha_lower']+1]
        upper=[T['tmp_E0']+2*T['tmp_E0_std'],T['tmp_E1']+2*T['tmp_E1_std'],T['tmp_E2']+2*T['tmp_E2_std'],T['tmp_E3']+2*T['tmp_E3_std'],
                             1000*T['tmp_E0'],1000*T['tmp_E0'],T['log_c1_upper']-2,T['log_c2_upper']-2,5,5,T['alpha_upper'],T['alpha_upper']-1]
        optimizer.set_bounds(lower=lower,upper=upper)
        #run the optimizer
        optimizer.run(num_particles=PSO_PARTICLES, num_iterations=PSO_ITER)
        #Update the initial guesses for mcmc walk only if we did better
        T['log_like_pso_finish'] = likelihood(optimizer.best)[0]
        if T['log_like_pso_finish']<T['log_like_pso_start']:
            [T['tmp_E0'],T['tmp_E1'],T['tmp_E2'],T['tmp_E3'],T['tmp_r1'],T['tmp_r2'],T['tmp_log_C1'],T['tmp_log_C2'],T['tmp_h1'],T['tmp_h2'],T['tmp_log_alpha1'],T['tmp_log_alpha2']] = optimizer.best
        if to_save:
            sav_fil = pd.HDFStore(direc+os.sep+'{}_{}_{}_{}_data.h5'.format(datetime.datetime.now().strftime('%Y%m%d'),target_cell_line,drug1_name,drug2_name),'w')
            pd.DataFrame(optimizer.logbook).to_hdf(sav_fil,'pso/optimizer_logbook',data_columns=True)
            pd.DataFrame(optimizer.history,columns=['E0','E1','E2','E3','r1','r2','log_C1','log_C2','h1','h2','log_alpha1','log_alpha2']).to_hdf(sav_fil,'pso/ptimizer_history',data_columns=True)
            pd.DataFrame(np.array(optimizer.best),index=['E0','E1','E2','E3','r1','r2','log_C1','log_C2','h1','h2','log_alpha1','log_alpha2'],columns=['value']).to_hdf(sav_fil,'pso/optimizer_best',data_columns=True)
            
    elif max_nest==6:
        def likelihood(parms):
            dip_fit =  Edrug2D_NDB_hill((d1,d2),parms[0],parms[1],parms[2],parms[3],parms[4],parms[5],10**parms[6],10**parms[7],parms[8],parms[9],10**parms[10],10**parms[11],10**parms[12],10**parms[13])
            log_likelihood =  sum(np.log(1/(dip_sd*np.sqrt(2*pi)))) - sum(((dip-dip_fit)/dip_sd)**2)/2
            if np.isnan(log_likelihood):
                log_likelihood = -10e10
            return -log_likelihood,
        
        #Starting point for the PSO optimization
        st=[T['tmp_E0'],T['tmp_E1'],T['tmp_E2'],T['tmp_E3'],100*T['tmp_E0'],100*T['tmp_E0'],T['tmp_log_C1'],T['tmp_log_C2'],T['tmp_h1'],T['tmp_h2'],0,0,0,0]
        #Declare the optimizer.  This optimization uses James Pinos PSO for python
        T['log_like_pso_start'] = likelihood(st)[0]
        optimizer = PSO(cost_function=likelihood,start=st,verbose=True)
        #Set the lower and upper bounds
        #The cost function is very sensitive to changes in h and c bounds
        #I have found <10 for h and log_c(1/2)_lower+2 to work
        #The sampling for alpha and EC50 are in log space
        lower=[T['tmp_E0']-2*T['tmp_E0_std'],T['tmp_E1']-2*T['tmp_E1_std'],T['tmp_E2']-2*T['tmp_E2_std'],T['tmp_E3']-2*T['tmp_E3_std'],
                             10*T['tmp_E0'],10*T['tmp_E0'],T['log_c1_lower'],T['log_c2_lower'],0.1,0.1,T['alpha_lower'],T['alpha_lower'],T['gamma_lower'],T['gamma_lower']]
        upper=[T['tmp_E0']+2*T['tmp_E0_std'],T['tmp_E1']+2*T['tmp_E1_std'],T['tmp_E2']+2*T['tmp_E2_std'],T['tmp_E3']+2*T['tmp_E3_std'],
                             1000*T['tmp_E0'],1000*T['tmp_E0'],T['log_c1_upper'],T['log_c2_upper'],5,5,T['alpha_upper'],T['alpha_upper'],T['gamma_upper'],T['gamma_upper']]
        optimizer.set_bounds(lower=lower,upper=upper)
        #run the optimizer
        optimizer.run(num_particles=PSO_PARTICLES, num_iterations=PSO_ITER)
        #Update the initial guesses for mcmc walk only if we did better
        T['log_like_pso_finish'] = likelihood(optimizer.best)[0]
        if T['log_like_pso_finish']<T['log_like_pso_start']:
            [T['tmp_E0'],T['tmp_E1'],T['tmp_E2'],T['tmp_E3'],T['tmp_r1'],T['tmp_r2'],T['tmp_log_C1'],T['tmp_log_C2'],T['tmp_h1'],T['tmp_h2'],T['tmp_log_alpha1'],T['tmp_log_alpha2'],T['tmp_log_gamma1'],T['tmp_log_gamma2']] = optimizer.best
        if to_save:
            sav_fil = pd.HDFStore(direc+os.sep+'{}_{}_{}_{}_data.h5'.format(datetime.datetime.now().strftime('%Y%m%d'),target_cell_line,drug1_name,drug2_name),'w')
            pd.DataFrame(optimizer.logbook).to_hdf(sav_fil,'pso/optimizer_logbook',data_columns=True)
            pd.DataFrame(optimizer.history,columns=['E0','E1','E2','E3','r1','r2','log_C1','log_C2','h1','h2','log_alpha1','log_alpha2','log_gamma1','log_gamma2']).to_hdf(sav_fil,'pso/ptimizer_history',data_columns=True)
            pd.DataFrame(np.array(optimizer.best),index=['E0','E1','E2','E3','r1','r2','log_C1','log_C2','h1','h2','log_alpha1','log_alpha2','log_gamma1','log_gamma2'],columns=['value']).to_hdf(sav_fil,'pso/optimizer_best',data_columns=True)

    elif max_nest==7:
        def likelihood(parms):
            dip_fit =  Edrug2D_NDB_hill((d1,d2),E0_fix,Emx_fix,Emx_fix,Emx_fix,parms[0],parms[1],10**parms[2],10**parms[3],parms[4],parms[5],10**parms[6],10**parms[7],10**parms[8],10**parms[9])
            log_likelihood =  sum(np.log(1/(dip_sd*np.sqrt(2*pi)))) - sum(((dip-dip_fit)/dip_sd)**2)/2
            if np.isnan(log_likelihood):
                log_likelihood = -10e10
            return -log_likelihood,
        
        #Starting point for the PSO optimization
        st=[100*T['tmp_E0'],100*T['tmp_E0'],T['tmp_log_C1'],T['tmp_log_C2'],T['tmp_h1'],T['tmp_h2'],0,0,0,0]
        #Declare the optimizer.  This optimization uses James Pinos PSO for python
        T['log_like_pso_start'] = likelihood(st)[0]
        optimizer = PSO(cost_function=likelihood,start=st,verbose=True)
        #Set the lower and upper bounds
        #The cost function is very sensitive to changes in h and c bounds
        #I have found <10 for h and log_c(1/2)_lower+2 to work
        #The sampling for alpha and EC50 are in log space
        lower=[10*T['tmp_E0'],10*T['tmp_E0'],T['log_c1_lower'],T['log_c2_lower'],0.1,0.1,T['alpha_lower'],T['alpha_lower'],T['gamma_lower'],T['gamma_lower']]
        upper=[1000*T['tmp_E0'],1000*T['tmp_E0'],T['log_c1_upper'],T['log_c2_upper'],5,5,T['alpha_upper'],T['alpha_upper'],T['gamma_upper'],T['gamma_upper']]
        optimizer.set_bounds(lower=lower,upper=upper)
        #run the optimizer
        optimizer.run(num_particles=PSO_PARTICLES, num_iterations=PSO_ITER)
        #Update the initial guesses for mcmc walk only if we did better
        T['log_like_pso_finish'] = likelihood(optimizer.best)[0]
        if T['log_like_pso_finish']<T['log_like_pso_start']:
            [T['tmp_r1'],T['tmp_r2'],T['tmp_log_C1'],T['tmp_log_C2'],T['tmp_h1'],T['tmp_h2'],T['tmp_log_alpha1'],T['tmp_log_alpha2'],T['tmp_log_gamma1'],T['tmp_log_gamma2']] = optimizer.best
        if to_save:
            sav_fil = pd.HDFStore(direc+os.sep+'{}_{}_{}_{}_data.h5'.format(datetime.datetime.now().strftime('%Y%m%d'),target_cell_line,drug1_name,drug2_name),'w')
            pd.DataFrame(optimizer.logbook).to_hdf(sav_fil,'pso/optimizer_logbook',data_columns=True)
            pd.DataFrame(optimizer.history,columns=['r1','r2','log_C1','log_C2','h1','h2','log_alpha1','log_alpha2','log_gamma1','log_gamma2']).to_hdf(sav_fil,'pso/ptimizer_history',data_columns=True)
            pd.DataFrame(np.array(optimizer.best),index=['r1','r2','log_C1','log_C2','h1','h2','log_alpha1','log_alpha2','log_gamma1','log_gamma2'],columns=['value']).to_hdf(sav_fil,'pso/optimizer_best',data_columns=True)


    ##########################################################################
    ##Now run the MCMC models using the PSO generated starting points
    ##########################################################################
    #Initialize class of nested models.  The nested model class is just a collection of functions....
    #Store the traces and the results in a list.
    print('Starting Monte Carlo Markov Chain simulation for fitting different model tiers.  Optimal model tier deterimined by minimizing Deviance Information Criterion.')
    converge = []
    dic_fit = []        
    nestedModels = NestedModels()
    #For each model tier
    for i in np.linspace(min_nest,max_nest,max_nest-min_nest+1):
        i = int(i)
        print('Running MCMC Optimization...nest: ' + str(i))
        #Save the mean and standard deviation of priors for this tier
        if to_save:
            pd.DataFrame([value for key,value in T.items() if 'tmp' in key],index=[key[4:] for key in T.keys() if 'tmp' in key],columns=['priors_'+str(i)]).to_hdf(sav_fil,'mcmc/' + 'priors_tier' + str(i),data_columns=True)
        #Find the model nest
        nest = nestedModels.funct['nest'+str(i)]
        #Initialize pymc3 model
        syn_Model = pm.Model()
        #Update model
        syn_Model = nest(syn_Model, T['tmp_E0'],T['tmp_E1'],T['tmp_E2'],T['tmp_E3'],
                                    T['tmp_log_C1'],T['tmp_log_C2'],
                                    T['tmp_h1'],T['tmp_h2'],
                                    T['tmp_E0_std'],T['tmp_E1_std'],T['tmp_E2_std'],T['tmp_E3_std'],
                                    T['tmp_log_C1_std'],T['tmp_log_C2_std'],
                                    T['tmp_h1_std'],T['tmp_h2_std'],
                                    T['tmp_log_alpha1'],T['tmp_log_alpha2'],
                                    T['tmp_log_alpha1_std'],T['tmp_log_alpha2_std'],
                                    T['tmp_log_gamma1'],T['tmp_log_gamma2'],
                                    T['tmp_log_gamma1_std'],T['tmp_log_gamma2_std'],
                                    T['alpha_lower'],T['alpha_upper'],
                                    T['gamma_lower'],T['gamma_upper'],
                                    T['log_c1_lower'],T['log_c2_lower']
                                    ,d1,d2,dip,dip_sd)
        #Run MCMC fit
        trace,geweke = pymc_fit(syn_Model,SAMPLES,BURN)
        #Track how much memory is being used
        #print resource.getrusage(resource.RUSAGE_SELF).ru_maxrss            
        #Append the results to a list
        converge.append(geweke)
        dic_fit.append(pm.stats.dic(model = syn_Model, trace = trace))
        T['DIC_'+str(i)] = dic_fit[-1]
        T['MCMC_converge_tier_'+str(i)]=converge[-1]
        if converge[-1]==1:
            for key in trace.varnames:
                if not key.endswith('_'):
                    T['tmp_' + key] = np.mean(trace[key])
                    #If the previous run failed to sample (ie std(parm)<10**-10) don't update the standard deviation estimate.
                    if np.std(trace[key])>10**-10:
                        T['tmp_' + key + '_std'] = np.std(trace[key])

            for key in trace.varnames:
                if not key.endswith('_'):
                    T['tmp_' + key + '_' + str(i)] = np.mean(trace[key])
                    #If the previous run failed to sample (ie std(parm)<10**-10) don't update the standard deviation estimate.
                    if np.std(trace[key])>10**-10:
                        T['tmp_' + key + '_std' +  '_' + str(i)] = np.std(trace[key])
        trace_df = pd.DataFrame()
        if to_save:
            for key in trace.varnames:
                if not key.endswith('_'):
                    trace_df[key]=trace[key]
                    #sav_fil.create_dataset('mcmc_trace_' + key + '_' + str(i),data=trace[key])
            trace_df.to_hdf(sav_fil,'mcmc/' + 'trace_tier_' + str(i),data_columns=True)
            pd.DataFrame([geweke,T['DIC_'+str(i)]],index=['geweke_convergence','deviance_information_criterion'],columns=['value']).to_hdf(sav_fil,'mcmc/' + 'convergence_tier_' + str(i),data_columns=True)
            
##If you want to see the plots of the MCMC traces uncommment  
#        level = 7
#        fig = pm.traceplot(trace)
#        plt.savefig(filename = '{}_{}_Traces.pdf'.format(drug1_name,drug2_name))
#        parm = 'log_alpha1'
#        ge =pm.diagnostics.geweke(trace)
#        fig = plt.figure(facecolor='w')
#        ax = plt.subplot(111)
#        plt.scatter(ge[parm][:,0],ge[parm][:,1])
#        x_lim = plt.xlim()
#        plt.plot(x_lim,[2,2],'k-')
#        plt.plot(x_lim,[-2,-2],'k-')
#        plt.xlabel('First iteration')
#        plt.ylabel('Z-score for' +  parm)
#        plt.title(parm)
#A way to test the model ....
#for RV in syn_Model.basic_RVs:
#    print(RV.name, RV.logp(syn_Model.test_point))

    #Replace the DIC for which didn't converge with inf
    dic_fit = [np.inf if np.isnan(x) else x for x in dic_fit]    
    dic_fit = [np.inf if converge[e]==0 else x for e,x in enumerate(dic_fit)]
    #Only move forward if something converged
    if sum(~np.isnan(dic_fit))>0 and sum(converge)>0:
        #Select the model which converged and had the lowest DIC
        #If all are nan, then nest0 is picked.
        min_dic, mod_lev= min((val, idx) for (idx, val) in enumerate(dic_fit))        
        mod_lev = mod_lev + min_nest  #Account for which nests were tested.
        print('Minimal DIC of converged models was tier:'+str(mod_lev))
        ###########################################################################
        #### Store all the information in the dict T
        ###########################################################################        
        T.update({'MCMC_converge':converge[mod_lev-min_nest],'MCMC_BURN':BURN,'MCMC_SAMPLE':SAMPLES,'model_level':mod_lev})
        for key in T.keys():
            if key.endswith('_' + str(mod_lev)) and key.startswith('tmp_'):
                k = key[4:-2]
                T[k] = T[key]
        #Delete place holder values            
        for key in T.keys():
            if key.startswith('tmp_'):
                T.pop(key)                
        #If model level was less than 5, then it is some approximation of the Detail balance case.
        if mod_lev < 5:     
            a = ufloat(T['log_alpha1'],T['log_alpha1_std'])
            b = ufloat(T['h1'],T['h1_std'])
            c = ufloat(T['h2'],T['h2_std'])
            d = a*(c/b)
            T['log_alpha2'] = d.n
            T['log_alpha2_std'] = d.s
            #Parameters of the fit to calculate an R2 value  
            popt3 = [ T['E0'],
                      T['E0_std'],
                      T['E1'],
                      T['E1_std'],
                      T['E2'],
                      T['E2_std'],
                      T['E3'],
                      T['E3_std'],
                      10**T['log_C1'],
                      np.log(10)*T['log_C1_std']*10**T['log_C1'],
                      10**T['log_C2'],
                      np.log(10)*T['log_C2_std']*10**T['log_C2'],
                      T['h1'],
                      T['h1_std'],
                      T['h2'],
                      T['h2_std'],
                      10**T['log_alpha1'],
                      np.log(10)*T['log_alpha1_std']*10**T['log_alpha1']]      
                      
            def likelihood(parms):
                dip_fit =  Edrug2D_DB((d1,d2),parms[0],parms[1],parms[2],parms[3],parms[4],parms[5],parms[6],parms[7],parms[8])
                log_likelihood =  sum(np.log(1/(dip_sd*np.sqrt(2*pi)))) - sum(((dip-dip_fit)/dip_sd)**2)/2
                if np.isnan(log_likelihood):
                    log_likelihood = -10e10
                return -log_likelihood,                
            
            T['log_like_mcmc'] = likelihood(popt3[0:17:2])[0]

            T['R2'] = 1-sum((dip - Edrug2D_DB((d1,d2),*popt3[0:17:2]))**2)/sum((dip-np.mean(dip))**2);
            T['E2_obs'] = Edrug2D_DB((0,max(d2[d1!=0])),*popt3[0:17:2])
            T['E1_obs'] = Edrug2D_DB((max(d1[d2!=0]),0),*popt3[0:17:2])
            
            T['E3_obs'] = Edrug2D_DB((max(d1[d2!=0]),max(d2[d1!=0])),*popt3[0:17:2])
            T['E2_obs_std'] = err_Edrug2D_DB((0,max(d2)),popt3)
            T['E1_obs_std'] = err_Edrug2D_DB((max(d1),0),popt3)
            T['E3_obs_std'] = err_Edrug2D_DB((max(d1[d2!=0]),max(d2[d1!=0])),popt3)
        
            #T['U'],T['A1'],T['A2'],T['A12'] = getU_lvl4(T['C1'],T['C2'],*popt3[0:17:2])
            T['VBS'],T['VBS_verr'] = dblquad(integrand_lvl4, min(np.log10(d1[d1!=0])), max(np.log10(d1)), lambda x: min(np.log10(d2[d2!=0])), lambda x: max(np.log10(d2)), args=(popt3[0:17:2]))
            T['DelSurf'] = sum(Edrug2D_DB((d1,d2),T['E0'],T['E1'],T['E2'],min(T['E2'],T['E1']),10**T['log_C1'],10**T['log_C2'],T['h1'],T['h2'],1)-dip)
         
            if to_plot==1:
                DosePlots_PLY(d1,d2,dip,dip_sd,drug1_name,drug2_name,popt3[0:17:2],'DB',target_cell_line,expt,metric_name,direc)
        elif mod_lev == 5: #It was best fit by a model which did not obey detail balance
            popt3 = [ T['E0'],
                      T['E0_std'],
                      T['E1'],
                      T['E1_std'],
                      T['E2'],
                      T['E2_std'],
                      T['E3'],
                      T['E3_std'],
                      T['r1'],
                      T['r1_std'],
                      T['r2'],
                      T['r2_std'],
                      10**T['log_C1'],
                      np.log(10)*T['log_C1_std']*10**T['log_C1'],
                      10**T['log_C2'],
                      np.log(10)*T['log_C2_std']*10**T['log_C2'],
                      T['h1'],
                      T['h1_std'],
                      T['h2'],
                      T['h2_std'],
                      10**T['log_alpha1'],
                      np.log(10)*T['log_alpha1_std']*10**T['log_alpha1'],
                      10**T['log_alpha2'],
                      np.log(10)*T['log_alpha2_std']*10**T['log_alpha2']]
            T['R2'] = 1-sum((dip - Edrug2D_NDB((d1,d2),*popt3[0:23:2]))**2)/sum((dip-np.mean(dip))**2);

            def likelihood(parms):
                dip_fit =  Edrug2D_NDB((d1,d2),parms[0],parms[1],parms[2],parms[3],parms[4],parms[5],parms[6],parms[7],parms[8],parms[9],parms[10],parms[11])
                log_likelihood =  sum(np.log(1/(dip_sd*np.sqrt(2*pi)))) - sum(((dip-dip_fit)/dip_sd)**2)/2
                if np.isnan(log_likelihood):
                    log_likelihood = -10e10
                return -log_likelihood,
                
            T['log_like_mcmc'] = likelihood(popt3[0:23:2])[0]
            
            T['E2_obs'] = Edrug2D_NDB((0,max(d2[d1!=0])),*popt3[0:23:2])
            T['E1_obs'] = Edrug2D_NDB((max(d1[d2!=0]),0),*popt3[0:23:2])
            T['E3_obs'] = Edrug2D_NDB((max(d1[d2!=0]),max(d2[d1!=0])),*popt3[0:23:2])
            
            T['E2_obs_std'] = err_Edrug2D_NDB((0,max(d2)),popt3)
            T['E1_obs_std'] = err_Edrug2D_NDB((max(d1),0),popt3)
            T['E3_obs_std'] = err_Edrug2D_NDB((max(d1[d2!=0]),max(d2[d1!=0])),popt3)
            
            #T['U'],T['A1'],T['A2'],T['A12'] = getU_lvl5(T['C1'],T['C2'],*popt3[0:23:2])
            T['VBS'],T['VBS_verr'] = dblquad(integrand_lvl5, min(np.log10(d1[d1!=0])), max(np.log10(d1)), lambda x: min(np.log10(d2[d2!=0])), lambda x: max(np.log10(d2)), args=(popt3[0:23:2]))
            T['DelSurf'] = sum(Edrug2D_NDB((d1,d2),T['E0'],T['E1'],T['E2'],min(T['E2'],T['E1']),T['r1'],T['r2'],10**T['log_C1'],10**T['log_C2'],T['h1'],T['h2'],1,1)-dip)
    
            if to_plot==1:
                DosePlots_PLY(d1,d2,dip,dip_sd,drug1_name,drug2_name,popt3[0:23:2],'NDB',target_cell_line,expt,metric_name,direc)     

        elif mod_lev >= 6: #It was best fit by model with cooperative synergy
            popt3 = [ T['E0'],
                      T['E0_std'],
                      T['E1'],
                      T['E1_std'],
                      T['E2'],
                      T['E2_std'],
                      T['E3'],
                      T['E3_std'],
                      T['r1'],
                      T['r1_std'],
                      T['r2'],
                      T['r2_std'],
                      10**T['log_C1'],
                      np.log(10)*T['log_C1_std']*10**T['log_C1'],
                      10**T['log_C2'],
                      np.log(10)*T['log_C2_std']*10**T['log_C2'],
                      T['h1'],
                      T['h1_std'],
                      T['h2'],
                      T['h2_std'],
                      10**T['log_alpha1'],
                      np.log(10)*T['log_alpha1_std']*10**T['log_alpha1'],
                      10**T['log_alpha2'],
                      np.log(10)*T['log_alpha2_std']*10**T['log_alpha2'],
                      10**T['log_gamma1'],
                      np.log(10)*T['log_gamma1_std']*10**T['log_gamma1'],
                      10**T['log_gamma2'],
                      np.log(10)*T['log_gamma2_std']*10**T['log_gamma2']]
            
            T['R2'] = 1-sum((dip - Edrug2D_NDB_hill((d1,d2),*popt3[0:27:2]))**2)/sum((dip-np.mean(dip))**2);

            def likelihood(parms):
                dip_fit =  Edrug2D_NDB_hill((d1,d2),parms[0],parms[1],parms[2],parms[3],parms[4],parms[5],parms[6],parms[7],parms[8],parms[9],parms[10],parms[11],parms[12],parms[13])
                log_likelihood =  sum(np.log(1/(dip_sd*np.sqrt(2*pi)))) - sum(((dip-dip_fit)/dip_sd)**2)/2
                if np.isnan(log_likelihood):
                    log_likelihood = -10e10
                return -log_likelihood,
                
            T['log_like_mcmc'] = likelihood(popt3[0:27:2])[0]
            
            T['E2_obs'] = Edrug2D_NDB_hill((0,max(d2[d1!=0])),*popt3[0:27:2])
            T['E1_obs'] = Edrug2D_NDB_hill((max(d1[d2!=0]),0),*popt3[0:27:2])
            T['E3_obs'] = Edrug2D_NDB_hill((max(d1[d2!=0]),max(d2[d1!=0])),*popt3[0:27:2])
            
            T['E2_obs_std'] = err_Edrug2D_NDB_hill((0,max(d2)),popt3)
            T['E1_obs_std'] = err_Edrug2D_NDB_hill((max(d1),0),popt3)
            T['E3_obs_std'] = err_Edrug2D_NDB_hill((max(d1[d2!=0]),max(d2[d1!=0])),popt3)
            
            #T['U'],T['A1'],T['A2'],T['A12'] = getU_lvl5(T['C1'],T['C2'],*popt3[0:23:2])
            #T['VBS'],T['VBS_verr'] = dblquad(integrand_lvl5, min(np.log10(d1[d1!=0])), max(np.log10(d1)), lambda x: min(np.log10(d2[d2!=0])), lambda x: max(np.log10(d2)), args=(popt3[0:23:2]))
            T['VBS']=np.nan;T['VBS_err']=np.nan
            T['DelSurf'] = sum(Edrug2D_NDB_hill((d1,d2),T['E0'],T['E1'],T['E2'],min(T['E2'],T['E1']),T['r1'],T['r2'],10**T['log_C1'],10**T['log_C2'],T['h1'],T['h2'],1,1,1,1)-dip)
    
            if to_plot==1:
                DosePlots_PLY(d1,d2,dip,dip_sd,drug1_name,drug2_name,popt3[0:27:2],'NDB_hill',target_cell_line,expt,metric_name,direc)     

        ###############################################################################################
        ########## Calculate the different flavors of beta              ###############################
        ###############################################################################################
        #Beta's definition depends on the orientation of the hill curve.  
        #If the minimum effect (ie lowest dip rate) is observed at the maximal dose 
        #hill_orient = 1
        if max_nest==7: #by definition no synergy of efficacy
            T['beta']                = 0;            T['beta_std']            = 0
            T['beta1']               = 0;            T['beta2']               = 0
            T['beta1_std']           = 0;            T['beta2_std']           = 0
            T['beta_obs']            = 0;            T['beta_obs_std']        = 0
            T['beta1_obs']           = 0;            T['beta2_obs']           = 0
            T['beta1_obs_std']       = 0;            T['beta2_obs_std']       = 0
        else:    
            if hill_orient==1:
                #Determine which was the index of the most efficacious single agent
                minE = ['E1','E2'][(T['E1'],T['E2']).index(min(T['E1'],T['E2']))]
                #Define formula for calculating the standard deviation in beta
                def beta_std(T,minE):
                    return np.sqrt(((T['E0']-T['E3'])/(T['E0']-T[minE])**2*T[minE + '_std'])**2 + (T['E3_std']/(T['E0']-T[minE + '_std']))**2 + ((T['E3']-T[minE])/(T['E0']-T[minE])**2*T['E0_std'])**2)            
                
                T['beta']                = (T[minE]-T['E3'])/(T['E0']-T[minE])
                T['beta_std']            = beta_std(T,minE)
                #Single beta's
                T['beta1']               = (T['E1']-T['E3'])/(T['E0']-T['E1'])
                T['beta2']               = (T['E2']-T['E3'])/(T['E0']-T['E2'])
                T['beta1_std']           = beta_std(T,'E1')
                T['beta2_std']           = beta_std(T,'E2')
                
                #Observed betas
                minE = ['E1_obs','E2_obs'][(T['E1_obs'],T['E2_obs']).index(min(T['E1_obs'],T['E2_obs']))]
                #Define formula for calculating the standard deviation in beta
                def beta_std(T,minE):
                    return np.sqrt(((T['E0']-T['E3_obs'])/(T['E0']-T[minE])**2*T[minE + '_std'])**2 + (T['E3_obs_std']/(T['E0']-T[minE + '_std']))**2 + ((T['E3_obs']-T[minE])/(T['E0']-T[minE])**2*T['E0_std'])**2)            
                
                T['beta_obs']            = (T[minE]-T['E3_obs'])/(T['E0']-T[minE])
                T['beta_obs_std']        = beta_std(T,minE)
                
                T['beta1_obs']           = (T['E1_obs']-T['E3_obs'])/(T['E0']-T['E1_obs'])
                T['beta2_obs']           = (T['E2_obs']-T['E3_obs'])/(T['E0']-T['E1_obs'])
                T['beta1_obs_std']       = beta_std(T,'E1_obs')
                T['beta2_obs_std']       = beta_std(T,'E2_obs')  
                
            else: #If the hill curve orientation is not 1
                #Determine which was the index of the most efficacious single agent
                maxE = ['E1','E2'][(T['E1'],T['E2']).index(min(T['E1'],T['E2']))]
                def beta_std(T,maxE):
                    return np.sqrt(((T['E3']-T['E0'])/(T[maxE]-T['E0'])**2*T[maxE + '_std'])**2 + (T['E3_std']/(T[maxE+'_std']-T['E0']))**2 + ((T[maxE]-T['E3'])/(T[maxE]-T['E0'])**2*T['E0_std'])**2)            
                
                T['beta']                = (T['E3']-T[maxE])/(T[maxE]-T['E0'])
                T['beta_std']            = beta_std(T,maxE)
                
                T['beta1']               = (T['E3']-T['E1'])/(T['E1']-T['E3'])
                T['beta2']               = (T['E3']-T['E2'])/(T['E2']-T['E3'])            
                T['beta1_std']           = beta_std(T,'E1')
                T['beta2_std']           = beta_std(T,'E2')
                
                
                maxE = ['E1_obs','E2_obs'][(T['E1_obs'],T['E2_obs']).index(min(T['E1_obs'],T['E2_obs']))]
                def beta_std(T,maxE):
                    return np.sqrt(((T['E3_obs']-T['E0'])/(T[maxE]-T['E0'])**2*T[maxE + '_std'])**2 + (T['E3_obs_std']/(T[maxE+'_std']-T['E0']))**2 + ((T[maxE]-T['E3_obs'])/(T[maxE]-T['E0'])**2*T['E0_std'])**2)            
    
                T['beta_obs']            = (T['E3_obs']-T[maxE])/(T[maxE]-T['E0'])
                T['beta_obs_std']        = beta_std(T,maxE)
                
                T['beta1_obs']           = (T['E3_obs']-T['E1_obs'])/(T['E1_obs']-T['E0'])
                T['beta2_obs']           = (T['E3_obs']-T['E2_obs'])/(T['E2_obs']-T['E0'])
                T['beta1_obs_std']       = beta_std(T,'E1_obs')
                T['beta2_obs_std']       = beta_std(T,'E2_obs')
                                        
    else:  #If did not converge update the dict to contain all the same fields just filled with nans
        print('NO TIER OF TESTED MODELS CONVERGED')
        T.update({'MCMC_converge':0,'MCMC_BURN':BURN,'MCMC_SAMPLE':SAMPLES,'model_level':np.nan})
        #Delete place holder values            
        for key in T.keys():
            if key.startswith('tmp_'):
                T.pop(key)
        key = ['R2','log_like_mcmc','E2_obs','E1_obs','E3_obs','E2_obs_std','E1_obs_std','E3_obs_std','VBS','VBS_verr','DelSurf',
              'beta','beta_std','beta_obs','beta_obs_std',
              'beta1','beta2','beta1_std','beta2_std',
              'beta1_obs','beta2_obs','beta1_obs_std','beta2_obs_std']
        for keys in key:
            T[keys]=np.nan

    ##########################################################################    
    #Finish up recording some details and close
    ##########################################################################    
    #Record the time...
    stop_time = time.time()
    T.update({'drug1_name':drug1_name,'drug2_name':drug2_name,
              'max_conc_d1':max(d1),'max_conc_d2':max(d2),
              'min_conc_d1':min(d1[d1!=0]),'min_conc_d2':min(d2[d2!=0]),
              'time': (stop_time-start_time)/60})
    if to_save:
        sav_fil.close()
    #How much memory did I use?  
    if os.name=='posix':
        T['memory_Mb'] = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss/1000
    else:
        T['memory_Mb'] = np.nan
    return T    
    
    
############################################################
#Function to plot the combination surface plots in plotly    
############################################################
def DosePlots_PLY(d1,d2,dip,dip_sd,drug1_name,drug2_name,popt3,which_model,target_cell_line,expt,metric_name,direc):
    #To plot doses on log space
    t_d1 = d1.copy()
    t_d2 = d2.copy()
    t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10
    t_d2[t_d2==0] = min(t_d2[t_d2!=0])/10

    trace1 = go.Scatter3d(x=np.log10(t_d1),y=np.log10(t_d2),z=dip,mode='markers',
                        marker=dict(size=3,color = dip,colorscale = 'coolwarm',
                                    line=dict(
                                              color='rgb(0,0,0)',width=1)),
                        )    
    #Plot the fit
    conc1 = 10**np.linspace(np.log10(min(t_d1)),np.log10(max(t_d1)), 30)
    conc2 = 10**np.linspace(np.log10(min(t_d2)), np.log10(max(t_d2)), 30)
    conc3 = np.array([(c1,c2) for c1 in conc1 for c2 in conc2])
    if which_model == 'DB':
        twoD_fit = np.array([Edrug2D_DB(d,*popt3) for d in conc3])
    elif which_model=='NDB':
        twoD_fit = np.array([Edrug2D_NDB(d,*popt3) for d in conc3])
    elif which_model=='NDB_hill':
        twoD_fit = np.array([Edrug2D_NDB_hill(d,*popt3) for d in conc3])
        
    zl = [min(twoD_fit),max(twoD_fit)]
    twoD_fit = np.resize(twoD_fit, (len(conc1),len(conc2)))
    
    [X,Y] = np.meshgrid(np.log10(conc1), np.log10(conc2))

    trace2 = go.Surface(x=X,y=Y,z=twoD_fit.transpose(),
                      colorscale = 'coolwarm',
                      opacity=.8,
                      contours  = dict( 
                                        y = dict(highlight=False,show=False,color='#444',width=1),
                                        x = dict(highlight=False,show=False,color='#444',width = 1 ),
                                        z = dict(highlight=True,show=True,highlightwidth=4,width=3,usecolormap=False)
                                        ))

                      
    
    layout = go.Layout(
                       title = target_cell_line,
                       scene = dict(
                                    xaxis=dict(    
                                               range = [np.log10(min(t_d1)),np.log10(max(t_d1))],
                                               title = 'log(' + drug1_name + ')',
                                               autorange=True,
                                               showgrid=True,
                                               zeroline=False,
                                               showline=False,
                                               ticks='',
                                               showticklabels=True,
                                               gridwidth = 5,
                                               gridcolor = 'k'
                                    ),
                                    yaxis=dict(    
                                               range = [np.log10(min(t_d1)),np.log10(max(t_d1))],
                                               title = 'log(' + drug2_name + ')',
                                               autorange=True,
                                               showgrid=True,
                                               zeroline=False,
                                               showline=False,
                                               ticks='',
                                               showticklabels=True,
                                               gridwidth = 5,
                                               gridcolor = 'k'
                                    ),
                                    zaxis=dict(
                                               range = zl,
                                               title = metric_name,
                                               autorange=True,
                                               tick0=np.min(twoD_fit),
                                               dtick=(np.max(twoD_fit)-np.min(twoD_fit))/5,
                                               showgrid=True,
                                               zeroline=True,
                                               zerolinewidth=5,
                                               showline=False,
                                               ticks='',
                                               showticklabels=True,
                                               gridwidth=5,
                                               gridcolor = 'k'
                                    )
                                    ),
                    margin=dict(
                                l=0,
                                r=0,
                                b=0,
                                t=35
                    ),
                    showlegend=False,
                    font=dict(family='Arial', size=18),
    )
    
    data = [trace1,trace2]

    for e,i in enumerate(dip_sd):
        x = np.log10(t_d1[e])*np.ones((2,))
        y = np.log10(t_d2[e])*np.ones((2,))
        z = np.array([dip[e]+i,dip[e]-i])
        trace = go.Scatter3d(
            x=x, y=y, z=z,
            mode = 'lines',
            line=dict(
                color='#1f77b4',
                width=2
            )
        )
        data.append(trace)
        
    fig = go.Figure(data=data,layout=layout)
    camera = dict(
                  up=dict(x=0,y=0,z=1),
                  center=dict(x=0,y=0,z=0),
                  eye = dict(x=1.25,y=-1.25,z=1.25))
    fig['layout'].update(scene=dict(camera=camera))
    s_idx = expt.rfind(os.sep)
    expt = expt[s_idx+1:-4]
    plot(fig,filename = direc + os.sep + '{}_{}_{}_{}_DoseResponseSurface.html'.format(datetime.datetime.now().strftime('%Y%m%d'),target_cell_line,drug1_name,drug2_name),auto_open=False)

##############################################################################
#Functions to run the initial 1D hill fits#
##############################################################################
def init_fit(T,d1,d2,dip,dip_sd,drug1_name,drug2_name,hill_orient=1):
    #Cannot have a zero standard deviation.  A result from Darren's code on occasion.  Just set to minimal std.
    dip_sd[dip_sd==0] = min(dip_sd[dip_sd!=0])  
    #Try and fit the single dose response curves....
    if sum(d1==0)==0 and sum(d2==0)==0:
        popt1 = np.zeros((4,))*np.NAN; perr1 = np.zeros((4,))*np.NAN
        popt2 = np.zeros((4,))*np.NAN; perr2 = np.zeros((4,))*np.NAN
    elif sum(d1==0)==0:
        print(drug1_name + ' has no zero condition...skipping single dose fit.')
        #Fit the single drug dose response curve
        popt1, p1 , pcov1 = fit_drc(d1[d2==0], dip[d2==0], dip_sd[d2==0],hill_orient=hill_orient)
        perr1 = np.sqrt(np.diag(pcov1)) if pcov1.any() else np.zeros((4,)) * np.NAN
        popt1 = popt1 if popt1.any() else np.zeros((4,)) * np.NAN
        popt2 = np.zeros((4,))*np.NAN; perr2 = np.zeros((4,))*np.NAN
    elif sum(d2==0)==0:
        print(drug2_name + ' has no zero condition...skipping single dose fit.')
        popt1 = np.zeros((4,))*np.NAN; perr1 = np.zeros((4,))*np.NAN
        popt2, p2 , pcov2 = fit_drc(d2[d1==0], dip[d1==0], dip_sd[d1==0],hill_orient=hill_orient)
        perr2 = np.sqrt(np.diag(pcov2)) if pcov2.any() else np.zeros((4,)) * np.NAN
        popt2 = popt2 if popt2.any() else np.zeros((4,)) * np.NAN
    else:
        popt1, p1 , pcov1 = fit_drc(d1[d2==0], dip[d2==0], dip_sd[d2==0],hill_orient=hill_orient)  
        popt2, p2 , pcov2 = fit_drc(d2[d1==0], dip[d1==0], dip_sd[d1==0],hill_orient=hill_orient)
        popt1 = popt1 if popt1.any() else np.zeros((4,)) * np.NAN
        popt2 = popt2 if popt2.any() else np.zeros((4,)) * np.NAN
        perr1 = np.sqrt(np.diag(pcov1)) if ~np.isnan(pcov1).any() else np.zeros((4,)) * np.NAN
        perr2 = np.sqrt(np.diag(pcov2)) if ~np.isnan(pcov1).any() else np.zeros((4,)) * np.NAN
    #If fit failed make approximations:
    if sum(np.isnan(popt1))>0 or p1>0.0001:
        print(drug1_name + ' single fit failed.  Will make approximations based on data')              
        popt1 = np.array([1,
                          np.mean(dip[np.where(d1-d2 == max(d1-d2))]),
                          np.mean(dip[np.where(d1+d2 == min(d1+d2))]),
                          np.log10(max(d1)/1000)])        
    else:
        print(drug1_name + ' single fit succeeded with p-val:' + '%.2e'%p1)              
        #Necessary to log the output of fit_drc
        popt1[3] = np.log10(popt1[3])
    if sum(np.isnan(popt2))>0 or p2>0.0001:
        print(drug2_name + ' single fit failed. Will make approximations based on data')
        popt2 = np.array([1,
                          np.mean(dip[np.where(d2-d1 == max(d2-d1))]),
                          np.mean(dip[np.where(d1+d2 == min(d1+d2))]),
                          np.log10(max(d2)/1000)])        
    else:
        print(drug2_name + ' single fit succeeded with p-val:' + '%.2e'%p2)              
        #Necessary to log the output of fit_drc
        popt2[3] = np.log10(popt2[3])
        
        
    #If fit failed make approximations:
    if np.isnan(perr1).any() or p1>0.0001:
        print(drug1_name + ' negative coraviariance.  Will make approximations for uncertainty based on data')                     
        perr1 = np.array([1,
                          np.mean(dip_sd[np.where(d1-d2 == max(d1-d2))]),
                          np.mean(dip_sd[np.where(d1+d2 == min(d1+d2))]),
                          1])
    if np.isnan(perr2).any() or p2>0.0001:
        print(drug2_name + ' negative coraviariance.  Will make approximations for uncertainty based on data')                            
        perr2 = np.array([1,
                          np.mean(dip_sd[np.where(d2-d1 == max(d2-d1))]),
                          np.mean(dip_sd[np.where(d1+d2 == min(d1+d2))]),
                          1])

    # initial guesses for parameters to be fit initially using the lm method in the curve fit function
    #based on the fits resulting from the single dose response curves
    E0      = 0.5*(popt1[2]+popt2[2]);           E0_std      = 0.5*(perr1[2]+perr2[2]);
    E1      = popt1[1];                          E1_std      = perr1[1];
    E2      = popt2[1];                          E2_std      = perr2[1];
    
    #Find dose farthest from the origion
    E3     = dip[np.argmax(np.sqrt(d1**2+d2**2))];                          
    E3_std = max(E1_std,E2_std);

        
    log_C1  = popt1[3];                          log_C1_std  = perr1[3]
    log_C2  = popt2[3];                          log_C2_std  = perr2[3]
    h1      = popt1[0];                          h1_std      = perr1[0]
    h2      = popt2[0];                          h2_std      = perr2[0]

    #For alpha parameter bounds set alpha upper equal to 10**2 X the max(d)/min(d)
    T['alpha_upper'] = 7
    T['alpha_lower'] = -7
    #For gamma bound between 2 and -2
    T['gamma_upper'] = 2
    T['gamma_lower'] = -2
    
    #Set the lowest EC50 available to fit to be 1000X less than the lowest concentration tested
    #This happens when drugs range is innappropriate
    T['log_c1_lower'] = np.log10(min(d1[d1!=0])/100)
    T['log_c2_lower'] = np.log10(min(d2[d2!=0])/100)
    T['log_c1_upper'] = np.log10(max(d1[d1!=0])*100)
    T['log_c2_upper'] = np.log10(max(d2[d2!=0])*100)
    ###########################################################################
    ###  Begin fitting dose response surfaces.
    ###  Fits are done in an MCMC walk with initial values esimtated from a PSO optimization
    ###  The 'best' model is selected by comparing DIC values between successive models
    ###  Successful burn times varied depending on the strongly on the quality of the data
    ###########################################################################

    #Set up pymc3 model
    #Each nest represents a different amount of approximations with increasing number of parameters for each nest.
    T.update({'E0':E0,'E1':E1,'E2':E2,'E3':E3,'log_C1':log_C1,'log_C2':log_C2,'h1':h1,'h2':h2,
              'E0_std':E0_std,'E1_std':E1_std,'E2_std':E2_std,'E3_std':E3_std,
              'log_C1_std':log_C1_std,'log_C2_std':log_C2_std,'h1_std':h1_std,'h2_std':h2_std,
              'log_alpha1':0,'log_alpha1_std':1,'log_alpha2':0,'log_alpha2_std':1,
              'r1':0,'r2':0,'r1_std':0,'r2_std':0,'log_gamma1':0,'log_gamma1_std':1,'log_gamma2':0,'log_gamma2_std':1})
    #Store all the parameters in temp variables which are updated for each converged fit.
    T.update({'tmp_E0':E0,'tmp_E1':E1,'tmp_E2':E2,'tmp_E3':E3,'tmp_log_C1':log_C1,'tmp_log_C2':log_C2,'tmp_h1':h1,'tmp_h2':h2,
              'tmp_E0_std':E0_std,'tmp_E1_std':E1_std,'tmp_E2_std':E2_std,'tmp_E3_std':E3_std,
              'tmp_log_C1_std':log_C1_std,'tmp_log_C2_std':log_C2_std,'tmp_h1_std':h1_std,'tmp_h2_std':h2_std,
              'tmp_log_alpha1':0,'tmp_log_alpha2':0,'tmp_log_alpha1_std':2,'tmp_log_alpha2_std':2,
              'tmp_log_gamma1':0,'tmp_log_gamma2':0,'tmp_log_gamma1_std':1,'tmp_log_gamma2_std':1})
    
    return T

#Fit for 2 parameter hill models
def init_fit_fix(T,d1,d2,dip,dip_sd,drug1_name,drug2_name,E0_fix,Emx_fix):
    #Cannot have a zero standard deviation.  A result from Darren's code on occasion.  Just set to minimal std.
    dip_sd[dip_sd==0] = min(dip_sd[dip_sd!=0])  
    #Try and fit the single dose response curves....
    if sum(d1==0)==0 and sum(d2==0)==0:
        popt1 = np.zeros((2,))*np.NAN; perr1 = np.zeros((2,))*np.NAN
        popt2 = np.zeros((2,))*np.NAN; perr2 = np.zeros((2,))*np.NAN
    elif sum(d1==0)==0:
        print(drug1_name + ' has no zero condition...skipping single dose fit.')
        #Fit the single drug dose response curve
        popt1, p1 , pcov1 = fit_drc_fix(d1[d2==0], dip[d2==0], dip_sd[d2==0],E0_fix=E0_fix,Emx_fix=Emx_fix)
        perr1 = np.sqrt(np.diag(pcov1)) if pcov1.any() else np.zeros((2,)) * np.NAN
        popt1 = popt1 if popt1.any() else np.zeros((2,)) * np.NAN
        popt2 = np.zeros((2,))*np.NAN; perr2 = np.zeros((2,))*np.NAN
    elif sum(d2==0)==0:
        print(drug2_name + ' has no zero condition...skipping single dose fit.')
        popt1 = np.zeros((2,))*np.NAN; perr1 = np.zeros((2,))*np.NAN
        popt2, p2 , pcov2 = fit_drc_fix(d2[d1==0], dip[d1==0], dip_sd[d1==0],E0_fix=E0_fix,Emx_fix=Emx_fix)
        perr2 = np.sqrt(np.diag(pcov2)) if pcov2.any() else np.zeros((2,)) * np.NAN
        popt2 = popt2 if popt2.any() else np.zeros((2,)) * np.NAN
    else:
        popt1, p1 , pcov1 = fit_drc_fix(d1[d2==0], dip[d2==0], dip_sd[d2==0],E0_fix=E0_fix,Emx_fix=Emx_fix)
        popt2, p2 , pcov2 = fit_drc_fix(d2[d1==0], dip[d1==0], dip_sd[d1==0],E0_fix=E0_fix,Emx_fix=Emx_fix)
        popt1 = popt1 if popt1.any() else np.zeros((2,)) * np.NAN
        popt2 = popt2 if popt2.any() else np.zeros((2,)) * np.NAN
        perr1 = np.sqrt(np.diag(pcov1)) if ~np.isnan(pcov1).any() else np.zeros((2,)) * np.NAN
        perr2 = np.sqrt(np.diag(pcov2)) if ~np.isnan(pcov1).any() else np.zeros((2,)) * np.NAN
    #If fit failed make approximations:
    if sum(np.isnan(popt1))>0 or p1>0.0001:
        print(drug1_name + ' single fit failed.  Will make approximations based on data')              
        popt1 = np.array([1,np.log10(max(d1))-np.log10(min(d1[d1!=0]))])        
    else:
        print(drug1_name + ' single fit succeeded with p-val:' + '%.2e'%p1)              
        #Necessary to log the output of fit_drc
        popt1[1] = np.log10(popt1[1])
    if sum(np.isnan(popt2))>0 or p2>0.0001:
        print(drug2_name + ' single fit failed. Will make approximations based on data')
        popt2 = np.array([1,np.log10(max(d2))-np.log10(min(d2[d2!=0]))])        
    else:
        print(drug2_name + ' single fit succeeded with p-val:' + '%.2e'%p2)              
        #Necessary to log the output of fit_drc
        popt2[1] = np.log10(popt2[1])
    
    #If fit failed make approximations:
    if np.isnan(perr1).any() or p1>0.0001:
        print(drug1_name + ' negative coraviariance.  Will make approximations for uncertainty based on data')                     
        perr1 = np.array([1,1])
    if np.isnan(perr2).any() or p2>0.0001:
        print(drug2_name + ' negative coraviariance.  Will make approximations for uncertainty based on data')                            
        perr2 = np.array([1,1])

    # initial guesses for parameters to be fit initially using the lm method in the curve fit function
    #based on the fits resulting from the single dose response curves
    E0      = E0_fix;                            E0_std      = 0;
    E1      = Emx_fix;                           E1_std      = 0;
    E2      = Emx_fix;                           E2_std      = 0;
    
    #Find dose farthest from the origion
    E3     = Emx_fix;                          
    E3_std = 0;

    log_C1  = popt1[1];                          log_C1_std  = perr1[1]
    log_C2  = popt2[1];                          log_C2_std  = perr2[1]
    h1      = popt1[0];                          h1_std      = perr1[0]
    h2      = popt2[0];                          h2_std      = perr2[0]

    #For alpha parameter bounds set alpha upper equal to 10**2 X the max(d)/min(d)
    T['alpha_upper'] = 7
    T['alpha_lower'] = -7
    #For gamma bound between 2 and -2
    T['gamma_upper'] = 2
    T['gamma_lower'] = -2
    
    #Set the lowest EC50 available to fit to be 1000X less than the lowest concentration tested
    #This happens when drugs range is innappropriate
    T['log_c1_lower'] = np.log10(min(d1[d1!=0])/100)
    T['log_c2_lower'] = np.log10(min(d2[d2!=0])/100)
    T['log_c1_upper'] = np.log10(max(d1[d1!=0])*100)
    T['log_c2_upper'] = np.log10(max(d2[d2!=0])*100)
    ###########################################################################
    ###  Begin fitting dose response surfaces.
    ###  Fits are done in an MCMC walk with initial values esimtated from a PSO optimization
    ###  The 'best' model is selected by comparing DIC values between successive models
    ###  Successful burn times varied depending on the strongly on the quality of the data
    ###########################################################################
    #Set up pymc3 model
    #Each nest represents a different amount of approximations with increasing number of parameters for each nest.
    T.update({'E0':E0,'E1':E1,'E2':E2,'E3':E3,'log_C1':log_C1,'log_C2':log_C2,'h1':h1,'h2':h2,
              'E0_std':E0_std,'E1_std':E1_std,'E2_std':E2_std,'E3_std':E3_std,
              'log_C1_std':log_C1_std,'log_C2_std':log_C2_std,'h1_std':h1_std,'h2_std':h2_std,
              'log_alpha1':0,'log_alpha1_std':1,'log_alpha2':0,'log_alpha2_std':1,
              'r1':0,'r2':0,'r1_std':0,'r2_std':0,'log_gamma1':0,'log_gamma1_std':1,'log_gamma2':0,'log_gamma2_std':1})
    #Store all the parameters in temp variables which are updated for each converged fit.
    T.update({'tmp_E0':E0,'tmp_E1':E1,'tmp_E2':E2,'tmp_E3':E3,'tmp_log_C1':log_C1,'tmp_log_C2':log_C2,'tmp_h1':h1,'tmp_h2':h2,
              'tmp_E0_std':E0_std,'tmp_E1_std':E1_std,'tmp_E2_std':E2_std,'tmp_E3_std':E3_std,
              'tmp_log_C1_std':log_C1_std,'tmp_log_C2_std':log_C2_std,'tmp_h1_std':h1_std,'tmp_h2_std':h2_std,
              'tmp_log_alpha1':0,'tmp_log_alpha2':0,'tmp_log_alpha1_std':2,'tmp_log_alpha2_std':2,
              'tmp_log_gamma1':0,'tmp_log_gamma2':0,'tmp_log_gamma1_std':1,'tmp_log_gamma2_std':1})
    
    return T
    
##############################################################################
#Functions from Alex's pyDRC library used for initial fits to the 1D hill eqn#
##############################################################################
def ll4(x, b, c, d, e):
    """
    Four parameter log-logistic function ("Hill curve")

     - b: Hill slope
     - c: min response
     - d: max response
     - e: EC50
     """
    return c+(d-c)/(1+np.exp(b*(np.log(x)-np.log(e))))

# Fitting function
def fit_drc(doses, dip_rates, dip_std_errs=None, hill_fn=ll4,
            null_rejection_threshold=0.05,hill_orient=1):
    dip_rate_nans = np.isnan(dip_rates)
    if np.any(dip_rate_nans):
        doses = doses[~dip_rate_nans]
        dip_rates = dip_rates[~dip_rate_nans]
        if dip_std_errs is not None:
            dip_std_errs = dip_std_errs[~dip_rate_nans]
    popt = np.zeros((4,))*np.nan
    pcov = np.zeros((4,))*np.nan
    curve_initial_guess = list(ll4_initials(doses, dip_rates))
    if hill_orient==1:
        if curve_initial_guess[0]<0: #If the hill slope is less than 0 set equal to 1
            curve_initial_guess[0] = 1
    elif hill_orient==0:
        if curve_initial_guess[0]<0: #If the hill slope is less than 0 set equal to 1
            curve_initial_guess[0] = abs(curve_initial_guess[0])
            tmp = curve_initial_guess[1]
            curve_initial_guess[1]=curve_initial_guess[2]
            curve_initial_guess[2]=tmp
            
    if curve_initial_guess[3]>max(doses): #If the EC50 is greater than the maximum dose
        curve_initial_guess[3]=max(doses)
    if curve_initial_guess[3]<min(doses): #If the EC50 is less than the minimum dose
        curve_initial_guess[3]=min(doses[doses!=0])            
    try:
        popt, pcov = scipy.optimize.curve_fit(hill_fn,
                                              doses,
                                              dip_rates,
                                              p0=curve_initial_guess,
                                              sigma=dip_std_errs,
                                              maxfev=100000
                                              )

    except RuntimeError:
        pass

    p=1 #pvalue
    if sum(np.isnan(popt))!=4:
        # DIP rate fit
        dip_rate_fit_curve = hill_fn(doses, *popt)
        # F test vs flat linear "no effect" fit
        ssq_model = ((dip_rate_fit_curve - dip_rates) ** 2).sum()
        ssq_null = ((np.mean(dip_rates) - dip_rates) ** 2).sum()
        df = len(doses) - 4
        f_ratio = (ssq_null-ssq_model)/(ssq_model/df)
        p = 1 - scipy.stats.f.cdf(f_ratio, 1, df)
        if p > null_rejection_threshold:
            popt = np.zeros((4,))*np.nan
            pcov = np.zeros((4,))*np.nan
    return popt, p, pcov


# Fitting function for fixed min/max effect
def fit_drc_fix(doses, dip_rates, dip_std_errs=None, hill_fn=ll4,
            null_rejection_threshold=0.05,E0_fix=1.,Emx_fix=0.):
    dip_rate_nans = np.isnan(dip_rates)
    if np.any(dip_rate_nans):
        doses = doses[~dip_rate_nans]
        dip_rates = dip_rates[~dip_rate_nans]
        if dip_std_errs is not None:
            dip_std_errs = dip_std_errs[~dip_rate_nans]
    popt = np.zeros((2,))*np.nan
    pcov = np.zeros((2,))*np.nan
    curve_initial_guess = list(ll2_initials(doses, dip_rates,E0_fix,Emx_fix))
    if curve_initial_guess[0]<0:        
        if E0_fix>Emx_fix:
                curve_initial_guess[0] = 1
        else:
                curve_initial_guess[0] = abs(curve_initial_guess[0])
    if curve_initial_guess[1]>max(doses): #If the EC50 is greater than the maximum dose
        curve_initial_guess[1]=max(doses)
    if curve_initial_guess[1]<min(doses): #If the EC50 is less than the minimum dose
        curve_initial_guess[1]=min(doses[doses!=0])            
    hill_fn = lambda x,b,e: ll4(x, b, E0_fix, Emx_fix, e)
    try:
        popt, pcov = scipy.optimize.curve_fit(hill_fn,
                                              doses,
                                              dip_rates,
                                              p0=curve_initial_guess,
                                              sigma=dip_std_errs,
                                              maxfev=100000
                                              )

    except RuntimeError:
        pass

    p=1 #pvalue
    if sum(np.isnan(popt))!=2:
        # DIP rate fit
        dip_rate_fit_curve = hill_fn(doses, *popt)
        # F test vs flat linear "no effect" fit
        ssq_model = ((dip_rate_fit_curve - dip_rates) ** 2).sum()
        ssq_null = ((np.mean(dip_rates) - dip_rates) ** 2).sum()
        df = len(doses) - 2
        f_ratio = (ssq_null-ssq_model)/(ssq_model/df)
        p = 1 - scipy.stats.f.cdf(f_ratio, 1, df)
        if p > null_rejection_threshold:
            popt = np.zeros((2,))*np.nan
            pcov = np.zeros((2,))*np.nan
    return popt, p, pcov

# Functions for finding initial parameter estimates for curve fitting
def ll4_initials(x, y):
    c_val, d_val = _find_cd_ll4(y)
    b_val, e_val = _find_be_ll4(x, y, c_val, d_val)
    return b_val, c_val, d_val, e_val
def ll2_initials(x, y,E0_fix,Emx_fix):
    b_val, e_val = _find_be_ll4(x, y, E0_fix, Emx_fix)
    return b_val, e_val

def _response_transform(y, c_val, d_val):
    return np.log((d_val - y) / (y - c_val))


def _find_be_ll4(x, y, c_val, d_val, slope_scaling_factor=1,
                 dose_transform=np.log,
                 dose_inv_transform=np.exp):
    x[x==0]=min(x[x!=0])/100 #deal with zero dose
    y = _response_transform(y,c_val,d_val)
    x = dose_transform(x[(~np.isnan(y)) & (~np.isinf(y))])
    y = y[(~np.isnan(y)) & (~np.isinf(y))]
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x,y)
    b_val = slope_scaling_factor * slope
    e_val = dose_inv_transform(-intercept / (slope_scaling_factor * b_val))
    if np.isnan(e_val):
        e_val = min(x)*100
    if np.isnan(b_val):
        b_val = 1
    return b_val, e_val


def _find_cd_ll4(y, scale=0.001):
    ymin = np.min(y)
    ymax = np.max(y)
    len_y_range = scale * (ymax - ymin)

    return ymin - len_y_range, ymax + len_y_range

##############################################################################
#2D hill curve functions and class of MCMC model tiers to fit
##############################################################################
##Fitting functions   
# 1D Hill function for fitting
def Edrug1D(d,E0,Em,C,h):
    return Em + (E0-Em) / (1 + (d/C)**h)

# 2D Hill function for fitting
def Edrug2D_DB(d,E0,E1,E2,E3,C1,C2,h1,h2,alpha):
    d1 = d[0]
    d2 = d[1]
    Ed = ( C1**h1*C2**h2*E0 + d1**h1*C2**h2*E1 + C1**h1*d2**h2*E2 + alpha**h2*d1**h1*d2**h2*E3 ) / \
                ( C1**h1*C2**h2 + d1**h1*C2**h2 + C1**h1*d2**h2 + alpha**h2*d1**h1*d2**h2 )
    return Ed

#For propogating error in the Detail balance model
def err_Edrug2D_DB(d,x):
    d1      = d[0]
    d2      = d[1]
    E0      = ufloat(x[0],x[1])
    E1      = ufloat(x[2],x[3])
    E2      = ufloat(x[4],x[5])
    E3      = ufloat(x[6],x[7])
    C1      = ufloat(x[8],x[9])
    C2      = ufloat(x[10],x[11])
    h1      = ufloat(x[12],x[13])
    h2      = ufloat(x[14],x[15])
    alpha   = ufloat(x[16],x[17])
    try:
        if d1==0 and d2!=0:
            Ed = Edrug1D(d2,E0,E2,C2,h2)
        elif d1!=0 and d2==0:
            Ed = Edrug1D(d1,E0,E1,C1,h1) 
        elif d1==0 and d2==0:
            Ed = E0
        else:            
            Ed = ( C1**h1*C2**h2*E0 + d1**h1*C2**h2*E1 + C1**h1*d2**h2*E2 + alpha**h2*d1**h1*d2**h2*E3 ) / \
                        ( C1**h1*C2**h2 + d1**h1*C2**h2 + C1**h1*d2**h2 + alpha**h2*d1**h1*d2**h2 )
        return Ed.std_dev
    except:
        return np.nan
    
#2D dose response surface function not assuming detail balance but assuming proliferation 
# is << than the rate of transition between the different states.
def Edrug2D_NDB(d,E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2):
    d1 = d[0]
    d2 = d[1]
    Ed = (E3*r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
          E3*r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
          C2**h2*E0*r1*(C1*alpha2*d1)**h1 + 
          C1**h1*E0*r2*(C2*alpha1*d2)**h2 + 
          C2**h2*E1*r1*(alpha2*d1**2)**h1 + 
          C1**h1*E2*r2*(alpha1*d2**2)**h2 + 
          E3*d2**h2*r1*(C1*alpha2*d1)**h1 + 
          E3*d1**h1*r2*(C2*alpha1*d2)**h2 + 
          C1**(2*h1)*C2**h2*E0*r1 + 
          C1**h1*C2**(2*h2)*E0*r2 + 
          C1**(2*h1)*E2*d2**h2*r1 + 
          C2**(2*h2)*E1*d1**h1*r2 + 
          E1*r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
          E2*r1*(C1*d1)**h1*(alpha1*d2)**h2 +
          C2**h2*E1*r1*(C1*d1)**h1 +
          C1**h1*E2*r2*(C2*d2)**h2)/  \
         (r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
          r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
          C2**h2*r1*(C1*alpha2*d1)**h1 +
          C1**h1*r2*(C2*alpha1*d2)**h2 + 
          C2**h2*r1*(alpha2*d1**2)**h1 + 
          C1**h1*r2*(alpha1*d2**2)**h2 + 
          d2**h2*r1*(C1*alpha2*d1)**h1 + 
          d1**h1*r2*(C2*alpha1*d2)**h2 + 
          C1**(2*h1)*C2**h2*r1 + 
          C1**h1*C2**(2*h2)*r2 + 
          C1**(2*h1)*d2**h2*r1 + 
          C2**(2*h2)*d1**h1*r2 + 
          r1*(C1*d1)**h1*(alpha1*d2)**h2 + 
          r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
          C2**h2*r1*(C1*d1)**h1 + 
          C1**h1*r2*(C2*d2)**h2)
         
    return Ed

#For propogating error in the non-detail balance case
def err_Edrug2D_NDB(d,x):
    d1      = d[0]
    d2      = d[1]
    E0      = ufloat(x[0],x[1])
    E1      = ufloat(x[2],x[3])
    E2      = ufloat(x[4],x[5])
    E3      = ufloat(x[6],x[7])
    r1      = ufloat(x[8],x[9])
    r2      = ufloat(x[10],x[11])
    C1      = ufloat(x[12],x[13])
    C2      = ufloat(x[14],x[15])
    h1      = ufloat(x[16],x[17])
    h2      = ufloat(x[18],x[19])
    alpha1  = ufloat(x[20],x[21])
    alpha2  = ufloat(x[22],x[23])
    try:
        if d1==0 and d2!=0:
            Ed = Edrug1D(d2,E0,E2,C2,h2)
                          
        elif d1!=0 and d2==0:
            Ed = Edrug1D(d1,E0,E1,C1,h1)
            
        elif d1==0 and d2==0:
            Ed = E0
        else:
            Ed = (E3*r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
                  E3*r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
                  C2**h2*E0*r1*(C1*alpha2*d1)**h1 + 
                  C1**h1*E0*r2*(C2*alpha1*d2)**h2 + 
                  C2**h2*E1*r1*(alpha2*d1**2)**h1 + 
                  C1**h1*E2*r2*(alpha1*d2**2)**h2 + 
                  E3*d2**h2*r1*(C1*alpha2*d1)**h1 + 
                  E3*d1**h1*r2*(C2*alpha1*d2)**h2 + 
                  C1**(2*h1)*C2**h2*E0*r1 + 
                  C1**h1*C2**(2*h2)*E0*r2 + 
                  C1**(2*h1)*E2*d2**h2*r1 + 
                  C2**(2*h2)*E1*d1**h1*r2 + 
                  E1*r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
                  E2*r1*(C1*d1)**h1*(alpha1*d2)**h2 +
                  C2**h2*E1*r1*(C1*d1)**h1 +
                  C1**h1*E2*r2*(C2*d2)**h2)/  \
                 (r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
                  r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
                  C2**h2*r1*(C1*alpha2*d1)**h1 +
                  C1**h1*r2*(C2*alpha1*d2)**h2 + 
                  C2**h2*r1*(alpha2*d1**2)**h1 + 
                  C1**h1*r2*(alpha1*d2**2)**h2 + 
                  d2**h2*r1*(C1*alpha2*d1)**h1 + 
                  d1**h1*r2*(C2*alpha1*d2)**h2 + 
                  C1**(2*h1)*C2**h2*r1 + 
                  C1**h1*C2**(2*h2)*r2 + 
                  C1**(2*h1)*d2**h2*r1 + 
                  C2**(2*h2)*d1**h1*r2 + 
                  r1*(C1*d1)**h1*(alpha1*d2)**h2 + 
                  r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
                  C2**h2*r1*(C1*d1)**h1 + 
                  C1**h1*r2*(C2*d2)**h2)
                 
        return Ed.std_dev
    except:
        return np.nan
   

def Edrug2D_NDB_hill(d,E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2):
    d1 = d[0]
    d2 = d[1]
    Ed =    (C1**(2*h1)*C2**h2*E0*r1 + 
             C1**h1*C2**(2*h2)*E0*r2 + 
             E2*r1*(C1*d1)**h1*(alpha1*d2)**(gamma1*h2) + 
             E1*r2*(C2*d2)**h2*(alpha2*d1)**(gamma2*h1) + 
             C1**(2*h1)*E2*d2**h2*r1 + 
             C2**(2*h2)*E1*d1**h1*r2 + 
             C2**h2*E1*r1*(C1*d1)**h1 + 
             C1**h1*E2*r2*(C2*d2)**h2 + 
             E3*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + 
             E3*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1) +
             C1**h1*E2*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + 
             C2**h2*E1*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1 + 
             C1**h1*C2**h2*E0*r1*(alpha2*d1)**(gamma2*h1) + 
             C1**h1*C2**h2*E0*r2*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*E3*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + 
             C2**h2*E3*d1**h1*r2*(alpha1*d2)**(gamma1*h2))/ \
            (C1**(2*h1)*C2**h2*r1 + 
             C1**h1*C2**(2*h2)*r2 + 
             r1*(C1*d1)**h1*(alpha1*d2)**(gamma1*h2) + 
             r2*(C2*d2)**h2*(alpha2*d1)**(gamma2*h1) + 
             C1**(2*h1)*d2**h2*r1 + 
             C2**(2*h2)*d1**h1*r2 + 
             C2**h2*r1*(C1*d1)**h1 + 
             C1**h1*r2*(C2*d2)**h2 + 
             C1**h1*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + 
             C2**h2*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1 + 
             C1**h1*C2**h2*r1*(alpha2*d1)**(gamma2*h1) + 
             C1**h1*C2**h2*r2*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + 
             C2**h2*d1**h1*r2*(alpha1*d2)**(gamma1*h2) + 
             alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + 
             alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1))
    return Ed

#For propogating error in the non-detail balance case
def err_Edrug2D_NDB_hill(d,x):
    d1      = d[0]
    d2      = d[1]
    E0      = ufloat(x[0],x[1])
    E1      = ufloat(x[2],x[3])
    E2      = ufloat(x[4],x[5])
    E3      = ufloat(x[6],x[7])
    r1      = ufloat(x[8],x[9])
    r2      = ufloat(x[10],x[11])
    C1      = ufloat(x[12],x[13])
    C2      = ufloat(x[14],x[15])
    h1      = ufloat(x[16],x[17])
    h2      = ufloat(x[18],x[19])
    alpha1  = ufloat(x[20],x[21])
    alpha2  = ufloat(x[22],x[23])
    gamma1  = ufloat(x[24],x[25])
    gamma2  = ufloat(x[26],x[27])    
    try:
        if d1==0 and d2!=0:
            Ed = Edrug1D(d2,E0,E2,C2,h2)
            
        elif d1!=0 and d2==0:
            Ed = Edrug1D(d1,E0,E1,C1,h1)
      
        elif d1==0 and d2==0:
            Ed = E0
            
        else:
            Ed =    (C1**(2*h1)*C2**h2*E0*r1 + 
                     C1**h1*C2**(2*h2)*E0*r2 + 
                     E2*r1*(C1*d1)**h1*(alpha1*d2)**(gamma1*h2) + 
                     E1*r2*(C2*d2)**h2*(alpha2*d1)**(gamma2*h1) + 
                     C1**(2*h1)*E2*d2**h2*r1 + 
                     C2**(2*h2)*E1*d1**h1*r2 + 
                     C2**h2*E1*r1*(C1*d1)**h1 + 
                     C1**h1*E2*r2*(C2*d2)**h2 + 
                     E3*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + 
                     E3*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1) +
                     C1**h1*E2*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + 
                     C2**h2*E1*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1 + 
                     C1**h1*C2**h2*E0*r1*(alpha2*d1)**(gamma2*h1) + 
                     C1**h1*C2**h2*E0*r2*(alpha1*d2)**(gamma1*h2) + 
                     C1**h1*E3*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + 
                     C2**h2*E3*d1**h1*r2*(alpha1*d2)**(gamma1*h2))/ \
                    (C1**(2*h1)*C2**h2*r1 + 
                     C1**h1*C2**(2*h2)*r2 + 
                     r1*(C1*d1)**h1*(alpha1*d2)**(gamma1*h2) + 
                     r2*(C2*d2)**h2*(alpha2*d1)**(gamma2*h1) + 
                     C1**(2*h1)*d2**h2*r1 + 
                     C2**(2*h2)*d1**h1*r2 + 
                     C2**h2*r1*(C1*d1)**h1 + 
                     C1**h1*r2*(C2*d2)**h2 + 
                     C1**h1*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + 
                     C2**h2*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1 + 
                     C1**h1*C2**h2*r1*(alpha2*d1)**(gamma2*h1) + 
                     C1**h1*C2**h2*r2*(alpha1*d2)**(gamma1*h2) + 
                     C1**h1*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + 
                     C2**h2*d1**h1*r2*(alpha1*d2)**(gamma1*h2) + 
                     alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + 
                     alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1))
                 
        return Ed.std_dev
    except:
        return np.nan
   
    
###################################
##Now for models with different resolution titled nest(0-6)
#As nests increases fewer approximations are made and therefore more parameters are fit
#This class is essentially a group of functions and as none need refer to the other, I don't use self.
class NestedModels():
    #Lowest granulatity nest.  Only fitting for E3 and alpha.  
    #Approximations are:
        #E0 is the minimally observed effect
        #E1, E2 are assumed to be the maximally observed effect
        #h1,h2 are from single drug fits or 1 other wise
        #C1,C2 are from single drug fits or the median concentration other-wise
    def nest0(syn_Model,E0,E1,E2,E3,log_C1,log_C2,h1,h2,E0_std,E1_std,E2_std,E3_std,log_C1_std,log_C2_std,h1_std,h2_std,log_alpha1,log_alpha2,log_alpha1_std,log_alpha2_std,log_gamma1,log_gamma2,log_gamma1_std,log_gamma2_std,alpha_lower,alpha_upper,gamma_lower,gamma_upper,log_c1_lower,log_c2_lower,d1,d2,dip,dip_sd):
        #Alpha will be sampled in log scale
        with syn_Model:
            # Prior distributions based on initial fits
            E3_prior = pm.Normal('E3', mu=E3, sd=E3_std,testval=E3)
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_alpha1_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha1', mu=log_alpha1, sd=log_alpha1_std, testval=log_alpha1)
            #Expected distribution
            mu = Edrug2D_DB((d1,d2),E0,E1,E2,E3_prior,10**log_C1,10**log_C2,h1,h2,10**log_alpha1_prior)
            #Likelihood (sampling distribution) of observations.
            dip_obs = pm.Normal('dip_obs',mu=mu, sd = dip_sd, observed = dip)
        return syn_Model
        
    #Now fit for the maximal effect of each drug alone.  
    #Approximations are:
        #E0 is the minimally observed effect
        #h1,h2 are from single drug fits or 1 other wise
        #C1,C2 are from single drug fits or the median concentration other-wise
    def nest1(syn_Model,E0,E1,E2,E3,log_C1,log_C2,h1,h2,E0_std,E1_std,E2_std,E3_std,log_C1_std,log_C2_std,h1_std,h2_std,log_alpha1,log_alpha2,log_alpha1_std,log_alpha2_std,log_gamma1,log_gamma2,log_gamma1_std,log_gamma2_std,alpha_lower,alpha_upper,gamma_lower,gamma_upper,log_c1_lower,log_c2_lower,d1,d2,dip,dip_sd):
        #Alpha will be sampled in log scale
        with syn_Model:
            # Prior distributions based on initial fits
            E1_prior = pm.Normal('E1', mu=E1, sd=E1_std, testval=E1)
            E2_prior = pm.Normal('E2', mu=E2, sd=E2_std, testval=E2)
            E3_prior = pm.Normal('E3', mu=E3, sd=E3_std, testval=E3)
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_alpha1_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha1', mu=log_alpha1, sd=log_alpha1_std, testval=log_alpha1)
            #Expected distribution
            mu = Edrug2D_DB((d1,d2),E0,E1_prior,E2_prior,E3_prior,10**log_C1,10**log_C2,h1,h2,10**log_alpha1_prior)
            #Likelihood (sampling distribution) of observations.
            dip_obs = pm.Normal('dip_obs',mu=mu,sd = dip_sd, observed = dip)
        return syn_Model
    
    #Now fit for the EC50 values.  
    #Approximations are:
        #E0 is the minimally observed effect
        #h1,h2 are from single drug fits or 1 other wise
    def nest2(syn_Model,E0,E1,E2,E3,log_C1,log_C2,h1,h2,E0_std,E1_std,E2_std,E3_std,log_C1_std,log_C2_std,h1_std,h2_std,log_alpha1,log_alpha2,log_alpha1_std,log_alpha2_std,log_gamma1,log_gamma2,log_gamma1_std,log_gamma2_std,alpha_lower,alpha_upper,gamma_lower,gamma_upper,log_c1_lower,log_c2_lower,d1,d2,dip,dip_sd):
        #Alpha will be sampled in log scale
        with syn_Model:
            # Prior distributions based on initial fits
            E1_prior = pm.Normal('E1', mu=E1, sd=E1_std, testval=E1)
            E2_prior = pm.Normal('E2', mu=E2, sd=E2_std, testval=E2)
            E3_prior = pm.Normal('E3', mu=E3, sd=E3_std, testval=E3)
            log_C1_prior = pm.Bound(pm.Normal, lower= log_c1_lower)('log_C1', mu=log_C1, sd=1, testval=log_C1)
            log_C2_prior = pm.Bound(pm.Normal, lower= log_c2_lower)('log_C2', mu=log_C2, sd=1, testval=log_C2)
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_alpha1_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha1', mu=log_alpha1, sd=log_alpha1_std, testval=log_alpha1)
            #Expected distribution
            mu = Edrug2D_DB((d1,d2),E0,E1_prior,E2_prior,E3_prior,10**log_C1_prior,10**log_C2_prior,h1,h2,10**log_alpha1_prior)
            #Likelihood (sampling distribution) of observations.
            dip_obs = pm.Normal('dip_obs',mu=mu,sd = dip_sd, observed = dip)
        return syn_Model
    
    #Now fit for the hill values.  
    #Approximations are:
        #E0 is the minimally observed effect
    def nest3(syn_Model,E0,E1,E2,E3,log_C1,log_C2,h1,h2,E0_std,E1_std,E2_std,E3_std,log_C1_std,log_C2_std,h1_std,h2_std,log_alpha1,log_alpha2,log_alpha1_std,log_alpha2_std,log_gamma1,log_gamma2,log_gamma1_std,log_gamma2_std,alpha_lower,alpha_upper,gamma_lower,gamma_upper,log_c1_lower,log_c2_lower,d1,d2,dip,dip_sd):
        #Alpha will be sampled in log scale
        with syn_Model:
            # Prior distributions based on initial fits
            E1_prior = pm.Normal('E1', mu=E1, sd=E1_std, testval=E1)
            E2_prior = pm.Normal('E2', mu=E2, sd=E2_std, testval=E2)
            E3_prior = pm.Normal('E3', mu=E3, sd=E3_std, testval=E3)
            log_C1_prior = pm.Bound(pm.Normal, lower= log_c1_lower)('log_C1', mu=log_C1, sd=1, testval=log_C1)
            log_C2_prior = pm.Bound(pm.Normal, lower= log_c2_lower)('log_C2', mu=log_C2, sd=1, testval=log_C2)
            h1_prior = pm.Bound(pm.Normal, lower=0)('h1', mu=h1, sd=h1_std, testval=h1)
            h2_prior = pm.Bound(pm.Normal, lower=0)('h2', mu=h2, sd=h2_std, testval=h2)
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_alpha1_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha1', mu=log_alpha1, sd=log_alpha1_std, testval=log_alpha1)
            #Expected distribution
            mu = Edrug2D_DB((d1,d2),E0,E1_prior,E2_prior,E3_prior,10**log_C1_prior,10**log_C2_prior,h1_prior,h2_prior,10**log_alpha1_prior)
            #Likelihood (sampling distribution) of observations.
            dip_obs = pm.Normal('dip_obs',mu=mu,sd = dip_sd, observed = dip)
        return syn_Model
    
    #Now full model fit assuming detail balance
    #Approximations are:
        #Sytem obeys detail balance
    def nest4(syn_Model,E0,E1,E2,E3,log_C1,log_C2,h1,h2,E0_std,E1_std,E2_std,E3_std,log_C1_std,log_C2_std,h1_std,h2_std,log_alpha1,log_alpha2,log_alpha1_std,log_alpha2_std,log_gamma1,log_gamma2,log_gamma1_std,log_gamma2_std,alpha_lower,alpha_upper,gamma_lower,gamma_upper,log_c1_lower,log_c2_lower,d1,d2,dip,dip_sd):
        #Alpha will be sampled in log scale
        with syn_Model:
            # Prior distributions based on initial fits
            E0_prior = pm.Normal('E0', mu=E0, sd=E0_std, testval=E0)
            E1_prior = pm.Normal('E1', mu=E1, sd=E1_std, testval=E1)
            E2_prior = pm.Normal('E2', mu=E2, sd=E2_std, testval=E2)
            E3_prior = pm.Normal('E3', mu=E3, sd=E3_std, testval=E3)
            log_C1_prior = pm.Bound(pm.Normal, lower= log_c1_lower)('log_C1', mu=log_C1, sd=1, testval=log_C1)
            log_C2_prior = pm.Bound(pm.Normal, lower= log_c2_lower)('log_C2', mu=log_C2, sd=1, testval=log_C2)
            h1_prior = pm.Bound(pm.Normal, lower=0)('h1', mu=h1, sd=h1_std, testval=h1)
            h2_prior = pm.Bound(pm.Normal, lower=0)('h2', mu=h2, sd=h2_std, testval=h2)
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_alpha1_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha1', mu=log_alpha1, sd=log_alpha1_std, testval=log_alpha1)
            #Expected distribution
            mu = Edrug2D_DB((d1,d2),E0_prior,E1_prior,E2_prior,E3_prior,10**log_C1_prior,10**log_C2_prior,h1_prior,h2_prior,10**log_alpha1_prior)
            #Likelihood (sampling distribution) of observations.
            dip_obs = pm.Normal('dip_obs',mu=mu,sd = dip_sd, observed = dip)
        return syn_Model
    
    #Now full fit without assuming detail balance
    #Approximations are:
        #rate of transition r1 and r2 is >> than E0 
    def nest5(syn_Model,E0,E1,E2,E3,log_C1,log_C2,h1,h2,E0_std,E1_std,E2_std,E3_std,log_C1_std,log_C2_std,h1_std,h2_std,log_alpha1,log_alpha2,log_alpha1_std,log_alpha2_std,log_gamma1,log_gamma2,log_gamma1_std,log_gamma2_std,alpha_lower,alpha_upper,gamma_lower,gamma_upper,log_c1_lower,log_c2_lower,d1,d2,dip,dip_sd):        
        with syn_Model:
            # Prior distributions based on initial fits
            E0_prior = pm.Normal('E0', mu=E0, sd=E0_std, testval=E0)
            E1_prior = pm.Normal('E1', mu=E1, sd=E1_std, testval=E1)
            E2_prior = pm.Normal('E2', mu=E2, sd=E2_std, testval=E2)
            E3_prior = pm.Normal('E3', mu=E3, sd=E3_std, testval=E3)
            log_C1_prior = pm.Bound(pm.Normal, lower= log_c1_lower)('log_C1', mu=log_C1, sd=1, testval=log_C1)
            log_C2_prior = pm.Bound(pm.Normal, lower= log_c2_lower)('log_C2', mu=log_C2, sd=1, testval=log_C2)
            h1_prior = pm.Bound(pm.Normal, lower=0)('h1', mu=h1, sd=h1_std, testval=h1)
            h2_prior = pm.Bound(pm.Normal, lower=0)('h2', mu=h2, sd=h2_std, testval=h2)
            r1_prior = pm.Bound(pm.Normal, lower=E0)('r1', mu=E0*100, sd=E0_std*100)
            r2_prior = pm.Bound(pm.Normal, lower=E0)('r2', mu=E0*100, sd=E0_std*100)

            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_alpha1_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha1', mu=log_alpha1, sd=log_alpha1_std, testval=log_alpha1)
            log_alpha2_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha2', mu=log_alpha2, sd=log_alpha2_std, testval=log_alpha2)
            #Expected distribution
            mu = Edrug2D_NDB((d1,d2),E0_prior,E1_prior,E2_prior,E3_prior,r1_prior,r2_prior,10**log_C1_prior,10**log_C2_prior,h1_prior,h2_prior,10**log_alpha1_prior,10**log_alpha2_prior)
            #Likelihood (sampling distribution) of observations.
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            dip_obs = pm.Normal('dip_obs',mu=mu,sd = dip_sd, observed = dip)
        return syn_Model
    
    def nest6(syn_Model,E0,E1,E2,E3,log_C1,log_C2,h1,h2,E0_std,E1_std,E2_std,E3_std,log_C1_std,log_C2_std,h1_std,h2_std,log_alpha1,log_alpha2,log_alpha1_std,log_alpha2_std,log_gamma1,log_gamma2,log_gamma1_std,log_gamma2_std,alpha_lower,alpha_upper,gamma_lower,gamma_upper,log_c1_lower,log_c2_lower,d1,d2,dip,dip_sd):        
        with syn_Model:
            # Prior distributions based on initial fits
            E0_prior = pm.Normal('E0', mu=E0, sd=E0_std, testval=E0)
            E1_prior = pm.Normal('E1', mu=E1, sd=E1_std, testval=E1)
            E2_prior = pm.Normal('E2', mu=E2, sd=E2_std, testval=E2)
            E3_prior = pm.Normal('E3', mu=E3, sd=E3_std, testval=E3)
            log_C1_prior = pm.Bound(pm.Normal, lower= log_c1_lower)('log_C1', mu=log_C1, sd=1, testval=log_C1)
            log_C2_prior = pm.Bound(pm.Normal, lower= log_c2_lower)('log_C2', mu=log_C2, sd=1, testval=log_C2)
            h1_prior = pm.Bound(pm.Normal, lower=0)('h1', mu=h1, sd=h1_std, testval=h1)
            h2_prior = pm.Bound(pm.Normal, lower=0)('h2', mu=h2, sd=h2_std, testval=h2)
            r1_prior = pm.Bound(pm.Normal, lower=E0)('r1', mu=E0*100, sd=E0_std*100)
            r2_prior = pm.Bound(pm.Normal, lower=E0)('r2', mu=E0*100, sd=E0_std*100)
    
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_alpha1_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha1', mu=log_alpha1, sd=log_alpha1_std, testval=log_alpha1)
            log_alpha2_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha2', mu=log_alpha2, sd=log_alpha2_std, testval=log_alpha2)

           #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_gamma1_prior = pm.Bound(pm.Normal, lower=gamma_lower,upper=gamma_upper)('log_gamma1', mu=log_gamma1, sd=log_gamma1_std, testval=log_gamma1)
            log_gamma2_prior = pm.Bound(pm.Normal, lower=gamma_lower,upper=gamma_upper)('log_gamma2', mu=log_gamma2, sd=log_gamma2_std, testval=log_gamma2)

            #Expected distribution
            mu = Edrug2D_NDB_hill((d1,d2),E0_prior,E1_prior,E2_prior,E3_prior,r1_prior,r2_prior,10**log_C1_prior,10**log_C2_prior,h1_prior,h2_prior,10**log_alpha1_prior,10**log_alpha2_prior,10**log_gamma1_prior,10**log_gamma2_prior)
            #Likelihood (sampling distribution) of observations.
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            dip_obs = pm.Normal('dip_obs',mu=mu,sd = dip_sd, observed = dip)
        return syn_Model
 
    def nest7(syn_Model,E0,E1,E2,E3,log_C1,log_C2,h1,h2,E0_std,E1_std,E2_std,E3_std,log_C1_std,log_C2_std,h1_std,h2_std,log_alpha1,log_alpha2,log_alpha1_std,log_alpha2_std,log_gamma1,log_gamma2,log_gamma1_std,log_gamma2_std,alpha_lower,alpha_upper,gamma_lower,gamma_upper,log_c1_lower,log_c2_lower,d1,d2,dip,dip_sd):        
        with syn_Model:
            # Prior distributions based on initial fits
            log_C1_prior = pm.Bound(pm.Normal, lower= log_c1_lower)('log_C1', mu=log_C1, sd=1, testval=log_C1)
            log_C2_prior = pm.Bound(pm.Normal, lower= log_c2_lower)('log_C2', mu=log_C2, sd=1, testval=log_C2)
            h1_prior = pm.Bound(pm.Normal, lower=0)('h1', mu=h1, sd=h1_std, testval=h1)
            h2_prior = pm.Bound(pm.Normal, lower=0)('h2', mu=h2, sd=h2_std, testval=h2)
            r1_prior = pm.Normal('r1', mu=E0*100, sd=E0, testval=E0*100)
            r2_prior = pm.Normal('r2', mu=E0*100, sd=E0, testval=E0*100)
    
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_alpha1_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha1', mu=log_alpha1, sd=log_alpha1_std, testval=log_alpha1)
            log_alpha2_prior = pm.Bound(pm.Normal, lower=alpha_lower,upper=alpha_upper)('log_alpha2', mu=log_alpha2, sd=log_alpha2_std, testval=log_alpha2)

           #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            log_gamma1_prior = pm.Bound(pm.Normal, lower=gamma_lower,upper=gamma_upper)('log_gamma1', mu=log_gamma1, sd=log_gamma1_std, testval=log_gamma1)
            log_gamma2_prior = pm.Bound(pm.Normal, lower=gamma_lower,upper=gamma_upper)('log_gamma2', mu=log_gamma2, sd=log_gamma2_std, testval=log_gamma2)

            #Expected distribution
            mu = Edrug2D_NDB_hill((d1,d2),E0,E1,E2,E3,r1_prior,r2_prior,10**log_C1_prior,10**log_C2_prior,h1_prior,h2_prior,10**log_alpha1_prior,10**log_alpha2_prior,10**log_gamma1_prior,10**log_gamma2_prior)
            #Likelihood (sampling distribution) of observations.
            #Sample uniformly for alpha over log space.  This gives equal chance to the potency antagonism and synergy
            dip_obs = pm.Normal('dip_obs',mu=mu,sd = dip_sd, observed = dip)
        return syn_Model       
    
    #Store all the nested models in a dictionary for the class        
    funct = {'nest0':nest0,'nest1':nest1,'nest2':nest2,'nest3':nest3,'nest4':nest4,'nest5':nest5,'nest6':nest6,'nest7':nest7}

##############################################################################
#Misc code from david to get the percent affected and the integral between
#the null and combination surface for the different model tiers
##############################################################################

#Get the percent of affected and unaffected cells in each state for the different model nests 
#Code from David Wooten
##Still need to add integrand_lvl6 ...
#def getU_lvl4(d1, d2, e0, e1, e2, e3, ec50_1, ec50_2, h1, h2, alpha1):
#    r1  = 100.*e0
#    r2  = 100.*e0
#    alpha2 = alpha1**(h2/h1)
#    return getU_lvl5(d1, d2, e0, e1, e2, e3, r1, r2, ec50_1, ec50_2, h1, h2, alpha1, alpha2)
#
#def getU_lvl5(d1, d2, e0, e1, e2, e3, r1, r2, ec50_1, ec50_2, h1, h2, alpha1, alpha2):
#    r1r = ec50_1**h1*r1;
#    r2r = ec50_2**h2*r2;
#    U   = r1r*r2r*(r1*(alpha2*d1)**h1 + r1r + r2*(alpha1*d2)**h2 + r2r)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)    
#    A1  = r1*r2r*(d1**h1*r1*(alpha2*d1)**h1 + d1**h1*r1r + d1**h1*r2r + d2**h2*r2*(alpha2*d1)**h1)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
#    A2  = r1r*r2*(d1**h1*r1*(alpha1*d2)**h2 + d2**h2*r1r + d2**h2*r2*(alpha1*d2)**h2 + d2**h2*r2r)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
#    A12 = r1*r2*(d1**h1*r1*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r2r*(alpha1*d2)**h2 + d2**h2*r1r*(alpha2*d1)**h1 + d2**h2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
#    return U, A1, A2, A12

#Find the volume between the null surface and fit surface (VBS)
def integrand_lvl4(d1, d2, e0, e1, e2, e3, ec50_1, ec50_2, h1, h2, alpha):
    d1 = np.power(10.,d1*1.)
    d2 = np.power(10.,d2*1.)
    beta = min(e1,e2)-e3
    return ( ec50_1**h1*ec50_2**h2*e0 + d1**h1*ec50_2**h2*e1 + ec50_1**h1*d2**h2*e2 + d1**h1*d2**h2*(e3+beta) ) / \
                ( ec50_1**h1*ec50_2**h2 + d1**h1*ec50_2**h2 + ec50_1**h1*d2**h2 + d1**h1*d2**h2 ) - \
           ( ec50_1**h1*ec50_2**h2*e0 + d1**h1*ec50_2**h2*e1 + ec50_1**h1*d2**h2*e2 + alpha**h2*d1**h1*d2**h2*e3 ) / \
                ( ec50_1**h1*ec50_2**h2 + d1**h1*ec50_2**h2 + ec50_1**h1*d2**h2 + alpha**h2*d1**h1*d2**h2 )

def integrand_lvl5(d1, d2, e0, e1, e2, e3, r1, r2, ec50_1, ec50_2, h1, h2, alpha1, alpha2):
    d1 = np.power(10.,d1*1.)
    d2 = np.power(10.,d2*1.)
    r1r = ec50_1**h1*r1;
    r2r = ec50_2**h2*r2;
    U   = r1r*r2r*(r1*(alpha2*d1)**h1 + r1r + r2*(alpha1*d2)**h2 + r2r)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
    A1  = r1*r2r*(d1**h1*r1*(alpha2*d1)**h1 + d1**h1*r1r + d1**h1*r2r + d2**h2*r2*(alpha2*d1)**h1)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
    A2  = r1r*r2*(d1**h1*r1*(alpha1*d2)**h2 + d2**h2*r1r + d2**h2*r2*(alpha1*d2)**h2 + d2**h2*r2r)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
    A12 = r1*r2*(d1**h1*r1*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r2r*(alpha1*d2)**h2 + d2**h2*r1r*(alpha2*d1)**h1 + d2**h2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
    return ( ec50_1**h1*ec50_2**h2*e0 + d1**h1*ec50_2**h2*e1 + ec50_1**h1*d2**h2*e2 + d1**h1*d2**h2*np.min((e1,e2)) ) / \
                ( ec50_1**h1*ec50_2**h2 + d1**h1*ec50_2**h2 + ec50_1**h1*d2**h2 + d1**h1*d2**h2 ) - \
           (e0*U + e1*A1 + e2*A2 + e3*A12)
           
###############################################################################################
###############################################################################################
#James Pino's PSO implemenation
###############################################################################################
###############################################################################################
import pathos.multiprocessing as multiprocessing
from deap import base, creator, tools
class PSO:
    """ Simple interface to run particle swarm optimization

        This class provides a simple interface to run particle swarm optimization.
        It builds off the deap package, but provides a simple interface.

        - **parameters**, **types**, **return** and **return types**::

            :param cost_function:
            :param start:
            :param save_sampled:

          :Example:

            optimizer = simplepso.pso.PSO(cost_function,start_position)
            optimizer.set_bounds()
            optimizer.set_speed()
            optimizer.run(number_of_particles, number_of_iterations)


        .. note::
            Must set 1.) cost_function, 2.) starting position, 3.) bounds (can be vector or float)
            To run need to supply number of particles and number of iterations. 20 particles is a good starting place.


        """

    def __init__(self, cost_function=None, start=None, num_proc=1,
                 save_sampled=False, verbose=False):
        """

        :param cost_function:
        :param start:
        :param save_sampled:
        """

        self.cost_function = cost_function
        self.save_sampled = save_sampled
        if start is not None:
            self.set_start_position(start)
        else:
            self.start = None
            self.size = None
        self.verbose = verbose
        self.method = 'single_min'
        self.num_proc = num_proc
        self.best = None
        self.max_speed = None
        self.min_speed = None
        self.lb = None
        self.ub = None
        self.bounds_set = False
        self.range = 2
        self.population = []
        self.toolbox = base.Toolbox()
        self.stats = tools.Statistics(lambda ind: ind.fitness.values)
        self.logbook = tools.Logbook()
        self.all_history = None
        self.all_fitness = None
        self.values = []
        self.history = []
        self.w = 1
        self.update_w = True
        self._is_setup = False
        self.update_scheme = 'constriction'
        if self.update_scheme == 'constriction':
            fi = 2.05 + 2.05
            self.w = 2.0 / np.abs(2.0 - fi - np.sqrt(np.power(fi, 2) - 4 * fi))

    def _generate(self):
        """ Creates Particles and sets their speed

        :return:
        """
        start_position = np.random.uniform(self.lb, self.ub, self.size)
        part = creator.Particle(start_position)
        part.speed = np.random.uniform(self.min_speed, self.max_speed,
                                       self.size)
        part.smin = self.min_speed
        part.smax = self.max_speed
        return part

    def set_w(self, option):
        """ Set if you want to use a constriction factor that shrinks. This shrinks the step size if true.
        :param option:
        :return:
        """
        self.update_w = option

    def get_best_value(self):
        """ Returns the best fitness value of the population

        :return:
        """
        return self.best.fitness.values

    def get_history(self):
        """ Returns the history of the run

        :return:
        """
        return self.history

    def _update_particle_position(self, part, phi1, phi2):
        """ Updates particles position

        :param part: particles
        :param phi1: scaling factor for particles best position
        :param phi2: scaling factor for populations best position
        :return:
        """
        v_u1 = np.random.uniform(0, 1, self.size) * phi1 * (part.best - part)
        v_u2 = np.random.uniform(0, 1, self.size) * phi2 * (self.best - part)
        part.speed = self.w * (part.speed + v_u1 + v_u2)
        np.place(part.speed, part.speed < part.smin, part.smin)
        np.place(part.speed, part.speed > part.smax, part.smax)
        part += part.speed
        for i, pos in enumerate(part):
            if pos < self.lb[i]:
                part[i] = self.lb[i]
            elif pos > self.ub[i]:
                part[i] = self.ub[i]

    def set_cost_function(self, cost_function):
        """ Sets the cost function for PSO. Must return a scalar followed by a , (tuple)

        :param cost_function:
        :return:
        """
        self.cost_function = cost_function

    def setup_pso(self):
        """ Sets up everything for PSO. Only does once

        :return:
        """
        if self.max_speed is None or self.min_speed is None:
            self.set_speed()

        if not self.bounds_set:
            self.set_bounds()

        assert self.start is not None, "Error: Must provide a starting position in order to set size of each particle\
                       **** Provide PSO.set_start_position() your initial starting coordinates ****\
                       Exiting due to failure"

        assert self.cost_function is not None, "Error: Must set a cost function. Use PSO.set_cost_function()."

        if self.method == 'single_min':
            creator.create("FitnessMin", base.Fitness, weights=(-1.00,))
        creator.create("Particle", np.ndarray, fitness=creator.FitnessMin,
                       speed=list, smin=list, smax=list, best=None)
        self.toolbox.register("particle", self._generate)
        self.toolbox.register("population", tools.initRepeat, list,
                              self.toolbox.particle)
        self.toolbox.register("update", self._update_particle_position,
                              phi1=2.05, phi2=2.05)
        self.toolbox.register("evaluate", self.cost_function)

        self.stats.register("avg", np.mean, axis=0)
        self.stats.register("std", np.std, axis=0)
        self.stats.register("min", np.min, axis=0)
        self.stats.register("max", np.max, axis=0)

        self.logbook.header = ["iteration", "best"] + self.stats.fields
        pool = multiprocessing.Pool(self.num_proc)

        self.toolbox.register("map", pool.map)
        self.toolbox.register("join", pool.join)
        self.toolbox.register("close", pool.close)
        self.toolbox.register("terminate", pool.terminate)
        self._is_setup = True

    def update_connected(self):
        """ Updates the population of particles according to the connected population scheme.

        :return:
        """
        for part in self.population:
            if part.best is None or part.best.fitness < part.fitness:
                part.best = creator.Particle(part)
                part.best.fitness.values = part.fitness.values
            if self.best is None or self.best.fitness < part.fitness:
                self.best = creator.Particle(part)
                self.best.fitness.values = part.fitness.values

    def return_ranked_populations(self):
        """ Returns population of particles

        :return: fitness array, parameter array
        """
        positions = np.zeros(np.shape(self.population))
        fitnesses = np.zeros(len(self.population))
        for n, part in enumerate(self.population):
            fitnesses[n] = part.best.fitness.values[0]
            positions[n] = part.best
        idx = np.argsort(fitnesses)
        return fitnesses[idx], positions[idx]

    def set_start_position(self, position):
        """ Set the starting position for the population of particles.

        :param position: vector of parameters
        :return:
        """
        self.start = np.array(position)
        self.size = len(position)

    def set_speed(self, speed_min=-10000, speed_max=10000):
        """ Sets the max and min speed of the particles.
        This is usually a fraction of the range of values that the particles can travel.


        :param speed_min: negative scalar
        :param speed_max: positive scalar
        :return:
        """
        self.min_speed = speed_min
        self.max_speed = speed_max

    def set_bounds(self, parameter_range=None, lower=None, upper=None):
        """ Set the search space bounds that the parameters may search.

        This can be a single float, in which the particles can search plus or minus the starting values around this.
        It can also be an array that must be the same length of the starting position.

        :param parameter_range: scalar
        :param lower: array of len(starting position)
        :param upper: array of len(starting position)
        :return:
        """
        assert self.start is not None, "Must provide starting array: %r" % self.start
        all_set_to_none = False
        if parameter_range is None and upper is None and lower is None:
            all_set_to_none = True
        assert all_set_to_none is False, 'Need to provide parameter range or' \
                                         ' upper and lower bounds'
        if parameter_range is None:
            assert self.range is not None
            parameter_range = self.range

        if lower is None:
            lower = self.start - parameter_range
        else:
            assert self.size == len(lower), "If providing array for " \
                                            "bounds, must equal length of" \
                                            " starting position"
        if upper is None:
            upper = self.start + parameter_range
        else:
            assert self.size == len(
                    upper), "If providing array for bounds, " \
                            "must equal length of starting position"
        self.lb = lower
        self.ub = upper
        self.bounds_set = True

    def run(self, num_particles, num_iterations, save_samples=False):
        """ runs the pso

        :param num_particles:
        :param num_iterations:
        :return:
        """
        if self._is_setup:
            pass
        else:
            self.setup_pso()
        assert type(self.cost_function(
                self.start)) == tuple, "Cost function must return a tuple. An error " \
                                       "is occuring when running your starting position"

        history = np.zeros((num_iterations, len(self.start)))
        if self.save_sampled or save_samples:
            self.all_history = np.zeros(
                    (num_iterations, num_particles, len(self.start)))
            self.all_fitness = np.zeros((num_iterations, num_particles))
        values = np.zeros(num_iterations)
        self.population = self.toolbox.population(num_particles)
        for g in range(1, num_iterations + 1):
            if self.update_w:
                self.w = (num_iterations - g + 1.) / num_iterations
            population_fitness = self.toolbox.map(self.toolbox.evaluate,
                                                  self.population)
            for ind, fit in zip(self.population, population_fitness):
                ind.fitness.values = fit
            self.update_connected()
            for part in self.population:
                self.toolbox.update(part)
            values[g - 1] = self.best.fitness.values[0]
            history[g - 1] = self.best
            if self.save_sampled or save_samples:
                curr_fit, curr_pop = self.return_ranked_populations()
                self.all_history[g - 1, :, :] = curr_pop
                self.all_fitness[g - 1, :] = curr_fit
            self.logbook.record(iteration=g, best=self.best.fitness.values[0],
                                **self.stats.compile(self.population))
            if self.logbook.select('std')[-1] < 1e-12:
                break
            if self.verbose:
                print(self.logbook.stream)
        self.toolbox.close()
        self.toolbox.terminate()
        self.values = values[:g]
        self.history = history[:g, :]



