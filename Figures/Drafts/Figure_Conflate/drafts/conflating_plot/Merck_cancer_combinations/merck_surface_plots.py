# -*- coding: utf-8 -*-
"""
Created on Mon Mar  5 12:00:54 2018

@author: xnmeyer
"""
##Import Packages/Install missing ones
import numpy as np
import pandas as pd
import plotly.graph_objs as go
from plotly.offline import  plot
#import matplotlib.pyplot as plt
import os

#2D dose response surface function not assuming detail balance but assuming proliferation 
# is << than the rate of transition between the different states.
def Edrug2D_NDB(d,E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2):
    d1 = d[0]
    d2 = d[1]
    Ed = (E3*r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
          E3*r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
          C2**h2*E0*r1*(C1*alpha2*d1)**h1 + 
          C1**h1*E0*r2*(C2*alpha1*d2)**h2 + 
          C2**h2*E1*r1*(alpha2*d1**2)**h1 + 
          C1**h1*E2*r2*(alpha1*d2**2)**h2 + 
          E3*d2**h2*r1*(C1*alpha2*d1)**h1 + 
          E3*d1**h1*r2*(C2*alpha1*d2)**h2 + 
          C1**(2*h1)*C2**h2*E0*r1 + 
          C1**h1*C2**(2*h2)*E0*r2 + 
          C1**(2*h1)*E2*d2**h2*r1 + 
          C2**(2*h2)*E1*d1**h1*r2 + 
          E1*r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
          E2*r1*(C1*d1)**h1*(alpha1*d2)**h2 +
          C2**h2*E1*r1*(C1*d1)**h1 +
          C1**h1*E2*r2*(C2*d2)**h2)/  \
         (r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
          r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
          C2**h2*r1*(C1*alpha2*d1)**h1 +
          C1**h1*r2*(C2*alpha1*d2)**h2 + 
          C2**h2*r1*(alpha2*d1**2)**h1 + 
          C1**h1*r2*(alpha1*d2**2)**h2 + 
          d2**h2*r1*(C1*alpha2*d1)**h1 + 
          d1**h1*r2*(C2*alpha1*d2)**h2 + 
          C1**(2*h1)*C2**h2*r1 + 
          C1**h1*C2**(2*h2)*r2 + 
          C1**(2*h1)*d2**h2*r1 + 
          C2**(2*h2)*d1**h1*r2 + 
          r1*(C1*d1)**h1*(alpha1*d2)**h2 + 
          r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
          C2**h2*r1*(C1*d1)**h1 + 
          C1**h1*r2*(C2*d2)**h2)
         
    return Ed


def plotDSurf(T):
    #Should the dose response surfaces be plotted?
    expt = T['expt']#Experiment file
    metric_name = "Percent Viable"#Name of metric
    target_cell_line = T['cell_line']#The cell line to consider
    
    #Read in the data into a pandas data frame
    data = pd.read_table(expt, delimiter=',')
    #Subset by target cell line
    data = data[data['cell.line']==target_cell_line]
    #data = data.reset_index()
    
    drug1_name = T['drug1_name']#Drug1 name to replace in array gen function
    drug2_name = T['drug2_name']#Drug2 name to replace in ArrayGen Function
    data['drug1'] = data['drug1'].str.lower()    
    data['drug2'] = data['drug2'].str.lower()    
    
    #This is different than the template for the dip calculations due to the way the merck data is organized.
    #control data for drug 1
    d1      = data[(data['drug1']==drug1_name) & (data['drug2']=='control')]['drug1.conc'].values
    d2      = data[(data['drug1']==drug1_name) & (data['drug2']=='control')]['drug2.conc'].values
    dip     = data[(data['drug1']==drug1_name) & (data['drug2']=='control')]['rate'].values
    dip_95ci= data[(data['drug1']==drug1_name) & (data['drug2']=='control')]['rate.95ci'].values
    #Control data for drug 2
    d1      = np.concatenate((d1,data[(data['drug1']==drug2_name) & (data['drug2']=='control')]['drug2.conc'].values))
    d2      = np.concatenate((d2,data[(data['drug1']==drug2_name) & (data['drug2']=='control')]['drug1.conc'].values))
    dip     = np.concatenate((dip,data[(data['drug1']==drug2_name) & (data['drug2']=='control')]['rate'].values))
    dip_95ci= np.concatenate((dip_95ci,data[(data['drug1']==drug2_name) & (data['drug2']=='control')]['rate.95ci'].values))
    #Combination experiment
    d1      = np.concatenate((d1,data[(data['drug1']==drug1_name) & (data['drug2']==drug2_name)]['drug1.conc'].values))
    d2      = np.concatenate((d2,data[(data['drug1']==drug1_name) & (data['drug2']==drug2_name)]['drug2.conc'].values))
    dip     = np.concatenate((dip,data[(data['drug1']==drug1_name) & (data['drug2']==drug2_name)]['rate'].values))
    dip_95ci= np.concatenate((dip_95ci,data[(data['drug1']==drug1_name) & (data['drug2']==drug2_name)]['rate.95ci'].values))
    #Set as standard deviation
    dip_sd = dip_95ci/(2*1.96)
    #Add the zero concentration making the certainty equal to the most certain value
    d1 = np.concatenate((d1,np.zeros(2,)))
    d2 = np.concatenate((d2,np.zeros(2,)))
    dip = np.concatenate((dip,np.ones(2,)))
    dip_sd = np.concatenate((dip_sd,np.ones(2,)*np.min(dip_sd)))
    
    #Remove nan values
    d1      = d1[~np.isnan(dip)]
    d2      = d2[~np.isnan(dip)]
    dip_sd  = dip_sd[~np.isnan(dip)]
    dip     = dip[~np.isnan(dip)]
    
    #Force all the drug names to lower case...
    drug1_name = drug1_name.lower()
    drug2_name = drug2_name.lower()
    #Force all the cell_line names to be upper case
    target_cell_line = target_cell_line.upper()

    popt3 = [ T['E0'],
              T['E0_std'],
              T['E1'],
              T['E1_std'],
              T['E2'],
              T['E2_std'],
              T['E3'],
              T['E3_std'],
              T['r1'],
              T['r1_std'],
              T['r2'],
              T['r2_std'],
              10**T['log_C1'],
              np.log(10)*T['log_C1_std']*10**T['log_C1'],
              10**T['log_C2'],
              np.log(10)*T['log_C2_std']*10**T['log_C2'],
              T['h1'],
              T['h1_std'],
              T['h2'],
              T['h2_std'],
              10**T['log_alpha1'],
              np.log(10)*T['log_alpha1_std']*10**T['log_alpha1'],
              10**T['log_alpha2'],
              np.log(10)*T['log_alpha2_std']*10**T['log_alpha2']]
              
    DosePlots_PLY(d1,d2,dip,dip_sd,drug1_name,drug2_name,popt3[0:23:2],'NDB',target_cell_line,expt,metric_name)        



############################################################
#Function to plot the combination surface plots in plotly    
############################################################
def DosePlots_PLY(d1,d2,dip,dip_sd,drug1_name,drug2_name,popt3,which_model,target_cell_line,expt,metric_name):
    #To plot doses on log space
    t_d1 = d1.copy()
    t_d2 = d2.copy()
    t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10
    t_d2[t_d2==0] = min(t_d2[t_d2!=0])/10

    trace1 = go.Scatter3d(x=np.log10(t_d1),y=np.log10(t_d2),z=dip,mode='markers',
                        marker=dict(size=3,color = dip,colorscale = 'coolwarm',
                                    line=dict(
                                              color='rgb(0,0,0)',width=1)),
                        )    
    #Plot the fit
    conc1 = 10**np.linspace(np.log10(min(t_d1)),np.log10(max(t_d1)), 30)
    conc2 = 10**np.linspace(np.log10(min(t_d2)), np.log10(max(t_d2)), 30)
    conc3 = np.array([(c1,c2) for c1 in conc1 for c2 in conc2])
    if which_model == 'DB':
        twoD_fit = np.array([Edrug2D_DB(d,*popt3) for d in conc3])
    elif which_model=='NDB':
        twoD_fit = np.array([Edrug2D_NDB(d,*popt3) for d in conc3])
        
    zl = [min(twoD_fit),max(twoD_fit)]
    twoD_fit = np.resize(twoD_fit, (len(conc1),len(conc2)))
    
    [X,Y] = np.meshgrid(np.log10(conc1), np.log10(conc2))

    trace2 = go.Surface(x=X,y=Y,z=twoD_fit.transpose(),
                      colorscale = 'coolwarm',
                      opacity=.8,
                      contours  = dict( 
                                        y = dict(highlight=False,show=False,color='#444',width=1),
                                        x = dict(highlight=False,show=False,color='#444',width = 1 ),
                                        z = dict(highlight=True,show=True,highlightwidth=4,width=3,usecolormap=False)
                                        ))

                      
    
    layout = go.Layout(
                       scene = dict(
                                    xaxis=dict(    
                                               range = [np.log10(min(t_d1)),np.log10(max(t_d1))],
                                               title = 'log(' + drug1_name + ')',
                                               autorange=True,
                                               showgrid=True,
                                               zeroline=False,
                                               showline=False,
                                               ticks='',
                                               showticklabels=True,
                                               gridwidth = 5,
                                               gridcolor = 'k'
                                    ),
                                    yaxis=dict(    
                                               range = [np.log10(min(t_d1)),np.log10(max(t_d1))],
                                               title = 'log(' + drug2_name + ')',
                                               autorange=True,
                                               showgrid=True,
                                               zeroline=False,
                                               showline=False,
                                               ticks='',
                                               showticklabels=True,
                                               gridwidth = 5,
                                               gridcolor = 'k'
                                    ),
                                    zaxis=dict(
                                               range = zl,
                                               title = metric_name,
                                               autorange=False,
                                               tick0=np.min(twoD_fit),
                                               dtick=(np.max(twoD_fit)-np.min(twoD_fit))/5,
                                               showgrid=True,
                                               zeroline=True,
                                               zerolinewidth=5,
                                               showline=False,
                                               ticks='',
                                               showticklabels=True,
                                               gridwidth=5,
                                               gridcolor = 'k'
                                    )
                                    ),
                    margin=dict(
                                l=0,
                                r=0,
                                b=0,
                                t=0
                    ),
                    showlegend=False,
                    font=dict(family='Arial', size=18),
                    title = target_cell_line
    )
        
                    
    data = [trace1,trace2]


    for e,i in enumerate(dip_sd):
        x = np.log10(t_d1[e])*np.ones((2,))
        y = np.log10(t_d2[e])*np.ones((2,))
        z = np.array([dip[e]+i,dip[e]-i])
        trace = go.Scatter3d(
            x=x, y=y, z=z,
            mode = 'lines',
            line=dict(
                color='#1f77b4',
                width=2
            )
        )
        data.append(trace)
    
    fig = go.Figure(data=data,layout=layout)
    
    camera = dict(
                  up=dict(x=0,y=0,z=1),
                  center=dict(x=0,y=0,z=0),
                  eye = dict(x=1.25,y=-1.25,z=1.25))
    fig['layout'].update(scene=dict(camera=camera))
    s_idx = expt.rfind(os.sep)
    expt = expt[s_idx+1:-4]
    plot(fig,filename = '{}_{}_{}_{}_{}_plotly_doseResponseSurface.html'.format(target_cell_line,drug1_name,drug2_name,which_model,expt),auto_open=False)


