# -*- coding: utf-8 -*-
"""
Created on Mon Mar  5 11:46:38 2018

@author: xnmeyer
"""

import pandas as pd
import numpy as np
from itertools import compress
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
import time

T = pd.read_csv('MasterResults.csv')
T = T[T['MCMC_converge']==1]
#T = T[abs(T['beta_obs_norm_std'].values/T['beta_obs_norm'].values)<.5]

#cl_mod = pd.read_csv('cell_lines.csv',header=-1)
#dr_mod = pd.read_csv('drug_MOA_cm-3-7-18.csv')
#fig = plt.figure(figsize=(10,20))
#
#cl = np.unique(T['cell_line'])
#for e,cline in enumerate(cl):
#    ax = plt.subplot(10,4,e+1)    
#    subT = T[T['cell_line']==cline]
#    plt.scatter(subT[['log_alpha1','log_alpha2']].min(axis=1),subT['beta_obs_norm'].values)
#    ax.set_xlim([-5,5])
#    ax.set_ylim([-4,4])

T['bliss_med']=0
T['bliss_avg']=0
T['bliss_std']=0

T['E1_pv'] = 0
T['h1_pv']= 0
T['C1_pv']= 0
T['E1_std_pv']= 0
T['h1_std_pv']= 0
T['C1_std_pv']= 0
T['p1_pv'] = 1

T['E2_pv'] = 0
T['h2_pv']= 0
T['C2_pv']= 0
T['E2_std_pv']= 0
T['h2_std_pv']= 0
T['C2_std_pv']= 0
T['p2_pv'] = 1    
    
tt = time.time()
for e,i in enumerate(T.index):
    print e
    #Should the dose response surfaces be plotted?
    expt = T['expt'].loc[i]#Experiment file
    metric_name = "Percent Viable"#Name of metric
    target_cell_line = T['cell_line'].loc[i]#The cell line to consider
    
    #Read in the data into a pandas data frame
    data = pd.read_table(expt, delimiter=',')
    data['cell.line']=data['cell.line'].str.upper()
    #Subset by target cell line
    data = data[data['cell.line']==target_cell_line]
    #data = data.reset_index()
    
    drug1_name = T['drug1_name'].loc[i]#Drug1 name to replace in array gen function
    drug2_name = T['drug2_name'].loc[i]#Drug2 name to replace in ArrayGen Function
    data['drug1'] = data['drug1'].str.lower()    
    data['drug2'] = data['drug2'].str.lower()    
    #This is different than the template for the dip calculations due to the way the merck data is organized.
    #control data for drug 1
    d1      = data[(data['drug1']==drug1_name) & (data['drug2']=='control')]['drug1.conc'].values
    dip1     = data[(data['drug1']==drug1_name) & (data['drug2']=='control')]['rate'].values
    dip1_sd =  data[(data['drug1']==drug1_name) & (data['drug2']=='control')]['rate.95ci'].values/(2*1.96)

    [popt1,p1,pcov1] = fit_drc(d1,dip1,dip1_sd)
    if sum(np.isnan(popt1))==3:
        perr1 = pcov1
    else:
        perr1 = np.sqrt(np.diag(pcov1)) 
    T['E1_pv'].loc[i] = popt1[1]
    T['h1_pv'].loc[i] = popt1[0]
    T['C1_pv'].loc[i] = popt1[2]
    T['E1_std_pv'].loc[i] = popt1[1]
    T['h1_std_pv'].loc[i] = popt1[0]
    T['C1_std_pv'].loc[i] = popt1[2]
    T['p1_pv'].loc[i] = p1

    #Control data for drug 2
    d2       = data[(data['drug1']==drug2_name) & (data['drug2']=='control')]['drug1.conc'].values
    dip2     = data[(data['drug1']==drug2_name) & (data['drug2']=='control')]['rate'].values
    dip2_sd =  data[(data['drug1']==drug2_name) & (data['drug2']=='control')]['rate.95ci'].values/(2*1.96)

    [popt2,p2,pcov2] = fit_drc(d2,dip2,dip2_sd)
    if sum(np.isnan(popt2))==3:
        perr2 = pcov2
    else:
        perr2 = np.sqrt(np.diag(pcov2)) 
    T['E2_pv'].loc[i] = popt2[1]
    T['h2_pv'].loc[i] = popt2[0]
    T['C2_pv'].loc[i] = popt2[2]
    T['E2_std_pv'].loc[i] = popt2[1]
    T['h2_std_pv'].loc[i] = popt2[0]
    T['C2_std_pv'].loc[i] = popt2[2]
    T['p2_pv'].loc[i] = p2
    
    if ~np.isnan(popt1).all() and ~np.isnan(popt2).all():
        #Combination experiment
        d1      = data[(data['drug1']==drug1_name) & (data['drug2']==drug2_name)]['drug1.conc'].values
        d2      = data[(data['drug1']==drug1_name) & (data['drug2']==drug2_name)]['drug2.conc'].values
        dip     = data[(data['drug1']==drug1_name) & (data['drug2']==drug2_name)]['rate'].values
        dip_sd  = data[(data['drug1']==drug1_name) & (data['drug2']==drug2_name)]['rate.95ci'].values/(2*1.96)
        #bliss formula
        bl = dip-ll3(d1,*popt1)*ll3(d2,*popt2)
        T['bliss_med'].loc[i]=np.median(bl)
        T['bliss_avg'].loc[i]=np.mean(bl)
        T['bliss_std'].loc[i]=np.std(bl)


        
T['time'] = (time.time()-tt)/3600.
        
T.to_csv('bliss_calc_merck_dataset-4-2-18.csv',index=False)



    
##############################################################################
#Functions from Alex's pyDRC library used for initial fits to the 1D hill eqn#
##############################################################################
import scipy.stats
def ll4(x, b, c, d, e):
    """
    Four parameter log-logistic function ("Hill curve")

     - b: Hill slope
     - c: min response
     - d: max response
     - e: EC50
     """     
    return c+(d-c)/(1+(x/e)**b)

def ll3(x, b, c, e):
    """
    Four parameter log-logistic function ("Hill curve")

     - b: Hill slope
     - c: min response
     - d: 1
     - e: EC50
     """     
    return c+(1-c)/(1+(x/e)**b)
# Fitting function
def fit_drc(doses, dip_rates, dip_std_errs=None, hill_fn=ll3,
            null_rejection_threshold=0.05):
    dip_rate_nans = np.isnan(dip_rates)
    if np.any(dip_rate_nans):
        doses = doses[~dip_rate_nans]
        dip_rates = dip_rates[~dip_rate_nans]
        if dip_std_errs is not None:
            dip_std_errs = dip_std_errs[~dip_rate_nans]
    popt = np.zeros((3,))*np.nan
    pcov = np.zeros((3,))*np.nan
    curve_initial_guess = ll4_initials(doses, dip_rates)
    curve_initial_guess = [curve_initial_guess[0],curve_initial_guess[1],curve_initial_guess[3]]
    if curve_initial_guess[0]<0: #If the hill slope is less than 1
        curve_initial_guess[0] = 1
    if curve_initial_guess[2]>max(doses): #If the EC50 is greater than the maximum dose
        curve_initial_guess[2]=max(doses)
    if curve_initial_guess[2]<min(doses): #If the EC50 is less than the maximum dose
        curve_initial_guess[2]=min(doses[doses!=0])
    if curve_initial_guess[1]<0:#If the Emax is less than zero.
        curve_initial_guess[1]=0
    try:
        popt, pcov = scipy.optimize.curve_fit(hill_fn,
                                              doses,
                                              dip_rates,
                                              p0=curve_initial_guess,
                                              sigma=dip_std_errs,
                                              bounds=(0.,[25.,10.,max(doses)*1e3])
                                              )

        if  popt[0] < 0:
            # Reject fit if curve goes upwards
            popt = np.zeros((3,))*np.nan
            pcov = np.zeros((3,))*np.nan
    except:
        pass
    p=1 #pvalue
    if sum(np.isnan(popt))!=3:
        # DIP rate fit
        dip_rate_fit_curve = hill_fn(doses, *popt)
        # F test vs flat linear "no effect" fit
        ssq_model = ((dip_rate_fit_curve - dip_rates) ** 2).sum()
        ssq_null = ((np.mean(dip_rates) - dip_rates) ** 2).sum()
        df = len(doses) - 3
        f_ratio = (ssq_null-ssq_model)/(ssq_model/df)
        p = 1 - scipy.stats.f.cdf(f_ratio, 1, df)
        if p > null_rejection_threshold:
            popt = np.zeros((3,))*np.nan
            pcov = np.zeros((3,))*np.nan
    return popt, p, pcov

# Functions for finding initial parameter estimates for curve fitting
def ll4_initials(x, y):
    c_val, d_val = _find_cd_ll4(y)
    b_val, e_val = _find_be_ll4(x, y, c_val, d_val)
    return b_val, c_val, d_val, e_val

def _response_transform(y, c_val, d_val):
    return np.log((d_val - y) / (y - c_val))

def _find_be_ll4(x, y, c_val, d_val, slope_scaling_factor=1,
                 dose_transform=np.log,
                 dose_inv_transform=np.exp):
    x[x==0]=min(x[x!=0])/100
    y = _response_transform(y,c_val,d_val)
    x = dose_transform(x[(~np.isnan(y)) & (~np.isinf(y))])
    y = y[(~np.isnan(y)) & (~np.isinf(y))]
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x,y)
    b_val = slope_scaling_factor * slope
    e_val = dose_inv_transform(-intercept / (slope_scaling_factor * b_val))
    if np.isnan(e_val):
        e_val = min(x)*10
    if np.isnan(b_val):
        b_val = 1
    return b_val, e_val

def _find_cd_ll4(y, scale=0.001):
    ymin = np.min(y)
    ymax = np.max(y)
    len_y_range = scale * (ymax - ymin)
    return ymin - len_y_range, ymax + len_y_range
