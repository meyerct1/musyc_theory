#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 11:53:25 2018

@author: xnmeyer
"""

import pandas as pd
import numpy as np
import glob
####Generate array of files
import os


sa = pd.read_csv('single.csv')
co = pd.read_csv('combos.csv')
sa['effect.95ci'] = np.nanstd(sa[['viability1','viability2','viability3','viability4','viability5','viability6']],axis=1)*2*1.96
co['effect.95ci'] = np.nanstd(co[['viability1','viability2','viability3','viability4']],axis=1)*2*1.96

sa = sa.drop(['viability1','viability2','viability3','viability4','viability5','viability6','BatchID','mu/muMax'],axis=1)
co = co.drop(['viability1','viability2','viability3','viability4','BatchID','mu/muMax','combination_name'],axis=1)
sa = sa.rename(index = str,columns={'drug_name':'drugA_name','Drug_concentration (\xc2\xb5M)':'drugA Conc (\xc2\xb5M)'})
sa['drugB Conc (\xc2\xb5M)'] = 0
sa['drugB_name'] = 'control'
data = co.append(sa,ignore_index=True)
data = data.rename(index=str,columns={'drugA_name':'drug1','drugB_name':'drug2','cell_line':'sample','drugB Conc (\xc2\xb5M)':'drug2.conc','drugA Conc (\xc2\xb5M)':'drug1.conc','X/X0':'effect'})
data.to_csv('merck_perVia_06-21-2018.csv',sep=',',index=False)

experiments = ['merck_perVia_06-21-2018.csv']
metric_name = 'Percent Viable'
hill_orient = 1
filename = 'SynCode_preCalcDIP_template'
max_nest  = 5
min_nest = 0

#Function to switch the index in SynCode.py
def StringSwitch(string,file_string,val):
    idx1 = file_string.find(string)
    idx2 = file_string.find('#',idx1)
    file_string = file_string[0:idx1+len(string)] + str(val) + file_string[idx2:]
    return file_string    
    
#Functions to generate array files by reading in the file name and saving an editted version with
#a new index corresponding to a new drug combination.
def Accre_Array_gen_darren(filename,target_cell_line,expt,drug1_name,drug2_name,metric_name,cnt):
    fil = str(filename) + '.py'
    f = open(fil, 'r')
    file_string = f.read()
    string = 'expt = '
    file_string = StringSwitch(string,file_string,'"%s"' % expt)  
    string = 'metric_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % metric_name)  
    string = 'min_nest = '
    file_string = StringSwitch(string,file_string,min_nest)
    string = 'max_nest = '
    file_string = StringSwitch(string,file_string,max_nest)
    string = 'hill_orient = '
    file_string = StringSwitch(string,file_string,hill_orient)  
    string = 'target_cell_line = '
    file_string = StringSwitch(string,file_string,'"%s"' % target_cell_line)
    string = 'drug1_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % drug1_name)
    string = 'drug2_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % drug2_name)  
    bol = True
    while bol:
        if os.path.isfile(filename + '_' + str(cnt) + '.py'):
            cnt = cnt + 1
        else:
            f = open(filename + '_' + str(cnt) + '.py', 'w')
            f.write(file_string)
            bol = False
    return cnt

cnt = 830
for expt in experiments:                
    #Read in the data into a pandas data frame
    data = pd.read_table(expt, delimiter=',')
    for target_cell_line in np.unique(data['sample']):
        #Subset by target cell line
        sub_data = data[data['sample']==target_cell_line]
        drug_combinations = sub_data[list(['drug1','drug2'])].drop_duplicates()
        #Remove the double control condition...
        drug_combinations = drug_combinations[np.logical_and(drug_combinations['drug1']!='control', drug_combinations['drug2']!='control')]
        drug_combinations = drug_combinations.reset_index()
        #For all unique drug combinations:
        for e in drug_combinations.index:
            drug1_name = drug_combinations['drug1'][e]
            drug2_name = drug_combinations['drug2'][e]
            cnt = Accre_Array_gen_darren(filename,target_cell_line,expt,drug1_name,drug2_name,metric_name,cnt)          










