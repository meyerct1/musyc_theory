#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
This is the template code which is replicated by ArrayGen_dipDB_fits.py function
to create an array of scripts which can be run in parallel for fitting the 
drug combination studies.

Commented and up-to-date 9-4-17
Christian Meyer
christian.t.meyer@vanderbilt.edu
"""

#Import modules
#Either need the pydrc library in the same folder or add to search path...
import os
#os.sys.path.append('/home/xnmeyer/Documents/Lab/Repos/pyDRC')
###Or for ACCRE
import SynergyCalculator as SC
import numpy as np
import pandas as pd  #Make sure this is version 0.20.3 or higher
#if pd.__version__ != u'0.20.3':
#    raise('Need to have pandas version 0.20.3')
import datetime

#Should the dose response surfaces be plotted?
to_plot = 0#Should the results be plotted?
to_save = 0#Should the traces/pso optimization be saved?
expt = 'dasatinib_osimertinib_cellavista_cm_8-24-17.csv'#Experiment file
metric_name = 'DIP Rate'#Name of metric
min_nest = 0 #first model tier
max_nest = 5 #Model tier to fit up to
hill_orient = 1 #orientation of hill curve.  1 == min effect (ie lowest dip rate) is observed at maximum concentration of drug.  0== the converse.
target_cell_line = 'PC9c1' #The cell line to consider

#Read in the data into a pandas data frame
data = pd.read_table(expt, delimiter=',')
#Subset by target cell line
sub_data = data[data['sample']==target_cell_line]
#data = data.reset_index()

drug1_name = 'dasatinib'#Drug1 name to replace in array gen function
drug2_name = 'osimertinib'#Drug2 name to replace in ArrayGen Function
#control data for drug 1.  Either
indx    = ((sub_data['drug1']==drug1_name) & (sub_data['drug2.conc']==0))  & (sub_data['drug1.conc']!=0)
d1      = sub_data[indx]['drug1.conc'].values
d2      = sub_data[indx]['drug2.conc'].values
dip     = sub_data[indx]['effect'].values
dip_95ci= sub_data[indx]['effect.95ci'].values

indx    = ((sub_data['drug2']==drug1_name) & (sub_data['drug1.conc']==0))  & (sub_data['drug2.conc']!=0)
d1      = np.concatenate([d1,sub_data[indx]['drug2.conc'].values])
d2      = np.concatenate([d2,sub_data[indx]['drug1.conc'].values])
dip     = np.concatenate([dip,sub_data[indx]['effect'].values])
dip_95ci= np.concatenate([dip_95ci,sub_data[indx]['effect.95ci'].values])

#control for drug 2
indx    = ((sub_data['drug1']==drug2_name) & (sub_data['drug2.conc']==0))  & (sub_data['drug1.conc']!=0)
d1      = np.concatenate([d1,sub_data[indx]['drug2.conc'].values])
d2      = np.concatenate([d2,sub_data[indx]['drug1.conc'].values])
dip     = np.concatenate([dip,sub_data[indx]['effect'].values])
dip_95ci= np.concatenate([dip_95ci,sub_data[indx]['effect.95ci'].values])

indx    = ((sub_data['drug2']==drug2_name) & (sub_data['drug1.conc']==0))  & (sub_data['drug2.conc']!=0)
d1      = np.concatenate([d1,sub_data[indx]['drug1.conc'].values])
d2      = np.concatenate([d2,sub_data[indx]['drug2.conc'].values])
dip     = np.concatenate([dip,sub_data[indx]['effect'].values])
dip_95ci= np.concatenate([dip_95ci,sub_data[indx]['effect.95ci'].values])

#Combination experiment
indx    = ((sub_data['drug1']==drug1_name) & (sub_data['drug2']==drug2_name)) & ((sub_data['drug1.conc']!=0) & (sub_data['drug2.conc']!=0)) 
d1      = np.concatenate([d1,sub_data[indx]['drug1.conc'].values])
d2      = np.concatenate([d2,sub_data[indx]['drug2.conc'].values])
dip     = np.concatenate([dip,sub_data[indx]['effect'].values])
dip_95ci= np.concatenate([dip_95ci,sub_data[indx]['effect.95ci'].values])

indx    = ((sub_data['drug2']==drug1_name) & (sub_data['drug1']==drug2_name)) & ((sub_data['drug1.conc']!=0) & (sub_data['drug2.conc']!=0)) 
d1      = np.concatenate([d1,sub_data[indx]['drug2.conc'].values])
d2      = np.concatenate([d2,sub_data[indx]['drug1.conc'].values])
dip     = np.concatenate([dip,sub_data[indx]['effect'].values])
dip_95ci= np.concatenate([dip_95ci,sub_data[indx]['effect.95ci'].values])

#The double control condition
indx    = ((sub_data['drug1.conc']==0) & (sub_data['drug2.conc']==0))
d1      = np.concatenate([d1,sub_data[indx]['drug1.conc'].values])
d2      = np.concatenate([d2,sub_data[indx]['drug2.conc'].values])
dip     = np.concatenate([dip,sub_data[indx]['effect'].values])
dip_95ci= np.concatenate([dip_95ci,sub_data[indx]['effect.95ci'].values])

#Set as standard deviation
dip_sd = dip_95ci/(2*1.96)

#Remove nan values
d1      = d1[~np.isnan(dip)]
d2      = d2[~np.isnan(dip)]
dip_sd  = dip_sd[~np.isnan(dip)]
dip     = dip[~np.isnan(dip)]

if sum(dip_sd<=0)>0:
    string = 'Combination Screen: Drugs(' + drug1_name + ' ' + drug2_name + ') Sample: ' + target_cell_line + ' will not be fit as the effect.95ci column has a value equal to or less than zero.  Confidence intervals (CI) on effect MUST be positive.  If CI is unknown, assign a small finite number to all conditions.'
    raise ValueError(string)
    
#Force all the drug names to lower case...
drug1_name = drug1_name.lower()
drug2_name = drug2_name.lower()
#Force all the cell_line names to be upper case
target_cell_line = target_cell_line.upper()


#Run the Synergy fitting function
df = SC.SyngCalc(d1,d2,dip,dip_sd,drug1_name,drug2_name,to_plot,target_cell_line,expt,min_nest=min_nest,
                  max_nest=max_nest,metric_name=metric_name,hill_orient=hill_orient,to_save=to_save,direc=os.getcwd())
df = pd.DataFrame([df])
cols = df.columns.tolist()
cols = sorted(cols, key=lambda s: s.lower())
df = df[cols]
to_drop = ['alpha_lower','alpha_upper','DelSurf',
           'log_c1_upper','log_c2_upper','log_c1_lower','log_c2_lower',
           'log_gamma1','log_gamma1_std','log_gamma2','log_gamma2_std',
           'VBS','VBS_verr']

#Append the results to a MasterResults file
if not os.path.isfile('MasterResults.csv'):
   df.to_csv('MasterResults.csv')
else:
    with open('MasterResults.csv', 'a') as f:
        df.to_csv(f, header=False)
        f.close()
        
#Write what drugs have finished and date stamp...        
if not os.path.isfile('Drugs_Finished.txt'):
   f = open('Drugs_Finished.txt','w')
   f.write(expt + ', ' + target_cell_line + ', ' + drug1_name + ', ' + drug2_name + '  Time: ' + str(datetime.datetime.now()) + '\n')
   f.close()
else:
    with open('Drugs_Finished.txt', 'a') as f:
       f.write(expt + ', ' + target_cell_line + ', ' + drug1_name + ', ' + drug2_name + '  Time: ' + str(datetime.datetime.now()) + '\n')
       f.close()


                
            
            
