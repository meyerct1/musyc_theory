#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 28 14:18:12 2018

@author: xnmeyer
"""
import pandas as pd
import numpy as np
import matplotlib.pylab as plt
#from matplotlib.patches import FancyArrowPatch
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from plotting_surfaces import plot_surface,get_params,plot_slices
from plotting_surfaces import dip
from pythontools.synergy import synergy_tools, synergy_plots
from scipy.interpolate import interp1d
from conflating_plot import SynergyCalculator as SC
import os

df = pd.read_csv('conflating_plot/Malarial_combinations/MasterResults.csv')
df = df[df['MCMC_converge']==1]
df = df[df['R2']>.9]
df.reset_index(inplace=True,drop=True)
df['zimmer_a12'] = np.nan
df['zimmer_a21'] = np.nan
df['zip'] = np.nan

for indx in df.index:
    print indx
    expt= 'conflating_plot/Malarial_combinations/'+df['expt'].loc[indx]
    drug1_name = df['drug1_name'].loc[indx]
    drug2_name = df['drug2_name'].loc[indx]
    target_cell_line = df['cell_line'].loc[indx]
    d1,d2,dip,dip_sd = synergy_tools.get_data(expt,drug1_name,drug2_name,target_cell_line)
    C1 = np.power(10.,df['log_C1'].loc[indx])
    C2 = np.power(10.,df['log_C2'].loc[indx])
    h1 = df['h1'].loc[indx]
    h2 = df['h2'].loc[indx]
    df.loc[indx,'zimmer_a12'],df.loc[indx,'zimmer_a21']= synergy_tools.zimmer_old(d1,d2,dip/100., C1, C2, h1, h2,vector_input=True)[0:2]
    df.loc[indx,'zip']=synergy_tools.zip(d1,d2,dip/100.,vector_input=True)
    
    
dif_df = pd.DataFrame([(indx1,indx2) for indx1 in df.index for indx2 in np.arange(indx1+1,len(df))],columns=['indx1','indx2'])
for indx in dif_df.index:
    indx1=dif_df['indx1'].loc[indx]
    indx2=dif_df['indx2'].loc[indx]
    dif_df.loc[indx,'val_zim'] = np.abs(df[['zimmer_a21','zimmer_a12']].loc[indx2].min()-df[['zimmer_a21','zimmer_a12']].loc[indx1].min())+np.abs(df[['zimmer_a21','zimmer_a12']].loc[indx2].max()-df[['zimmer_a21','zimmer_a12']].loc[indx1].max())
    dif_df.loc[indx,'val_alp'] = np.abs(df[['log_alpha2','log_alpha1']].loc[indx2].min()-df[['log_alpha2','log_alpha1']].loc[indx1].min())+np.abs(df[['log_alpha2','log_alpha1']].loc[indx2].max()-df[['log_alpha2','log_alpha1']].loc[indx1].max())
    dif_df.loc[indx,'val_gam'] = np.abs(df[['log_gamma2','log_gamma1']].loc[indx2].min()-df[['log_gamma2','log_gamma1']].loc[indx1].min())+np.abs(df[['log_gamma2','log_gamma1']].loc[indx2].max()-df[['log_gamma2','log_gamma1']].loc[indx1].max())
    dif_df.loc[indx,'val_zip'] = np.abs(df['zip'].loc[indx2]-df['zip'].loc[indx1])

dif_df.to_csv('save_combination_comparisions_8-31-18.csv',index=False)
dif_df = dif_df[~np.isnan(dif_df['val_zim'])]
dif_df = dif_df.sort_values('val_zim',ascending=True)

#For Yeast Dataset Zimmer
plot_slices(pd.DataFrame([df.loc[300]]),dd=.05,zlim=(-5,100.),incl_gamma=True,gN=50, N=6,fname='zimmer_conflation_surf_yeast_d1', zero_conc=1,metric_name='%% Viable',title=None)  
plot_slices(pd.DataFrame([df.loc[339]]),dd=.05,zlim=(-5,100.),incl_gamma=True,gN=50, N=6,fname='zimmer_conflation_surf_yeast_d2', zero_conc=1,metric_name="%% Viable",title=None)
plot_surface(pd.DataFrame([df.loc[300]]),metric_name="Drug Effect",elev=41, azim=7,incl_gamma=True,fname='zimmer_conflation_surf_yeast_d1',
             zlim=(-5,100),data_pts=True,path='conflating_plot/Malarial_combinations/')
plot_surface(pd.DataFrame([df.loc[339]]),metric_name="Drug Effect",elev=41, azim=7,incl_gamma=True,fname='zimmer_conflation_surf_yeast_d2',
             zlim=(-5,100),data_pts=True,path='conflating_plot/Malarial_combinations/')

   


dif_df = pd.read_csv('save_combination_comparisions_8-31-18.csv')
dif_df = dif_df[~np.isnan(dif_df['val_zip'])]
dif_df = dif_df.sort_values('val_zip',ascending=True)

#For Yeast Dataset Zimmer
plot_slices(pd.DataFrame([df.loc[17]]),dd=.05,zlim=(-5,100.),incl_gamma=True,gN=50, N=6,fname=None, zero_conc=1,metric_name='%% Viable',title=None)  
plot_slices(pd.DataFrame([df.loc[152]]),dd=.1,zlim=(-5,100.),incl_gamma=True,gN=50, N=10,fname=None, zero_conc=1,metric_name="%% Viable",title=None)
plot_surface(pd.DataFrame([df.loc[17]]),metric_name="Drug Effect",elev=41, azim=7,incl_gamma=True,fname=None,
             zlim=(-5,100),data_pts=True,path='conflating_plot/Malarial_combinations/')
plot_surface(pd.DataFrame([df.loc[152]]),metric_name="Drug Effect",elev=41, azim=7,incl_gamma=True,fname=None,
             zlim=(-5,100),data_pts=True,path='conflating_plot/Malarial_combinations/')


df.loc[[17,152],['log_alpha1','log_alpha2','log_gamma1','log_gamma2']]

df = pd.read_csv('conflating_plot/Merck_cancer_combinations/MasterResults.csv')
df = df[df['MCMC_converge']==1]
df = df[df['R2']>.9]
df.reset_index(inplace=True,drop=True)
df['bliss'] = np.nan
df['hsa'] = np.nan

for indx in df.index:
    print indx
    expt= 'conflating_plot/Malarial_combinations/'+df['expt'].loc[indx]
    drug1_name = df['drug1_name'].loc[indx]
    drug2_name = df['drug2_name'].loc[indx]
    target_cell_line = df['cell_line'].loc[indx]
    d1,d2,dip,dip_sd = synergy_tools.get_data(expt,drug1_name,drug2_name,target_cell_line)
    C1 = np.power(10.,df['log_C1'].loc[indx])
    C2 = np.power(10.,df['log_C2'].loc[indx])
    h1 = df['h1'].loc[indx]
    h2 = df['h2'].loc[indx]
    df.loc[indx,'zimmer_a12'],df.loc[indx,'zimmer_a21']= synergy_tools.zimmer_old(d1,d2,dip/100., C1, C2, h1, h2,vector_input=True)[0:2]
    df.loc[indx,'zip']=synergy_tools.zip(d1,d2,dip/100.,vector_input=True)
    
    
dif_df = pd.DataFrame([(indx1,indx2) for indx1 in df.index for indx2 in np.arange(indx1+1,len(df))],columns=['indx1','indx2'])
for indx in dif_df.index:
    indx1=dif_df['indx1'].loc[indx]
    indx2=dif_df['indx2'].loc[indx]
    dif_df.loc[indx,'val_zim'] = np.abs(df[['zimmer_a21','zimmer_a12']].loc[indx2].min()-df[['zimmer_a21','zimmer_a12']].loc[indx1].min())+np.abs(df[['zimmer_a21','zimmer_a12']].loc[indx2].max()-df[['zimmer_a21','zimmer_a12']].loc[indx1].max())
    dif_df.loc[indx,'val_alp'] = np.abs(df[['log_alpha2','log_alpha1']].loc[indx2].min()-df[['log_alpha2','log_alpha1']].loc[indx1].min())+np.abs(df[['log_alpha2','log_alpha1']].loc[indx2].max()-df[['log_alpha2','log_alpha1']].loc[indx1].max())
    dif_df.loc[indx,'val_gam'] = np.abs(df[['log_gamma2','log_gamma1']].loc[indx2].min()-df[['log_gamma2','log_gamma1']].loc[indx1].min())+np.abs(df[['log_gamma2','log_gamma1']].loc[indx2].max()-df[['log_gamma2','log_gamma1']].loc[indx1].max())
    dif_df.loc[indx,'val_zip'] = np.abs(df['zip'].loc[indx2]-df['zip'].loc[indx1])

dif_df.to_csv('save_combination_comparisions_8-31-18.csv',index=False)
dif_df = dif_df[~np.isnan(dif_df['val_zim'])]
dif_df = dif_df.sort_values('val_zim',ascending=True)



#For Yeast Dataset Zimmer
plot_slices(pd.DataFrame([df.loc[300]]),dd=.05,zlim=(-5,100.),incl_gamma=True,gN=50, N=6, fname=None, zero_conc=1,metric_name='%% Viable',title=None)  
plot_slices(pd.DataFrame([df.loc[339]]),dd=.05,zlim=(-5,100.), incl_gamma=True,gN=50, N=6, fname=None, zero_conc=1,metric_name="%% Viable",title=None)
plot_surface(pd.DataFrame([df.loc[300]]),metric_name="Drug Effect",elev=41, azim=7,incl_gamma=True,
             zlim=(-5,100),data_pts=True,path='conflating_plot/Malarial_combinations/')
plot_surface(pd.DataFrame([df.loc[339]]),metric_name="Drug Effect",elev=41, azim=7,incl_gamma=True,
             zlim=(-5,100),data_pts=True,path='conflating_plot/Malarial_combinations/')











#
#df = df[~np.isnan(df[['zimmer_a12','zimmer_a21']].max(axis=1))]
#df = df[df[['zimmer_a12','zimmer_a21']].max(axis=1)<2]
#fig = plt.figure()
#ax = plt.subplot(121)
#ax.scatter(df[['log_gamma1','log_gamma2']].mean(axis=1),df[['log_alpha1','log_alpha2']].mean(axis=1),c=df[['zimmer_a12','zimmer_a21']].mean(axis=1))
#
#fig = plt.figure()
#ax = plt.subplot(121)
#ax.scatter(df['log_gamma1'],df['log_gamma2'],c=df['zimmer_a12'])
#ax = plt.subplot(122)
#ax.scatter(df['log_alpha1'],df['log_alpha2'],c=df[['zimmer_a12','zimmer_a21']].max(axis=1))
#



indx=119
expt= 'conflating_plot/Malarial_combinations/'+df['expt'].loc[indx]
drug1_name = df['drug1_name'].loc[indx]
drug2_name = df['drug2_name'].loc[indx]
target_cell_line = df['cell_line'].loc[indx]
d1,d2,dip,dip_sd = synergy_tools.get_data(expt,drug1_name,drug2_name,target_cell_line)
T = df.loc[indx].to_dict()
popt3 = [ T['E0'],
          T['E0_std'],
          T['E1'],
          T['E1_std'],
          T['E2'],
          T['E2_std'],
          T['E3'],
          T['E3_std'],
          T['r1'],
          T['r1_std'],
          T['r2'],
          T['r2_std'],
          10**T['log_C1'],
          np.log(10)*T['log_C1_std']*10**T['log_C1'],
          10**T['log_C2'],
          np.log(10)*T['log_C2_std']*10**T['log_C2'],
          T['h1'],
          T['h1_std'],
          T['h2'],
          T['h2_std'],
          10**T['log_alpha1'],
          np.log(10)*T['log_alpha1_std']*10**T['log_alpha1'],
          10**T['log_alpha2'],
          np.log(10)*T['log_alpha2_std']*10**T['log_alpha2'],
          10**T['log_gamma1'],
          np.log(10)*T['log_gamma1_std']*10**T['log_gamma1'],
          10**T['log_gamma2'],
          np.log(10)*T['log_gamma2_std']*10**T['log_gamma2']]
SC.DosePlots_PLY(d1,d2,dip,dip_sd,drug1_name,drug2_name,popt3[0:27:2],'NDB_hill',target_cell_line,df.loc[indx,'expt'],'Percent',os.getcwd())          


