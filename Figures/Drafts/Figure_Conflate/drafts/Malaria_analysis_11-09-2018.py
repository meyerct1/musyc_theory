#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Nov  9 10:00:40 2018

@author: xnmeyer
"""

import pandas as pd
import numpy as np
import matplotlib.pylab as plt

df1 = pd.read_csv('test_Malaria (copy)/MasterResults.csv')
df2 = pd.read_csv('test_Malaria/MasterResults.csv')

df = pd.concat([df1,df2],sort='True')
key = ['log_gamma1','log_gamma2','log_gamma1_std','log_gamma2_std','fit_gamma']
for k in key: df.loc[~np.isnan(df['fit_gamma']),k] = 0.


plt.figure()
ax = plt.subplot(111)
ax.hist(df1['R2'][~df1['R2'].isna()].values,bins=50,alpha=.5,color='r',label='fit gamma')
ax.hist(df2['R2'][~df2['R2'].isna()].values,bins=50,alpha=.5,color='b',label='no gamma')
plt.legend()



np.sum(df2['R2']>.9)