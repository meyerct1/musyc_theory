#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 14:11:32 2018

@author: xnmeyer
"""

#Code to create plots for conflation figure 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
from adjustText import adjust_text
import scipy.stats as st
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from mpl_toolkits.mplot3d import Axes3D
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface,matplotlibDoseResponseSlices
from SynergyCalculator.gatherData import subset_data
#Calculate other synergymetrics using calcOtherSynergy function in each folder


##############################################################################
##############################################################################
#Begin with Merck Anti-cancer combination analysis
T = pd.read_csv('MasterResults_otherSynergyMetrics_merck.csv')
#Generate graphs
#T = T[~T['loewe_fit_ec50'].isna()]
#Remove the samples which did not converge past initial_fit
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)

def plotBox(T,key,ylim):
    plt.figure(facecolor='w',figsize=(2.75,2.5))
    ax = plt.subplot(111)
    p = []
    mask = []
    sub_T = T[~T[key+'_fit_ec50'].isna()]
    mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']>0.)&(sub_T['beta']>0.))
    mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']<0.))
    mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']>0.))
    mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']>0.)&(sub_T['beta']<0.))
    ymin=0;ymax=0;
    for e in range(4):
        p.append(plt.boxplot(sub_T[key+'_fit_ec50'].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
        ymin = np.min((ymin,sub_T[key+'_fit_ec50'].loc[mask[e]].min()))
        ymax = np.max((ymax,sub_T[key+'_fit_ec50'].loc[mask[e]].max()))
    
    for e,bp in enumerate(p):
        plt.setp(bp['medians'],color='r',linewidth=2)
        plt.setp(bp['fliers'],color='k')
        plt.setp(bp['whiskers'],color='k')
        plt.setp(bp['caps'],color='k')
        plt.setp(bp['boxes'],edgecolor='k')
        
        for patch in bp['boxes']:
            patch.set(facecolor=[.25,.25,.25])    

    plt.xlim((-.5,3.5))
    if ylim is None:
        plt.ylim((ymin,ymax))
    else:
        plt.ylim(ylim)
    plt.xticks(range(4))
    ax.set_xticklabels(['$\\alpha_1,\\alpha_2>0$\n$\\beta>0$',
                        '$\\alpha_1,\\alpha_2<0$\n$\\beta<0$',
                        '$\\alpha_1,\\alpha_2<0$\n$\\beta>0$',
                        '$\\alpha_1,\\alpha_2>0$\n$\\beta<0$'
                     ])
    
    ax.axhline(y=0.,color='k',linestyle='--',linewidth=3)
    ax.fill([ax.get_xlim()[0],ax.get_xlim()[1],ax.get_xlim()[1],ax.get_xlim()[0]],[0,0,ax.get_ylim()[1],ax.get_ylim()[1]],color='yellow',alpha=.5,zorder=-10)
    ax.fill([ax.get_xlim()[0],ax.get_xlim()[1],ax.get_xlim()[1],ax.get_xlim()[0]],[0,0,ax.get_ylim()[0],ax.get_ylim()[0]],color='b',alpha=.5,zorder=-10)
    
    ax.text(3.6,.05,'<--Ant    ' + key.upper()+'    Syn-->',     rotation=90,ha='left',va='center')
    plt.ylabel(key.upper() +' @ EC50',labelpad=0)
    plt.tight_layout()
    plt.savefig(key+'_fit_ec50_conflation.pdf', bbox_inches = 'tight',pad_inches = 0)
    return (ymin,ymax)

key = 'hsa'
ylim = (-.5, .5)
plotBox(T,key,ylim)

key = 'bliss'
ylim = (-0.68, 0.68)
plotBox(T,key,ylim)

key = 'loewe'
ylim = (-1.1,1.1)
plotBox(T,key,ylim)




#Find two surfaces which are very similar by HSA and different in beta and alpha
#Remove the samples which did not converge past initial_fit
from scipy.spatial.distance import pdist
from sklearn.preprocessing import normalize
mat2 = normalize(T[['log_alpha1','log_alpha2','beta_obs']])
for cl in T['sample'].unique():
    mask = T['sample']==cl  
    sub_T = T[mask]
    sub_T = sub_T.reset_index(drop=True)
    d1 = pdist(np.array(sub_T['hsa_fit_ec50']).reshape((-1,1)))
    d2 = pdist(mat2[mask])    
    diff = d2-d1
    sort_index = np.argsort(diff)
    bst10 = sort_index[-2:]    
    lktab = np.zeros((len(d1),2))
    cnt = 0
    for i in range(len(sub_T)):
        for j in np.arange(i+1,len(sub_T)):
            lktab[cnt,0]=i
            lktab[cnt,1]=j
            cnt = cnt + 1          
    bst10_ind = [lktab[ind,:] for ind in bst10]
    for i in range(2):
        if sub_T['hsa_fit_ec50'].loc[[bst10_ind[i][0],bst10_ind[i][1]]].max()<0.:
           if np.diff(np.sign(sub_T['beta'].loc[[bst10_ind[i][0],bst10_ind[i][1]]]))[0]==2:     
                print cl
                print i
                print sub_T[['log_alpha1','log_alpha2','beta_obs','hsa_fit_ec50','drug1_name','drug2_name','sample']].loc[[bst10_ind[i][0],bst10_ind[i][1]]]
          
            

#Select MDAMB436 (doxorubicin and mk-4827) and (l778123 and etoposide)            
#Find antagonistic by hsa synergistic by beta antagonistic by alpha
#MDAMB436
#      log_alpha1  log_alpha2  beta_obs  hsa_fit_ec50   drug1_name drug2_name    sample
#22.0    0.626728   -4.000000 -0.180228     -0.423390  doxorubicin    mk-4827  MDAMB436
#32.0   -0.168941    1.083052  0.159145     -0.428632      l778123  etoposide  MDAMB436

sub_T = T[(T['drug1_name']=='doxorubicin')&(T['drug2_name']=='mk-4827')&(T['sample']=='MDAMB436')]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
matplotlibDoseResponseSurface(T,d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.0), zero_conc=0,title=None,d1lim=(-3,-1.2),d2lim=(-2,-.1))
    
sub_T = T[(T['drug1_name']=='l778123')&(T['drug2_name']=='etoposide')&(T['sample']=='MDAMB436')]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
matplotlibDoseResponseSurface(T,d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.0), zero_conc=0,title=None,d1lim=(-3,-1.2),d2lim=(-2,-.1))
    
            
##################################################################################
#Find antagonistic by hsa synergistic by beta antagonistic by alpha
sub_T = T[(T['hsa_fit_ec50']<0.)&(T['beta']>0.3)&(T['beta_obs']>0.3)&(T['log_alpha1']<0.)&(T['log_alpha2']<0)]
sub_T = sub_T[['hsa_fit_ec50','beta','beta_obs','log_alpha1','log_alpha2','drug1_name','drug2_name','sample']]
print sub_T.sort_values('hsa_fit_ec50')

sub_T =T[(T['drug1_name']=='paclitaxel')&(T['drug2_name']=='abt-888')&(T['sample']=='SKOV3')].loc[8056]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
matplotlibDoseResponseSurface(T,d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.1), zero_conc=0,title=None)









T = T[~T['loewe_fit_ec50'].isna()]

plt.figure(facecolor='w',figsize=(2.75,2.5))
ax=plt.subplot(111)

mask = []
mask.append((T['hsa_fit_ec50']>0.)&(T['loewe_fit_ec50']>0.)&(T['bliss_fit_ec50']>0.))
mask.append((T['hsa_fit_ec50']<0.)&(T['loewe_fit_ec50']<0.)&(T['bliss_fit_ec50']<0.))
mask.append((~mask[0])&(~mask[1]))
keys = ['log_alpha1','log_alpha2','beta']
labkeys = [r'$log(\alpha_1)$',r'$log(\alpha_2)$']
c = ['yellow','purple','blue','green','grey','pink']
x1= sum((T.loc[mask[0],'log_alpha1']<0) & (T.loc[mask[0],'log_alpha2']<0) & (T.loc[mask[0],'beta']<0))/float(sum(mask[0]))
x2= sum((T.loc[mask[0],'log_alpha1']>0) & (T.loc[mask[0],'log_alpha2']>0) & (T.loc[mask[0],'beta']>0))/float(sum(mask[0]))
x3= sum((T.loc[mask[0],'log_alpha1']>0) & (T.loc[mask[0],'log_alpha2']>0) & (T.loc[mask[0],'beta']<0))/float(sum(mask[0]))
x4= sum((T.loc[mask[0],'log_alpha1']<0) & (T.loc[mask[0],'log_alpha2']<0) & (T.loc[mask[0],'beta']>0))/float(sum(mask[0]))
x5= sum((((T.loc[mask[0],'log_alpha1']<0) & (T.loc[mask[0],'log_alpha2']>0)) | ((T.loc[mask[0],'log_alpha1']>0) & (T.loc[mask[0],'log_alpha2']<0)))  & (T.loc[mask[0],'beta']>0))/float(sum(mask[0]))
x6= sum((((T.loc[mask[0],'log_alpha1']<0) & (T.loc[mask[0],'log_alpha2']>0)) | ((T.loc[mask[0],'log_alpha1']>0) & (T.loc[mask[0],'log_alpha2']<0)))  & (T.loc[mask[0],'beta']<0))/float(sum(mask[0]))

plt.bar(.2,x1,color=c[0],width=.8,label=r'$log(\alpha_1),log(\alpha_2),\beta<0$')
plt.bar(.2,x2,bottom=x1,color=c[1],width=.8,label=r'$log(\alpha_1),log(\alpha_2),\beta>0$')
plt.bar(.2,x3,bottom=x1+x2,color=c[2],width=.8,label=r'$log(\alpha_1),log(\alpha_2)>0,\beta<0$')
plt.bar(.2,x4,bottom=x1+x2+x3,color=c[3],width=.8,label=r'$log(\alpha_1),log(\alpha_2)<0,\beta>0$')
plt.bar(.2,x5,bottom=x1+x2+x3+x4,color=c[4],width=.8,label=r'$log(\alpha_1)<0,log(\alpha_2)>0,\beta>0$')
plt.bar(.2,x6,bottom=x1+x2+x3+x4+x5,color=c[5],width=.8,label=r'$log(\alpha_1)<0,log(\alpha_2)>0,\beta<0$')

x1= sum((T.loc[mask[1],'log_alpha1']<0) & (T.loc[mask[1],'log_alpha2']<0) & (T.loc[mask[1],'beta']<0))/float(sum(mask[1]))
x2= sum((T.loc[mask[1],'log_alpha1']>0) & (T.loc[mask[1],'log_alpha2']>0) & (T.loc[mask[1],'beta']>0))/float(sum(mask[1]))
x3= sum((T.loc[mask[1],'log_alpha1']>0) & (T.loc[mask[1],'log_alpha2']>0) & (T.loc[mask[1],'beta']<0))/float(sum(mask[1]))
x4= sum((T.loc[mask[1],'log_alpha1']<0) & (T.loc[mask[1],'log_alpha2']<0) & (T.loc[mask[1],'beta']>0))/float(sum(mask[1]))
x5= sum((((T.loc[mask[1],'log_alpha1']<0) & (T.loc[mask[1],'log_alpha2']>0)) | ((T.loc[mask[1],'log_alpha1']>0) & (T.loc[mask[1],'log_alpha2']<0)))  & (T.loc[mask[1],'beta']>0))/float(sum(mask[1]))
x6= sum((((T.loc[mask[1],'log_alpha1']<0) & (T.loc[mask[1],'log_alpha2']>0)) | ((T.loc[mask[1],'log_alpha1']>0) & (T.loc[mask[1],'log_alpha2']<0)))  & (T.loc[mask[1],'beta']<0))/float(sum(mask[1]))

plt.bar(1.2,x1,color=c[0],width=.8)
plt.bar(1.2,x2,bottom=x1,color=c[1],width=.8)
plt.bar(1.2,x3,bottom=x1+x2,color=c[2],width=.8)
plt.bar(1.2,x4,bottom=x1+x2+x3,color=c[3],width=.8)
plt.bar(1.2,x5,bottom=x1+x2+x3+x4,color=c[4],width=.8)
plt.bar(1.2,x6,bottom=x1+x2+x3+x4+x5,color=c[5],width=.8)


x1= sum((T.loc[mask[2],'log_alpha1']<0) & (T.loc[mask[2],'log_alpha2']<0) & (T.loc[mask[2],'beta']<0))/float(sum(mask[2]))
x2= sum((T.loc[mask[2],'log_alpha1']>0) & (T.loc[mask[2],'log_alpha2']>0) & (T.loc[mask[2],'beta']>0))/float(sum(mask[2]))
x3= sum((T.loc[mask[2],'log_alpha1']>0) & (T.loc[mask[2],'log_alpha2']>0) & (T.loc[mask[2],'beta']<0))/float(sum(mask[2]))
x4= sum((T.loc[mask[2],'log_alpha1']<0) & (T.loc[mask[2],'log_alpha2']<0) & (T.loc[mask[2],'beta']>0))/float(sum(mask[2]))
x5= sum((((T.loc[mask[2],'log_alpha1']<0) & (T.loc[mask[2],'log_alpha2']>0)) | ((T.loc[mask[2],'log_alpha1']>0) & (T.loc[mask[2],'log_alpha2']<0)))  & (T.loc[mask[2],'beta']>0))/float(sum(mask[2]))
x6= sum((((T.loc[mask[2],'log_alpha1']<0) & (T.loc[mask[2],'log_alpha2']>0)) | ((T.loc[mask[2],'log_alpha1']>0) & (T.loc[mask[2],'log_alpha2']<0)))  & (T.loc[mask[2],'beta']<0))/float(sum(mask[2]))

plt.bar(2.2,x1,color=c[0],width=.8)
plt.bar(2.2,x2,bottom=x1,color=c[1],width=.8)
plt.bar(2.2,x3,bottom=x1+x2,color=c[2],width=.8)
plt.bar(2.2,x4,bottom=x1+x2+x3,color=c[3],width=.8)
plt.bar(2.2,x5,bottom=x1+x2+x3+x4,color=c[4],width=.8)
plt.bar(2.2,x6,bottom=x1+x2+x3+x4+x5,color=c[5],width=.8)

plt.ylabel('Percent of combinations')
plt.xlim(0,3.2)
plt.xticks([.5,1.5,2.5])
ax.set_xticklabels(['Synergistic\nby HSA,Bliss','Antagonistic','Conflicting'])
ax.legend(bbox_to_anchor=(.92, 2))
plt.tight_layout()
plt.savefig('syn_trends_merck.pdf', bbox_inches = 'tight',pad_inches = 0)








    

################
#Show global trends in synergy can be understood by MuSyC
def plotBoxSyn(T,ylim):
    plt.figure(facecolor='w',figsize=(2.75,2.5))
    ax = []
    ax.append(plt.subplot2grid((8,3),(0,0),rowspan=7))
    ax.append(plt.subplot2grid((8,3),(0,1),rowspan=7))
    ax.append(plt.subplot2grid((8,3),(0,2),rowspan=7))
    ax.append(plt.subplot2grid((8,3),(7,0),colspan=3))
    
    mask = []
    mask.append((T['hsa_fit_ec50']>0.)&(T['bliss_fit_ec50']>0.)&(T['loewe_fit_ec50']>0.))
    mask.append((T['hsa_fit_ec50']<0.)&(T['bliss_fit_ec50']<0.)&(T['loewe_fit_ec50']<0.))
    mask.append((~mask[0])&(~mask[1]))
    keys = ['beta','log_alpha1','log_alpha2']
    labkeys = [r'$\beta$',r'$log(\alpha_1)$',r'$log(\alpha_2)$']
    c = ['yellow','b','green']
    for e1,k in enumerate(keys):
        ymin=0;ymax=0;
        plt.sca(ax[e1])
        p = []
        for e in range(3):
            p.append(plt.boxplot(T[k].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
            ymin = np.min((ymin,T[k].loc[mask[e]].min()))
            ymax = np.max((ymax,T[k].loc[mask[e]].max()))
        
        for e,bp in enumerate(p):
            plt.setp(bp['medians'],color='r',linewidth=2)
#            plt.setp(bp['fliers'],color='k')
            plt.setp(bp['whiskers'],color='k')
            plt.setp(bp['caps'],color='k')
            plt.setp(bp['boxes'],edgecolor='k')
            plt.setp(bp['boxes'],facecolor=c[e])
    
        plt.xlim((-.75,2.75))
        if ylim is not None:
            (ymin,ymax)=ylim[e1]            
        plt.ylim((ymin,ymax))
        plt.xticks(range(3))
        ax[e1].set_xticklabels([])
        ax[e1].set_yticks([ymin,0,ymax])
        ax[e1].axhline(y=0.,color='k',linestyle='--',linewidth=1)
#        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[1],ax[e1].get_ylim()[1]],color='r',alpha=.5,zorder=-10)
#        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[0],ax[e1].get_ylim()[0]],color='b',alpha=.5,zorder=-10)
        
        plt.title(labkeys[e1])
    
    plt.tight_layout()
    plt.subplots_adjust(wspace=0.4, hspace=.2)
    
    ax[-1].spines['right'].set_visible(False)
    ax[-1].spines['top'].set_visible(False)
    ax[-1].spines['bottom'].set_visible(False)
    ax[-1].spines['left'].set_visible(False)
    ax[-1].set_xticks([])
    ax[-1].set_yticks([])
    ax[-1].scatter(.15,.75,s=50,marker='s',c='yellow')
    ax[-1].scatter(.5,.75,s=50,marker='s',c='b')
    ax[-1].scatter(.85,.75,s=50,marker='s',c='green')
    ax[-1].text(.15,.25,'Synergistic',ha='center',va='top')
    ax[-1].text(.5,.25,'Antagonistic\nby HSA, Loewe, Bliss @ EC50',ha='center',va='top')
    ax[-1].text(.85,.25,'Conflicting',ha='center',va='top')
    ax[-1].set_ylim((0,1))
    ax[-1].set_xlim((0,1))
    plt.savefig('syn_trends.pdf', bbox_inches = 'tight',pad_inches = 0)
    return (ymin,ymax)

ylim=((-1,1),(-2,2),(-2,2))
plotBoxSyn(T,ylim)


#
#
#
##Calculate HSA along the line of equal dose
##Calculate HSA at constant-ratio dose from fit
#from SynergyCalculator.calcOtherSynergyMetrics import get_params
#from SynergyCalculator.NDHillFun import Edrug2D_NDB_hill, Edrug1D
#mat = np.zeros((100,len(T)))
#for i in T.index:
#    E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, C1, C2, alpha1, alpha2,gamma1,gamma2, d1min, d1max, d2min, d2max, drug1_name, drug2_name, expt = get_params(pd.DataFrame([T.loc[i]]))
#    C1 = C1/1000. if h1>20 else C1
#    C2 = C2/1000. if h2>20 else C2
#    d1c = np.logspace(d1min,d1max,100)
#    d2c = np.logspace(d2min,d2max,100)
##    if C2>C1:
##        d1c = np.logspace(d1min,d1max,100)
##        d2c = C2/C1*d1c
##    else:
##        d2c = np.logspace(d2min,d2max,100)
##        d1c = C1/C2*d2c 
#    #To avoid ValueError in calculating E if h is really large scale the dose and ec50.
#    d1c = d1c/1000. if h1>20. else d1c
#    d2c = d2c/1000. if h1>20. else d2c
#    E = Edrug2D_NDB_hill((d1c,d2c),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
#    if not np.isnan(E).any():
#        mat[:,i] = np.min((Edrug1D(d1c,E0,E1,C1,h1),Edrug1D(d2c,E0,E2,C2,h2)),axis=0)-E
#       
#
#plt.figure(facecolor='w',figsize=(5,2.5))
#ax = []
#ax.append(plt.subplot2grid((4,2),(0,0),rowspan=2))
#ax.append(plt.subplot2grid((4,2),(0,1),rowspan=2))
#ax.append(plt.subplot2grid((4,3),(2,0),rowspan=2,colspan=2))
#plt.sca(ax[0])
#ax[0].spines['right'].set_visible(False)
#ax[0].spines['top'].set_visible(False)
#ax[0].yaxis.set_ticks_position('left')
#ax[0].xaxis.set_ticks_position('bottom')
#ax[0].plot((0,1),(0,.85),'k',linewidth=2)
#ax[0].scatter((.1), (.085),c=['r'],s=100,marker='o')
#ax[0].scatter((.5), (.425),c=['b'],s=100,marker='o')
#ax[0].scatter((1.), (.85),c=['m'],s=100,marker='o')
#xmin = ax[0].get_xlim()[0]
#ymin = ax[0].get_ylim()[0]
#ax[0].plot((0.1,0.1),(ymin,0.085),'r--',linewidth=2)
#ax[0].plot((0.5,0.5),(ymin,0.425),'b--',linewidth=2)
#ax[0].plot((1.0,1.0),(ymin,0.850),'m--',linewidth=2)
#
#ax[0].plot((xmin,0.1),(0.085,0.085),'r--',linewidth=2)
#ax[0].plot((xmin,0.5),(0.425,0.425),'b--',linewidth=2)
#ax[0].plot((xmin,1.0),(0.850,0.850),'m--',linewidth=2)
#
#ax[0].set_xticks([])
#ax[0].set_yticks([])
#ax[0].set_xlabel('[drug1]')
#ax[0].set_ylabel('[drug2]')
#ax[1].spines['right'].set_visible(False)
#ax[1].spines['top'].set_visible(False)
#ax[1].spines['bottom'].set_visible(False)
#ax[1].spines['left'].set_visible(False)
#ax[1].set_xticks([])
#ax[1].set_yticks([])
#ax[1].scatter(0.1,.85,s=50,marker='o',c='r')
#ax[1].scatter(0.1,.6,s=50,marker='o',c='b')
#ax[1].scatter(0.1,.35,s=50,marker='o',c='m')
#ax[1].text(.7,.85,r'min(d1),$\frac{C2}{C1}$*min(d2)',ha='center',va='center',rotation=0)
#ax[1].text(.7,.6,'(C1,C2)',ha='center',va='center',rotation=0)
#ax[1].text(.7,.35,r'max(d1),$\frac{C2}{C1}$*max(d2)',ha='center',va='center',rotation=0)
#ax[1].plot((0,.2),(.1,.1),'k',linewidth=2)
#ax[1].text(.7,.0,'Constant-ratio\ncombination',ha='center',va='center')
#ax[1].set_ylim((0,1))
#ax[1].set_xlim((0,1))
#plt.sca(ax[2])
#plt.title('Synergistic by HSA')
#plt.xlabel('[d1]/[d2]')
#from statsmodels.robust import mad
#x = np.linspace(0,1,mat.shape[0])
#y = np.zeros(mat.shape[0]) 
#from scipy.stats import wilcoxon
#for i in range(mat.shape[0]):
#    y[i] =  np.log10(wilcoxon(T['beta'],mat[i,:]).pvalue)
#
#
#plt.plot(x,y,'k',label='log(p-val)',lw=3)
#
#
#ax[2].set_ylabel(r'$\beta$',labelpad=-2)
##ax[2].set_ylim((-.3,.3))
##ax[2].set_yticks([-.2,0,.2])
##ax[2].legend(loc='lower right')
#ax[2].axhline(y=0,c='k',lw=2,linestyle='--')
#
#plt.tight_layout()
#plt.subplots_adjust(wspace=0.6, hspace=.8)
#plt.savefig('hsa_beta_trends.pdf', bbox_inches = 'tight',pad_inches = 0)
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#





##############################################################################
##Junk pile
##############################################################################

#
#
#
#
##Calculate HSA along the line of equal dose
#
#plt.figure(facecolor='w',figsize=(5,2.5))
#ax = []
#ax.append([])
#ax.append(plt.subplot2grid((4,2),(0,0),rowspan=4))
#ax.append(plt.subplot2grid((4,2),(0,1),rowspan=2,colspan=2))
#ax.append(plt.subplot2grid((4,2),(2,1),rowspan=2,colspan=2))
#ax[1].spines['right'].set_visible(False)
#ax[1].spines['top'].set_visible(False)
#ax[1].spines['bottom'].set_visible(False)
#ax[1].spines['left'].set_visible(False)
#ax[1].set_xticks([])
#ax[1].set_yticks([])
#ax[1].scatter(0.1,.85,s=50,marker='o',c='r')
#ax[1].scatter(0.1,.6,s=50,marker='o',c='b')
#ax[1].scatter(0.1,.35,s=50,marker='o',c='m')
#ax[1].text(.6,.85,'Non-zero\nmin(d1),min(d2)',ha='center',va='center',rotation=0)
#ax[1].text(.6,.6,'EC50(d1,d2)',ha='center',va='center',rotation=0)
#ax[1].text(.6,.35,'max(d1),max(d2)',ha='center',va='center',rotation=0)
#ax[1].plot((0,.2),(.1,.1),'k',linewidth=2)
#ax[1].text(.6,.1,'Constant-ratio\ncombination',ha='center',va='center')
#ax[1].set_ylim((0,1))
#ax[1].set_xlim((0,1))
#
#
##Calculate HSA at constant-ratio dose from fit
#from SynergyCalculator.calcOtherSynergyMetrics import get_params
#from SynergyCalculator.NDHillFun import Edrug2D_NDB_hill, Edrug1D
#mat = np.zeros((3,len(T)))
#for i in T.index:
#    E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, C1, C2, alpha1, alpha2,gamma1,gamma2, d1min, d1max, d2min, d2max, drug1_name, drug2_name, expt = get_params(pd.DataFrame([T.loc[i]]))
#    C1 = C1/1000. if h1>20 else C1
#    C2 = C2/1000. if h2>20 else C2
#    d1c = np.array([10**d1min,C1,10**d1max])
#    d2c = np.array([10**d2min,C2,10**d2max])
#
#    #To avoid ValueError in calculating E if h is really large scale the dose and ec50.
#    d1 = d1/1000. if h1>20. else d1;d1c = d1c/1000. if h1>20. else d1c
#    d2 = d2/1000. if h2>20. else d2;d2c = d2c/1000. if h1>20. else d2c
#    E = Edrug2D_NDB_hill((d1c,d2c),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
#    if not np.isnan(E).any():
#        mat[:,i] = np.min((Edrug1D(d1c,E0,E1,C1,h1),Edrug1D(d2c,E0,E2,C2,h2)),axis=0)-E
#        
#        
#
#plt.sca(ax[2])
#plt.title('Synergistic by HSA')
#p = []
#c = ['r','b','m']
#for e in range(mat.shape[0]):
#    p.append(plt.boxplot(T['beta_obs'].loc[mat[e,:]>0].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
#
#for e,bp in enumerate(p):
#    plt.setp(bp['medians'],color='r',linewidth=2)
##            plt.setp(bp['fliers'],color='k')
#    plt.setp(bp['whiskers'],color='k')
#    plt.setp(bp['caps'],color='k')
#    plt.setp(bp['boxes'],edgecolor='k')
#    plt.setp(bp['boxes'],facecolor=c[e])
#
#
#plt.sca(ax[3])
#p=[]
#plt.title('Anagonistic by HSA')
#plt.xlabel('[d1]/[d2]',labelpad=-3)
#for e in range(mat.shape[0]):
#    p.append(plt.boxplot(T['beta_obs'].loc[mat[e,:]<0].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
#
#for e,bp in enumerate(p):
#    plt.setp(bp['medians'],color='r',linewidth=2)
##            plt.setp(bp['fliers'],color='k')
#    plt.setp(bp['whiskers'],color='k')
#    plt.setp(bp['caps'],color='k')
#    plt.setp(bp['boxes'],edgecolor='k')
#    plt.setp(bp['boxes'],facecolor=c[e])
#
#plt.sca(ax[2])
#ax[2].set_ylabel(r'$\beta$',labelpad=-2)
#ax[2].set_ylim((-.2,.2))
#ax[2].set_xlim((-.25,2.25))
#ax[2].set_yticks([-.2,0,.2])
#ax[2].set_xlabel([])
#ax[2].set_xticks([])
#ax[2].set_xticklabels([])
#ax[2].axhline(y=0,c='k',lw=2,linestyle='--')
#
#ax[3].set_ylabel(r'$\beta$',labelpad=-2)
#ax[3].set_ylim((-.2,.2))
#ax[3].set_yticks([-.4,0,.4])
#ax[3].set_xticks([0,1])
#ax[3].set_xlim((-.25,2.25))
#ax[3].axhline(y=0,c='k',lw=2,linestyle='--')
#
#plt.tight_layout()
#plt.subplots_adjust(wspace=0.4, hspace=.8)
#plt.savefig('hsa_beta_trends.pdf', bbox_inches = 'tight',pad_inches = 0)
#
#
#
