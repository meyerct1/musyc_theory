#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 08:51:16 2018

@author: xnmeyer
"""

import pandas as pd
import numpy as np
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface,matplotlibDoseResponseSlices
from SynergyCalculator.gatherData import subset_data
import os

T = pd.read_csv('../../Data/oneil_anticancer/MasterResults_otherSynergyMetrics_merck.csv')
T# = T[(T['R2']>.98)]
T = T[(T['drug1_name']=='l778123')&(T['drug2_name']=='mk-8669')]
T = T.reset_index(drop=True)
for ind in T.index:
    sub_T       = T.loc[ind].to_dict()
    sub_T['to_save_plots'] = 1
    #expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
    expt        = '../../Data/oneil_anticancer/merck_perVia_10-29-2018.csv'
    sub_T['save_direc'] = '../../Data/oneil_anticancer'
    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1'] = data['drug1'].str.lower()
    data['drug2'] = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
    plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
    
    #matplotlibDoseResponseSurface(T,d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0,1.1), zero_conc=0,title=None,d1lim=None,d2lim=None)
    #fig2, [ax1,ax2,ax11,ax22,ax111,ax222] = matplotlibDoseResponseSlices(T,fname=None,zlim=(0.,1.0),zero_conc=1,title=None)
    
    
    
    
    
    
    
    
    
    #drug1_name+'_'+drug2_name+'_'+sample