#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 30 15:49:16 2018

@author: xnmeyer
"""

#Code to create plots for conflation figure 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
from adjustText import adjust_text
import scipy.stats as st
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from mpl_toolkits.mplot3d import Axes3D
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface,matplotlibDoseResponseSlices
from SynergyCalculator.gatherData import subset_data
#Calculate other synergymetrics using calcOtherSynergy function in each folder


##############################################################################
##############################################################################
#Begin with Merck Anti-cancer combination analysis
T = pd.read_csv('../../Data/oneil_anticancer/MasterResults_otherSynergyMetrics_merck.csv')
#Begin with Merck Anti-cancer combination analysis
#Generate graphs
#T = T[~T['loewe_fit_ec50'].isna()]
#Remove the samples which did not converge past initial_fit
T = T[(T['R2']>.8)]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)

##Create matrix
#g = T.groupby(['drug1_name','drug2_name'])
#np.where(((g['beta_obs'].mean()-2*g['beta_obs'].std())>0))
#g = pd.DataFrame(g['beta_obs'].mean()-g['beta_obs'].std()).reset_index(drop=False)
#g.loc[414]
#
#
##find promiscuous synergizers
#drgs = np.sort(np.unique((T['drug2_name'].unique(),T['drug1_name'].unique())))
#d_cnt = []
#for d in drgs:
#    msk = (T['drug1_name']==d) | (T['drug2_name']==d)
#    d_cnt.append(sum(T.loc[msk,'beta_obs']>0.05)/float(sum(msk)))
#   
#tmp = pd.DataFrame({'drugs':drgs,'perct':d_cnt})
#tmp.sort_values('perct')
#



drgs_mech = pd.read_csv('drug_classes.csv')
drgs_mech = drgs_mech[np.in1d(drgs_mech['class'],['Kinase'])]
drgs = drgs_mech.sort_values('subclass')['drug_name'].values
import seaborn as sns
#create interaction map
cnt = 0
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
            cnt+=1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T['beta'].median() #sum(sub_T['beta_obs']>.05)/float(sum(msk))
    tmp = drgs_mech[drgs_mech['drug_name']==drgs[e1]]['subclass'].values[0]        
    if tmp == 'Cell Cycle':
        cols.append('green')
    elif tmp == 'PI3K_AKT':
        cols.append('yellow')
    elif tmp =='Multi-kinase':
        cols.append('blue')
    elif tmp=='MAPK':
        cols.append('purple')
    elif tmp=='AMPK':
        cols.append('black')
            
d_mat[d_mat==-1]=np.nan
plt.figure(facecolor='w',figsize=(2.5,2.5))
ax = []
ax.append(plt.subplot2grid((10,1),(0,0),rowspan=9))
ax.append(plt.subplot2grid((10,1),(9,0)))
plt.subplots_adjust(hspace=0)
plt.sca(ax[0])
g1 = sns.heatmap(pd.DataFrame(d_mat,index=drgs,columns=drgs),linewidths=.25,vmin=-.15,vmax=.15)
plt.xticks([])
plt.sca(ax[1])
ax[1].scatter(range(len(drgs)),np.zeros(len(drgs)),c=cols,marker='s',s=35,edgecolor='face')
ax[1].axis('off')
plt.savefig('merck_promiscuous_synergizers.pdf', bbox_inches = 'tight',pad_inches = 0)


###############################################################################
###############################################################################



plt.figure(facecolor='w',figsize=(4.5,2))
ax = []
ax.append(plt.subplot2grid((8,3),(0,0),rowspan=6))
ax.append(plt.subplot2grid((8,3),(0,1),rowspan=6))
ax.append(plt.subplot2grid((8,3),(0,2),rowspan=6))
ax.append(plt.subplot2grid((8,3),(6,0),colspan=3,rowspan=2))

plt.sca(ax[0])
ylim=None
key = 'beta'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = ['green','yellow','blue']
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)

plt.sca(ax[1])
ylim=None
key = 'log_alpha'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)-1):
    for e2 in np.arange(e1+1,len(drgs)):
        msk1 = (T['drug1_name']==drgs[e1]) & (T['drug2_name']==drgs[e2])
        msk2 = (T['drug2_name']==drgs[e1]) & (T['drug1_name']==drgs[e2])
        if sum(msk1)>sum(msk2):
            sub_T = T.loc[msk1]
            d_mat[e1,e2] = sub_T['log_alpha1'].median()
            d_mat[e2,e1] = sub_T['log_alpha2'].median()
        else:
            sub_T = T.loc[msk2]
            d_mat[e2,e1] = sub_T['log_alpha1'].median()
            d_mat[e1,e2] = sub_T['log_alpha2'].median()
            
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = ['green','yellow','blue']
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)




plt.sca(ax[2])
ylim=None
key = 'hsa_fit_ec50'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<5 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = ['green','yellow','blue']
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
import matplotlib.ticker as ticker

ax[2].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))




ax[-1].spines['right'].set_visible(False)
ax[-1].spines['top'].set_visible(False)
ax[-1].spines['bottom'].set_visible(False)
ax[-1].spines['left'].set_visible(False)
ax[-1].set_xticks([])
ax[-1].set_yticks([])
ax[-1].scatter(.15,.85,s=50,marker='s',c='green')
ax[-1].scatter(.5,.85,s=50,marker='s',c='yellow')
ax[-1].scatter(.85,.85,s=50,marker='s',c='blue')
ax[-1].text(.15,.65,'Cell Cycle\n+\nCell Cycle',ha='center',va='top')
ax[-1].text(.5,.65,'Cell Cycle\n+\nAKT/MAPK/Mult-Kinase',ha='center',va='top')
ax[-1].text(.85,.65,'AKT/MAPK/Multi-Kinase\n+\nAKT/MAPK/Multi-Kinase',ha='center',va='top')
ax[-1].set_ylim((0,1))
ax[-1].set_xlim((0,1))
ax[0].set_title(r'$\beta$')
ax[1].set_title(r'log($\alpha$)')
ax[2].set_title(r'HSA @ EC50')
plt.tight_layout()
plt.subplots_adjust(hspace=.0,wspace=.25)
plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)




ax.set_xticklabels(['Cell Cycle\n+Cell Cycle','Cell Cycle\n+AKT/MAPK/Multi-kinase','AKT/MAPK/Multi-kinase\n+AKT/MAPK/Multi-kinase'])









#
#
#
#
#
#plt.figure()
#d_mat[np.isnan(d_mat)]=0
#col_cols = pd.DataFrame({'Pathway':cols},index=drgs)
##Cluster using ward linkage and cosine distance
#metric='euclidean'
#method='ward'
##Cluster
#g1 = sns.clustermap(pd.DataFrame(d_mat,index=drgs,columns=drgs),metric=metric,method=method,col_colors=col_cols,robust=False, annot_kws={"size": 8},figsize=(2.5,2.5))
##plt.setp(g1.ax_heatmap.yaxis.get_majorticklabels(), rotation=0)
#g1.ax_heatmap.xaxis.set_ticks(range(len(drgs)))
#g1.ax_heatmap.yaxis.set_ticks(range(len(drgs)))
#
#g1.ax_heatmap.xaxis.set_ticklabels(drgs[g1.dendrogram_col.reordered_ind],fontsize=8)
#g1.ax_heatmap.yaxis.set_ticklabels(drgs[g1.dendrogram_row.reordered_ind],fontsize=8)
#
##g1.ax_heatmap.xaxis.set_label('Samples')
##g1.ax_heatmap.yaxis.set_label('Genes')
##g1.ax_heatmap.yaxis.set_ticklabels([])
#
#
#
#
#
#tmp = pd.DataFrame({'drugs':drgs,'perct':d_cnt})
#
#
#
#
#



