#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 10:23:56 2019

@author: xnmeyer
"""
##############################################################################
##############################################################################
#Code to create plots for conflation figure 
import pandas as pd
from conflation_plots import plotViolin
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.calcOtherSynergyMetrics import loewe,Edrug2D_NDB_hill

#Calculate other synergymetrics using calcOtherSynergy function in each folder
import os
import numpy as np
import seaborn as sns
import numpy as np
#Plotting functions for the conflation figure:
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
import scipy.stats as st
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
##############################################################################
##############################################################################
#Begin with Cancer combination analysis
#Figure B
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_noGamma_otherSynergyMetricsCalculation.csv')
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)
#Select out the important conditions
key = 'hsa'
mask = []
sub_T = T[~T[key+'_fit_ec50'].isna()]
#sub_T = sub_T[(sub_T['E1']<.1)&(sub_T['E2']<.1)&(sub_T['E3']<.1)]
sub_T = sub_T.reset_index(drop=True)
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']>0.))
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']<0.))
mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']>0.)&(sub_T['beta']>0.))
xlim = (-.3,.7)
yticklabs=['$\\alpha_1,\\alpha_2<0$\n$\\beta>0$',
           '$\\alpha_1,\\alpha_2<0$\n$\\beta<0$',
           '$\\alpha_1,\\alpha_2>0$\n$\\beta>0$'
           ]
ax = plotViolin(sub_T,mask,key,xlim,yticklabs)
#################################################################################   
##################################################################################
#Figure C
#Find antagonistic by hsa synergistic by beta antagonistic by alpha
sub_T = T[(T['hsa_fit_ec50']<0.)&(T['beta']>.8)&(T['beta_obs']>.8)&(T['log_alpha1']<-1.)&(T['log_alpha2']<-1.)]
sub_T = sub_T[['hsa_fit_ec50','beta','beta_obs','log_alpha1','log_alpha2','drug1_name','drug2_name','sample']]
print sub_T.sort_values('hsa_fit_ec50')

sub_T = T.loc[5386]
sub_T['save_direc'] = '/home/meyerct6/Desktop/MuSyC_Theory/MuSyC_Theory/Data/oneil_anticancer'
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
title = 'HSA @ EC50:%.2f'%sub_T['hsa_fit_ec50']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
#cell line characteristics: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4350329/
#Dexamethasone glucocorticoid: https://www.ncbi.nlm.nih.gov/pubmed/12204894
#Dexamethasone and mtor http://www.jbc.org/content/281/51/39128.full     https://www.sciencedirect.com/science/article/pii/S1550413111000027

###############################################################################
###############################################################################
#Figure D
#Show HSA masks kinase combination trends
#Read in the drug mechanism data from drug_classes.csv
drgs_mech = pd.read_csv('drug_classes.csv')
drgs_mech = drgs_mech[np.in1d(drgs_mech['class'],['Kinase'])]
drgs = drgs_mech.sort_values('subclass')['drug_name'].values

#create interaction map
cnt = 0
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<3 or e1==e2:
            d_mat[e1,e2]=-1
            cnt+=1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T['beta'].median() #sum(sub_T['beta_obs']>.05)/float(sum(msk))
    tmp = drgs_mech[drgs_mech['drug_name']==drgs[e1]]['subclass'].values[0]        
    if tmp == 'Cell Cycle':
        cols.append('green')
    elif tmp == 'PI3K_AKT':
        cols.append('yellow')
    elif tmp =='Multi-kinase':
        cols.append('blue')
    elif tmp=='MAPK':
        cols.append('purple')
    elif tmp=='AMPK':
        cols.append('black')
            
d_mat[d_mat==-1]=np.nan
plt.figure(facecolor='w',figsize=(4,2))
ax = []
ax.append(plt.subplot2grid((10,1),(0,0),rowspan=9))
ax.append(plt.subplot2grid((10,1),(9,0)))
plt.subplots_adjust(hspace=0)
plt.sca(ax[0])
g1 = sns.heatmap(pd.DataFrame(d_mat,index=drgs,columns=drgs),linewidths=.25,vmin=-.15,vmax=.15)
plt.xticks([])
plt.sca(ax[1])
ax[1].scatter(range(len(drgs)),np.zeros(len(drgs)),c=cols,marker='s',s=35,edgecolor='face')
ax[1].axis('off')
plt.savefig('merck_promiscuous_synergizers.pdf', bbox_inches = 'tight',pad_inches = 0)

###############################################################################
###############################################################################
#Box plot of different classes.
plt.figure(facecolor='w',figsize=(6,1.7))
ax = []
ax.append(plt.subplot2grid((8,5),(0,0),rowspan=6))
ax.append(plt.subplot2grid((8,5),(0,1),rowspan=6))
ax.append(plt.subplot2grid((8,5),(0,2),rowspan=6))
ax.append(plt.subplot2grid((8,5),(0,3),rowspan=6))
ax.append(plt.subplot2grid((8,5),(0,4),rowspan=6))

ax.append(plt.subplot2grid((8,5),(6,0),colspan=5,rowspan=2))

plt.sca(ax[0])
ylim=None
key = 'beta'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<3 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)

plt.sca(ax[1])
ylim=None
key = 'log_alpha'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)-1):
    for e2 in np.arange(e1+1,len(drgs)):
        msk1 = (T['drug1_name']==drgs[e1]) & (T['drug2_name']==drgs[e2])
        msk2 = (T['drug2_name']==drgs[e1]) & (T['drug1_name']==drgs[e2])
        if sum(msk1)>sum(msk2):
            sub_T = T.loc[msk1]
            d_mat[e1,e2] = sub_T['log_alpha1'].median()
            d_mat[e2,e1] = sub_T['log_alpha2'].median()
        else:
            sub_T = T.loc[msk2]
            d_mat[e2,e1] = sub_T['log_alpha1'].median()
            d_mat[e1,e2] = sub_T['log_alpha2'].median()
            
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)

#Loewe
plt.sca(ax[2])
ylim=None
key = 'loewe_fit_ec50'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<3 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Loewe'
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
import matplotlib.ticker as ticker

ax[2].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))





#Bliss
plt.sca(ax[3])
ylim=None
key = 'bliss_fit_ec50'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<3 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'Bliss'
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
import matplotlib.ticker as ticker

ax[3].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))


#HSA
plt.sca(ax[4])
ylim=None
key = 'hsa_fit_ec50'
d_mat = -np.ones((len(drgs),len(drgs)))
cols = []
for e1 in range(len(drgs)):
    for e2 in range(len(drgs)):
        msk = ((T['drug1_name']==drgs[e1]) | (T['drug2_name']==drgs[e1])) & ((T['drug1_name']==drgs[e2]) | (T['drug2_name']==drgs[e2]))
        if sum(msk)<3 or e1==e2:
            d_mat[e1,e2]=-1
        else:
            sub_T= T.loc[msk]
            d_mat[e1,e2] = sub_T[key].median()
d_mat[d_mat==-1]=np.nan
d_mat = np.triu(d_mat)+np.tril(-np.ones((len(drgs),len(drgs))))
d_mat[d_mat==-1]=np.nan

tgr = d_mat[1:5,1:5].reshape((-1,))
tgr = tgr[~np.isnan(tgr)]

tyl = d_mat[0:5,5:].reshape((-1,))
tyl = tyl[~np.isnan(tyl)]

tbl = d_mat[5:,5:].reshape((-1,))
tbl = tbl[~np.isnan(tbl)]

p1 = plt.boxplot([tgr,tyl,tbl],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)

cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
for e,bp in enumerate(p1['boxes']):
    plt.setp(bp,edgecolor='k')
    plt.setp(bp,facecolor=cols[e])
for e,bp in enumerate(p1['medians']):
    plt.setp(bp,color='r',linewidth=2)
for e,bp in enumerate(p1['whiskers']):    
    plt.setp(bp,color='k')
for e,bp in enumerate(p1['caps']):
    plt.setp(bp,color='k')

plt.xlim((.25,3.75))
if ylim is not None:
    plt.ylim(ylim)
plt.xticks([])
from scipy.stats import ttest_ind
print 'HSA'
print 'Yellow-Green: ' + str(ttest_ind(tyl,tgr).pvalue) 
print 'Blue-Green: ' + str(ttest_ind(tbl,tgr).pvalue)
print 'Yellow-Blue: ' + str(ttest_ind(tyl,tbl).pvalue)
#plt.savefig('merck_box_differences_'+key+'.pdf', bbox_inches = 'tight',pad_inches = 0)
import matplotlib.ticker as ticker

ax[4].yaxis.set_major_formatter(ticker.FormatStrFormatter('%0.1f'))

ax[-1].spines['right'].set_visible(False)
ax[-1].spines['top'].set_visible(False)
ax[-1].spines['bottom'].set_visible(False)
ax[-1].spines['left'].set_visible(False)
ax[-1].set_xticks([])
ax[-1].set_yticks([])
ax[-1].scatter(.15,.85,s=50,marker='s',c='green')
ax[-1].scatter(.5,.85,s=50,marker='s',c='yellow')
ax[-1].scatter(.85,.85,s=50,marker='s',c='blue')
ax[-1].text(.15,.65,'Cell Cycle\n+\nCell Cycle',ha='center',va='top')
ax[-1].text(.5,.65,'Cell Cycle\n+\nAKT/MAPK/Mult-Kinase',ha='center',va='top')
ax[-1].text(.85,.65,'AKT/MAPK/Multi-Kinase\n+\nAKT/MAPK/Multi-Kinase',ha='center',va='top')
ax[-1].set_ylim((0,1))
ax[-1].set_xlim((0,1))
ax[0].set_xlabel(r'$\beta$')
ax[1].set_xlabel(r'log($\alpha$)')
ax[4].set_xlabel(r'HSA @ EC50')
ax[3].set_xlabel(r'Bliss @ EC50')
ax[2].set_xlabel(r'Loewe @ EC50')

plt.tight_layout()
plt.subplots_adjust(hspace=.0,wspace=.7)
plt.savefig('merck_box_differences.pdf', bbox_inches = 'tight',pad_inches = 0)

##############################################################################
##############################################################################
#Now look at conflation inthe bliss dataset
##############################################################################
##############################################################################
#Begin with Malaria combination analysis
i = 'mott_antimalaria'
T = pd.read_csv('../../Data/' + i + '/MasterResults_noGamma_otherSynergyMetricsCalculation.csv')
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)

#Select out the important conditions
key = 'bliss'
mask = []
sub_T = T[~T[key+'_fit_ec50'].isna()]
sub_T = sub_T[(sub_T['E1']<.1)&(sub_T['E2']<.1)&(sub_T['E3']<.1)]
sub_T = sub_T.reset_index(drop=True)
mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']<0.) | (sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']>0.))
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.))
mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']>0.))
    
ylim = (-.25,.35)
xticklabs =  ['$\\alpha_1<0$\n$\\alpha_2>0$',
              '$\\alpha_1,\\alpha_2<0$',
              '$\\alpha_1,\\alpha_2>0$',
                     ]
plotViolin(sub_T,mask,key,ylim,xticklabs)

##################################################################################
#Find antagonistic by bliss synergistic by beta antagonistic by alpha
sub_T = T[(T['bliss_fit_ec50']>0.)&(((T['log_alpha1']>0.)&(T['log_alpha2']<0))|((T['log_alpha1']<0.)&(T['log_alpha2']>0)))]
sub_T = sub_T[['bliss_fit_ec50','log_alpha1','log_alpha2','drug1_name','drug2_name','sample']]
print sub_T.sort_values('bliss_fit_ec50')

sub_T = T[(T['drug1_name']=='mefloquine')&(T['drug2_name']=='artesunate')&(T['sample']=='PLASMODIUM_FALCIPARUM_DD2')].to_dict(orient='record')[0]

ky = ['E0','E1','E2','E3']
for k in ky:
    sub_T[k]=sub_T[k]*100.
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
title = 'Bliss @ EC50:%.2f'%sub_T['bliss_fit_ec50']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(-20,120), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
    
#Differences between mefloquine and artesunate pharmacokinetics
#https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2486514/

###############################################################################
###############################################################################
#Look at loewe bias in yeast dataset.  Must correct for hill bias
##############################################################################
##############################################################################
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_noGamma_otherSynergyMetricsCalculation.csv')
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)
#Select out the important conditions
key = 'loewe'
mask = []
sub_T = T[~T[key+'_fit_ec50'].isna()]

#sub_T = sub_T[(sub_T['E1']<.1)&(sub_T['E2']<.1)&(sub_T['E3']<.1)]
sub_T = sub_T.reset_index(drop=True)
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']>0.))
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']<0.))
mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']>0.)&(sub_T['beta']>0.))

xlim = (-.3,.7)
yticklabs=['$\\alpha_1,\\alpha_2<0$\n$\\beta>0$',
           '$\\alpha_1,\\alpha_2<0$\n$\\beta<0$',
           '$\\alpha_1,\\alpha_2>0$\n$\\beta>0$',
           ]
ax = plotViolin(sub_T,mask,key,xlim,yticklabs)


sub_T= sub_T[(sub_T['loewe_fit_ec50']>0)&(sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta_obs']<0.)&(sub_T['beta']<0.)&(sub_T['h1']>1)&(sub_T['h2']>1)]

sub_T = sub_T.loc[4284]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_csv(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
title = 'Loewe @ EC50:%.2f'%sub_T['loewe_fit_ec50']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))

#matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0,450), zero_conc=1,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3),plt_zeropln=False)

#####
# =============================================================================
# Calculate how many combinations have intermediate effects
# =============================================================================
i = ['oneil_anticancer','mott_antimalaria']
for f in i:
    T = pd.read_csv('../../../Data/' + f + '/MasterResults_noGamma_otherSynergyMetricsCalculation.csv')
    T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
    T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
    T.reset_index(drop=True,inplace=True)
    tmp = np.sum((T[['E1','E2']]>.15).any(axis=1) & (T[['E1','E2']]<.50).any(axis=1))
    print tmp/float(len(T))
    