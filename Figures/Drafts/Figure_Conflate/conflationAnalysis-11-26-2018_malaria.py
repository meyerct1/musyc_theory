#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 14:11:32 2018

@author: xnmeyer
"""

#Code to create plots for conflation figure 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
from adjustText import adjust_text
import scipy.stats as st
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from mpl_toolkits.mplot3d import Axes3D
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface,matplotlibDoseResponseSlices
from SynergyCalculator.gatherData import subset_data
#Calculate other synergymetrics using calcOtherSynergy function in each folder
import os

##############################################################################
##############################################################################
#Begin with Malaria combination analysis
T = pd.read_csv('MasterResults_otherSynergyMetrics_malaria.csv')
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[~np.isnan(T['loewe_fit_ec50'])]
#T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)

def plotBox(T,key,ylim):
    plt.figure(facecolor='w',figsize=(2.75,2.5))
    ax = plt.subplot(111)
    p = []
    mask = []
    sub_T = T[~T[key+'_fit_ec50'].isna()]
    mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']>0.))
    mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.))
    mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']<0.) | (sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']>0.))
    ymin=0;ymax=0;
    for e in range(3):
        p.append(plt.boxplot(sub_T[key+'_fit_ec50'].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
        ymin = np.min((ymin,sub_T[key+'_fit_ec50'].loc[mask[e]].min()))
        ymax = np.max((ymax,sub_T[key+'_fit_ec50'].loc[mask[e]].max()))
    
    for e,bp in enumerate(p):
        plt.setp(bp['medians'],color='r',linewidth=2)
        plt.setp(bp['whiskers'],color='k')
        plt.setp(bp['caps'],color='k')
        plt.setp(bp['boxes'],edgecolor='k')
        
        for patch in bp['boxes']:
            patch.set(facecolor=[.25,.25,.25])    

    plt.xlim((-.5,2.5))
    if ylim is None:
        plt.ylim((ymin,ymax))
    else:
        plt.ylim(ylim)
    plt.xticks(range(4))
    ax.set_xticklabels(['$\\alpha_1,\\alpha_2>0$',
                        '$\\alpha_1,\\alpha_2<0$',
                        '$\\alpha_1<0,\\alpha_2>0$',
                     ])
    
    ax.axhline(y=0.,color='k',linestyle='--',linewidth=3)
    ax.fill([ax.get_xlim()[0],ax.get_xlim()[1],ax.get_xlim()[1],ax.get_xlim()[0]],[0,0,ax.get_ylim()[1],ax.get_ylim()[1]],color='yellow',alpha=.5,zorder=-10)
    ax.fill([ax.get_xlim()[0],ax.get_xlim()[1],ax.get_xlim()[1],ax.get_xlim()[0]],[0,0,ax.get_ylim()[0],ax.get_ylim()[0]],color='b',alpha=.5,zorder=-10)
    
    ax.text(3.6,.05,'<--Ant    ' + key.upper()+'    Syn-->',     rotation=90,ha='left',va='center')
    plt.ylabel(key.upper() +' @ EC50',labelpad=0)
    plt.tight_layout()
    plt.savefig(key+'_fit_ec50_conflation.pdf', bbox_inches = 'tight',pad_inches = 0)
    return (ymin,ymax)

key = 'hsa'
ylim = (-.5,.5)
plotBox(T,key,ylim)

key = 'bliss'
ylim = (-.25,.25)
plotBox(T,key,ylim)

key = 'loewe'
ylim = (-.5,.5)
plotBox(T,key,ylim)




            
##################################################################################
#Find antagonistic by bliss synergistic by beta antagonistic by alpha
sub_T = T[(T['bliss_fit_ec50']>0.)&(((T['log_alpha1']>0.)&(T['log_alpha2']<0))|((T['log_alpha1']<0.)&(T['log_alpha2']>0)))]
sub_T = sub_T[['bliss_fit_ec50','log_alpha1','log_alpha2','drug1_name','drug2_name','sample']]
print sub_T.sort_values('bliss_fit_ec50')

sub_T = T[(T['drug1_name']=='mefloquine')&(T['drug2_name']=='artesunate')&(T['sample']=='PLASMODIUM_FALCIPARUM_DD2')]

ky = ['E0','E1','E2','E3']
for k in ky:
    sub_T[k]=sub_T[k]*100.
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,120), zero_conc=0,title=None,d1lim=None,d2lim=None)
    

#Differences between mefloquine and artesunate pharmacokinetics
#https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2486514/

################
#Show global trends in synergy can be understood by MuSyC
def plotBoxSyn(T,ylim):
    plt.figure(facecolor='w',figsize=(2.75,2.5))
    ax = []
    ax.append(plt.subplot2grid((8,2),(0,0),rowspan=7))
    ax.append(plt.subplot2grid((8,2),(0,1),rowspan=7))
    ax.append(plt.subplot2grid((8,2),(7,0),colspan=2))
    
    mask = []
    hsa_bsl = T['hsa_fit_ec50'].mean()
    bliss_bsl = T['bliss_fit_ec50'].mean()
    loewe_bsl = T['loewe_fit_ec50'].mean()
    mask.append((T['hsa_fit_ec50']>hsa_bsl)&(T['bliss_fit_ec50']>bliss_bsl)&(T['loewe_fit_ec50']>loewe_bsl))
    mask.append((T['hsa_fit_ec50']<hsa_bsl)&(T['bliss_fit_ec50']<bliss_bsl)&(T['loewe_fit_ec50']<loewe_bsl))
    mask.append((~mask[0])&(~mask[1]))
    keys = ['log_alpha1','log_alpha2']
    labkeys = [r'$log(\alpha_1)$',r'$log(\alpha_2)$']
    c = ['yellow','purple','green']
    for e1,k in enumerate(keys):
        ymin=0;ymax=0;
        plt.sca(ax[e1])
        p = []
        for e in range(3):
            if sum(mask[e])==0:
                plt.scatter(e,0,s=10,marker='*',c=c[e])
            else:
                p.append(plt.boxplot(T[k].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
                ymin = np.min((ymin,T[k].loc[mask[e]].min()))
                ymax = np.max((ymax,T[k].loc[mask[e]].max()))
            
        for e,bp in enumerate(p):
            plt.setp(bp['medians'],color='r',linewidth=2)
#            plt.setp(bp['fliers'],color='k')
            plt.setp(bp['whiskers'],color='k')
            plt.setp(bp['caps'],color='k')
            plt.setp(bp['boxes'],edgecolor='k')
            plt.setp(bp['boxes'],facecolor=c[e])
    
        plt.xlim((-.75,3.75))
        if ylim is not None:
            (ymin,ymax)=ylim[e1]            
        plt.ylim((ymin,ymax))
        plt.xticks(range(3))
        ax[e1].set_xticklabels([])
        ax[e1].set_yticks([ymin,0,ymax])
        ax[e1].axhline(y=0.,color='k',linestyle='--',linewidth=1)
#        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[1],ax[e1].get_ylim()[1]],color='r',alpha=.5,zorder=-10)
#        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[0],ax[e1].get_ylim()[0]],color='b',alpha=.5,zorder=-10)
        
        plt.title(labkeys[e1])
    
    plt.tight_layout()
    plt.subplots_adjust(wspace=0.4, hspace=.2)
    
    ax[-1].spines['right'].set_visible(False)
    ax[-1].spines['top'].set_visible(False)
    ax[-1].spines['bottom'].set_visible(False)
    ax[-1].spines['left'].set_visible(False)
    ax[-1].set_xticks([])
    ax[-1].set_yticks([])
    ax[-1].scatter(.15,.75,s=50,marker='s',c='yellow')
    ax[-1].scatter(.85,.75,s=50,marker='s',c='purple')
    ax[-1].text(.15,.25,'Synergistic',ha='center',va='top')
    ax[-1].text(.85,.25,'Conflicting',ha='center',va='top')
    ax[-1].set_ylim((0,1))
    ax[-1].set_xlim((0,1))
    plt.savefig('syn_trends.pdf', bbox_inches = 'tight',pad_inches = 0)
    return (ymin,ymax)

ylim=None
plotBoxSyn(T,ylim)




















plt.figure(facecolor='w',figsize=(2.75,2.5))
ax=plt.subplot(111)

mask = []

mask.append((T['hsa_fit_ec50']>0.)&(T['bliss_fit_ec50']>0.)&(T['loewe_fit_ec50']>0.))
mask.append(~mask[0])
keys = ['log_alpha1','log_alpha2']
labkeys = [r'$log(\alpha_1)$',r'$log(\alpha_2)$']
c = ['yellow','purple','blue']
x1=sum(np.sum(T.loc[mask[0]][['log_alpha1','log_alpha2']]>0,axis=1)==0)/float(sum(mask[0]))
x2=sum(np.sum(T.loc[mask[0]][['log_alpha1','log_alpha2']]>0,axis=1)==1)/float(sum(mask[0]))
x3=sum(np.sum(T.loc[mask[0]][['log_alpha1','log_alpha2']]>0,axis=1)==2)/float(sum(mask[0]))
plt.bar(.2,x1,color=c[0],width=.8,label=r'$log(\alpha_1),log(\alpha_2)<0$')
plt.bar(.2,x2,bottom=x1,color=c[1],width=.8,label=r'$log(\alpha_1)<0,log(\alpha_2)>0$')
plt.bar(.2,x3,bottom=x1+x2,color=c[2],width=.8,label=r'$log(\alpha_1),log(\alpha_2)>0$')
x1=sum(np.sum(T.loc[mask[1]][['log_alpha1','log_alpha2']]>0,axis=1)==0)/float(sum(mask[1]))
x2=sum(np.sum(T.loc[mask[1]][['log_alpha1','log_alpha2']]>0,axis=1)==1)/float(sum(mask[1]))
x3=sum(np.sum(T.loc[mask[1]][['log_alpha1','log_alpha2']]>0,axis=1)==2)/float(sum(mask[1]))
plt.bar(1.2,x1,color=c[0],width=.8)
plt.bar(1.2,x2,bottom=x1,color=c[1],width=.8)
plt.bar(1.2,x3,bottom=x1+x2,color=c[2],width=.8)
plt.ylabel('Percent of combinations')
plt.xlim(0,2.2)
plt.xticks([.5,1.5])
ax.set_xticklabels(['Synergistic\nby HSA,Loewe,Bliss','Conflicting'])
ax.legend(bbox_to_anchor=(.92, 1.5))
plt.tight_layout()
plt.savefig('syn_trends.pdf', bbox_inches = 'tight',pad_inches = 0)



