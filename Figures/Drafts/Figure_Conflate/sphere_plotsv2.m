%Set defaults for plotting
set(0,'DefaultAxesFontSize',8)
set(0,'DefaultLineLinewidth',2)
set(0, 'DefaultAxesFontWeight','normal')
set(0, 'DefaultAxesLineWidth',2)
set(0, 'DefaultFigureColor','w')
set(0, 'DefaultTextFontSize',8)
set(0, 'DefaultTextFontWeight','normal')
set(0, 'defaultTextFontName','arial')
set(0, 'defaultAxesFontName','arial')

radius = [1,1,1];
%Solve the 2D hill equation at the ec50 of both drugs with the following
%values
syms r1 r1_1 r2 r2_1 h1 h2 d1 d2 alpha1 alpha2 positive
syms E0 E1 E2 E3 real
syms C1 C2
r1 = r2;
C1 = 1;C2 = 1;
d1 = C1; d2 = C2;
h1 = 1;  h2 = 1;
E0 = 1;  E1 = 0;  E2 = 0;
r1_1 = C1^h1*r1; r2_1 = C2^h2*r2;
T = [-(r1*d1^h1+r2*d2^h2),r1_1,r2_1,0;
    r1*d1^h1,-(r1_1+r2*(alpha1*d2)^h2),0,r2_1;
    r2*d2^h2,0,-(r2_1+r1*(alpha2*d1)^h1),r1_1;
    1,1,1,1];
Eqn = [E0,E1,E2,E3] * inv(T)*[0;0;0;1];
eq1 = simplify(Eqn);
eq2 = combine(eq1);
%Equation is of the form:
%(alpha1/2+alpha2/2+(E3*alpha1)/2 + (E3*alpha2)/2 + E3*alpha1*alpha2 +1)/
%(2*alpha1 + 2*alpha2 + alpha1*alpha2 + 3)

%Solve for loewe,bliss,and hsa over a 1x1x1 grid for alpha1,alpha2,beta,
[x3,y3,z3] = meshgrid(linspace(-1,1,25));
e3 = -z3;%Beta is equal to -E3 when E0=1,E1=E2=0
%Solve for the expected effect
ed = (x3/2 + y3/2 + (e3.*x3)/2 + (e3.*y3)/2 + e3.*x3.*y3 + 1)./(2*x3 + 2*y3 + x3.*y3 + 3);
%Matrix to store loewe bliss and hsa calc
f1 = zeros([size(x3),3]);
%Calculate loewe
%Where loewe is undefined log10 returns complex numbers.  Just take the
%real component and set all places where infinite to nan
tmp = real(-log10(2/(1./ed-1)));
tmp(isinf(tmp))=nan;
f1(:,:,:,1) =tmp;
%Calculate Bliss  Expected effect at ec50 for both is .5*.5=.25
f1(:,:,:,2) = 1/4-ed;
%Calculate hsa at ec50 max(e1,e2)=1/2
f1(:,:,:,3) = 1/2-ed;
%Define sphere to plot each metric on
f2 = x3.^2+y3.^2+z3.^2;
[x2, y2] = meshgrid(linspace(-1,1,100));
%For each metric
label = {'Loewe','Bliss','HSA'};
for i=1:3
    % Find contour where synergy metric is zero
    p1 = isosurface(x3, y3, z3, f1(:,:,:,i), 0);
    %Some nan vertices have been observed before... remove them along with
    %the faces they touch
    newVertices = p1.vertices;
    verticesToRemove = [find(isnan(mean(p1.vertices')))];
    newVertices(verticesToRemove,:) = [];
    % Find the new index for each of the new vertices
    [~, newVertexIndex] = ismember(p1.vertices, newVertices, 'rows');
    % Find any faces that used the vertices that we removed and remove them
    facesToRemove = [];
    cnt=1;
    for j=1:size(p1.faces,1)
        if sum(ismember(p1.faces(j,:),verticesToRemove))>0
            facesToRemove(cnt) = j;
            cnt=cnt+1;
        end
    end
    newFaces = p1.faces;
    newFaces(facesToRemove,:)=[];
    % Now update the vertex indices to the new ones
    newFaces = newVertexIndex(newFaces);
    p1.vertices = newVertices;
    p1.faces = newFaces;

    %Create a sphere
    p2 = isosurface(x3, y3, z3, f2, radius(i));
    %Create Figure
    fig = figure('Color','white','rend','painters');
    fig.PaperUnits = 'inches';
    fig.Units = 'inches';
    fig.Position=[1,1,2.5,2.5];
    %Create 2 axes
    ax2 = axes('Position',[0.0 0.0 0.35 0.35]);    ax1 = axes('Position',[0.0 0.0 1 1]);
    axes(ax1)
    hold on
    %Plot the sphere and color based on the value of each synergy metric
    p = patch(p2);
    isonormals(x3,y3,z3,f2,p)
    isocolors(x3,y3,z3,f1(:,:,:,i),p)
    p.FaceColor = 'interp';
    p.EdgeColor = 'none';
    %Set aspect ratio and view
    daspect([1 1 1])
    view(55,30)
    %Add and modify colorbar and change color max and min so zero is centered
    h = colorbar('East','AxisLocation','out');
    h.Label.String = strcat('<--Ant',{'    '},label(i), '     Syn-->');
    cmax = max(p.FaceVertexCData); cmin=min(p.FaceVertexCData);
    if abs(cmin)>cmax
        cmax=-cmin;
    else
        cmin=-cmax;
    end
    caxis([cmin,cmax])
    h.Ticks = [min(h.Ticks),0,max(h.Ticks)];
    h.Position = [0.87,0.25,0.03,0.55];
    h.Label.Position = [2.2,0,0];

    %Remove background axis
    axis('off')
    %Freeze colors to prevent the colorbar from changing with new plot
    freezeColors
    %Find the intersection of the sphere and the zero contour surfaces
    try
        [~, s12] = SurfaceIntersection(p1, p2);
        %Plot as black line
        S=s12; trisurf(S.faces, S.vertices(:,1),S.vertices(:,2),S.vertices(:,3),'EdgeColor', 'k', 'FaceColor', 'none','LineWidth',1.5);
    end
    %Create x,y,z planes for marking axes
    ps1 = isosurface(x3,y3,z3,x3,0);
    ps2 = isosurface(x3,y3,z3,y3,0);
    ps3 = isosurface(x3,y3,z3,z3,0);
    patch(ps1,'FaceColor',[.5,.5,.5],'FaceAlpha',.5,'EdgeColor','none');
    patch(ps2,'FaceColor',[.5,.5,.5],'FaceAlpha',.5,'EdgeColor','none');
    patch(ps3,'FaceColor',[.5,.5,.5],'FaceAlpha',.5,'EdgeColor','none');

    %Create axis which axis labels
    axes(ax2)
    mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
    mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
    mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
    xlim([-1,1]);    ylim([-1,1]);    zlim([-1,1]);
    %Set view
    view(55,30)
    axis('off')
    %Add labels
    text(0,-1.8,-1,'\alpha_1','fontsize',10,'fontweight','bold')
    text(-2.1,0.3,-1,'\alpha_2','fontsize',10,'fontweight','bold')
    text(-1.3,-1.3,-.3,'\beta','fontsize',10,'fontweight','bold')
    %Stop clipping to avoid loosing text
    ax2.Clipping='off';
    tightfig
    %Beautify
    camlight
    lighting 'gouraud'
    export_fig(strcat(label{i},'_sphere'),'-m3')
    for j=0:2:360
        axes(ax1)
        view(55+j,30)
        axes(ax2)
        view(55+j,30)
        export_fig(strcat(label{i},'_',int2str(j)),'-m3')
    end
    close(gcf)
end

% %Export figures while maintaining resolution and size
% export_fig HSA_sphere -m3
% close(gcf)
% export_fig Bliss_sphere -m3
% close(gcf)
% export_fig Loewe_sphere -m3
% close(gcf)
