#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 26 14:11:32 2018

@author: xnmeyer
"""

#Code to create plots for conflation figure 
import pandas as pd
from conflation_plots import plotViolin
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
#Calculate other synergymetrics using calcOtherSynergy function in each folder
import os
import numpy as np


##############################################################################
##############################################################################
#Begin with Malaria combination analysis
i = 'cokol_antifungal'
T = pd.read_csv('../../Data/' + i + '/MasterResults_noGamma_otherSynergyMetricsCalculation.csv')
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)


#Select out the important conditions
key = 'loewe'
mask = []
sub_T = T[~T[key+'_fit_ec50'].isna()]
sub_T = sub_T.reset_index(drop=True)
mask.append((sub_T['log_alpha1']>0.2)&(sub_T['log_alpha2']>0.2)&(sub_T['beta']>-0.03))
mask.append((sub_T['log_alpha1']<0.2)&(sub_T['log_alpha2']<0.2)&(sub_T['beta']<-0.03))
mask.append((sub_T['log_alpha1']>0.2)&(sub_T['log_alpha2']>0.2)&(sub_T['beta']<-0.03))

    
ylim = (-.25, .5)
xticklabs=['$\\alpha_1,\\alpha_2>0.2$\n$\\beta>-0.03$',
           '$\\alpha_1,\\alpha_2<0.2$\n$\\beta<-0.03$',
           '$\\alpha_1,\\alpha_2>0.2$\n$\\beta<-0.03$',
                     ]

plotViolin(sub_T,mask,key,ylim,xticklabs)





#List of Synthetic lethal interactions
t = ['lat_ter',    'lat_tac',    'dyc_lat',    'sta_tac',    'fen_lat',    'rad_ter',    'hal_lat',    'lit_tac',    'fen_sta',    'dyc_sta',    'rap_ter',    'ben_pen',    'hal_sta',    'aba_wor',    'hyg_myr',    'myr_rad',    'ben_rad',    'lit_rad',    'rad_rap',    'qnn_rad',    'myr_tac',    'lit_sta',    'hyg_rad',    'lat_rap',    'hyg_sta',    'lit_rap',    'lat_tun',    'clo_rad',    'hyg_rap',    'sta_wor',    'myr_qnn',    'aba_lit',    'ben_lat',    'ben_cal',    'hyg_lat',    'rad_sta',    'lat_rad',    'cyc_rad']
ind = []
for i in t:
    d1 = i.split('_')[1]
    d2 = i.split('_')[0]
    tmp = T[(T['drug1_name']==d1)&(T['drug2_name']==d2)].index
    if len(tmp)>0:
        ind.append(T[(T['drug1_name']==d1)&(T['drug2_name']==d2)].index[0])
 
mask = []
mask.append(np.in1d(T.index,ind))

sub_T = T.loc[mask[0],:]
sub_T=sub_T.reset_index(drop=True)

sub_T = sub_T[(sub_T['loewe_fit_ec50']>0.)&(sub_T['log_alpha1']>0.2)&(T['log_alpha2']>0.2)&(T['beta']<-0.03)]

sub_T = T[(T['drug1_name']=='myr')&(T['drug2_name']=='hyg')&(T['sample']=='YEAST')].to_dict(orient='records')[0]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_csv(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
title = 'Loewe @ EC50:%.2f'%sub_T['loewe_fit_ec50']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(100.,550.), zero_conc=1,title=title,d1lim=None,d2lim=None)
    









    
#
#
##Find two surfaces which are very similar by Loewe and different in beta and alpha
##Remove the samples which did not converge past initial_fit
#T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
#T.reset_index(drop=True,inplace=True)
#from scipy.spatial.distance import pdist
#from sklearn.preprocessing import normalize
#mat2 = normalize(T[['log_alpha1','log_alpha2','beta_obs']])
#for cl in T['sample'].unique():
#    mask = T['sample']==cl  
#    sub_T = T[mask]
#    sub_T = sub_T.reset_index(drop=True)
#    d1 = pdist(np.array(sub_T['loewe_fit_ec50']).reshape((-1,1)))
#    d2 = pdist(mat2[mask])    
#    diff = d2-d1
#    sort_index = np.argsort(diff)
#    bst10 = sort_index[-2:]    
#    lktab = np.zeros((len(d1),2))
#    cnt = 0
#    for i in range(len(sub_T)):
#        for j in np.arange(i+1,len(sub_T)):
#            lktab[cnt,0]=i
#            lktab[cnt,1]=j
#            cnt = cnt + 1          
#    bst10_ind = [lktab[ind,:] for ind in bst10]
#    for i in range(2):
#        if sub_T['loewe_fit_ec50'].loc[[bst10_ind[i][0],bst10_ind[i][1]]].max()<0.:
#           if np.diff(np.sign(sub_T['beta'].loc[[bst10_ind[i][0],bst10_ind[i][1]]]))[0]==2:     
#                print cl
#                print i
#                print sub_T[['log_alpha1','log_alpha2','beta_obs','hsa_fit_ec50','drug1_name','drug2_name','sample']].loc[[bst10_ind[i][0],bst10_ind[i][1]]]
#          
#
#sub_T = T[(T['drug1_name']=='doxorubicin')&(T['drug2_name']=='mk-4827')&(T['sample']=='MDAMB436')]
#sub_T['to_save_plots'] = 1
#expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
#drug1_name  = sub_T['drug1_name']
#drug2_name  = sub_T['drug2_name']
#sample      = sub_T['sample']
#data        = pd.read_table(expt, delimiter=',')        
#data['drug1'] = data['drug1'].str.lower()
#data['drug2'] = data['drug2'].str.lower()
#data['sample'] = data['sample'].str.upper()
#d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
#plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
#matplotlibDoseResponseSurface(T,d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.0), zero_conc=0,title=None,d1lim=(-3,-1.2),d2lim=(-2,-.1))
#    
#    
##Find antagonistic by hsa synergistic by beta antagonistic by alpha
#
#sub_T = T[(T['drug1_name']=='l778123')&(T['drug2_name']=='etoposide')&(T['sample']=='MDAMB436')]
#sub_T['to_save_plots'] = 1
#expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
#drug1_name  = sub_T['drug1_name']
#drug2_name  = sub_T['drug2_name']
#sample      = sub_T['sample']
#data        = pd.read_table(expt, delimiter=',')        
#data['drug1'] = data['drug1'].str.lower()
#data['drug2'] = data['drug2'].str.lower()
#data['sample'] = data['sample'].str.upper()
#d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
#plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
#matplotlibDoseResponseSurface(T,d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.0), zero_conc=0,title=None,d1lim=(-3,-1.2),d2lim=(-2,-.1))
#    
#            
##################################################################################
#Find synergistic by loewe but antagonistic by beta and alpha
sub_T = T[(T['loewe_fit_ec10']<0.)&(T['log_alpha1']>0.)&(T['log_alpha2']>0)]
sub_T = sub_T[['loewe_fit_ec10','beta','beta_obs','log_alpha1','log_alpha2','drug1_name','drug2_name','sample']]
print sub_T.sort_values('loewe_fit_ec10')

sub_T = T[(T['drug1_name']=='myr')&(T['drug2_name']=='hyg')&(T['sample']=='YEAST')].to_dict(orient='records')[0]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_csv(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(100.,550.), zero_conc=1,title=None,d1lim=None,d2lim=None)
    



#
#plt.figure(facecolor='w',figsize=(2.75,2.5))
#ax=plt.subplot(111)
#
#mask = []
#mask.append((T['hsa_fit_ec50']>0.)&(T['loewe_fit_ec50']>0.))
#mask.append(~mask[0])
#keys = ['log_alpha1','log_alpha2']
#labkeys = [r'$log(\alpha_1)$',r'$log(\alpha_2)$']
#c = ['yellow','purple','blue']
#x1=sum(np.sum(T.loc[mask[0]][['log_alpha1','log_alpha2']]>0,axis=1)==0)/float(sum(mask[0]))
#x2=sum(np.sum(T.loc[mask[0]][['log_alpha1','log_alpha2']]>0,axis=1)==1)/float(sum(mask[0]))
#x3=sum(np.sum(T.loc[mask[0]][['log_alpha1','log_alpha2']]>0,axis=1)==2)/float(sum(mask[0]))
#plt.bar(.2,x1,color=c[0],width=.8,label=r'$log(\alpha_1),log(\alpha_2)<0$')
#plt.bar(.2,x2,bottom=x1,color=c[1],width=.8,label=r'$log(\alpha_1)<0,log(\alpha_2)>0$')
#plt.bar(.2,x3,bottom=x1+x2,color=c[2],width=.8,label=r'$log(\alpha_1),log(\alpha_2)>0$')
#x1=sum(np.sum(T.loc[mask[1]][['log_alpha1','log_alpha2']]>0,axis=1)==0)/float(sum(mask[1]))
#x2=sum(np.sum(T.loc[mask[1]][['log_alpha1','log_alpha2']]>0,axis=1)==1)/float(sum(mask[1]))
#x3=sum(np.sum(T.loc[mask[1]][['log_alpha1','log_alpha2']]>0,axis=1)==2)/float(sum(mask[1]))
#plt.bar(1.2,x1,color=c[0],width=.8)
#plt.bar(1.2,x2,bottom=x1,color=c[1],width=.8)
#plt.bar(1.2,x3,bottom=x1+x2,color=c[2],width=.8)
#plt.ylabel('Percent of combinations')
#plt.xlim(0,2.2)
#plt.xticks([.5,1.5])
#ax.set_xticklabels(['Synergistic\nby HSA,Loewe','Conflicting'])
#ax.legend(bbox_to_anchor=(.92, 1.5))
#plt.tight_layout()
#plt.savefig('syn_trends_yeast.pdf', bbox_inches = 'tight',pad_inches = 0)
#
#
#





















################
#Show Chemical genetic pairs can be explained by MuSyC
def plotBoxSyn(T,ylim):
    plt.figure(facecolor='w',figsize=(2.75,2.5))
    ax = []
    ax.append(plt.subplot2grid((8,3),(0,0),rowspan=7))
    ax.append(plt.subplot2grid((8,3),(0,1),rowspan=7))
    ax.append(plt.subplot2grid((8,3),(0,2),rowspan=7))
    ax.append(plt.subplot2grid((8,3),(7,0),colspan=3))
    #List of Synthetic lethal interactions
    t = ['lat_ter',    'lat_tac',    'dyc_lat',    'sta_tac',    'fen_lat',    'rad_ter',    'hal_lat',    'lit_tac',    'fen_sta',    'dyc_sta',    'rap_ter',    'ben_pen',    'hal_sta',    'aba_wor',    'hyg_myr',    'myr_rad',    'ben_rad',    'lit_rad',    'rad_rap',    'qnn_rad',    'myr_tac',    'lit_sta',    'hyg_rad',    'lat_rap',    'hyg_sta',    'lit_rap',    'lat_tun',    'clo_rad',    'hyg_rap',    'sta_wor',    'myr_qnn',    'aba_lit',    'ben_lat',    'ben_cal',    'hyg_lat',    'rad_sta',    'lat_rad',    'cyc_rad']
    ind = []
    for i in t:
        d1 = i.split('_')[1]
        d2 = i.split('_')[0]
        tmp = T[(T['drug1_name']==d1)&(T['drug2_name']==d2)].index
        if len(tmp)>0:
            ind.append(T[(T['drug1_name']==d1)&(T['drug2_name']==d2)].index[0])
     
    mask = []
    mask.append(np.in1d(T.index,ind))
    mask.append(~np.in1d(T.index,ind))
    keys = ['beta','log_alpha1','log_alpha2']
    labkeys = [r'$\beta$',r'$log(\alpha_1)$',r'$log(\alpha_2)$']
    c = ['yellow','blue']
    for e1,k in enumerate(keys):
        ymin=0;ymax=0;
        plt.sca(ax[e1])
        p = []
        for e in range(2):
            p.append(plt.boxplot(T[k].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False))
            ymin = np.min((ymin,T[k].loc[mask[e]].min()))
            ymax = np.max((ymax,T[k].loc[mask[e]].max()))
        
        for e,bp in enumerate(p):
            plt.setp(bp['medians'],color='r',linewidth=2)
#            plt.setp(bp['fliers'],color='k')
            plt.setp(bp['whiskers'],color='k')
            plt.setp(bp['caps'],color='k')
            plt.setp(bp['boxes'],edgecolor='k')
            plt.setp(bp['boxes'],facecolor=c[e])
    
        plt.xlim((-.75,1.75))
        if ylim is not None:
            (ymin,ymax)=ylim[e1]            
        plt.ylim((ymin,ymax))
        plt.xticks(range(3))
        ax[e1].set_xticklabels([])
        ax[e1].set_yticks([ymin,0,ymax])
        ax[e1].axhline(y=0.,color='k',linestyle='--',linewidth=1)
#        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[1],ax[e1].get_ylim()[1]],color='r',alpha=.5,zorder=-10)
#        ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[0],ax[e1].get_ylim()[0]],color='b',alpha=.5,zorder=-10)
        
        plt.title(labkeys[e1])
    
    plt.tight_layout()
    #Add significance bars
    
    plt.subplots_adjust(wspace=0.75, hspace=.2)
    
    ax[-1].spines['right'].set_visible(False)
    ax[-1].spines['top'].set_visible(False)
    ax[-1].spines['bottom'].set_visible(False)
    ax[-1].spines['left'].set_visible(False)
    ax[-1].set_xticks([])
    ax[-1].set_yticks([])
    ax[-1].scatter(.2,.75,s=50,marker='s',c='yellow')
    ax[-1].scatter(.8,.75,s=50,marker='s',c='blue')
    ax[-1].text(.2,.25,'Predicted Synergy\nby Synthetic Lethality',ha='center',va='top')
    ax[-1].text(.8,.25,'No Predicted Synergy\nby Synthetic Lethality',ha='center',va='top')
    ax[-1].set_ylim((0,1))
    ax[-1].set_xlim((0,1))
    plt.savefig('syn_trends.pdf', bbox_inches = 'tight',pad_inches = 0)
    return [(ymin,ymax),mask]

ylim=((-.15,.05),(-1.5,2),(-1.5,2))
#ylim = None
[_,mask] = plotBoxSyn(T,ylim)
from scipy.stats import ttest_ind
print ttest_ind(T['log_alpha1'].loc[mask[0]],T['log_alpha1'].loc[mask[1]]).pvalue
print ttest_ind(T['log_alpha2'].loc[mask[0]],T['log_alpha2'].loc[mask[1]]).pvalue













plt.figure(facecolor='w',figsize=(2.75,2.5))
ax = []    
ax.append(plt.subplot2grid((1,4),(0,0)))
ax.append(plt.subplot2grid((1,4),(0,1)))
ax.append(plt.subplot2grid((1,4),(0,2)))
ax.append(plt.subplot2grid((1,4),(0,3)))
    
t = ['lat_ter',    'lat_tac',    'dyc_lat',    'sta_tac',    'fen_lat',    'rad_ter',    'hal_lat',    'lit_tac',    'fen_sta',    'dyc_sta',    'rap_ter',    'ben_pen',    'hal_sta',    'aba_wor',    'hyg_myr',    'myr_rad',    'ben_rad',    'lit_rad',    'rad_rap',    'qnn_rad',    'myr_tac',    'lit_sta',    'hyg_rad',    'lat_rap',    'hyg_sta',    'lit_rap',    'lat_tun',    'clo_rad',    'hyg_rap',    'sta_wor',    'myr_qnn',    'aba_lit',    'ben_lat',    'ben_cal',    'hyg_lat',    'rad_sta',    'lat_rad',    'cyc_rad']
ind = []
for i in t:
    d1 = i.split('_')[1]
    d2 = i.split('_')[0]
    tmp = T[(T['drug1_name']==d1)&(T['drug2_name']==d2)].index
    if len(tmp)>0:
        ind.append(T[(T['drug1_name']==d1)&(T['drug2_name']==d2)].index[0])
 
mask = np.in1d(T.index,ind)
sub_T = T[mask]
keys = ['beta','log_alpha1','log_alpha2','loewe_fit_ec10']
labkeys = [r'$\beta$',r'$log(\alpha_1)$',r'$log(\alpha_2)$','$Loewe$\n$at EC10$']
c = ['purple','red','gray','green']
ylim=((-.5,.5),(-1.,1.),(-1.,1.),(-.4,.4))
for e1,k in enumerate(keys):
    plt.sca(ax[e1])
    p = plt.boxplot(sub_T[k].values,positions=[0],notch=False,patch_artist=True,widths=.75,showfliers=False)
    plt.setp(p['medians'],color='k',linewidth=2)
    plt.setp(p['whiskers'],color='k')
    plt.setp(p['caps'],color='k')
    plt.setp(p['boxes'],edgecolor='k')
    plt.setp(p['boxes'],facecolor=c[e1])
    ax[e1].spines['right'].set_visible(False)
    ax[e1].spines['top'].set_visible(False)
    plt.xlim(-.5,.5)
    plt.ylim(ylim[e1])
    plt.yticks((ylim[e1][0],0.,ylim[e1][1]),rotation=45)
    plt.xticks([])
    plt.title(labkeys[e1])
    ax[e1].axhline(y=0.,color='k',linestyle='--',linewidth=3)
    ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[1],ax[e1].get_ylim()[1]],color='yellow',alpha=.5,zorder=-10)
    ax[e1].fill([ax[e1].get_xlim()[0],ax[e1].get_xlim()[1],ax[e1].get_xlim()[1],ax[e1].get_xlim()[0]],[0,0,ax[e1].get_ylim()[0],ax[e1].get_ylim()[0]],color='b',alpha=.5,zorder=-10)
    

ax[-1].text(0.75,.0,'<--Ant     Syn-->',     rotation=90,ha='center',va='center')
plt.tight_layout()    
plt.subplots_adjust(wspace=1, hspace=.0)

plt.savefig('synthetic_lethal_yeast.pdf', bbox_inches = 'tight',pad_inches = 0)

