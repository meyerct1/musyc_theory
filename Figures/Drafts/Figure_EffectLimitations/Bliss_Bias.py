#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Jun 30 04:31:57 2018

@author: xnmeyer
"""

####################################################################
#Import packages
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
from matplotlib.colors import ListedColormap
import matplotlib.cm as cm
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 12}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
import scipy

def Edrug_2D_full(d1,d2,E0,E1,E2,beta,C1,C2,h1,h2,r1,r2,alpha1,alpha2,gamma1,gamma2):
    E3 = -beta*(E0-min(E1,E2))+min(E1,E2);
    return (C1**(2*gamma1*h1)*E2*d2**h2*r1 + C2**(2*gamma2*h2)*E1*d1**h1*r2 + C1**(2*gamma1*h1)*C2**(gamma2*h2)*E0*r1 + C1**(gamma1*h1)*C2**(2*gamma2*h2)*E0*r2 + E3*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + E3*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*E2*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + C2**(gamma2*h2)*E1*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1 + C1**(gamma1*h1)*C2**(gamma2*h2)*E0*r1*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*C2**(gamma2*h2)*E0*r2*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*E2*d1**h1*r1*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*E3*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + C2**(gamma2*h2)*E1*d2**h2*r2*(alpha2*d1)**(gamma2*h1) + C2**(gamma2*h2)*E3*d1**h1*r2*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*C2**(gamma2*h2)*E1*d1**h1*r1 + C1**(gamma1*h1)*C2**(gamma2*h2)*E2*d2**h2*r2)/(C1**(2*gamma1*h1)*C2**(gamma2*h2)*r1 + C1**(gamma1*h1)*C2**(2*gamma2*h2)*r2 + C1**(2*gamma1*h1)*d2**h2*r1 + C2**(2*gamma2*h2)*d1**h1*r2 + C1**(gamma1*h1)*C2**(gamma2*h2)*r1*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*C2**(gamma2*h2)*r2*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*d1**h1*r1*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + C2**(gamma2*h2)*d1**h1*r2*(alpha1*d2)**(gamma1*h2) + C2**(gamma2*h2)*d2**h2*r2*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*C2**(gamma2*h2)*d1**h1*r1 + C1**(gamma1*h1)*C2**(gamma2*h2)*d2**h2*r2 + alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + C2**(gamma2*h2)*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1);
def Edrug1D(d,E0,Em,C,h):
    return Em + (E0-Em) / (1 + (d/C)**h)
def surfGen(ax,yy,tt,zz,zmin=0.,zmax=1.):
    if np.abs(zmin) > np.abs(zmax): zmax = np.abs(zmin)
    else: zmin = -np.abs(zmax)
    # Plot the surface for real, with appropriate alpha
    alpha = .8
    surf = ax.plot_surface(np.log10(tt), np.log10(yy), zz, cstride=1, rstride=1, alpha=alpha, cmap = cm.plasma, vmin=zmin, vmax=zmax, linewidth=0)
    # colored curves on left and right
    lw = 5
    ax.plot(np.log10(tt.min()*np.ones(len(tt))),np.log10(yy[0,:]),zz[0,:],linewidth=lw)
    ax.plot(np.log10(tt.max()*np.ones(d1.shape)),np.log10(yy[0,:]),zz[-1,:],linewidth=lw)
    ax.plot(np.log10(tt[:,0]),np.log10(yy.min()*np.ones(len(yy))),zz[:,0],linewidth=lw)
    ax.plot(np.log10(tt[:,0]),np.log10(yy.max()*np.ones(len(yy))),zz[:,-1],linewidth=lw)
    for i in range(len(tt)):
        if i%5==0:
            ax.plot(np.log10(tt[i,0]*np.ones(d1.shape)),np.log10(yy[0,:]),zz[i,:],'-k',linewidth=1,alpha=.1)
    for i in range(len(yy)):
        if i%5==0:        
            ax.plot(np.log10(tt[:,0]),np.log10(yy[0,i]*np.ones(d1.shape)),zz[:,i],'-k',linewidth=1,alpha=.1)
    # Set the view
    ax.view_init(elev=19, azim=27)
    ax.set_ylabel("log(d1)")
    ax.set_xlabel("log(d2)")
    ax.set_zlabel("Effect")
    return ax


#Set parameters for surfaces
d1 = np.logspace(-5,0,50);
d2 = np.logspace(-5,0,50);
h1 = 1;
h2 = 1;
C1 = .005;
C2 = .001;
E0 = 1;
E1 = .5;
E2 = .5;
beta = 0;
alpha1 = 1;
alpha2 = 1;
gamma1 = 1;
gamma2 = 1;
r1 = 100*E0;
r2 = 100*E0;

#mat = np.zeros((100,100))
#for e1,E1 in enumerate(np.linspace(0,1,100)):
#    for e2,E2 in enumerate(np.linspace(0,1,100)):
#            ed = Edrug_2D_full(yy.flatten(),tt.flatten(),E0,E1,E2,beta,C1,C2,h1,h2,r1,r2,alpha1,alpha2,gamma1,gamma2).reshape(yy.shape)
#            bl = np.array(np.matrix(Edrug1D(d2,E0,E2,C2,h2)).T*np.matrix(Edrug1D(d1,E0,E1,C1,h1)))
#            mat[e1,e2]=ed[-1,-1]-bl[-1,-1]
#
#plt.figure()
#X,Y = np.meshgrid(np.linspace(0,1,100),np.linspace(0,1,100))
#ax = plt.subplot(111,projection='3d')
#ax.plot_surface(X,Y,mat,cmap=cm.plasma)
#
#Z = np.mean((X,Y),axis=0)+X+Y
#plt.figure()
#plt.scatter(Z.flatten(),mat.flatten())


yy,tt = np.meshgrid(d1,d2)
#ed = Edrug_2D_full(yy.flatten(),tt.flatten(),E0,E1,E2,beta,C1,C2,h1,h2,r1,r2,alpha1,alpha2,gamma1,gamma2).reshape(yy.shape)
#fig = plt.figure(figsize=(18,6))
#ax1 = fig.add_subplot(131, projection='3d')
#ax1 = surfGen(ax1,yy,tt,ed)
#ax2 = fig.add_subplot(132, projection='3d')
#bl = np.array(np.matrix(Edrug1D(d2,E0,E2,C2,h2)).T*np.matrix(Edrug1D(d1,E0,E1,C1,h1)))
#ax2 = surfGen(ax2,yy,tt,bl)
#ax3 = fig.add_subplot(133, projection='3d')
#ax2 = surfGen(ax3,yy,tt,ed-bl)
#ax1.set_title('MuSyC')
#ax2.set_title('Bliss')
#ax3.set_title('Difference')
#plt.show()
#plt.savefig('im0.png')






for e,i in enumerate(np.concatenate((np.linspace(.5,0,50),np.linspace(0,.5,50),np.linspace(.5,1.,50),np.linspace(1.0,.5,50)))):
    E1 = i;
    E2 = i;
    beta = 0;
    fig = plt.figure(figsize=(18,6))
    yy,tt = np.meshgrid(d1,d2)
    ed = Edrug_2D_full(yy.flatten(),tt.flatten(),E0,E1,E2,beta,C1,C2,h1,h2,r1,r2,alpha1,alpha2,gamma1,gamma2).reshape(yy.shape)
    ax1 = fig.add_subplot(131, projection='3d')
    ax1 = surfGen(ax1,yy,tt,ed)
    ax2 = fig.add_subplot(132, projection='3d')
    bl = np.array(np.matrix(Edrug1D(d2,E0,E2,C2,h2)).T*np.matrix(Edrug1D(d1,E0,E1,C1,h1)))
    ax2 = surfGen(ax2,yy,tt,bl)
    ax3 = fig.add_subplot(133, projection='3d')
    ax3 = surfGen(ax3,yy,tt,ed-bl,zmax=.25)
    ax1.set_title('MuSyC')
    ax2.set_title('Bliss')
    ax3.set_title('Difference')
    ax3.set_zlabel(r'$\Delta(MuSyC-Bliss)$')
    ax1.set_zlim(0,1.)
    ax2.set_zlim(0,1.)
    ax3.set_zlim(0,.25)
    plt.savefig('im'+str(e)+'.png')
    plt.close()
        
       
for e1,i in enumerate(np.concatenate((np.linspace(0,0.5,50),np.linspace(0.5,0.0,50)))):
    E1 = .5-i;
    E2 = .5+i;
    beta = 0;
    fig = plt.figure(figsize=(18,6))
    yy,tt = np.meshgrid(d1,d2)
    ed = Edrug_2D_full(yy.flatten(),tt.flatten(),E0,E1,E2,beta,C1,C2,h1,h2,r1,r2,alpha1,alpha2,gamma1,gamma2).reshape(yy.shape)
    ax1 = fig.add_subplot(131, projection='3d')
    ax1 = surfGen(ax1,yy,tt,ed)
    ax2 = fig.add_subplot(132, projection='3d')
    bl = np.array(np.matrix(Edrug1D(d2,E0,E2,C2,h2)).T*np.matrix(Edrug1D(d1,E0,E1,C1,h1)))
    ax2 = surfGen(ax2,yy,tt,bl)
    ax3 = fig.add_subplot(133, projection='3d')
    ax3 = surfGen(ax3,yy,tt,ed-bl,zmax=.25)
    ax1.set_title('MuSyC')
    ax2.set_title('Bliss')
    ax3.set_zlabel(r'$\Delta(MuSyC-Bliss)$')
    ax1.set_zlim(0,1)
    ax2.set_zlim(0,1)
    ax3.set_zlim(0,.25)
    plt.savefig('im'+str(e+e1+1)+'.png')
    plt.close()
 
    

covxy = np.zeros((100))
covxx = np.zeros((100))
covxz = np.zeros((100))
covyz = np.zeros((100))
covzz = np.zeros((100))
covyy = np.zeros((100))
for e,i in enumerate(np.linspace(0,1,100)):
    E1 = i;
    E2 = i;
    beta = 0;
    yy,tt = np.meshgrid(d1,d2)
    ed = Edrug_2D_full(yy.flatten(),tt.flatten(),E0,E1,E2,beta,C1,C2,h1,h2,r1,r2,alpha1,alpha2,gamma1,gamma2).reshape(yy.shape)
    bl = np.array(np.matrix(Edrug1D(d2,E0,E2,C2,h2)).T*np.matrix(Edrug1D(d1,E0,E1,C1,h1)))
    cov =np.cov([np.log10(yy).flatten(),np.log10(tt).flatten(),(ed-bl).flatten()])
    covxy[e] = cov[1,0]
    covxx[e] = cov[0,0]
    covxz[e] = cov[2,0]
    covyz[e] = cov[2,1]
    covzz[e] = cov[2,2]
    covyy[e] = cov[1,1]



fig = plt.figure()
ax  = plt.subplot(131)
plt.title('Cov(z,z)')
plt.plot(np.linspace(0,1,100),covzz)
ax  = plt.subplot(132)
plt.title('Cov(x,z)')
plt.plot(np.linspace(0,1,100),covxz)
ax  = plt.subplot(133)
plt.title('Cov(y,z)')
plt.plot(np.linspace(0,1,100),covyz)
plt.xlabel('E1=E2=...')

l,v = np.linalg.eig(np.cov([np.log10(yy).flatten(),np.log10(tt).flatten(),(ed-bl).flatten()]))
 

# Based on code written by Alex Lubbuck for the PyDRC package
import scipy.stats    
import pandas as pd
def ll4(x, b, c, d, e):
    """
    Four parameter log-logistic function ("Hill curve")

     - b: Hill slope
     - c: min response
     - d: max response
     - e: EC50
     """
    return c+(d-c)/(1+np.exp(b*(np.log(x)-np.log(e))))
# Fitting function
def fit_drc(doses, dip_rates, dip_std_errs=None, hill_fn=ll4,
            null_rejection_threshold=0.000063211):
    dip_rate_nans = np.isnan(dip_rates)
    if np.any(dip_rate_nans):
        doses = doses[~dip_rate_nans]
        dip_rates = dip_rates[~dip_rate_nans]
        if dip_std_errs is not None:
            dip_std_errs = dip_std_errs[~dip_rate_nans]
    popt = np.zeros((4,))*np.nan
    pcov = np.zeros((4,))*np.nan
    curve_initial_guess = list(ll4_initials(doses, dip_rates))
    if curve_initial_guess[0]<0: #If the hill slope is less than 0 set equal to 1
        curve_initial_guess[0] = 1
    if curve_initial_guess[3]>max(doses): #If the EC50 is greater than the maximum dose
        curve_initial_guess[3]=max(doses)
    if curve_initial_guess[3]<min(doses): #If the EC50 is less than the minimum dose
        curve_initial_guess[3]=min(doses[doses!=0])
    try:
        popt, pcov = scipy.optimize.curve_fit(hill_fn,
                                              doses,
                                              dip_rates,
                                              p0=curve_initial_guess,
                                              sigma=dip_std_errs,
                                              maxfev=100000
                                              )

    except RuntimeError:
        pass

    p=1 #pvalue
    if sum(np.isnan(popt))!=4:
        # DIP rate fit
        dip_rate_fit_curve = hill_fn(doses, *popt)
        # F test vs flat linear "no effect" fit
        ssq_model = ((dip_rate_fit_curve - dip_rates) ** 2).sum()
        ssq_null = ((np.mean(dip_rates) - dip_rates) ** 2).sum()
        df = len(doses) - 4
        f_ratio = (ssq_null-ssq_model)/(ssq_model/df)
        p = 1 - scipy.stats.f.cdf(f_ratio, 1, df)
        if p > null_rejection_threshold:
            popt = np.zeros((4,))*np.nan
            pcov = np.zeros((4,))*np.nan
    return popt, p, pcov

# Functions for finding initial parameter estimates for curve fitting
def ll4_initials(x, y):
    c_val, d_val = _find_cd_ll4(y)
    b_val, e_val = _find_be_ll4(x, y, c_val, d_val)
    return b_val, c_val, d_val, e_val

def _response_transform(y, c_val, d_val):
    return np.log((d_val - y) / (y - c_val))

def _find_be_ll4(x, y, c_val, d_val, slope_scaling_factor=1,
                 dose_transform=np.log,
                 dose_inv_transform=np.exp):
    x[x==0]=min(x[x!=0])/100 #deal with zero dose
    y = _response_transform(y,c_val,d_val)
    x = dose_transform(x[(~np.isnan(y)) & (~np.isinf(y))])
    y = y[(~np.isnan(y)) & (~np.isinf(y))]
    slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x,y)
    b_val = slope_scaling_factor * slope
    e_val = dose_inv_transform(-intercept / (slope_scaling_factor * b_val))
    if np.isnan(e_val):
        e_val = min(x)*100
    if np.isnan(b_val):
        b_val = 1
    return b_val, e_val

def _find_cd_ll4(y, scale=0.001):
    ymin = np.min(y)
    ymax = np.max(y)
    len_y_range = scale * (ymax - ymin)

    return ymin - len_y_range, ymax + len_y_range

import glob
from scipy.integrate import simps
###Read in data
kr=[];E1=[];E2=[];E1_std=[];E2_std=[];d1n=[];d2n=[];per_und=[];
E1_obs=[];E2_obs=[];
fils = glob.glob('Malarial_combinations/*_0.csv')
bl_hi=[]
bl_md=[]
bl_lw=[]
bl_men = []
bl_med = []
for f in fils:
    df = pd.read_csv(f,sep=',')
    d1 = df['drug1.conc']
    d2 = df['drug2.conc']
    dip = df['effect']/100.
    dip_sd = df['effect.95ci']/10.
    drug1_name = df['drug1'].unique()[0]
    drug2_name = df['drug2'].unique()[0]


    #Try and fit the single dose response curves....
    if sum(d1==0)==0 and sum(d2==0)==0:
        popt1 = np.zeros((4,))*np.NAN; perr1 = np.zeros((4,))*np.NAN
        popt2 = np.zeros((4,))*np.NAN; perr2 = np.zeros((4,))*np.NAN
    elif sum(d1==0)==0:
        print(drug1_name + ' has no zero condition...skipping single dose fit.')
        #Fit the single drug dose response curve
        popt1, p1 , pcov1 = fit_drc(d1[d2==0], dip[d2==0], dip_sd[d2==0])
        perr1 = np.sqrt(np.diag(pcov1)) if pcov1.any() else np.zeros((4,)) * np.NAN
        popt1 = popt1 if popt1.any() else np.zeros((4,)) * np.NAN
        popt2 = np.zeros((4,))*np.NAN; perr2 = np.zeros((4,))*np.NAN
    elif sum(d2==0)==0:
        print(drug2_name + ' has no zero condition...skipping single dose fit.')
        popt1 = np.zeros((4,))*np.NAN; perr1 = np.zeros((4,))*np.NAN
        popt2, p2 , pcov2 = fit_drc(d2[d1==0], dip[d1==0], dip_sd[d1==0])
        perr2 = np.sqrt(np.diag(pcov2)) if pcov2.any() else np.zeros((4,)) * np.NAN
        popt2 = popt2 if popt2.any() else np.zeros((4,)) * np.NAN
    else:
        popt1, p1 , pcov1 = fit_drc(d1[d2==0], dip[d2==0], dip_sd[d2==0])  
        popt2, p2 , pcov2 = fit_drc(d2[d1==0], dip[d1==0], dip_sd[d1==0])
        popt1 = popt1 if popt1.any() else np.zeros((4,)) * np.NAN
        popt2 = popt2 if popt2.any() else np.zeros((4,)) * np.NAN
        perr1 = np.sqrt(np.diag(pcov1)) if ~np.isnan(pcov1).any() else np.zeros((4,)) * np.NAN
        perr2 = np.sqrt(np.diag(pcov2)) if ~np.isnan(pcov1).any() else np.zeros((4,)) * np.NAN
    #If fit failed make approximations:
    if sum(np.isnan(popt1))>0:
        print(drug1_name + ' single fit failed.')              
        popt1 = np.array([np.nan,
                          np.nan,
                          np.nan,
                          np.nan])        
    else:
        print(drug1_name + ' single fit succeeded with p-val:' + '%.2e'%p1)              
        #Necessary to log the output of fit_drc
        popt1[3] = np.log10(popt1[3])
    if sum(np.isnan(popt2))>0:
        print(drug2_name + ' single fit failed.')
        popt2 = np.array([np.nan,
                          np.nan,
                          np.nan,
                          np.nan])              
    else:
        print(drug2_name + ' single fit succeeded with p-val:' + '%.2e'%p2)              
        #Necessary to log the output of fit_drc
        popt2[3] = np.log10(popt2[3])
        
        
    #If fit failed make approximations:
    if (sum(np.isnan(perr1))>0).all():
        print(drug1_name + ' negative coraviariance.')                     
        perr1 = np.array([np.nan,
                          np.nan,
                          np.nan,
                          np.nan])        
    if (sum(np.isnan(perr2))>0).all():
        print(drug2_name + ' negative coraviariance.')                            
        perr2 = np.array([np.nan,
                          np.nan,
                          np.nan,
                          np.nan])   

    E1.append(popt1[1])
    E2.append(popt2[1])
    E1_std.append(perr1[1])
    E2_std.append(perr2[1])
    E1_obs.append(Edrug1D(max(d1),popt1[2],popt1[1],10**popt1[3],popt1[0]))
    E2_obs.append(Edrug1D(max(d2),popt2[2],popt2[1],10**popt2[3],popt2[0]))

    
    d1n.append(drug1_name)
    d2n.append(drug2_name)
    bliss = [];d1tmp=d1[(d1!=0)&(d2!=0)];d2tmp=d2[(d1!=0)&(d2!=0)];
    for e in d1tmp.index:
        bliss.append(Edrug1D(d1tmp[e],popt1[2],popt1[1],10**popt1[3],popt1[0])*Edrug1D(d2tmp[e],popt2[2],popt2[1],10**popt2[3],popt2[0])-dip[(d1==d1tmp[e])&(d2==d2tmp[e])].values[0])
    bliss = np.array(bliss)
    bl_men.append(np.nanmedian(bliss))
    d1un = np.sort(np.unique(d1))
    d2un = np.sort(np.unique(d2))
    stp1 = len(d1un)/2
    stp2 = len(d2un)/2
    mask1 = (d1tmp>d1un[-stp1-1]) & (d2tmp>d2un[-stp2-1])
    mask2 = (d1tmp>d1un[-2*stp1+2]) & (d2tmp>d2un[-2*stp2+2]) & ~mask1
    mask3 = (~mask1) & (~mask2)
    bl_hi.append(np.nanmedian(bliss[mask1]))
    bl_md.append(np.nanmedian(bliss[mask2]))
    bl_lw.append(np.nanmedian(bliss[mask3]))
    
    

import matplotlib.pylab as plt
from matplotlib.pyplot import cm
from mpl_toolkits.mplot3d import *
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
from adjustText import adjust_text
import scipy.stats as st
font = {'family' : 'normal',
        'weight':'bold',
        'size'   : 12}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)



fig = plt.figure(figsize=(10,10))
ax = plt.subplot(111,projection='3d')

ax.scatter(E1_obs,E2_obs,bl_men)
ax.set_xlim(-.1,.5)
ax.set_ylim(-.1,.5)
ax.set_zlim(-.1,.1)


mask = (E1_obs>.8)&(E2_obs>.8)&(bl_men<-.57)&(E1_obs<1.)&(E2_obs<1.)
for idx in  np.where(mask):
    print 'E1_obs ' + str(E1_obs[idx])+'\nE2_obs ' +str(E2_obs[idx])+'\nbliss median ' +str(bl_men[idx])

E1_obs = np.array(E1_obs)
E2_obs = np.array(E2_obs)
bl_men = np.array(bl_men)
fig = plt.figure(figsize=(10,10))
ax = plt.subplot(111)
mask1 = (E1_obs<.25)&(E2_obs<.25)
mask2 = (E1_obs<.75)&(E2_obs<.75)&(~mask1)
mask3 = (E1_obs>=.75)&(E2_obs>=.75)&(~mask2)&(~mask1)
ax.boxplot(bl_men[mask1],notch=True,positions=[0],labels=['E1(obs),E2(obs)<.25'])
ax.boxplot(bl_men[mask2],notch=True,positions=[1],labels=['E1(obs),E2(obs)<.75'])
ax.boxplot(bl_men[mask3],notch=True,positions=[2],labels=['E1(obs),E2(obs)>.75'])
ax.set_xlim(-1,3)



fig = plt.figure(figsize=(10,10))
ax = plt.subplot(121)
ax.scatter(E1_obs,blcovxz)
ax.set_xlim(-.1,1.)
ax.set_ylim(-4.,4)
ax2 = plt.subplot(122)
ax2.scatter(E2_obs,blcovyz)
ax2.set_xlim(-.1,1.)
ax2.set_ylim(-4.,4)






fig = plt.figure(figsize=(10,10))
ax = plt.subplot(111,projection='3d')

ax.scatter(E1_obs,E2_obs,blcovxz)
ax.set_xlim(-.1,1)
ax.set_ylim(-.1,1)
ax.set_zlim(-2,2)



plt.figure()
plt.plot(np.log(d1[d2==min(d2)]),Edrug1D(d1[d2==min(d2)],popt1[2],popt1[1],10**popt1[3],popt1[0]))
plt.scatter(np.log(d1[d2==min(d2)]),dip[d2==min(d2)])


plt.figure()
ax = plt.subplot(111,projection='3d')
ax.plot_trisurf(np.log10(d1),np.log10(d2),v)
ax.scatter(np.log10(d1),np.log10(d2),dip)
ax.set_zlim(-.1,3)







E1_obs = np.array(E1_obs)
E2_obs = np.array(E2_obs)
bl_men = np.array(bl_men)
bl_hi  = np.array(bl_hi)
bl_md  = np.array(bl_md)
bl_lw  = np.array(bl_lw)
fig = plt.figure(figsize=(10,5))
ax = plt.subplot(111)
mask1 = (E1_obs<.25)&(E2_obs<.25)
mask2 = (E1_obs<.75)&(E2_obs<.75)&(~mask1)
mask3 = (E1_obs>=.75)&(E2_obs>=.75)&(~mask2)&(~mask1)
ax.boxplot(bl_men[mask1],notch=True,positions=[0],labels=['E1(obs),E2(obs)<.25'])
ax.boxplot(bl_men[mask2],notch=True,positions=[1],labels=['E1(obs),E2(obs)<.75'])
ax.boxplot(bl_men[mask3],notch=True,positions=[2],labels=['E1(obs),E2(obs)>.75'])
ax.set_xlim(-1,3)
ax.set_xticks([0,1,2])
ax.set_xticklabels(['E1(obs),E2(obs)<.25','.25<E1(obs),E2(obs)<.5','E1(obs),E2(obs)>.75'])
ax.set_ylabel('del(Bliss) [>0=synergy]')




fig = plt.figure(figsize=(10,10))
ax = plt.subplot(111)
mask1 = (E1_obs<.25)&(E2_obs<.25)
mask2 = (E1_obs<.75)&(E2_obs<.75)&(~mask1)
mask3 = (E1_obs>=.75)&(E2_obs>=.75)&(~mask2)&(~mask1)
ax.boxplot(bl_lw[mask1],notch=True,positions=[-.2],widths=.2,labels=['E1(obs),E2(obs)<.25'])
ax.boxplot(bl_lw[mask2],notch=True,positions=[.8] ,widths=.2,labels=['E1(obs),E2(obs)<.75'])
ax.boxplot(bl_lw[mask3],notch=True,positions=[1.8],widths=.2,labels=['E1(obs),E2(obs)>.75'])
ax.boxplot(bl_md[mask1],notch=True,positions=[0]  ,widths=.2,labels=['E1(obs),E2(obs)<.25'])
ax.boxplot(bl_md[mask2],notch=True,positions=[1]  ,widths=.2,labels=['E1(obs),E2(obs)<.75'])
ax.boxplot(bl_md[mask3],notch=True,positions=[2]  ,widths=.2,labels=['E1(obs),E2(obs)>.75'])
ax.boxplot(bl_hi[mask1],notch=True,positions=[0.2],widths=.2,labels=['E1(obs),E2(obs)<.25'])
ax.boxplot(bl_hi[mask2],notch=True,positions=[1.2],widths=.2,labels=['E1(obs),E2(obs)<.75'])
ax.boxplot(bl_hi[mask3],notch=True,positions=[2.2],widths=.2,labels=['E1(obs),E2(obs)>.75'])
ax.set_xlim(-1,3)
ax.set_xticklabels(['E1(obs),E2(obs)<.25','.25<E1(obs),E2(obs)<.5','E1(obs),E2(obs)>.75'])
ax.set_xticks([0,1,2])
ax.set_ylabel('del(Bliss) [>0=synergy]')
ax.text(-.2,.4,'A')
ax.text(-.0,.4,'B')
ax.text(.2,.4,'C')




fig = plt.figure(figsize=(10,10))
ax = plt.subplot(111)
ax.boxplot(bl_lw[~np.isnan(bl_lw)],notch=True,positions=[0],labels=['E1(obs),E2(obs)<.25'])
ax.boxplot(bl_md[~np.isnan(bl_lw)],notch=True,positions=[1],labels=['E1(obs),E2(obs)<.75'])
ax.boxplot(bl_hi[~np.isnan(bl_lw)],notch=True,positions=[2],labels=['E1(obs),E2(obs)>.75'])
ax.set_xlim(-1,3)
ax.set_xticklabels(['A','B','C'])
ax.set_xticks([0,1,2])
ax.set_ylabel('del(Bliss) [>0=synergy]')


stats.ttest_ind(bl_lw[~np.isnan(bl_lw)], bl_md[~np.isnan(bl_lw)], axis=0, equal_var=True)




d1n=[];d2n=[];
E1_obs=[];E2_obs=[];
fils = glob.glob('Malarial_combinations/*_0.csv')
bl_hi=[]
bl_md=[]
bl_lw=[]
bl_men = []
for e1,f in enumerate(fils):
    df = pd.read_csv(f,sep=',')
    d1 = df['drug1.conc']
    d2 = df['drug2.conc']
    dip = df['effect']/100.
    dip_sd = df['effect.95ci']/10.
    drug1_name = df['drug1'].unique()[0]
    drug2_name = df['drug2'].unique()[0]
    
    E1_obs.append(np.mean(dip[(d2==min(d2))&(d1==max(d1))]))
    E2_obs.append(np.mean(dip[(d1==min(d1))&(d2==max(d2))]))

    d1n.append(drug1_name)
    d2n.append(drug2_name)
    bliss = [];d1tmp=d1[(d1!=0)&(d2!=0)];d2tmp=d2[(d1!=0)&(d2!=0)];
    for e in d1tmp.index:
        bliss.append(dip[(d1==d1tmp[e])&(d2==0)].values[0]*dip[(d2==d2tmp[e])&(d1==0)].values[0]-dip[(d1==d1tmp[e])&(d2==d2tmp[e])].values[0])
    bliss = np.array(bliss)
    bl_men.append(np.nanmedian(bliss))
    d1un = np.sort(np.unique(d1))
    d2un = np.sort(np.unique(d2))
    stp1 = len(d1un)/2
    stp2 = len(d2un)/2
    mask1 = (d1tmp>d1un[-stp1-1]) & (d2tmp>d2un[-stp2-1])
    mask2 = (d1tmp>d1un[-2*stp1+2]) & (d2tmp>d2un[-2*stp2+2]) & ~mask1
    mask3 = (~mask1) & (~mask2)
    bl_hi.append(np.nanmedian(bliss[mask1]))
    bl_md.append(np.nanmedian(bliss[mask2]))
    bl_lw.append(np.nanmedian(bliss[mask3]))
    print e1

