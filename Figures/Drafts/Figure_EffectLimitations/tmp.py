#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 20:40:28 2019

@author: xnmeyer
"""


################################################################################
################################################################################

T2 = pd.read_csv("../../Data/" + fit_fil2)
T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.9)]
T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]

df = T2
df = df[~np.isnan(df['bliss_fit_mxd1d2'])]
cell_line = []
drug =[]
emax=[]
emax_sd=[]
ec50=[]
ec50_sd=[]
bliss = []

for c in df['sample'].unique():
    sub_df = df[df['sample']==c]
    dgls = np.unique(list(sub_df['drug2_name'].unique())+list(sub_df['drug1_name'].unique()))
    for d in dgls:
        emax.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['E1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['E2'].mean()))))
        cell_line.append(c)
        drug.append(d)
        ec50.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['C1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['C2'].mean()))))

sensitivity_map = pd.DataFrame({'sample':cell_line,'drug':drug,'emax':emax}) 
        
sens_map = sensitivity_map.groupby('drug')[['emax']].mean().merge(pd.read_csv('drug_classes.csv'),left_on='drug',right_on='drug_name')
sens_map = sens_map.sort_values('emax').reset_index(drop=False)

grp_cols = ['#fffe40','aqua','springgreen']
fig = plt.figure(figsize=(6,3.5),facecolor='w')
ax = []
ax.append(plt.subplot2grid((13,12),(2,0),colspan=7,rowspan=7))
ax.append(plt.subplot2grid((13,12),(0,0),colspan=7,rowspan=2))
ax.append(plt.subplot2grid((13,12),(2,7),colspan=2,rowspan=7))
ax.append(plt.subplot2grid((13,12),(5,9),colspan=2,rowspan=5))
ax.append(plt.subplot2grid((13,12),(9,0),colspan=3,rowspan=3))


mat = np.zeros((len(sens_map),len(sens_map)))*np.nan
for e1,d1 in enumerate(sens_map['drug_name']):
    for e2,d2 in enumerate(sens_map['drug_name']):
        if e1!=e2:
            msk = ((df['drug1_name']==d1)&(df['drug2_name']==d2))|((df['drug1_name']==d2)&(df['drug2_name']==d1))
            mat[e1,e2] = df.loc[msk,'bliss_fit_mxd1d2'].mean()

to_remove = np.where(np.sum(np.isnan(mat),axis=0)>15)[0]
mat=np.delete(mat,to_remove,axis=1)
mat=np.delete(mat,to_remove,axis=0)

sens_map = sens_map.loc[~np.in1d(sens_map.index.values,to_remove)]
sens_map.reset_index(drop=True,inplace=True)
hm = ax[0].pcolor(mat,cmap=cm.seismic,vmin=np.nanmin(mat), vmax=np.nanmax(mat))
hm.cmap.set_under('silver')
plt.sca(ax[0])
plt.axis('off')

mask = []
mask.append((sens_map['emax']<.15))
mask.append((sens_map['emax']>.15)&(sens_map['emax']<.5))
mask.append((sens_map['emax']>.5))
p=[]
for e,m in enumerate(mask):
    x = np.where(m)[0][0]
    w = sum(m)
    p.append(Rectangle((x,x),w,w,fill=False,color=grp_cols[e],linewidth=3,zorder=100))
    ax[0].add_patch(p[-1])

un_class = list(sens_map['class'].unique())
cols = cm.nipy_spectral(np.linspace(0,1,len(sens_map['class'].unique())))
col_list = []
for ind in sens_map.index:
   col_list.append(cols[un_class.index(sens_map.loc[ind,'class'])])

ax[1].bar(range(len(sens_map)),sens_map['emax'],width=1,color=col_list,edgecolor=col_list,lw=0)
ax[1].set_xticks(np.arange(len(sens_map)))
ax[1].set_xlim((0,len(sens_map)-.5))
ax[1].set_xticklabels([])
ax[1].set_ylabel("%-viability",labelpad=0)
ax[1].set_title("Avg Emax Across Cell Panel\n<---Sensitive   Resistant--->",fontsize=8)
ax[1].set_ylim((0,1))
ax[1].set_yticks([0,.5,1])
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].xaxis.set_ticks_position('bottom')
ax[1].yaxis.set_ticks_position('left')

ax[2].barh(range(len(sens_map)),sens_map['emax'],height=1,color=col_list,edgecolor=col_list,lw=0)
ax[2].set_yticks(np.arange(len(sens_map)))
ax[2].set_ylim((0,len(sens_map)-.5))
ax[2].set_yticklabels([])
ax[2].set_xlabel("%-viability",labelpad=0)
ax[2].set_xlim((0,1))
ax[2].set_xticks([0,.5,1])
ax[2].spines['top'].set_visible(False)
ax[2].spines['right'].set_visible(False)
ax[2].xaxis.set_ticks_position('bottom')
ax[2].yaxis.set_ticks_position('left')


ax[3].patch.set_facecolor('w')
ax[3].patch.set_alpha(0.)
ax[3].spines['top'].set_visible(False)
ax[3].spines['right'].set_visible(False)
ax[3].spines['bottom'].set_visible(False)
ax[3].spines['left'].set_visible(False)
ax[3].set_xticks([])
ax[3].set_yticks([])
ax[3].set_ylim((0,1.1))
ax[3].set_xlim((0,1))

for e,i in enumerate(un_class):
    ax[3].scatter(.05,1-(e)/10.,s=50,marker='s',c=cols[e])
    ax[3].text(.25,1-(e)/10.,i,ha='left',va='center')




mask1 = []
mask1.append(sens_map['emax']<.15)
mask1.append((sens_map['emax']>.15)&(sens_map['emax']<.5))
mask1.append((sens_map['emax']>.5))
labs = ['Emax<15%','15%<Emax<50%','50%<Emax']
mask = []
for j in range(3):
    mask.append(np.in1d(df['drug1_name'],sens_map['drug_name'].loc[mask1[j]]) | np.in1d(df['drug2_name'],sens_map['drug_name'].loc[mask1[j]]))

p = []
for e in range(len(mask)):
    p.append(ax[4].boxplot(df['bliss_fit_mxd1d2'].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False,zorder=20))

for e,bp in enumerate(p):
    plt.setp(bp['medians'],color='r',linewidth=2)
    plt.setp(bp['whiskers'],color='k')
    plt.setp(bp['caps'],color='k')
    plt.setp(bp['boxes'],edgecolor='k')
    
    for patch in bp['boxes']:
        patch.set(facecolor=grp_cols[e]) 

ax[4].set_xticks([])
ax[4].set_xlim((-.5,2.5))
ax[4].set_xticklabels([])
ax[4].set_ylabel("Bliss @ max(d1,d2)",labelpad=-2)
ax[4].set_ylim((-.3,.2))
ax[4].set_yticks([-.2,0.,.2])
ax[4].spines['top'].set_visible(False)
ax[4].spines['right'].set_visible(False)
ax[4].spines['bottom'].set_position('zero')
ax[4].xaxis.set_ticks_position('bottom')
ax[4].yaxis.set_ticks_position('left')
ax[4].tick_params(axis='x',direction='out')
ax[4].legend([bp['boxes'][0] for bp in p], labs,ncol=1,bbox_to_anchor=(1.05, 1.0))
ax[4].plot((0,1),(-.3,-.3),color='k',linewidth=2,clip_on=False)
ax[4].plot((1,2),(-.4,-.4),color='k',linewidth=2,clip_on=False)
ax[4].plot((0,2),(-.5,-.5),color='k',linewidth=2,clip_on=False)

for e in range(2):
    print ttest_ind(df['bliss_fit_mxd1d2'].loc[mask[e]].values,df['bliss_fit_mxd1d2'].loc[mask[e+1]].values).pvalue
print ttest_ind(df['bliss_fit_mxd1d2'].loc[mask[0]].values,df['bliss_fit_mxd1d2'].loc[mask[2]].values).pvalue
