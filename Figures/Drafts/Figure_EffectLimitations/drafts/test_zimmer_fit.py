from pythontools.synergy import synergy_tools, synergy_plots
import numpy as np
import pandas as pd

# Make up some fake parameters
E0, E1, E2, E3, alpha1, alpha2, d1min, d1max, d2min, d2max, C1, C2, h1, h2, r1, r1r, r2, r2r = synergy_tools.get_example_params()

# 1D numpy arrays containing doses of drug1 and 2
d1 = np.logspace(d1min,d1max,50)
d2 = np.logspace(d2min,d2max,50)

# Meshgrid of doses
DD1, DD2 = np.meshgrid(d1,d2)




E = synergy_tools.hill_2D(DD1, DD2, E0, E1, E2, E3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
zC1, zC2, zh1, zh2, a12, a21, perr = synergy_tools.zimmer(DD1, DD2, E)
print zC1, zC2, zh1, zh2, a12, a21

#zE = synergy_tools.zimmer_effective_dose(DD1, DD2, np.power(10.,zC1), np.power(10.,zC2), zh1, zh2, a12, a21)




E0, E1, E2, E3, alpha1, alpha2, d1min, d1max, d2min, d2max, C1, C2, h1, h2, r1, r1r, r2, r2r = synergy_tools.get_example_params(alpha1=3., alpha2=0.3)
E = synergy_tools.hill_2D(DD1, DD2, E0, E1, E2, E3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
zC1, zC2, zh1, zh2, a12, a21, perr = synergy_tools.zimmer(DD1, DD2, E)
print zC1, zC2, zh1, zh2, a12, a21




E0, E1, E2, E3, alpha1, alpha2, d1min, d1max, d2min, d2max, C1, C2, h1, h2, r1, r1r, r2, r2r = synergy_tools.get_example_params(E1=0.5,E2=0.5)
E = synergy_tools.hill_2D(DD1, DD2, E0, E1, E2, E3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
zC1, zC2, zh1, zh2, a12, a21, perr = synergy_tools.zimmer(DD1, DD2, E)
print zC1, zC2, zh1, zh2, a12, a21
