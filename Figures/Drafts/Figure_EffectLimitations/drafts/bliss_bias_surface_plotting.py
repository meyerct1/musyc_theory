#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 24 12:48:18 2019

@author: xnmeyer
"""





T2 = pd.read_csv("../../Data/" + fit_fil2)
T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.9)]
T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]

df = T2
df = df[~np.isnan(df['bliss_fit_mxd1d2'])]
cell_line = []
drug =[]
emax=[]
emax_sd=[]
ec50=[]
ec50_sd=[]
bliss = []

for c in df['sample'].unique():
    sub_df = df[df['sample']==c]
    dgls = np.unique(list(sub_df['drug2_name'].unique())+list(sub_df['drug1_name'].unique()))
    for d in dgls:
        emax.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['E1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['E2'].mean()))))
        cell_line.append(c)
        drug.append(d)
        ec50.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['C1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['C2'].mean()))))
        bliss.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['bliss_fit_mxd1d2'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['bliss_fit_mxd1d2'].mean()))))

sensitivity_map = pd.DataFrame({'sample':cell_line,'drug':drug,'emax':emax,'bliss':bliss}) 
        
sens_map = sensitivity_map.groupby('drug')[['emax','bliss']].mean().merge(pd.read_csv('drug_classes.csv'),left_on='drug',right_on='drug_name')
sens_map = sens_map.sort_values('emax').reset_index(drop=False)

un_class = list(sens_map['class'].unique())
cols = cm.nipy_spectral(np.linspace(0,1,len(sens_map['class'].unique())))
col_list = []
for ind in sens_map.index:
   col_list.append(cols[un_class.index(sens_map.loc[ind,'class'])])

fig = plt.figure(figsize=(6,3.5),facecolor='w')
ax = []
ax.append(plt.subplot2grid((2,5),(0,0),colspan=4))
ax.append(plt.subplot2grid((2,5),(1,0),colspan=4))
ax.append(plt.subplot2grid((3,5),(0,4),rowspan=2))

ax[0].bar(range(len(sens_map)),sens_map['emax'],width=1,color=col_list,edgecolor=col_list,lw=0)
ax[0].set_xticks(np.arange(len(sens_map))+.5)
ax[0].set_xticklabels([])
ax[0].set_ylabel("%-viability",labelpad=-2)
ax[0].set_title("Avg Efficacy Across Cell Panel\n<---Sensitive   Resistant--->",fontsize=8)
ax[0].set_ylim((0,1))
ax[0].set_yticks([0,.5,1])
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')

for i in range(len(sens_map)):
        msk  = np.in1d(df['drug1_name'],sens_map['drug_name'].loc[i]) | np.in1d(df['drug2_name'],sens_map['drug_name'].loc[i])
        bp = ax[1].boxplot(df['bliss_fit_mxd1d2'].loc[msk].values,positions=[i],notch=False,patch_artist=True,widths=.75,showfliers=False,zorder=20,showcaps=False,whis=.5)
        plt.setp(bp['medians'],color='r',linewidth=2)
        plt.setp(bp['whiskers'],color='k')
        plt.setp(bp['caps'],color='k')
        plt.setp(bp['boxes'],edgecolor='k')
        
        for patch in bp['boxes']:
            patch.set(facecolor=col_list[i]) 
        
#ax[1].bar(range(len(sens_map)),sens_map['bliss'],width=1,color=col_list,edgecolor=col_list,lw=0)
ax[1].set_xticks(np.arange(len(sens_map))+.5)
ax[1].set_xticklabels([])
ax[1].set_ylabel("Bliss @ max(d1,d2)",labelpad=-2)
ax[1].text(20,.01,"Avg Bliss Across\nDrug Combination Panel",ha='center')
ax[1].set_ylim((-.2,.1))
ax[1].set_yticks([-.1,0.])
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].spines['bottom'].set_position('zero')
ax[1].xaxis.set_ticks_position('bottom')
ax[1].yaxis.set_ticks_position('left')
ax[1].tick_params(axis='x',direction='out')

mask1 = []
mask1.append(sens_map['emax']<.15)
mask1.append((sens_map['emax']>.15)&(sens_map['emax']<.45))
mask1.append((sens_map['emax']>.45)&(sens_map['emax']<.75))
mask1.append((sens_map['emax']>.75))
labs = ['Emax<15%','15%<Emax<45%','45%<Emax<75%','Emax>75%']
mask = []
for j in range(4):
    mask.append(np.in1d(df['drug1_name'],sens_map['drug_name'].loc[mask1[j]]) | np.in1d(df['drug2_name'],sens_map['drug_name'].loc[mask1[j]]))

p = []
for e in range(len(mask)):
    p.append(ax[2].boxplot(df['bliss_fit_mxd1d2'].loc[mask[e]].values,positions=[np.mean(np.where(mask1[e])[0])],notch=False,patch_artist=True,widths=.75,showfliers=False,zorder=20))

for e,bp in enumerate(p):
    plt.setp(bp['medians'],color='r',linewidth=2)
    plt.setp(bp['whiskers'],color='k')
    plt.setp(bp['caps'],color='k')
    plt.setp(bp['boxes'],edgecolor='k')
    
    for patch in bp['boxes']:
        patch.set(facecolor=cols[e]) 

ax[2].set_xticks([])
ax[2].set_xlim((0,len(sens_map)))
ax[2].set_xticklabels([])
ax[2].set_ylabel("Bliss @ max(d1,d2)",labelpad=-2)
ax[2].set_ylim((-.2,.1))
ax[2].set_yticks([-.1,0.])
ax[2].spines['top'].set_visible(False)
ax[2].spines['right'].set_visible(False)
ax[2].spines['bottom'].set_position('zero')
ax[2].xaxis.set_ticks_position('bottom')
ax[2].yaxis.set_ticks_position('left')
ax[2].tick_params(axis='x',direction='out')
ax[2].legend([bp['boxes'][0] for bp in p], labs,ncol=1,bbox_to_anchor=(1.35, 1.2))

ax[3].patch.set_facecolor('w')
ax[3].patch.set_alpha(0.)
ax[3].spines['top'].set_visible(False)
ax[3].spines['right'].set_visible(False)
ax[3].spines['bottom'].set_visible(False)
ax[3].spines['left'].set_visible(False)
ax[3].set_xticks([])
ax[3].set_yticks([])
ax[3].set_ylim((0,1.1))
ax[3].set_xlim((0,1))

for e,i in enumerate(un_class):
    ax[3].scatter(.05,1-(e)/10.,s=50,marker='s',c=cols[e])
    ax[3].text(.25,1-(e)/10.,i,ha='left',va='center')

plt.tight_layout()
plt.savefig('bliss_drug_class_bias.pdf', bbox_inches = 'tight',pad_inches = 0)


e = 2
print ttest_ind(df['bliss_fit_mxd1d2'].loc[mask[1]].values,df['bliss_fit_mxd1d2'].loc[mask[3]].values)



##    
#xaxis = "avg_Emax"
#df[xaxis] = df[['E1','E2']].mean(axis=1)
#df = df[df['avg_Emax']<.75].reset_index(drop=True)
#T2[xaxis] = T2[['E1','E2']].mean(axis=1)
#T2 = T2[T2['avg_Emax']<.75].reset_index(drop=True)
#
## Calculate the moving windows of the scattered data
#percentiles = [i/10. for i in range(1,10)]
#n_steps=1000
#window_size=0.01 # This is in h-space
#window_df = moving_window_percentiles(df, xaxis, "bliss_fit_mxd1d2", percentiles = percentiles, n_steps=n_steps, window_size=window_size)
#window_df2 = moving_window_percentiles(T2, xaxis, "beta", percentiles = percentiles, n_steps=n_steps, window_size=window_size)
#x = np.float64(window_df['xmid'])
#
#xlim=(0.,.75)
#ylim=(-2,2)





ax.plot_trisurf(x,y,z,cmap=cm.cool,antialiased=False)

####
#
bin_size = 2000
df = df[(df['E1']<1.)&(df['E2']<1.)]
stp = len(df)/bin_size
df['E1E2'] = np.sqrt(df['E1']**2+df['E2']**2)
l = np.sort(df['E1E2'])
r_int = np.sort(df['E1E2'])[0::bin_size]
x=[];y=[];z=[];
c_x=[];c_y=[];c_z=[];
import math
for i in range(len(r_int)):
    if i==len(r_int)-1:
        rad_range = [r_int[i],df['E1E2'].max()]
    else:
        rad_range = [r_int[i],r_int[i+1]]
    msk = (df['E1E2']>rad_range[0]) & (df['E1E2']<rad_range[1])
    val = df.loc[msk,'bliss_fit_ec50'].mean()
    r_ran = np.linspace(rad_range[0],rad_range[1],50)
    for r in r_ran:
        for t in np.linspace(0,math.pi/2,50):
            x.append(r*math.cos(t))
            y.append(r*math.sin(t))
            z.append(val)
            
            
plt.figure()
ax = plt.subplot(111,projection='3d')
ax.plot_trisurf(x,y,z,cmap=cm.cool,antialiased=False)

from scipy.stats import gaussian_kde
kdx = gaussian_kde(df['E1'])
x = np.linspace(df['E1'].min(),df['E1'].max(),stp)
plt.figure()

y = np.linspace(df['E2'].min(),df['E2'].max(),stp)
for e1,E1 in enumerate(x[0:-1]):
    for e2,E2 in enumerate(y[0:-1]):
        msk = (df['E1']>E1)&(df['E1']<x[e1+1])&(df['E2']>E2)&(df['E2']<y[e2+1])
        mat[e1,e2] = df['bliss_fit_all'].loc[msk].mean()

plt.figure()
X,Y = np.meshgrid(x,y)
ax = plt.subplot(111,projection='3d')
ax.plot_surface(X,Y,mat)














fig = plt.figure(figsize=(2.5,1.25),facecolor='w')
ax = []
ax.append(plt.subplot(111))
ax[0].scatter(df[xaxis],df['bliss_fit_mxd1d2'],alpha=0.05, s=.5,rasterized=True)
plot_quantile_bias(window_df, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
ax[0].set_xlabel('Avg(E1,E2)')
ax[0].set_xticks([0.,.5])
ax[0].set_ylabel("Bliss @ EC50",labelpad=-4)
ax[0].set_yticks([-.1,0,.1])
ax[0].set_ylim((-.25,.25))
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')
ax[0].set_title('Bliss biased by Emax',fontsize=8)
plt.subplots_adjust(hspace=.0)
plt.savefig('bliss_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)

fig = plt.figure(figsize=(2.5,1.25),facecolor='w')
ax = []
ax.append(plt.subplot(111))
ax[0].scatter(df[xaxis],df['beta'],alpha=0.05, s=.5,rasterized=True)
plot_quantile_bias(window_df2, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
ax[0].set_xlabel('Avg(E1,E2)')
ax[0].set_xticks([0.,.5])
ax[0].set_ylabel(r"$\beta$",labelpad=-4)
ax[0].set_yticks([-1,0,1])
ax[0].set_ylim((-1,1))
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')
plt.subplots_adjust(hspace=.0)
plt.savefig('beta_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)
 



