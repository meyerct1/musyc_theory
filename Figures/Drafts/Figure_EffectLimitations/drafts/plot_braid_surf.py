#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 21 10:49:03 2018

@author: xnmeyer
"""

#Braid equation
import numpy as np
import matplotlib.pylab as plt
from mpl_toolkits.mplot3d import *

def braid_eq(d1,d2,E0,E1,E2,E3,h1,h2,C1,C2,kappa,delta):
    xb = (E2-E0)/(E3-E0)
    xa = (E1-E0)/(E3-E0)
    cb = (d2/C2)**h2
    ca = (d1/C1)**h1
    Db = xb*cb/(1+(1-xb)*cb)
    Da = xa*ca/(1+(1-xa)*ca)
    Dab = Da**(1/(delta*np.sqrt(h1*h2)))+Db**(1/(delta*np.sqrt(h1*h2))) + kappa*np.sqrt(Da**(1/(delta*np.sqrt(h1*h2)))*Db**(1/(delta*np.sqrt(h1*h2))))
    return E0+(E3-E0)/(1+Dab**(-delta*np.sqrt(h1*h2)))


Ebraid = braid_eq(DD1.reshape(-1,),DD2.reshape(-1,),1,0,0,0,1,1,np.power(10.,-7.5),np.power(10.,-7.5),0,1)
DD1tmp=DD1
DD1tmp[DD1==0]=np.min(DD1[DD1!=0])/10
DD2tmp=DD2
DD2tmp[DD2==0]=np.min(DD2[DD2!=0])/10

plt.figure()
ax = plt.subplot(111,projection='3d')
ax.scatter3D(np.log10(DD1tmp.reshape(-1,)),np.log10(DD2tmp.reshape(-1,)),Ebraid)


def zip_eq(d1,d2,h1,h2,C1,C2,C1p,C2p):
    x1 = (d1/C1)**h1/(1+(d1/C1)**h1)
    x2 = 