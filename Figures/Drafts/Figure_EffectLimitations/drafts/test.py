from pythontools.synergy import synergy_tools, synergy_plots
import numpy as np
import pandas as pd

# Make up some fake parameters
E0, E1, E2, E3, alpha1, alpha2, d1min, d1max, d2min, d2max, C1, C2, h1, h2, r1, r1r, r2, r2r = synergy_tools.get_example_params(E2=0.5, alpha1=2., alpha2=0.5, h1=0.5, E3=0.1)

# 1D numpy arrays containing doses of drug1 and 2
d1 = np.logspace(d1min,d1max,10) # 10 doses
d2 = np.logspace(d2min,d2max,20) # 20 doses

# Meshgrid of doses
DD1, DD2 = np.meshgrid(d1,d2)

# Transform C (logscale) -> c (linear)
c1 = np.power(10.,C1)
c2 = np.power(10.,C2)

###
# PLOT VARIOUS HEATMAPS AND SURFACES
###

# MuSyC
E = synergy_tools.hill_2D(DD1, DD2, E0, E1, E2, E3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
scatter_points = synergy_tools.get_test_scatter_points(DD1, DD2, E, logspace=False, ndivs=2) # Fake data to put into scatterplot

# Plot the surface
synergy_plots.plot_surface(d1min, d1max, d2min, d2max, E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, alpha1, alpha2, scatter_points=scatter_points, fname="musyc_surface.pdf")

# Calculate loewe and bliss synergies
l = synergy_tools.loewe(DD1, DD2, E, E0, E1, E2, h1, h2, c1, c2)
E1_alone = synergy_tools.hill_1D(DD1, E0, E1, h1, c1)
E2_alone = synergy_tools.hill_1D(DD2, E0, E2, h2, c2)
b = synergy_tools.bliss(E1_alone, E2_alone, E)

# Make heatmaps/contours of bliss and loewe
synergy_plots.plot_surface_contour(DD1, DD2, l, xlabel="Drug1", ylabel="Drug2", title="Loewe", fname="loewe_heatmap.pdf", semilog=True)
synergy_plots.plot_surface_contour(DD1, DD2, b, xlabel="Drug1", ylabel="Drug2", title="Bliss", fname="bliss_heatmap.pdf", semilog=True)


# Fit Zimmer params (Note my Zimmer code assumes you already know c1, c2, h1, and h2 - it does not fit them)
a12, a21, perr = synergy_tools.zimmer(scatter_points['drug1.conc'], scatter_points['drug2.conc'], scatter_points['effect'], c1, c2, h1, h2, sigma=scatter_points['effect.95ci'])
#E_zimmer = synergy_tools.zimmer_effective_dose(DD1, DD2, c1, c2, h1, h2, a12, a21)
#synergy_plots.plot_arbitrary_surface(DD1, DD2, E_zimmer, title="Zimmer", scatter_points=scatter_points, fname="zimmer_surface.pdf")


# Schindler
schindler_synergy = synergy_tools.schindler(DD1, DD2, E, E0, E1, E2, h1, h2, c1, c2)
synergy_plots.plot_surface_contour(DD1, DD2, schindler_synergy, xlabel="Drug1", ylabel="Drug2", title="Schindler", fname="schindler_heatmap.pdf", semilog=True)





###
# SHOW HOW BLISS AND LOEWE DO NOT ACCOUNT FOR POTENCY OR EFFICACY
###

# Make plot showing Bliss and Loewe iso-contours in DSD space (calculate loewe and bliss at EC50)
alpha_range = np.logspace(-2,2,20)
beta_range = np.linspace(-1,1,20)

l_surf = np.zeros((len(beta_range),len(alpha_range)))
b_surf = np.zeros((len(beta_range),len(alpha_range)))
hsa_surf = np.zeros((len(beta_range),len(alpha_range)))


for i,alpha in enumerate(alpha_range):
    for j,beta in enumerate(beta_range):
        E3 = min(E1,E2) - beta*(E0-min(E1,E2))
        E = synergy_tools.hill_2D(c1, c2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
        l = -np.log10(synergy_tools.loewe(c1, c2, E, E0, E1, E2, h1, h2, c1, c2))
        l_surf[j,i] = l
        
        e_drug1_alone = synergy_tools.hill_1D(c1, E0, E1, h1, c1)
        e_drug2_alone = synergy_tools.hill_1D(c2, E0, E2, h2, c2)
        b = synergy_tools.bliss(e_drug1_alone, e_drug2_alone, E)
        b_surf[j,i] = b
        
        hsa_surf[j,i] = synergy_tools.hsa(e_drug1_alone, e_drug2_alone, E)
        
synergy_plots.plot_surface_contour(alpha_range, beta_range, l_surf, semilogx=True, xlabel=r"$\log(\alpha)$", ylabel=r"$\overline{\beta}$", crosshairs=True, power=4., levels=[-0.8,-0.1,0,0.1,0.8], title="Loewe Contours", fname="iso_loewe.pdf")
synergy_plots.plot_surface_contour(alpha_range, beta_range, b_surf, semilogx=True, xlabel=r"$\log(\alpha)$", ylabel=r"$\overline{\beta}$", crosshairs=True, levels=None, title="Bliss Contours", fname="iso_bliss.pdf", power=4.) #power makes the colormap favor colors near the extrema
synergy_plots.plot_surface_contour(alpha_range, beta_range, hsa_surf, semilogx=True, xlabel=r"$\log(\alpha)$", ylabel=r"$\overline{\beta}$", crosshairs=True, levels=None, title="HSA Contours", fname="iso_hsa.pdf", power=4.)
