#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 28 13:48:43 2018

@author: xnmeyer
"""
#Import packages
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
# Based on code written by Alex Lubbuck for the PyDRC package
from scipy import stats   
import pandas as pd

def adjacent_values(vals, q1, q3):
    upper_adjacent_value = q3 + (q3 - q1) * 1.5
    upper_adjacent_value = np.clip(upper_adjacent_value, q3, vals[-1])

    lower_adjacent_value = q1 - (q3 - q1) * 1.5
    lower_adjacent_value = np.clip(lower_adjacent_value, vals[0], q1)
    return lower_adjacent_value, upper_adjacent_value


def set_axis_style(ax, labels):
    ax.get_xaxis().set_tick_params(direction='out')
    ax.xaxis.set_ticks_position('bottom')
    ax.set_xticks(np.arange(1, len(labels) + 1))
    ax.set_xticklabels(labels)
    ax.set_xlim(0.25, len(labels) + 0.75)
    ax.set_xlabel('Sample name')
    
###Read in data
df_man = pd.read_csv('Merck_cancer_combinations/MasterResults.csv')
df_man = df_man.loc[df_man['model_level']==5]
df_man = df_man.loc[(df_man['E1']<1.)&(df_man['E2']<1.)&(df_man['E0']<1.10)&(df_man['E0']>.90)]
df_man = df_man.loc[(df_man['R2']>.90)]
df_man.reset_index(drop=True,inplace=True)


fig = plt.figure(figsize=(1.5,1.5))
ax = plt.subplot(111)
mask2 = (df_man['E1']>=.5)&(df_man['E2']>=.5)
mask1 = ~mask2
x = list(df_man[mask1]['beta'].values)
y = list(df_man[mask2]['beta'].values)
data = [x,y]
parts = plt.violinplot(data,showmeans=False,showmedians=False,showextrema=False)
ax.set_xlim(.5,2.5)
ax.set_xticks([1,2])
ax.set_xticklabels(['E1,E2<.5','E1,E2>.5'])
ax.set_ylabel(r'$\beta$')

for pc in parts['bodies']:
    pc.set_facecolor('mediumorchid')
    pc.set_edgecolor('black')
    pc.set_alpha(1)

ax.spines['right'].set_visible(False)
ax.spines['top'].set_visible(False)

ttest = stats.ttest_ind(x,y,equal_var=False)

scbr = abs(plt.ylim()[1] - plt.ylim()[0])
ax.annotate("", xy=(1, plt.ylim()[1]-scbr*.09), xycoords='data',
           xytext=(2, plt.ylim()[1]-scbr*.09), textcoords='data',
           arrowprops=dict(arrowstyle="-", ec='#aaaaaa',
                           connectionstyle="bar,fraction=0.2"))
ax.text(1.5, plt.ylim()[1] + scbr*0.1,  'p-val={:.0e}'.format(ttest.pvalue),
        horizontalalignment='center',verticalalignment='center')
plt.hlines(0,plt.xlim()[0],plt.xlim()[1],color='r',linestyle='--')
plt.savefig('effect_bias_synergy.pdf')

#
#
#fig = plt.figure(figsize=(14,5))
#ax = plt.subplot(111)
#x = df_man[['E1_obs','E2_obs']].mean(axis=1)
#y = df_man['beta_obs'].astype('float')
#ax.scatter(x,y)
#ax.set_xlim(0,1)
#ax.set_xticks([0,1])
#ax.set_xticklabels(['E1,E2<.25','E1,E2>.25'])
#ax.set_ylabel('beta_obs')
#
#
