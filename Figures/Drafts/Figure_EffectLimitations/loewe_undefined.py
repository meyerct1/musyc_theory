#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  8 12:15:44 2019

@author: xnmeyer
"""

#Import packages
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
from matplotlib.colors import ListedColormap
import matplotlib.cm as cm
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
import scipy
# Based on code written by Alex Lubbuck for the PyDRC package
import scipy.stats    
import pandas as pd



fit_fil = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
dat_fil = 'oneil_anticancer/merck_perVia_10-29-2018.csv'
T = pd.read_csv("../../Data/" + fit_fil)
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)
df = T


plt.figure()
plt.scatter(T[['E1','E2']].mean(axis=1),T['loewe_fit_perUnd'])



def moving_window_percentiles(df, xaxis, yaxis, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True):
    """
    Calculates moving window percentiles.
    The first window begins at df[xaxis].min()-window_size/2
    The final window ends at df[xaxis].max()+window_size/2
    The window step_size is calculated so that the result will have n_steps many values
    x values for each window are the midpoint of xmin,xmax of that window
    """
    dff = pd.DataFrame(df)[xaxis]
    if logx: dff = np.log(dff)
    XMIN, XMAX = dff.min()-window_size/2., dff.max()+window_size/2.
    step_size = (XMAX - XMIN - window_size) / (1.*(n_steps-1))
    w_df = pd.DataFrame(index = range(n_steps), columns=percentiles + ['xmin','xmax','xmid','n_rows'])
    
    for i in w_df.index:
        xmin = XMIN + step_size*i
        xmax = xmin + window_size
        xmid = xmin + window_size/2.
        if logx: w_df.loc[i,['xmin','xmax','xmid']] = np.exp(xmin), np.exp(xmax), np.exp(xmid)
        else: w_df.loc[i,['xmin','xmax','xmid']] = xmin, xmax, xmid
        sub_df = df.loc[(dff >= xmin) & (dff <= xmax)]
        w_df.loc[i,"n_rows"] = sub_df.shape[0]
        w_df.loc[i, percentiles] = sub_df[yaxis].quantile(percentiles)

    return w_df
        
def plot_quantile_bias(window_df, xaxis, ax, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True, alpha_lim=(0.2,0.8), color='gold', bias=None, xlim=None, ylim=None, plot_midline=True):
    """
    Plot the bias of df[yaxis] as a function of df['xmid']
    Assumes window_df has been returned as window_df=moving_window_percentiles(df, ...)
    """
    
    x = np.float64(window_df['xmid'])
    
    n_color_shades = len(percentiles)/2
    
    for i in range(n_color_shades):
        alpha = alpha_lim[0] + (alpha_lim[1] - alpha_lim[0])*i/(n_color_shades-1.)
        y1 = np.float64(window_df[percentiles[i+1]])
        y2 = np.float64(window_df[percentiles[i]])
        ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
        
        if i<n_color_shades-1 or len(percentiles)%2==1:
            y1 = np.float64(window_df[percentiles[-i-1]])
            y2 = np.float64(window_df[percentiles[-i-2]])
            ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
    if bias is not None: ax.plot(bias[0], bias[1],'b-',lw=2,zorder=1000,label='Predicted Bias')
    ax.plot([x.min(), x.max()], [0,0],'k--')
    if plot_midline: ax.plot(x, window_df[percentiles[n_color_shades]], 'r-',label='Median') # Draw the median, (assuming 0.5 is the middle percentile)
    
    if xlim is not None: ax.set_xlim(xlim)
    if ylim is not None: ax.set_ylim(ylim)
    if logx: ax.set_xscale('log')
    
    


xaxis = "avg_Emax"
df[xaxis] = df[['E1','E2']].mean(axis=1)

# Calculate the moving windows of the scattered data
percentiles = [i/10. for i in range(1,10)]
n_steps=200
window_size=0.1 # This is in h-space
window_df2 = moving_window_percentiles(df, xaxis, "loewe_fit_perUnd", percentiles = percentiles, n_steps=n_steps, window_size=window_size)

x = np.float64(window_df2['xmid'])

xlim=(-.1,1)
ylim=(-.1,1)

fig = plt.figure(figsize=(2.5,1.25),facecolor='w')
ax = []
ax.append(plt.subplot(111))
ax[0].scatter(df[xaxis],df['loewe_fit_perUnd'],alpha=0.05, s=.5,rasterized=True)
plot_quantile_bias(window_df2, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
ax[0].set_xlabel('Avg(E1,E2)')
ax[0].set_xticks([0.,.5,1.])
ax[0].set_ylabel("Percent Conditions\nUndefined by Loewe",labelpad=-1)
ax[0].set_yticks([0,1])
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')
ax[0].set_title('Loewe undefined',fontsize=8)
plt.subplots_adjust(hspace=.0)
plt.savefig('loewe_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)





#######
df[(df['loewe_fit_perUnd']>.5)&(df['beta']>.5)&(df['E1']<.9)&(df['E2']<.9)][['drug1_name','drug2_name','beta','avg_alpha','loewe_fit_perUnd']].sort_values('avg_alpha')
df = df[(df['loewe_fit_perUnd']>.5)&(df['beta']>.5)&(df['E1']<.9)&(df['E2']<.9)]


sub_T =df.loc[270]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
plotDoseResponseSurf(sub_T,d1,d2,dip,dip_sd,zero_conc=1)
title = 'Per und Loewe @ EC50:%.2f'%sub_T['loewe_fit_perUnd']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.1), zero_conc=0,title=title)


    
    

#####Select a combination

    N=10;elev=20;azim=19;alpha = 0.9;path = T['save_direc'];metric_name=T['metric_name'].values[0];
    if 'fit_gamma' in T:
        if T['fit_gamma']==1:
            incl_gamma=True
    e0, e1, e2, e3, h1, h2, r1, r1r, r2, r2r, ec50_1, ec50_2, alpha1, alpha2, gamma1,gamma2,d1_min, d1_max, d2_min, d2_max, drug1_name, drug2_name, expt = get_params(T)
    
    if d1lim is not None:
        d1_min=d1lim[0];d1_max=d1lim[1]
    if d2lim is not None:
        d2_min=d2lim[0];d2_max=d2lim[1]    
        
    fig = plt.figure(figsize=(3.5,3),facecolor='w')
    ax = fig.gca(projection='3d')
    #ax.set_axis_off()
    y = np.linspace(d1_min-zero_conc, d1_max ,N)
    t = np.linspace(d2_min-zero_conc, d2_max ,N)

    if incl_gamma:
        tt, yy = np.meshgrid(t, y)
        conc3 = np.array([(np.power(10.,c1),np.power(10.,c2)) for c1 in y for c2 in t])
        zz = np.array([Edrug2D_NDB_hill(d,e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2) for d in conc3])
        zz = zz.reshape(N,N)
    else:
        yy, tt = np.meshgrid(y, t)
        zz = dip(yy,tt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
    if zlim is None:
        zmin = np.min(zz)
        zmax = np.max(zz)
        if np.abs(zmin) > np.abs(zmax): zmax = np.abs(zmin)
        else: zmin = -np.abs(zmax)
    else:
        zmin = zlim[0]
        zmax = zlim[1]
        
    # Plot it once on a throwaway axis, to get cbar without alpha problems
    my_cmap_rgb = plt.get_cmap('PRGn')(np.arange(256))

    for i in range(3): # Do not include the last column!
        my_cmap_rgb[:,i] = (1 - alpha) + alpha*my_cmap_rgb[:,i]
    my_cmap = ListedColormap(my_cmap_rgb, name='my_cmap')
    surf = ax.plot_surface(yy, tt, zz, cstride=1, rstride=1, cmap = my_cmap, vmin=zmin, vmax=zmax, linewidth=0)
#    cbar_ax = fig.add_axes([0.1, 0.9, 0.6, 0.05])
    cbar = fig.colorbar(surf,ticks=[zmin, 0, zmax],pad=-.08,fraction=.025)
    cbar.ax.set_yticklabels(['%.2f'%zmin, 0, '%.2f'%zmax])
    cbar.solids.set_rasterized(True)
    cbar.solids.set_edgecolor('face')
    cbar.outline.set_visible(False)
    
    plt.cla()
    ax.set_zlim(zmin,zmax)
    # Plot the surface for real, with appropriate alpha
    surf = ax.plot_surface(tt, yy, zz, cstride=1, rstride=1, alpha=alpha, cmap = cm.PRGn, vmin=zmin, vmax=zmax, linewidth=0)
    
    # colored curves on left and right
    lw = 5
    if incl_gamma:
        ax.plot(d2_min*np.ones(y.shape)-zero_conc, y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,d2_min*np.ones(y.shape)-zero_conc)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), linewidth=lw,color='b')
        ax.plot(d2_max*np.ones(y.shape), y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,d2_max*np.ones(y.shape))),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), linewidth=lw,color='b',linestyle='--')

        ax.plot(t, d1_min*np.ones(y.shape)-zero_conc, Edrug2D_NDB_hill((np.power(10.,d1_min*np.ones(t.shape)-zero_conc),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), linewidth=lw,color='r')
        ax.plot(t, d1_max*np.ones(y.shape), Edrug2D_NDB_hill((np.power(10.,d1_max*np.ones(t.shape)),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), linewidth=lw,color='r',linestyle='--')
    
    else:
        ax.plot(d2_min*np.ones(y.shape)-zero_conc, y, dip(y,d2_min-zero_conc, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), linewidth=lw,color='b')
        ax.plot(d2_max*np.ones(y.shape), y, dip(y,d2_max, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), linewidth=lw,color='b',linestyle='--')
    
        ax.plot(t, d1_min*np.ones(y.shape)-zero_conc, dip(d1_min-zero_conc,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), linewidth=lw,color='r')
        ax.plot(t, d1_max*np.ones(y.shape), dip(d1_max,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), linewidth=lw,color='r',linestyle='--')
        
    
    if incl_gamma:
        # light grey grid across surface
        for ttt in np.linspace(d2_min-zero_conc,d2_max,10):
            ax.plot(ttt*np.ones(y.shape), y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,ttt)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
            ax.plot(ttt*np.ones(y.shape), y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,ttt)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
            ax.plot(ttt*np.ones(y.shape), y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,ttt)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
    
        for yyy in np.linspace(d1_min-zero_conc, d1_max,10):
            ax.plot(t, yyy*np.ones(y.shape), Edrug2D_NDB_hill((np.power(10.,yyy),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
            ax.plot(t, yyy*np.ones(y.shape), Edrug2D_NDB_hill((np.power(10.,yyy),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
            ax.plot(t, yyy*np.ones(y.shape), Edrug2D_NDB_hill((np.power(10.,yyy),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
        
    else:
        # light grey grid across surface
        for ttt in np.linspace(d2_min-zero_conc,d2_max,10):
            ax.plot(ttt*np.ones(y.shape), y, dip(y,ttt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
            ax.plot(ttt*np.ones(y.shape), y, dip(y,ttt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
            ax.plot(ttt*np.ones(y.shape), y, dip(y,ttt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
    
        for yyy in np.linspace(d1_min-zero_conc, d1_max,10):
            ax.plot(t, yyy*np.ones(y.shape), dip(yyy,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
            ax.plot(t, yyy*np.ones(y.shape), dip(yyy,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
            ax.plot(t, yyy*np.ones(y.shape), dip(yyy,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)

    # Set the view
    ax.view_init(elev=elev, azim=azim)
    ax.set_ylabel("log(%s)[M]"%drug1_name,labelpad=-5)   
    ax.set_xlabel("log(%s)[M]"%drug2_name,labelpad=-5)
    ax.set_zlabel(metric_name,labelpad=-3)

    if plt_data:
        scat_d1 = d1.copy()
        scat_d2 = d2.copy()
        scat_dip = dip_rate.copy()
        scat_erb = dip_sd.copy()
        scat_d1 = np.log10(scat_d1)
        scat_d2 = np.log10(scat_d2)
        if d1lim is not None:
            scat_d1[scat_d1==-np.inf] = d1_min
        else:
            scat_d1[scat_d1==-np.inf] = scat_d1[scat_d1!=-np.inf].min()-zero_conc
        if d2lim is not None:
            scat_d2[scat_d2==-np.inf] = d2_min
        else:
            scat_d2[scat_d2==-np.inf] = scat_d2[scat_d2!=-np.inf].min()-zero_conc
        
        #Remove values which are outside bounds
        mask = ~np.isnan(scat_d1)
        if d1lim is not None:
            mask=(mask) & ((scat_d1>=d1_min) & (scat_d1<=d1_max))
        if d2lim is not None:
            mask=(mask) & ((scat_d2>=d2_min) & (scat_d2<=d2_max))
        scat_d1=scat_d1[mask];scat_d2=scat_d2[mask];
        scat_erb=scat_erb[mask];scat_dip=scat_dip[mask];
        
        ax.scatter(scat_d2, scat_d1, scat_dip, s=10,c='k', depthshade=False)
        # Plot error bars
        for _d1, _d2, _dip, _erb in zip(scat_d1, scat_d2, scat_dip, scat_erb):
            ax.plot([_d2,_d2], [_d1,_d1], [_dip-_erb, _dip+_erb], 'k-', alpha=0.3,linewidth=1)

    #format axis
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))

    np.linspace(np.ceil(2.3),np.floor(8.4),-np.ceil(2.3)+np.floor(8.4)+1)
    x_ax = np.linspace(np.ceil(d2_min),np.floor(d2_max),int(np.floor(d2_max)-np.ceil(d2_min)+1.))
    if len(x_ax)>3:    
        x_ax=x_ax[0:-1:np.round(len(x_ax)/3)]
    y_ax = np.linspace(np.ceil(d1_min),np.floor(d1_max),int(np.floor(d1_max)-np.ceil(d1_min)+1.))
    if len(y_ax)>3:
        y_ax=y_ax[0:-1:np.round(len(y_ax)/3)]
        
    ax.set_xticks(x_ax)
    ax.set_yticks(y_ax)
    ax.set_zticks([zmin,0,zmax])
    ax.xaxis.set_tick_params(pad=-4)
    ax.yaxis.set_tick_params(pad=-4)
    ax.zaxis.set_tick_params(pad=-2)


    # Plane of DIP=0
    if plt_zeropln:
        c_plane = colorConverter.to_rgba('k', alpha=0.3)
        verts = [np.array([(d2_min-zero_conc,d1_min-zero_conc), (d2_min-zero_conc,d1_max), (d2_max,d1_max), (d2_max,d1_min-zero_conc), (d2_min-zero_conc,d1_min-zero_conc)])]
        poly = PolyCollection(verts, facecolors=c_plane)
        ax.add_collection3d(poly, zs=[0], zdir='z')

        # Plot intersection of surface with DIP=0 plane
        plt.contour(tt,yy,zz,levels=[0], linewidths=3, colors='k')

    # If needed, manually set zlim
    if zlim is not None: ax.set_zlim(zlim)
    plt.tight_layout()

    if title is not None:      
        plt.title(title)

    # Save plot, or show it
    if fname is None: plt.show()
    else: plt.savefig(fname+'.'+fmt,pad_inches=0.,format=fmt);plt.close()









