#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 10 11:47:58 2018

@author: xnmeyer
"""

#Code to create plots for conflation figure 
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.calcOtherSynergyMetrics import zimmer,get_params,combination_index
import warnings
warnings.filterwarnings("ignore")
from scipy.interpolate import interp2d


#Effect limitations of previous methods
fit_fil = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
dat_fil = 'oneil_anticancer/merck_perVia_10-29-2018.csv'
T = pd.read_csv("../../Data/" + fit_fil)

#Calculate the R2 value for CI and Zimmer as a function of Emax
T = T[(T['R2']>.8)]
T = T.reset_index(drop=True)
keys = ['zimmer_raw_a12','zimmer_raw_a21','R2_zim','ci_raw_ec50','ci_fit','ci_raw_all']
for k in keys:
    T[k]=np.nan

prog=0;count=0;n_rows=len(T.index)
for ind in T.index:
    count += 100 # Print progress, counting up to 100
    if count/n_rows > prog:
        prog = count/n_rows
        print prog
    sub_T       = T.loc[ind].to_dict()
    E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, C1, C2, alpha1, alpha2,gamma1,gamma2, d1min, d1max, d2min, d2max, drug1_name, drug2_name, expt = get_params(pd.DataFrame([sub_T]))
    expt        = "../../Data/" + dat_fil
    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1']  = data['drug1'].str.lower()
    data['drug2']  = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
    try:
        _, _, _, _, a12, a21, _, R2  = zimmer(d1, d2, dip, logspace=False, sigma=None, vector_input=True,p0=[C1,C2,h1,h2,0,0])   
        T.loc[ind,'R2_zim'] = R2
        T.loc[ind,'zimmer_raw_a12'] = a12
        T.loc[ind,'zimmer_raw_a21'] = a21
    except:
        T.loc[ind,'R2_zim']=-1
    

    try:
        T.loc[ind,'ci_fit']=1
        ci,d1_full,d2_full = combination_index(d1,d2,dip)
        fun = interp2d(d1_full,d2_full,ci.reshape((-1,)))
        T.loc[ind,'ci_raw_ec50']=fun(C1,C2)[0]
        T.loc[ind,'ci_raw_all']=np.mean(ci)
    except:
        T.loc[ind,'ci_fit']=-1
T.to_csv('zimmer_ci_calc_merck.csv')






df = pd.read_csv('zimmer_ci_calc_merck.csv')
df = df[(df['C1']<df['max_conc_d1'])&(df['C2']<df['max_conc_d2'])]
#df = df[(~np.isnan(df['loewe_fit_perUnd']))&(df['E1']<df['E0'])&(df['E2']<df['E0'])]

plt.figure(facecolor='w')
xaxis = "avg_Emax"
df[xaxis] = df[['E1','E2']].mean(axis=1)
plt.scatter(df['avg_Emax'],df['loewe_fit_perUnd'])
plt.xlim(0,1)
plt.ylim(0,1)









df = pd.read_csv('zimmer_ci_calc_merck.csv')
df = df[(~np.isnan(df['R2_zim']))&(df['R2_zim']>0)]
xaxis = "avg_Emax"
df[xaxis] = df[['E1','E2']].mean(axis=1)

mask1 = df['avg_Emax']<.5
mask5 = df['avg_Emax']>=.5
plt.figure(figsize=(2.5,1),facecolor='w')
ax = plt.subplot(111)
vp = []
vp.append(ax.violinplot(df.loc[mask1,'R2_zim'].values,positions=[0]))
vp.append(ax.violinplot(df.loc[mask5,'R2_zim'].values,positions=[1]))
plt.xlim(-.5,1.5)
plt.ylim(.7,1)
colors = ['green','purple']
for e,v in enumerate(vp):
    for pc in v['bodies']:
        pc.set_facecolor(colors[e])
        pc.set_edgecolor('k')
        pc.set_linewidth(2)
    v['cbars'].set_linewidth(1)
    v['cbars'].set_color('k')
    
ax.set_xticks([0,1])
ax.set_xticklabels(['(E1+E2)/2<0.5','(E1+E2)/2>0.5'])
ax.set_ylabel('R2')
ax.set_yticks([.7,.85,1.])
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
from scipy.stats import ks_2samp
pval = ks_2samp(df.loc[mask1,'R2_zim'].values,df.loc[mask5,'R2_zim'].values).pvalue
ax.text(.5,.75,'KS-pval:{:.0e}'.format(pval),ha='center')
plt.savefig('zimmer_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)






df = pd.read_csv('zimmer_ci_calc_merck.csv')
df = df[(~np.isnan(df['zimmer_raw_a12']))&(~np.isnan(df['zimmer_raw_a21']))]
df = df[(abs(df['zimmer_raw_a12'])<30)&(abs(df['zimmer_raw_a21'])<30)]
df = df[(abs(df['zimmer_raw_a12'])!=0)&(abs(df['zimmer_raw_a21'])!=0)]



xaxis = "avg_Emax"
df[xaxis] = df[['E1','E2']].mean(axis=1)

mask1 = df['avg_Emax']<.5
mask5 = df['avg_Emax']>=.5
plt.figure(figsize=(2.5,1),facecolor='w')
ax = plt.subplot(111)
vp = []
vp.append(ax.violinplot(df.loc[mask1,'zimmer_raw_a12'].values,positions=[0]))
vp.append(ax.violinplot(df.loc[mask5,'zimmer_raw_a12'].values,positions=[1]))
plt.xlim(-.5,1.5)
plt.ylim(-10,10)
colors = ['green','purple']
for e,v in enumerate(vp):
    for pc in v['bodies']:
        pc.set_facecolor(colors[e])
        pc.set_edgecolor('k')
        pc.set_linewidth(2)
    v['cbars'].set_linewidth(1)
    v['cbars'].set_color('k')
    
ax.set_xticks([0,1])
ax.set_xticklabels(['(E1+E2)/2<0.5','(E1+E2)/2>0.5'])
ax.set_ylabel('R2')
ax.set_yticks([.7,.85,1.])
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
from scipy.stats import ks_2samp
pval = ks_2samp(df.loc[mask1,'R2_zim'].values,df.loc[mask5,'R2_zim'].values).pvalue
ax.text(.5,.75,'KS-pval:{:.0e}'.format(pval),ha='center')
plt.savefig('zimmer_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)



df = df[(~np.isnan(df['ci_raw_ec50']))&(df['ci_raw_ec50']>0.0000001)]
mask1 = df['avg_Emax']<.5
mask5 = df['avg_Emax']>=.5
plt.figure(figsize=(2.5,1),facecolor='w')
ax = plt.subplot(111)
vp = []
vp.append(ax.violinplot(-np.log10(df.loc[mask1,'ci_raw_ec50'].values),positions=[0]))
vp.append(ax.violinplot(-np.log10(df.loc[mask5,'ci_raw_ec50'].values),positions=[1]))
plt.xlim(-.5,1.5)
plt.ylim(-7,3)
colors = ['green','purple']
for e,v in enumerate(vp):
    for pc in v['bodies']:
        pc.set_facecolor(colors[e])
        pc.set_edgecolor('k')
        pc.set_linewidth(2)
    v['cbars'].set_linewidth(1)
    v['cbars'].set_color('k')
    
ax.set_xticks([0,1])
ax.set_xticklabels(['(E1+E2)/2<0.5','(E1+E2)/2>0.5'])
ax.set_ylabel('-log(CI)')
ax.set_yticks([3,0,-3,-6])
ax.spines['top'].set_visible(False)
ax.spines['right'].set_visible(False)
ax.xaxis.set_ticks_position('bottom')
ax.yaxis.set_ticks_position('left')
from scipy.stats import ks_2samp
pval = ks_2samp(-np.log10(df.loc[mask1,'ci_raw_ec50'].values),-np.log10(df.loc[mask5,'ci_raw_ec50'].values)).pvalue
ax.text(.5,-4,'KS-pval:{:.0e}'.format(pval),ha='center')















#Find specific example of poor fit by zimmer and combination index
df[(df['R2_zim']<.5)&(df['avg_Emax']>.5)&(df['R2']>.95)]



from SynergyCalculator.NDHillFun import Edrug2D_NDB_hill
from scipy.interpolate import interp2d
from SynergyCalculator.calcOtherSynergyMetrics import zimmer,combination_index
zC1=np.zeros(100);zC2=np.zeros(100);zh1=np.zeros(100);zh2=np.zeros(100);a12=np.zeros(100);a21=np.zeros(100);R2=np.zeros(100)
ci_ec50=np.zeros(100);ci_all=np.zeros(100)
emx = np.linspace(0.,.99,100)
for cnt,e in enumerate(emx):
    print cnt
    noise = 0.
    N = 7;rep = 2;
    E0=1.;E1=e;E2=e;E3=e;
    h1=1.;h2=1.;
    C1=10e-5;C2=10e-5;
    r1=100.;r2=100.;
    alpha1=1.;alpha2=1.
    gamma1=1.;gamma2=1.
    d1 = [];d2 = [];dip = []
    for r in range(rep):
        DD1,DD2 = np.meshgrid(np.concatenate(([0],np.logspace(-10,0,N))),np.concatenate(([0],np.logspace(-10,0,N))))
        d1_tmp = DD1.reshape((-1,))
        d2_tmp = DD2.reshape((-1,))
        dip_tmp = Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
        dip_tmp = np.random.randn(len(dip_tmp))*noise+dip_tmp
        d1 = np.concatenate((d1,d1_tmp))
        d2 = np.concatenate((d2,d2_tmp))
        dip = np.concatenate((dip,dip_tmp))
    
    try:
        zC1[cnt], zC2[cnt], zh1[cnt], zh2[cnt], a12[cnt], a21[cnt], _, R2[cnt]  = zimmer(d1, d2, dip, logspace=False, sigma=None, vector_input=True,p0=[C1,C2,h1,h2,1.,1.])   
        ci,d1_full,d2_full = combination_index(d1,d2,dip)
        fun = interp2d(d1_full,d2_full,ci.reshape((-1,)))
        ci_ec50[cnt]=fun(C1,C2)[0]
        ci_all[cnt]=np.mean(ci)
    except:
        continue
    
plt.figure(facecolor='w')
plt.scatter(emx,a12)
plt.scatter(emx,a21)
plt.ylim(-100,100)









