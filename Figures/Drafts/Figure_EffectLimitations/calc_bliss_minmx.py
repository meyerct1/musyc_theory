#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 12:42:49 2019

@author: xnmeyer
"""

from SynergyCalculator.calcOtherSynergyMetrics import get_params,bliss,zimmer
from SynergyCalculator.NDHillFun import Edrug2D_NDB_hill, Edrug1D
from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.initialFit import init_fit

import pandas as pd
import numpy as np
import os

fit_fil2 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
df = pd.read_csv("../../Data/" + fit_fil2)

df['bliss_fit_md1d2']=np.nan
df['bliss_fit_mxd1d2']=np.nan
for i in df.index:
    print i
    E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, C1, C2, alpha1, alpha2,gamma1,gamma2, d1min, d1max, d2min, d2max, drug1_name, drug2_name, expt = get_params(pd.DataFrame([df.loc[i].to_dict()]))
    #To avoid ValueError in calculating E if h is really large scale the dose and ec50.
    C1 = C1/1000. if h1>20 else C1;
    C2 = C2/1000. if h2>20 else C2;
    d1min = 10**d1min; d2min=10**d2min; d1max=10**d1max;d2max=10**d2max
    E_ec50   = Edrug2D_NDB_hill((C1,C2),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
    E_md1d2  = Edrug2D_NDB_hill((d1min,d2min),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
    E_mxd1d2 = Edrug2D_NDB_hill((d1max,d2max),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)

#    df.loc[i,'bliss_fit_ec50']   = bliss(E0-(E0-E1)/2, E0-(E0-E2)/2, E_ec50)
    df.loc[i,'bliss_fit_md1d2']  = bliss(Edrug1D(d1min,E0,E1,C1,h1), Edrug1D(d2min,E0,E2,C2,h2), E_md1d2)
    df.loc[i,'bliss_fit_mxd1d2'] = bliss(Edrug1D(d1max,E0,E1,C1,h1), Edrug1D(d2max,E0,E2,C2,h2), E_mxd1d2)
    
df.to_csv("../../Data/" + fit_fil2,index=False)






#################################################################################
#################################################################################
#Calculate Zimmer
fit_fil2 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
df = pd.read_csv("../../Data/" + fit_fil2)

df['zimmer_R2']=np.nan
df['zimmer_raw_a1']=np.nan
df['zimmer_raw_a2']=np.nan

E_fix = None
E_bnd = [[.9,0.,0.,0.,0.],[1.1,2.5,2.5,2.5]]
E_fx = [np.nan,np.nan,np.nan,np.nan] if E_fix is None else E_fix
E_bd = [[-np.inf,-np.inf,-np.inf,-np.inf],[np.inf,np.inf,np.inf,np.inf]] if E_bnd is None else E_bnd
    
for i in df.index:
    print i
    sub_T       = df.loc[i].to_dict()
    expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1'] = data['drug1'].str.lower()
    data['drug2'] = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)

    sub_T = init_fit(sub_T,d1,d2,dip,dip_sd,drug1_name,drug2_name,E_fx,E_bd)  
    
    C1 = 10**sub_T['log_C1']
    C2 = 10**sub_T['log_C2']
    h1 = 10**sub_T['log_h1']
    h2 = 10**sub_T['log_h2']
    try:
        _, _, _, _, a12, a21, _, R2  = zimmer(d1, d2, dip, logspace=False, sigma=None, vector_input=True,p0=[C1,C2,h1,h2,0,0])   
    
        df['zimmer_R2'].loc[i]=R2
        df['zimmer_raw_a1'].loc[i]=a12
        df['zimmer_raw_a2'].loc[i]=a21
    except RuntimeError:
        print 'Fail'

df.to_csv("../../Data/" + fit_fil2,index=False)


#Calculate Zimmer
fit_fil2 = 'mott_antimalaria/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
df = pd.read_csv("../../Data/" + fit_fil2)

df['zimmer_R2']=np.nan
df['zimmer_raw_a1']=np.nan
df['zimmer_raw_a2']=np.nan

E_fix = None
E_bnd = [[.9,0.,0.,0.],[1.1,2.0,2.0,2.0]]
E_fx = [np.nan,np.nan,np.nan,np.nan] if E_fix is None else E_fix
E_bd = [[-np.inf,-np.inf,-np.inf,-np.inf],[np.inf,np.inf,np.inf,np.inf]] if E_bnd is None else E_bnd
    
for i in df.index:
    print i
    sub_T       = df.loc[i].to_dict()
    expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1'] = data['drug1'].str.lower()
    data['drug2'] = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
    dip = dip/100.
    dip_sd=dip_sd/100.
    sub_T = init_fit(sub_T,d1,d2,dip,dip_sd,drug1_name,drug2_name,E_fx,E_bd)  
    
    C1 = 10**sub_T['log_C1']
    C2 = 10**sub_T['log_C2']
    h1 = 10**sub_T['log_h1']
    h2 = 10**sub_T['log_h2']
    try:
        _, _, _, _, a12, a21, _, R2  = zimmer(d1, d2, dip, logspace=False, sigma=None, vector_input=True,p0=[C1,C2,h1,h2,1,1])   
    
        df['zimmer_R2'].loc[i]=R2
        df['zimmer_raw_a1'].loc[i]=a12
        df['zimmer_raw_a2'].loc[i]=a21
    except RuntimeError:
        print 'Fail'

df.to_csv("../../Data/" + fit_fil2,index=False)

