#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 12:50:20 2019

@author: xnmeyer
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 16:42:04 2018

@author: xnmeyer
"""
#Import packages
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from matplotlib.colors import ListedColormap, Normalize, colorConverter
from matplotlib.collections import PolyCollection
from matplotlib.colorbar import ColorbarBase
from matplotlib.ticker import FormatStrFormatter
import matplotlib.cm as cm
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
# Based on code written by Alex Lubbuck for the PyDRC package
from scipy.stats import ttest_ind
import pandas as pd
import os

from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.doseResponseSurfPlot import matplotlibDoseResponseSurface
from SynergyCalculator.NDHillFun import Edrug1D,Edrug2D_NDB_hill

import sys
sys.path.insert(0, '../Plot_Functions')
from plotFun import moving_window_percentiles,plot_quantile_bias,surfGen

def Edrug_2D_full(d1,d2,E0,E1,E2,beta,C1,C2,h1,h2,r1,r2,alpha1,alpha2,gamma1,gamma2):
    E3 = -beta*(E0-min(E1,E2))+min(E1,E2);
    return (C1**(2*gamma1*h1)*E2*d2**h2*r1 + C2**(2*gamma2*h2)*E1*d1**h1*r2 + C1**(2*gamma1*h1)*C2**(gamma2*h2)*E0*r1 + C1**(gamma1*h1)*C2**(2*gamma2*h2)*E0*r2 + E3*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + E3*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*E2*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + C2**(gamma2*h2)*E1*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1 + C1**(gamma1*h1)*C2**(gamma2*h2)*E0*r1*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*C2**(gamma2*h2)*E0*r2*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*E2*d1**h1*r1*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*E3*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + C2**(gamma2*h2)*E1*d2**h2*r2*(alpha2*d1)**(gamma2*h1) + C2**(gamma2*h2)*E3*d1**h1*r2*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*C2**(gamma2*h2)*E1*d1**h1*r1 + C1**(gamma1*h1)*C2**(gamma2*h2)*E2*d2**h2*r2)/(C1**(2*gamma1*h1)*C2**(gamma2*h2)*r1 + C1**(gamma1*h1)*C2**(2*gamma2*h2)*r2 + C1**(2*gamma1*h1)*d2**h2*r1 + C2**(2*gamma2*h2)*d1**h1*r2 + C1**(gamma1*h1)*C2**(gamma2*h2)*r1*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*C2**(gamma2*h2)*r2*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*d1**h1*r1*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + C2**(gamma2*h2)*d1**h1*r2*(alpha1*d2)**(gamma1*h2) + C2**(gamma2*h2)*d2**h2*r2*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*C2**(gamma2*h2)*d1**h1*r1 + C1**(gamma1*h1)*C2**(gamma2*h2)*d2**h2*r2 + alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + C2**(gamma2*h2)*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1);

#Set parameters for surfaces
d1 = np.logspace(-5,0,50); d2 = np.logspace(-5,0,50);
h1 = 1; h2 = 1;
C1 = .005; C2 = .001;
E0 = 1; E1 = .5; E2 = .5;E3=.5;
alpha1 = 1; alpha2 = 1; gamma1 = 1; gamma2 = 1;
r1 = 100*E0; r2 = 100*E0;

zlim = (0.,1)
yy,tt = np.meshgrid(d1,d2)
ed = Edrug2D_NDB_hill((yy.flatten(),tt.flatten()),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2).reshape(yy.shape)
fig = plt.figure(figsize=(3,2))
ax1 = fig.add_subplot(121, projection='3d')
ax1 = surfGen(ax1,yy,tt,ed,zlim)
ax1.set_zlim(zlim)
ax1.set_xticks([]);ax1.set_yticks([])
ax2 = fig.add_subplot(122, projection='3d')
bl = np.array(np.matrix(Edrug1D(d2,E0,E2,C2,h2)).T*np.matrix(Edrug1D(d1,E0,E1,C1,h1)))
ax2 = surfGen(ax2,yy,tt,bl,zlim)
ax2.set_zlim(zlim)
ax1.set_title('Null MuSyC')
ax2.set_title('Null Bliss')
ax2.set_xticks([]);ax2.set_yticks([])

plt.show()
plt.savefig('bliss_theory_bias.pdf', bbox_inches = 'tight',pad_inches = 0)


#Data figures

fit_fil1 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation_allDoses.csv'
fit_fil2 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
T2 = pd.read_csv("../../Data/" + fit_fil2)
T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.8)]
T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]
T2.reset_index(drop=True,inplace=True)
df = T2
df = df[~np.isnan(df[['bliss_fit_mxd1d2']].mean(axis=1))]
#df = df[(df['E1']<1.)&(df['E2']<1.)]
df= df.reset_index(drop=True)
bin_size = 3000
stp = len(df)/bin_size
df['E1E2'] = np.sqrt(df['E1']**2+df['E2']**2)
l = np.sort(df['E1E2'])
r_int = np.sort(df['E1E2'])[0::bin_size]
verts = [];z_k=[]
import math
for i in range(len(r_int)):
    if i==len(r_int)-1:
        rad_range = [r_int[i],df['E1E2'].max()]
    else:
        rad_range = [r_int[i],r_int[i+1]]
    msk = (df['E1E2']>rad_range[0]) & (df['E1E2']<rad_range[1])
    val = df.loc[msk,'bliss_fit_mxd1d2'].median()
    x=[];y=[];z=[];z_k.append(val)
    c_x=[];c_y=[];c_z=[];
    theta = np.linspace(0,math.pi/2,50)
    for t in theta:
            x.append(rad_range[0]*math.cos(t))
            y.append(rad_range[0]*math.sin(t))
            z.append(val)
    for t in theta[::-1]:
        x.append(rad_range[1]*math.cos(t))
        y.append(rad_range[1]*math.sin(t))
        z.append(val) 
    verts.append(zip(x,y,z))
            
plt.figure(figsize=(2,2.5),facecolor='w')
ax = plt.subplot2grid((20,1),(0,0),rowspan=17,projection='3d')
cax = plt.subplot2grid((20,1),(19,0))
ax.xaxis.set_major_locator(plt.MaxNLocator(3))
ax.yaxis.set_major_locator(plt.MaxNLocator(3))
ax.zaxis.set_major_locator(plt.MaxNLocator(3))
ax.set_zlim((min(z_k),max(z_k)))
ax.set_xlim((0,df[['E1','E2']].max(axis=0).mean()))
ax.set_ylim((0,df[['E1','E2']].max(axis=0).mean()))
norm = Normalize(vmin=min(z_k), vmax=max(z_k))
cols = cm.cool(norm(z_k))
for e,v in enumerate(verts):
    ax.add_collection3d(Poly3DCollection([v],facecolor=cols[e,:],alpha=.75))
cb1 = ColorbarBase(cax, 
                   cmap=cm.cool,
                   norm=norm,
                   orientation='horizontal')
cb1.set_label('<---Ant  Syn--->\nMean Bliss @ max(d1,d2)')
cb1.set_ticks([0])
ax.set_title('ONeil et al.\nAnti Cancer Combinations',fontsize=8)
ax.set_xlabel('E1 (% viable)',labelpad=-9)
ax.set_ylabel('E2 (% viable)',labelpad=-9)
ax.set_zlabel('Bliss',labelpad=-3)
ax.plot([ax.get_xlim()[0],ax.get_xlim()[0]],ax.get_ylim(),[0,0],'k')
ax.plot(ax.get_xlim(),[ax.get_ylim()[1],ax.get_ylim()[1]],[0,0],'k')
ax.tick_params(pad=0)
ax.tick_params(axis='x',pad=-4)
ax.tick_params(axis='y',pad=-4)
ax.grid(False)
ax.view_init(15, -60)
plt.savefig('bliss_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)


################################################################################
################################################################################
T2 = pd.read_csv("../../Data/" + fit_fil2)
T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.9)]
T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]

df = T2
df = df[~np.isnan(df['bliss_fit_mxd1d2'])]
cell_line = []
drug =[]
emax=[]
emax_sd=[]
ec50=[]
ec50_sd=[]
bliss = []

for c in df['sample'].unique():
    sub_df = df[df['sample']==c]
    dgls = np.unique(list(sub_df['drug2_name'].unique())+list(sub_df['drug1_name'].unique()))
    for d in dgls:
        emax.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['E1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['E2'].mean()))))
        cell_line.append(c)
        drug.append(d)
        ec50.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['C1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['C2'].mean()))))

sensitivity_map = pd.DataFrame({'sample':cell_line,'drug':drug,'emax':emax}) 
        
sens_map = sensitivity_map.groupby('drug')[['emax']].mean().merge(pd.read_csv('drug_classes.csv'),left_on='drug',right_on='drug_name')
sens_map = sens_map.sort_values('emax').reset_index(drop=False)

grp_cols = ['#fffe40','aqua','springgreen']
fig = plt.figure(figsize=(6,3.5),facecolor='w')
ax = []
ax.append(plt.subplot2grid((13,12),(2,0),colspan=7,rowspan=7))
ax.append(plt.subplot2grid((13,12),(0,0),colspan=7,rowspan=2))
ax.append(plt.subplot2grid((13,12),(2,7),colspan=2,rowspan=7))
ax.append(plt.subplot2grid((13,12),(5,9),colspan=2,rowspan=5))
ax.append(plt.subplot2grid((13,12),(9,0),colspan=3,rowspan=3))


mat = np.zeros((len(sens_map),len(sens_map)))*np.nan
for e1,d1 in enumerate(sens_map['drug_name']):
    for e2,d2 in enumerate(sens_map['drug_name']):
        if e1!=e2:
            msk = ((df['drug1_name']==d1)&(df['drug2_name']==d2))|((df['drug1_name']==d2)&(df['drug2_name']==d1))
            mat[e1,e2] = df.loc[msk,'bliss_fit_mxd1d2'].mean()

to_remove = np.where(np.sum(np.isnan(mat),axis=0)>15)[0]
mat=np.delete(mat,to_remove,axis=1)
mat=np.delete(mat,to_remove,axis=0)

sens_map = sens_map.loc[~np.in1d(sens_map.index.values,to_remove)]
sens_map.reset_index(drop=True,inplace=True)
hm = ax[0].pcolor(mat,cmap=cm.seismic,vmin=np.nanmin(mat), vmax=np.nanmax(mat))
hm.cmap.set_under('silver')
plt.sca(ax[0])
plt.axis('off')

mask = []
mask.append((sens_map['emax']<.15))
mask.append((sens_map['emax']>.15)&(sens_map['emax']<.5))
mask.append((sens_map['emax']>.5))
p=[]
for e,m in enumerate(mask):
    x = np.where(m)[0][0]
    w = sum(m)
    p.append(Rectangle((x,x),w,w,fill=False,color=grp_cols[e],linewidth=3,zorder=100))
    ax[0].add_patch(p[-1])

un_class = list(sens_map['class'].unique())
cols = cm.nipy_spectral(np.linspace(0,1,len(sens_map['class'].unique())))
col_list = []
for ind in sens_map.index:
   col_list.append(cols[un_class.index(sens_map.loc[ind,'class'])])

ax[1].bar(range(len(sens_map)),sens_map['emax'],width=1,color=col_list,edgecolor=col_list,lw=0)
ax[1].set_xticks(np.arange(len(sens_map)))
ax[1].set_xlim((0,len(sens_map)-.5))
ax[1].set_xticklabels([])
ax[1].set_ylabel("%-viability",labelpad=0)
ax[1].set_title("Avg Emax Across Cell Panel\n<---Sensitive   Resistant--->",fontsize=8)
ax[1].set_ylim((0,1))
ax[1].set_yticks([0,.5,1])
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].xaxis.set_ticks_position('bottom')
ax[1].yaxis.set_ticks_position('left')

ax[2].barh(range(len(sens_map)),sens_map['emax'],height=1,color=col_list,edgecolor=col_list,lw=0)
ax[2].set_yticks(np.arange(len(sens_map)))
ax[2].set_ylim((0,len(sens_map)-.5))
ax[2].set_yticklabels([])
ax[2].set_xlabel("%-viability",labelpad=0)
ax[2].set_xlim((0,1))
ax[2].set_xticks([0,.5,1])
ax[2].spines['top'].set_visible(False)
ax[2].spines['right'].set_visible(False)
ax[2].xaxis.set_ticks_position('bottom')
ax[2].yaxis.set_ticks_position('left')


ax[3].patch.set_facecolor('w')
ax[3].patch.set_alpha(0.)
ax[3].spines['top'].set_visible(False)
ax[3].spines['right'].set_visible(False)
ax[3].spines['bottom'].set_visible(False)
ax[3].spines['left'].set_visible(False)
ax[3].set_xticks([])
ax[3].set_yticks([])
ax[3].set_ylim((0,1.1))
ax[3].set_xlim((0,1))

for e,i in enumerate(un_class):
    ax[3].scatter(.05,1-(e)/10.,s=50,marker='s',c=cols[e])
    ax[3].text(.25,1-(e)/10.,i,ha='left',va='center')




mask1 = []
mask1.append(sens_map['emax']<.15)
mask1.append((sens_map['emax']>.15)&(sens_map['emax']<.5))
mask1.append((sens_map['emax']>.5))
labs = ['Emax<15%','15%<Emax<50%','50%<Emax']
mask = []
for j in range(3):
    mask.append(np.in1d(df['drug1_name'],sens_map['drug_name'].loc[mask1[j]]) | np.in1d(df['drug2_name'],sens_map['drug_name'].loc[mask1[j]]))

p = []
for e in range(len(mask)):
    p.append(ax[4].boxplot(df['bliss_fit_mxd1d2'].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False,zorder=20))

for e,bp in enumerate(p):
    plt.setp(bp['medians'],color='r',linewidth=2)
    plt.setp(bp['whiskers'],color='k')
    plt.setp(bp['caps'],color='k')
    plt.setp(bp['boxes'],edgecolor='k')
    
    for patch in bp['boxes']:
        patch.set(facecolor=grp_cols[e]) 

ax[4].set_xticks([])
ax[4].set_xlim((-.5,2.5))
ax[4].set_xticklabels([])
ax[4].set_ylabel("Bliss @ max(d1,d2)",labelpad=-2)
ax[4].set_ylim((-.3,.2))
ax[4].set_yticks([-.2,0.,.2])
ax[4].spines['top'].set_visible(False)
ax[4].spines['right'].set_visible(False)
ax[4].spines['bottom'].set_position('zero')
ax[4].xaxis.set_ticks_position('bottom')
ax[4].yaxis.set_ticks_position('left')
ax[4].tick_params(axis='x',direction='out')
ax[4].legend([bp['boxes'][0] for bp in p], labs,ncol=1,bbox_to_anchor=(1.05, 1.0))
ax[4].plot((0,1),(-.3,-.3),color='k',linewidth=2,clip_on=False)
ax[4].plot((1,2),(-.4,-.4),color='k',linewidth=2,clip_on=False)
ax[4].plot((0,2),(-.5,-.5),color='k',linewidth=2,clip_on=False)

for e in range(2):
    print ttest_ind(df['bliss_fit_mxd1d2'].loc[mask[e]].values,df['bliss_fit_mxd1d2'].loc[mask[e+1]].values).pvalue
print ttest_ind(df['bliss_fit_mxd1d2'].loc[mask[0]].values,df['bliss_fit_mxd1d2'].loc[mask[2]].values).pvalue

plt.savefig('bliss_bias_drugClass.pdf',bbox_inches = 'tight',pad_inches = 0)


##########################################################################################
##########################################################################################
#Show the problems with Loewe
fit_fil = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
dat_fil = 'oneil_anticancer/merck_perVia_10-29-2018.csv'
T = pd.read_csv("../../Data/" + fit_fil)
T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T.reset_index(drop=True,inplace=True)
df = T

xaxis = "avg_Emax"
df[xaxis] = df[['E1','E2']].mean(axis=1)

# Calculate the moving windows of the scattered data
percentiles = [i/10. for i in range(1,10)]
n_steps=20
window_size=0.05 # This is in h-space
window_df2 = moving_window_percentiles(df, xaxis, "loewe_fit_perUnd", percentiles = percentiles, n_steps=n_steps, window_size=window_size)
x = np.float64(window_df2['xmid'])

xlim=(-.1,1)
ylim=(-.1,1)

fig = plt.figure(figsize=(3.5,1.5),facecolor='w')
ax = []
ax.append(plt.subplot(111))
ax[0].scatter(df[xaxis],df['loewe_fit_perUnd'],alpha=0.05, s=.5,rasterized=True)
plot_quantile_bias(window_df2, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
ax[0].set_xlabel('Avg(E1,E2)')
ax[0].set_xlim((0,1))
ax[0].set_xticks([0.,.5,1.])
ax[0].set_ylim((0,1))
ax[0].set_title("% Conditions\nUndefined by Loewe",fontsize=8)
ax[0].set_yticks([0,1])
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')
ax[0].set_ylabel('Percent',labelpad=-1)
plt.subplots_adjust(hspace=.0)
plt.savefig('loewe_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)

#######
df = df[(df['loewe_fit_perUnd']>.5)&(df['beta']>.5)&(df['E1']<.9)&(df['E2']<.9)]

sub_T =df.loc[270]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
title = '%% undefined Loewe:%.2f'%sub_T['loewe_fit_perUnd']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 
[[fig_surf,fig_slice],[ax_surf,ax_slice1,ax_slice2],[tt,yy,zz],surf] = matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,plt_zeropln=False,fname=None, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))

c_plane = colorConverter.to_rgba('k', alpha=0.3)
d2_min,d2_max = ax_surf.get_xlim()
d1_min,d1_max = ax_surf.get_ylim()
zero_conc=0
verts = [np.array([(d2_min-zero_conc,d1_min-zero_conc), (d2_min-zero_conc,d1_max), (d2_max,d1_max), (d2_max,d1_min-zero_conc), (d2_min-zero_conc,d1_min-zero_conc)])]
poly = PolyCollection(verts, facecolors=c_plane)
#ax.add_collection3d(poly, zs=[max(e1,e2)], zdir='z')
ax_surf.add_collection3d(poly, zs=[max(sub_T['E1_obs'],sub_T['E2_obs'])], zdir='z')
plt.sca(ax_surf)
# Plot intersection of surface with DIP=0 plane
#plt.contour(tt,yy,zz,levels=[max(e1,e2)], linewidths=3, colors='k')
plt.contour(tt,yy,zz,levels=[max(sub_T['E1_obs'],sub_T['E2_obs'])], linewidths=3, colors='k')
#    
#from matplotlib import colors
## define the bins and normalize
#cmap = colors.ListedColormap(['green','blue'])
#boundaries = [-np.inf,max(sub_T['E1_obs'],sub_T['E2_obs']),np.inf]
#norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)
#
#surf.set(cmap=cmap,norm=norm)

fig_surf.savefig(drug1_name+'_'+drug2_name+'_'+sample+'.pdf')
fig_slice.savefig(drug1_name+'_'+drug2_name+'_'+sample+'_slices.pdf')

################################################################################
################################################################################
#Show schindler is not a fix
#Show the problems with Loewe
fit_fil1 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation_allDoses.csv'
fit_fil2 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
T1 = pd.read_csv("../../Data/" + fit_fil1)
T2 = pd.read_csv("../../Data/" + fit_fil2)
T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.9)]
T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]

T = T1.merge(T2,on='drugunique')
T = T[~np.isnan(T['schindler'])]
df = T

df= df.reset_index(drop=True)
#    
xaxis = "delta_Emax"
df[xaxis] = np.abs(df['E1']-df['E2'])


# Calculate the moving windows of the scattered data
percentiles = [i/10. for i in range(1,10)]
n_steps=100
window_size=0.01 # This is in h-space
window_df = moving_window_percentiles(df, xaxis, "schindler", percentiles = percentiles, n_steps=n_steps, window_size=window_size,logx=False)
x = np.float64(window_df['xmid'])

xlim=(0.,1.1)
ylim=(-2,2)

fig = plt.figure(figsize=(3.5,1.5),facecolor='w')
ax = []
ax.append(plt.subplot(111))
ax[0].scatter(df[xaxis],df['schindler'],alpha=0.03, s=.1,rasterized=True)
plot_quantile_bias(window_df, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
ax[0].set_xlabel(r'$\Delta$(E1,E2)')
ax[0].set_xlim(xlim)
ax[0].set_xticks([0.,.5,1.])
ax[0].set_ylabel("Schindler",labelpad=-4)
ax[0].set_ylim((-.5,.5))
ax[0].set_yticks([-.5,0,.25])
ax[0].set_ylim((-.25,.35))
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')
ax[0].set_title('Schindler biased by difference in Emax',fontsize=8)
plt.subplots_adjust(hspace=.0)
plt.savefig('Schindler_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)


#Find a particularily extreme example
T2["delta_Emax"] = np.abs(T2['E1']-T2['E2'])
T2 = T2.merge(df[['schindler','drugunique']].groupby(['drugunique']).sum(),on='drugunique')
sub_T = T2[(T2[['log_alpha1','log_alpha2','beta']]<0).all(axis=1) & (T2['delta_Emax']>.9) & (T2[['E1','E2']]<1).all(axis=1)]


sub_T[['delta_Emax','schindler','beta','log_alpha1','log_alpha2']].sort_values('schindler')

import os
sub_T =T2.loc[10646]
sub_T['to_save_plots'] = 1
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
title = r'$\Sigma$ Schindler:%.2f'%sub_T['loewe_fit_perUnd']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$E_1=%.2f\pm%.2f$'%(sub_T['E1'],sub_T['E1_std']) + '\n' + r'$E_2=%.2f\pm%.2f$'%(sub_T['E2'],sub_T['E2_std']) 
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,plt_zeropln=False,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))

################################################################################
################################################################################
#Zimmer and CI errors
fit_fil1 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation_allDoses.csv'
fit_fil2 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
T2 = pd.read_csv("../../Data/" + fit_fil2)
T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.9)]
T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]
T2 = T2[(~np.isnan(T2['zimmer_raw_a1']) & (~np.isnan(T2['zimmer_raw_a2'])))]
T2.reset_index(drop=True,inplace=True)
df = T2
xaxis = "avg_Emax"
df[xaxis] = df[['E1','E2']].mean(axis=1)

xlim=(0.,1.)
ylim=(-2,2)

df[(df['zimmer_R2']<0.2)&(df['E1']<.7)&(df['E2']<.7)][['beta','log_alpha1','log_alpha2']]

from plotFun import matplotlibArbitrarySurface
from SynergyCalculator.calcOtherSynergyMetrics import get_params,bliss,zimmer,zimmer_effective_dose
from SynergyCalculator.initialFit import init_fit

elev=20;azim=70


i = 164#117
sub_T       = df.loc[i].to_dict()
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
title = r'MuSyC R2:%.2f'%sub_T['R2']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 
[fig1,fig2],[ax_surf,ax1,ax2],[tt,yy,zz],surf = matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,plt_zeropln=False,fname=None, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))

ax_surf.view_init(elev=elev,azim=azim)
fname = drug1_name + '_' + drug2_name + '_' + sample +'.pdf'
fig1.savefig(fname,pad_inches=0.,format='pdf')


E_fix = None
E_bnd = [[.9,0.,0.,0.,0.],[1.1,2.5,2.5,2.5]]
E_fx = [np.nan,np.nan,np.nan,np.nan] if E_fix is None else E_fix
E_bd = [[-np.inf,-np.inf,-np.inf,-np.inf],[np.inf,np.inf,np.inf,np.inf]] if E_bnd is None else E_bnd
   
sub_T = init_fit(sub_T,d1,d2,dip,dip_sd,drug1_name,drug2_name,E_fx,E_bd)  
C1 = 10**sub_T['log_C1']
C2 = 10**sub_T['log_C2']
h1 = 10**sub_T['log_h1']
h2 = 10**sub_T['log_h2']
C1, C2, h1, h2, a12, a21, _, R2  = zimmer(d1, d2, dip, logspace=False, sigma=None, vector_input=True,p0=[C1,C2,h1,h2,0,0])   
zero_conc = 0
N=15
d1_min=np.log10(sub_T['min_conc_d1']);d1_max=np.log10(sub_T['max_conc_d1'])
d2_min=np.log10(sub_T['min_conc_d2']);d2_max=np.log10(sub_T['max_conc_d2'])
y = np.linspace(d1_min-zero_conc, d1_max ,N)
t = np.linspace(d2_min-zero_conc, d2_max ,N)
yy,tt=np.meshgrid(y,t)
zz = zimmer_effective_dose(10**yy,10**tt,C1,C2,h1,h2,a12,a21)
title = r'Zimmer R2:%.2f'%sub_T['zimmer_R2']+'\n'+r'$a12=%.2f$'%(sub_T['zimmer_raw_a1'])+ '\n' + r'$a21 = %.2f$'%(sub_T['zimmer_raw_a2'])

fig1,ax = matplotlibArbitrarySurface(tt,yy,zz,d1,d2,dip,dip_sd,drug1_name,drug2_name,fname=None,metric_name='Percent',zlim=(0,1.1),zero_conc=0,title=title,d1lim=None,d2lim=None,fmt='pdf',plt_zeropln=False,plt_data=True,figsize_surf=(2.2,2.4))

fname = drug1_name+'_'+drug2_name+'_'+sample+'_zimmer.pdf'
elev=20;azim=70
ax.view_init(elev=elev,azim=azim)
fig1.savefig(fname,pad_inches=0.,format='pdf')





df[((df[['E1','E2']]<.7).all(axis=1))&((df[['E1','E2']]>.4).all(axis=1))&((df[['E1_std','E2_std']]<.01).all(axis=1))][['beta_obs']]


df[((df[['E1','E2']]<.7).all(axis=1))&((df[['E1','E2']]>.6).all(axis=1))&(df['beta']>1)][['beta_obs']]
i = 9918
sub_T       = df.loc[i].to_dict()
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)

from SynergyCalculator.calcOtherSynergyMetrics import combination_index

###Now show combination index plots
CI,d1_full,d2_full,Dm1,Dm2,m1,m2  = combination_index(d1,d2,dip)
def ll2(d,h,C):
    return 1/(1+(d/C)**h)
from SynergyCalculator.NDHillFun import Edrug1D

plt.figure(figsize=(3,2))
ax1=plt.subplot(121)
ax2=plt.subplot(122)
lw=2;sz=20
d1_tmp = d1[d2==0];d2_tmp=d2[d1==0]
d1_tmp[d1_tmp==0]=min(d1[d1!=0])/10.; d2_tmp[d2_tmp==0]=min(d2[d2!=0])/10.
ax1.scatter(np.log10(d1_tmp),dip[d2==0],s=sz,c='k')
d1_tmp = np.logspace(np.log10(min(d1_tmp)),np.log10(max(d1_tmp)),100)
ax1.plot(np.log10(d1_tmp),ll2(d1_tmp,m1,Dm1),c='r',label='CI',lw=lw)
ax1.plot(np.log10(d1_tmp),Edrug1D(d1_tmp,*[sub_T[i] for i in ['E0','E1','C1','h1']]),c='b',label='MuSyC',lw=lw)
ax1.set_ylim((0,1.1))
ax1.set_xlabel('log(%s)'%drug1_name)
ax1.set_ylabel('% Viability')
ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)

ax2.scatter(np.log10(d2_tmp),dip[d1==0],s=sz,c='k')
d2_tmp = np.logspace(np.log10(min(d2_tmp)),np.log10(max(d2_tmp)),100)
ax2.plot(np.log10(d2_tmp),ll2(d2_tmp,m2,Dm2),c='r',label='CI',lw=lw)
ax2.plot(np.log10(d2_tmp),Edrug1D(d2_tmp,*[sub_T[i] for i in ['E0','E2','C2','h2']]),c='b',label='MuSyC',lw=lw)
ax2.set_ylim((.0,1.1))
ax2.legend()
ax2.set_xlabel('log(%s)'%drug2_name)
ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)
ax2.set_yticklabels([])
plt.tight_layout()
plt.savefig('CI_misfits.pdf')
title = r'$Median CI:%.2f'%np.nanmedian(CI)+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$E_1=%.2f\pm%.2f$'%(sub_T['E1'],sub_T['E1_std']) + '\n' + r'$E_2=%.2f\pm%.2f$'%(sub_T['E2'],sub_T['E2_std']) 

matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,plt_zeropln=False,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))








#
#
#cell_line = []
#drug =[]
#emax=[]
#emax_sd=[]
#ec50=[]
#ec50_sd=[]
#zimmer = []
#
#df = df[abs(df['zimmer_raw_a1'])<100]
#for c in df['sample'].unique():
#    sub_df = df[df['sample']==c]
#    dgls = np.unique(list(sub_df['drug2_name'].unique())+list(sub_df['drug1_name'].unique()))
#    for d in dgls:
#        emax.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['E1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['E2'].mean()))))
#        cell_line.append(c)
#        drug.append(d)
#        ec50.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['C1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['C2'].mean()))))
#        zimmer.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['zimmer_raw_a1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['zimmer_raw_a1'].mean()))))
#
#sensitivity_map = pd.DataFrame({'sample':cell_line,'drug':drug,'emax':emax,'zimmer':zimmer}) 
#        
#sens_map = sensitivity_map.groupby('sample')[['emax','zimmer']].mean().merge(pd.read_csv('cell_lines_classification.csv'),on='sample')
#sens_map = sens_map.sort_values('emax').reset_index(drop=False)
#
#un_class = list(sens_map['class'].unique())
#cols = cm.nipy_spectral(np.linspace(0,1,len(sens_map['class'].unique())))
#col_list = []
#for ind in sens_map.index:
#   col_list.append(cols[un_class.index(sens_map.loc[ind,'class'])])
#
#fig = plt.figure(figsize=(6,3.5),facecolor='w')
#ax = []
#ax.append(plt.subplot2grid((3,5),(0,0),colspan=4))
#ax.append(plt.subplot2grid((3,5),(1,0),colspan=4))
#ax.append(plt.subplot2grid((3,5),(2,0),colspan=4))
#ax.append(plt.subplot2grid((3,5),(0,4),rowspan=3))
#
#ax[0].bar(range(len(sens_map)),sens_map['emax'],width=1,color=col_list,edgecolor=col_list,lw=0)
#ax[0].set_xticks(np.arange(len(sens_map))+.5)
#ax[0].set_xticklabels([])
#ax[0].set_ylabel("%-viability",labelpad=-2)
#ax[0].set_title("Avg Efficacy Across Cell Panel\n<---Sensitive   Resistant--->",fontsize=8)
#ax[0].set_ylim((0,1))
#ax[0].set_yticks([0,.5,1])
#ax[0].spines['top'].set_visible(False)
#ax[0].spines['right'].set_visible(False)
#ax[0].xaxis.set_ticks_position('bottom')
#ax[0].yaxis.set_ticks_position('left')
#
#ax[1].bar(range(len(sens_map)),sens_map['zimmer'],width=1,color=col_list,edgecolor=col_list,lw=0)
#ax[1].set_xticks(np.arange(len(sens_map))+.5)
#ax[1].set_xticklabels([])
#ax[1].set_ylabel("zimmer",labelpad=-2)
#ax[1].text(20,.01,"Avg zimmer Across\nDrug Combination Panel",ha='center')
#ax[1].set_ylim((-.1,.01))
#ax[1].set_yticks([-.1,0.])
#ax[1].spines['top'].set_visible(False)
#ax[1].spines['right'].set_visible(False)
#ax[1].spines['bottom'].set_position('zero')
#ax[1].xaxis.set_ticks_position('bottom')
#ax[1].yaxis.set_ticks_position('left')
#ax[1].tick_params(axis='x',direction='out')
#
#mask1 = []
#mask1.append(sens_map['emax']<.15)
#mask1.append((sens_map['emax']>.15)&(sens_map['emax']<.45))
#mask1.append((sens_map['emax']>.45)&(sens_map['emax']<.75))
#mask1.append((sens_map['emax']>.75))
#labs = ['Emax<15%','15%<Emax<45%','45%<Emax<75%','Emax>75%']
#mask = []
#for j in range(4):
#    mask.append(np.in1d(df['drug1_name'],sens_map['drug_name'].loc[mask1[j]]) | np.in1d(df['drug2_name'],sens_map['drug_name'].loc[mask1[j]]))
#
#p = []
#for e in range(len(mask)):
#    p.append(ax[2].boxplot(df['bliss_fit_mxd1d2'].loc[mask[e]].values,positions=[np.mean(np.where(mask1[e])[0])],notch=False,patch_artist=True,widths=.75,showfliers=False,zorder=20))
#
#for e,bp in enumerate(p):
#    plt.setp(bp['medians'],color='r',linewidth=2)
#    plt.setp(bp['whiskers'],color='k')
#    plt.setp(bp['caps'],color='k')
#    plt.setp(bp['boxes'],edgecolor='k')
#    
#    for patch in bp['boxes']:
#        patch.set(facecolor=cols[e]) 
#
#ax[2].set_xticks([])
#ax[2].set_xlim((0,len(sens_map)))
#ax[2].set_xticklabels([])
#ax[2].set_ylabel("Bliss @ max(d1,d2)",labelpad=-2)
#ax[2].set_ylim((-.2,.1))
#ax[2].set_yticks([-.1,0.])
#ax[2].spines['top'].set_visible(False)
#ax[2].spines['right'].set_visible(False)
#ax[2].spines['bottom'].set_position('zero')
#ax[2].xaxis.set_ticks_position('bottom')
#ax[2].yaxis.set_ticks_position('left')
#ax[2].tick_params(axis='x',direction='out')
#ax[2].legend([bp['boxes'][0] for bp in p], labs,ncol=1,bbox_to_anchor=(1.35, 1.2))
#
#ax[3].patch.set_facecolor('w')
#ax[3].patch.set_alpha(0.)
#ax[3].spines['top'].set_visible(False)
#ax[3].spines['right'].set_visible(False)
#ax[3].spines['bottom'].set_visible(False)
#ax[3].spines['left'].set_visible(False)
#ax[3].set_xticks([])
#ax[3].set_yticks([])
#ax[3].set_ylim((0,1.1))
#ax[3].set_xlim((0,1))
#
#for e,i in enumerate(un_class):
#    ax[3].scatter(.05,1-(e)/10.,s=50,marker='s',c=cols[e])
#    ax[3].text(.25,1-(e)/10.,i,ha='left',va='center')
#
#plt.tight_layout()
