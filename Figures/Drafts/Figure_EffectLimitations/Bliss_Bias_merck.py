#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 16:42:04 2018

@author: xnmeyer
"""
#Import packages
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
from matplotlib.colors import ListedColormap
import matplotlib.cm as cm
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
import scipy
# Based on code written by Alex Lubbuck for the PyDRC package
import scipy.stats    
import pandas as pd

fit_fil1 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation_allDoses.csv'
fit_fil2 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
#T1 = pd.read_csv("../../Data/" + fit_fil1)
T2 = pd.read_csv("../../Data/" + fit_fil2)
T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.9)]
T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]

#T = T1.merge(T2,on='drugunique')
#T = T[~np.isnan(T['bliss'])]

T2.reset_index(drop=True,inplace=True)
df = T2

def moving_window_percentiles(df, xaxis, yaxis, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True):
    """
    Calculates moving window percentiles.
    The first window begins at df[xaxis].min()-window_size/2
    The final window ends at df[xaxis].max()+window_size/2
    The window step_size is calculated so that the result will have n_steps many values
    x values for each window are the midpoint of xmin,xmax of that window
    """
    dff = pd.DataFrame(df)[xaxis]
    if logx: dff = np.log(dff)
    XMIN, XMAX = dff.min()-window_size/2., dff.max()+window_size/2.
    step_size = (XMAX - XMIN - window_size) / (1.*(n_steps-1))
    w_df = pd.DataFrame(index = range(n_steps), columns=percentiles + ['xmin','xmax','xmid','n_rows'])
    
    for i in w_df.index:
        xmin = XMIN + step_size*i
        xmax = xmin + window_size
        xmid = xmin + window_size/2.
        if logx: w_df.loc[i,['xmin','xmax','xmid']] = np.exp(xmin), np.exp(xmax), np.exp(xmid)
        else: w_df.loc[i,['xmin','xmax','xmid']] = xmin, xmax, xmid
        sub_df = df.loc[(dff >= xmin) & (dff <= xmax)]
        w_df.loc[i,"n_rows"] = sub_df.shape[0]
        w_df.loc[i, percentiles] = sub_df[yaxis].quantile(percentiles)

    return w_df
        
def plot_quantile_bias(window_df, xaxis, ax, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True, alpha_lim=(0.2,0.8), color='gold', bias=None, xlim=None, ylim=None, plot_midline=True):
    """
    Plot the bias of df[yaxis] as a function of df['xmid']
    Assumes window_df has been returned as window_df=moving_window_percentiles(df, ...)
    """
    
    x = np.float64(window_df['xmid'])
    
    n_color_shades = len(percentiles)/2
    
    for i in range(n_color_shades):
        alpha = alpha_lim[0] + (alpha_lim[1] - alpha_lim[0])*i/(n_color_shades-1.)
        y1 = np.float64(window_df[percentiles[i+1]])
        y2 = np.float64(window_df[percentiles[i]])
        ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
        
        if i<n_color_shades-1 or len(percentiles)%2==1:
            y1 = np.float64(window_df[percentiles[-i-1]])
            y2 = np.float64(window_df[percentiles[-i-2]])
            ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
    if bias is not None: ax.plot(bias[0], bias[1],'b-',lw=2,zorder=1000,label='Predicted Bias')
    ax.plot([x.min(), x.max()], [0,0],'k--')
    if plot_midline: ax.plot(x, window_df[percentiles[n_color_shades]], 'r-',label='Median') # Draw the median, (assuming 0.5 is the middle percentile)
    
    if xlim is not None: ax.set_xlim(xlim)
    if ylim is not None: ax.set_ylim(ylim)
    if logx: ax.set_xscale('log')
    
    

df = df[~np.isnan(df[['bliss_fit_ec50','bliss_fit_md1d2','bliss_fit_mxd1d2']].mean(axis=1))]
df= df.reset_index(drop=True)
#    
xaxis = "avg_Emax"
df[xaxis] = df[['E1','E2']].mean(axis=1)
df = df[df['avg_Emax']<.75].reset_index(drop=True)
T2[xaxis] = T2[['E1','E2']].mean(axis=1)
T2 = T2[T2['avg_Emax']<.75].reset_index(drop=True)

# Calculate the moving windows of the scattered data
percentiles = [i/10. for i in range(1,10)]
n_steps=1000
window_size=0.01 # This is in h-space
window_df = moving_window_percentiles(df, xaxis, "bliss_fit_mxd1d2", percentiles = percentiles, n_steps=n_steps, window_size=window_size)
window_df2 = moving_window_percentiles(T2, xaxis, "beta", percentiles = percentiles, n_steps=n_steps, window_size=window_size)
x = np.float64(window_df['xmid'])

xlim=(0.,.75)
ylim=(-2,2)
#fig = plt.figure(figsize=(2.5,3.5),facecolor='w')
#ax = []
#ax.append(plt.subplot2grid((6,1),(0,0),rowspan=2))
#ax.append(plt.subplot2grid((6,1),(2,0),rowspan=2))
#ax.append(plt.subplot2grid((6,1),(4,0),rowspan=2))
## Plot the loewe bias
#
#ax[0].scatter(df[xaxis],df['bliss_fit_md1d2'],alpha=0.05, s=.5,rasterized=True)
#plot_quantile_bias(window_df1, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
#ax[0].set_xticks([0.,.5,1.])
#ax[0].set_xticklabels([])
#ax[0].set_ylabel("Bliss @ min(d1,d2)",labelpad=-4)
#ax[0].set_yticks([-2,0,2])
#ax[0].spines['top'].set_visible(False)
#ax[0].spines['right'].set_visible(False)
#ax[0].xaxis.set_ticks_position('bottom')
#ax[0].yaxis.set_ticks_position('left')
#
#ax[1].scatter(df[xaxis],df['bliss_fit_ec50'],alpha=0.05, s=.5,rasterized=True)
#plot_quantile_bias(window_df2, xaxis, ax[1], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
#ax[1].set_xticks([0.,.5,1.])
#ax[1].set_xticklabels([])
#ax[1].set_ylabel("Bliss @ EC50",labelpad=-4)
#ax[1].set_yticks([-2,0,2])
#ax[1].spines['top'].set_visible(False)
#ax[1].spines['right'].set_visible(False)
#ax[1].xaxis.set_ticks_position('bottom')
#ax[1].yaxis.set_ticks_position('left')
#
#
#ax[2].scatter(df[xaxis],df['bliss_fit_mxd1d2'],alpha=0.05, s=.5,rasterized=True)
#plot_quantile_bias(window_df3, xaxis, ax[2], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
#ax[2].set_xlabel('Avg(E1,E2)')
#ax[2].set_xticks([0.,.5,1.])
#ax[2].set_ylabel("Bliss @ EC50",labelpad=-4)
#ax[2].set_title('Bliss biased by Emax',fontsize=8)
#ax[2].set_yticks([-2,0,2])
#ax[2].spines['top'].set_visible(False)
#ax[2].spines['right'].set_visible(False)
#ax[2].xaxis.set_ticks_position('bottom')
#ax[2].yaxis.set_ticks_position('left')
#
#plt.subplots_adjust(hspace=.0)


fig = plt.figure(figsize=(2.5,1.25),facecolor='w')
ax = []
ax.append(plt.subplot(111))
ax[0].scatter(df[xaxis],df['bliss_fit_mxd1d2'],alpha=0.05, s=.5,rasterized=True)
plot_quantile_bias(window_df, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
ax[0].set_xlabel('Avg(E1,E2)')
ax[0].set_xticks([0.,.5])
ax[0].set_ylabel("Bliss @ EC50",labelpad=-4)
ax[0].set_yticks([-.1,0,.1])
ax[0].set_ylim((-.25,.25))
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')
ax[0].set_title('Bliss biased by Emax',fontsize=8)
plt.subplots_adjust(hspace=.0)
plt.savefig('bliss_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)


fig = plt.figure(figsize=(2.5,1.25),facecolor='w')
ax = []
ax.append(plt.subplot(111))
ax[0].scatter(df[xaxis],df['beta'],alpha=0.05, s=.5,rasterized=True)
plot_quantile_bias(window_df2, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
ax[0].set_xlabel('Avg(E1,E2)')
ax[0].set_xticks([0.,.5])
ax[0].set_ylabel(r"$\beta$",labelpad=-4)
ax[0].set_yticks([-1,0,1])
ax[0].set_ylim((-1,1))
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')
ax[0].set_title('Beta as a function of Emax',fontsize=8)
plt.subplots_adjust(hspace=.0)
plt.savefig('bliss_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)



#
#
##df[(df['avg_Emax']>.5)&(df['beta']>0)&(df['log_alpha1']>0)&(df['log_alpha2']>0)][['avg_Emax','beta','log_alpha1','log_alpha2']].sort_values('beta')
##
##df.sort_values('avg_Emax')[['avg_Emax','beta','log_alpha1','log_alpha2']]
##
##
##
##
## df[(df['avg_Emax']>.7)&(df['beta']>.5)&(df['log_alpha1']>0)&(df['log_alpha2']>0)][['avg_Emax','beta','log_alpha1','log_alpha2','sample','drug1_name','drug2_name','R2']].sort_values('beta')
## 
## 
# 
# 
 
T2 = pd.read_csv("../../Data/" + fit_fil2)
T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.9)]
T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]

df = T2
 
cl = pd.read_csv('cell_lines_classification.csv')
 
cell_line = []
drug =[]
emax=[]
emax_sd=[]
ec50=[]
ec50_sd=[]

for c in df['sample'].unique():
    sub_df = df[df['sample']==c]
    dgls = np.unique(list(sub_df['drug2_name'].unique())+list(sub_df['drug1_name'].unique()))
    for d in dgls:
        emax.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['E1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['E2'].mean()))))
        cell_line.append(c)
        drug.append(d)
        ec50.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['C1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['C2'].mean()))))


sensitivity_map = pd.DataFrame({'sample':cell_line,'drug':drug,'emax':emax}) 
        
sens_map = pd.DataFrame(sensitivity_map.groupby('sample')['emax'].mean()).merge(cl,on='sample')
sens_map = sens_map.merge(pd.DataFrame(df.groupby('sample')['bliss_fit_mxd1d2'].mean()),on='sample')   
sens_map = sens_map.sort_values('emax').reset_index(drop=True)

un_class = list(sens_map['class'].unique())
cols = cm.nipy_spectral(np.linspace(0,1,len(sens_map['class'].unique())))
col_list = []
for ind in sens_map.index:
   col_list.append(cols[un_class.index(sens_map.loc[ind,'class'])])


fig = plt.figure(figsize=(6,3.5),facecolor='w')
ax = []
ax.append(plt.subplot2grid((3,1),(0,0)))
ax.append(plt.subplot2grid((3,1),(1,0)))
ax.append(plt.subplot2grid((3,1),(2,0)))


ax[0].bar(range(len(sens_map)),sens_map['emax'],width=1,color=col_list,edgecolor=col_list,lw=0)
for uc in un_class:
    tmp = sens_map[sens_map['class']==uc]
    col = cols[un_class.index(uc)]
    ax[0].bar(tmp.index[0],tmp['emax'].iloc[0],color=col,width=1,edgecolor=col,lw=0,label=uc)
ax[0].set_xticks(np.arange(len(sens_map))+.5)
ax[0].set_xticklabels([])
ax[0].set_ylabel("%-viability",labelpad=-2)
ax[0].set_title("Avg Efficacy Across Drug Panel\n<---Sensitive   Resistant--->",fontsize=8)
ax[0].set_ylim((0,1))
ax[0].set_yticks([0,.5,1])
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')
ax[0].legend(frameon=False,borderpad=.1,labelspacing=.25,handletextpad=0.1,ncol=len(un_class)/2,loc='upper center')

ax[1].bar(range(len(sens_map)),sens_map['bliss_fit_mxd1d2'],width=1,color=col_list,edgecolor=col_list,lw=0)
ax[1].set_xticks(np.arange(len(sens_map))+.5)
ax[1].set_xticklabels([])
ax[1].set_ylabel("Bliss @ max(d1,d2)",labelpad=-6)
ax[1].text(20,.01,"Avg Bliss Across\nDrug Combination Panel",ha='center')
ax[1].set_ylim((-.125,.01))
ax[1].set_yticks([-.1,0.])
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].spines['bottom'].set_position('zero')
ax[1].xaxis.set_ticks_position('bottom')
ax[1].yaxis.set_ticks_position('left')
ax[1].tick_params(axis='x',direction='out')

mask = []
mask.append(sens_map['emax']<.32)
mask.append((sens_map['emax']>.32)&(sens_map['emax']<.45))
mask.append((sens_map['emax']>.45)&(sens_map['emax']<.5))
mask.append((sens_map['emax']>.5))
pos = [.15,.4,.75]
for e in range(len(mask)):
    ax[2].boxplot(sens_map['bliss_fit_mxd1d2'].loc[mask[e]].values,positions=[np.mean(np.where(mask[e])[0])],notch=False,patch_artist=True,widths=.75,showfliers=False)

ax[2].set_xticks(np.arange(len(sens_map))+.5)
ax[2].set_xticklabels([])
ax[2].set_ylabel("Bliss @ max(d1,d2)",labelpad=-6)
ax[2].set_ylim((-.125,.01))
ax[2].set_yticks([-.1,0.])
ax[2].spines['top'].set_visible(False)
ax[2].spines['right'].set_visible(False)
ax[2].spines['bottom'].set_position('zero')
ax[2].xaxis.set_ticks_position('bottom')
ax[2].yaxis.set_ticks_position('left')
ax[2].tick_params(axis='x',direction='out')

plt.savefig('bliss_sensitivity_bias.pdf', bbox_inches = 'tight',pad_inches = 0)















df = df[~np.isnan(df['bliss_fit_mxd1d2'])]
cell_line = []
drug =[]
emax=[]
emax_sd=[]
ec50=[]
ec50_sd=[]
bliss = []

for c in df['sample'].unique():
    sub_df = df[df['sample']==c]
    dgls = np.unique(list(sub_df['drug2_name'].unique())+list(sub_df['drug1_name'].unique()))
    for d in dgls:
        emax.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['E1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['E2'].mean()))))
        cell_line.append(c)
        drug.append(d)
        ec50.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['C1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['C2'].mean()))))
        bliss.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['bliss_fit_mxd1d2'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['bliss_fit_mxd1d2'].mean()))))

sensitivity_map = pd.DataFrame({'sample':cell_line,'drug':drug,'emax':emax,'bliss':bliss}) 
        
sens_map = sensitivity_map.groupby('drug')[['emax','bliss']].mean().merge(pd.read_csv('drug_classes.csv'),left_on='drug',right_on='drug_name')
sens_map = sens_map.sort_values('emax').reset_index(drop=False)

un_class = list(sens_map['class'].unique())
cols = cm.nipy_spectral(np.linspace(0,1,len(sens_map['class'].unique())))
col_list = []
for ind in sens_map.index:
   col_list.append(cols[un_class.index(sens_map.loc[ind,'class'])])

fig = plt.figure(figsize=(6,3.5),facecolor='w')
ax = []
ax.append(plt.subplot2grid((3,5),(0,0),colspan=4))
ax.append(plt.subplot2grid((3,5),(1,0),colspan=4))
ax.append(plt.subplot2grid((3,5),(2,0),colspan=4))
ax.append(plt.subplot2grid((3,5),(0,4),rowspan=3))


ax[0].bar(range(len(sens_map)),sens_map['emax'],width=1,color=col_list,edgecolor=col_list,lw=0)

ax[0].set_xticks(np.arange(len(sens_map))+.5)
ax[0].set_xticklabels([])
ax[0].set_ylabel("%-viability",labelpad=-2)
ax[0].set_title("Avg Efficacy Across Cell Panel\n<---Sensitive   Resistant--->",fontsize=8)
ax[0].set_ylim((0,1))
ax[0].set_yticks([0,.5,1])
ax[0].spines['top'].set_visible(False)
ax[0].spines['right'].set_visible(False)
ax[0].xaxis.set_ticks_position('bottom')
ax[0].yaxis.set_ticks_position('left')

ax[1].bar(range(len(sens_map)),sens_map['bliss'],width=1,color=col_list,edgecolor=col_list,lw=0)
ax[1].set_xticks(np.arange(len(sens_map))+.5)
ax[1].set_xticklabels([])
ax[1].set_ylabel("Bliss @ max(d1,d2)",labelpad=-2)
ax[1].text(20,.01,"Avg Bliss Across\nDrug Combination Panel",ha='center')
ax[1].set_ylim((-.1,.01))
ax[1].set_yticks([-.1,0.])
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].spines['bottom'].set_position('zero')
ax[1].xaxis.set_ticks_position('bottom')
ax[1].yaxis.set_ticks_position('left')
ax[1].tick_params(axis='x',direction='out')

mask1 = []
mask1.append(sens_map['emax']<.15)
mask1.append((sens_map['emax']>.15)&(sens_map['emax']<.45))
mask1.append((sens_map['emax']>.45)&(sens_map['emax']<.75))
mask1.append((sens_map['emax']>.75))
labs = ['Emax<15%','15%<Emax<45%','45%<Emax<75%','Emax>75%']
mask = []
for j in range(4):
    mask.append(np.in1d(df['drug1_name'],sens_map['drug_name'].loc[mask1[j]]) | np.in1d(df['drug2_name'],sens_map['drug_name'].loc[mask1[j]]))

p = []
for e in range(len(mask)):
    p.append(ax[2].boxplot(df['bliss_fit_mxd1d2'].loc[mask[e]].values,positions=[np.mean(np.where(mask1[e])[0])],notch=False,patch_artist=True,widths=.75,showfliers=False,zorder=20))

for e,bp in enumerate(p):
    plt.setp(bp['medians'],color='r',linewidth=2)
    plt.setp(bp['whiskers'],color='k')
    plt.setp(bp['caps'],color='k')
    plt.setp(bp['boxes'],edgecolor='k')
    
    for patch in bp['boxes']:
        patch.set(facecolor=cols[e]) 

ax[2].set_xticks([])
ax[2].set_xlim((0,len(sens_map)))
ax[2].set_xticklabels([])
ax[2].set_ylabel("Bliss @ max(d1,d2)",labelpad=-2)
ax[2].set_ylim((-.2,.1))
ax[2].set_yticks([-.1,0.])
ax[2].spines['top'].set_visible(False)
ax[2].spines['right'].set_visible(False)
ax[2].spines['bottom'].set_position('zero')
ax[2].xaxis.set_ticks_position('bottom')
ax[2].yaxis.set_ticks_position('left')
ax[2].tick_params(axis='x',direction='out')
ax[2].legend([bp['boxes'][0] for bp in p], labs,ncol=1,bbox_to_anchor=(1.35, 1.2))

ax[3].patch.set_facecolor('w')
ax[3].patch.set_alpha(0.)
ax[3].spines['top'].set_visible(False)
ax[3].spines['right'].set_visible(False)
ax[3].spines['bottom'].set_visible(False)
ax[3].spines['left'].set_visible(False)
ax[3].set_xticks([])
ax[3].set_yticks([])
ax[3].set_ylim((0,1.1))
ax[3].set_xlim((0,1))

for e,i in enumerate(un_class):
    ax[3].scatter(.05,1-(e)/10.,s=50,marker='s',c=cols[e])
    ax[3].text(.25,1-(e)/10.,i,ha='left',va='center')

plt.tight_layout()
