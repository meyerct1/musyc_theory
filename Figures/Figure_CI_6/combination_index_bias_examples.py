#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  8 13:43:32 2018

@author: xnmeyer
"""
import numpy as np
import pandas as pd
import sys
sys.path.append('../../helper_code/')
from pythontools.SynergyCalculator.NDHillFun import Edrug2D_NDB_hill
from pythontools.SynergyCalculator.calcOtherSynergyMetrics import combination_index
#Code to create plots for conflation figure 
from pythontools.SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from pythontools.SynergyCalculator.gatherData import subset_data
import os
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
import matplotlib.cm as cm
from matplotlib import rc
from scipy.interpolate import interp1d
from matplotlib.ticker import FormatStrFormatter

rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

np.random.seed(123)
#Parameters used for all surfaces
C1=1.;C2=1.
r1=100.;r2=100.
alpha1=0.;alpha2=0.
gamma1=1.;gamma2=1.
noise = .0 #No noise
N = 15 #Number of samples
rep = 1 #replicates
# =============================================================================
# Show CI Efficacy bias with Hill slope of 1
# =============================================================================
E0=1.
E1=0.4
E2=0.4
E3=0.4
h1=1.
h2=1.
#generate synthetic data
d1 = []
d2 = []
dip = []
for r in range(rep):
    DD1,DD2 = np.meshgrid(np.concatenate(([0],np.logspace(-3,3,N))),np.concatenate(([0],np.logspace(-3,3,N))))
    d1_tmp = DD1.reshape((-1,))
    d2_tmp = DD2.reshape((-1,))
    dip_tmp = Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
    dip_tmp = np.random.randn(len(dip_tmp))*noise+dip_tmp
    d1 = np.concatenate((d1,d1_tmp))
    d2 = np.concatenate((d2,d2_tmp))
    dip = np.concatenate((dip,dip_tmp))
d1s = d1.copy();d2s = d2.copy()
d1s[d1s==0]=min(d1s[d1s!=0])/10.
d2s[d2s==0]=min(d2s[d2s!=0])/10.
#Calculate combination index
CI,d1_full,d2_full,Dm1,Dm2,m1,m2 = combination_index(d1,d2,dip)
ci_col = np.zeros(len(dip))*np.nan
for e in range(len(d1)):
    if d1[e] in d1_full and d2[e] in d2_full:
        ci_col[e]=-np.log10(CI[(d1_full==d1[e])&(d2_full==d2[e])])
#Create figure
plt.figure(figsize = (3,3))
cmap = plt.set_cmap('seismic')
cmap = cm.get_cmap()
cmap = cmap.set_bad(color='grey')
ax = plt.subplot(111,projection='3d')
#Plot dose response surface coloring the points using the value of the combination index
c = ax.scatter(np.log10(d1s),np.log10(d2s),dip,c=ci_col,cmap=cmap,vmin=-1.5,vmax=1.5)
ax.view_init(azim=30,elev=20)
plt.show()
ax.set_zlim((0,1))
cbar = plt.colorbar(c,label='Combination Index',fraction=.03,pad=-.1)
cbar.set_ticks([-1.5,0,1.5])

d1_proj = np.unique(d1s);d2_proj = np.unique(d2s)
ci_proj_fun = lambda d,Dm,m: 1/(1+(d/Dm)**m)
dip_proj_fun = lambda d,h,C,E0,Emx: Emx+(E0-Emx)/(1+(d/C)**h)
dip_proj_x = dip_proj_fun(d1_proj,h1,C1,E0,E1)
dip_proj_y = dip_proj_fun(d2_proj,h2,C2,E0,E2)

ci_proj_x = ci_proj_fun(d1_proj,Dm1,m1)
ci_proj_y = ci_proj_fun(d2_proj,Dm2,m2)
ax.plot3D(np.log10(np.ones(len(d1_proj))*min(d1s)),np.log10(d2_proj),ci_proj_y,c='k',linewidth=2)
ax.plot3D(np.log10(d1_proj),np.log10(np.ones(len(d2_proj))*min(d2s)),ci_proj_x,c='k',linewidth=2)

ax.scatter(np.log10(np.ones(len(d1_proj))*min(d1s)),np.log10(d2_proj),dip_proj_y,c='grey',linewidth=2)
ax.scatter(np.log10(d1_proj),np.log10(np.ones(len(d2_proj))*min(d2s)),dip_proj_x,c='grey',linewidth=2)

ax.set_xticks([-3,0,3])
ax.set_yticks([-3,0,3])
ax.set_zticks([0,.4,1.])
ax.set_xlabel('log(d1)',labelpad=-5)
ax.set_ylabel('log(d2)',labelpad=-5)
ax.set_zlabel('% effect',labelpad=-5)
ax.tick_params(axis='both',pad=-3)
plt.tight_layout()
plt.savefig('ci_efficacy_bias.pdf')

# =============================================================================
# Hill bias when h<1
# =============================================================================
#Parameters
E0=1.
E1=0.0
E2=0.0
E3=0.0
h1=.5
h2=.5
d1 = []
d2 = []
dip = []
for r in range(rep):
    DD1,DD2 = np.meshgrid(np.concatenate(([0],np.logspace(-3,3,N))),np.concatenate(([0],np.logspace(-3,3,N))))
    d1_tmp = DD1.reshape((-1,))
    d2_tmp = DD2.reshape((-1,))
    dip_tmp = Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
    dip_tmp = np.random.randn(len(dip_tmp))*noise+dip_tmp
    d1 = np.concatenate((d1,d1_tmp))
    d2 = np.concatenate((d2,d2_tmp))
    dip = np.concatenate((dip,dip_tmp))
d1s = d1.copy();d2s = d2.copy()
d1s[d1s==0]=min(d1s[d1s!=0])/10.
d2s[d2s==0]=min(d2s[d2s!=0])/10.
CI,d1_full,d2_full,Dm1,Dm2,m1,m2 = combination_index(d1,d2,dip)
ci_col = np.zeros(len(dip))*np.nan
for e in range(len(d1)):
    if d1[e] in d1_full and d2[e] in d2_full:
        ci_col[e]=-np.log10(CI[(d1_full==d1[e])&(d2_full==d2[e])])
 
cmap = plt.set_cmap('seismic')
cmap = cm.get_cmap()
cmap = cmap.set_bad(color='grey')

plt.figure(figsize = (3,3))
ax = plt.subplot(111,projection='3d')
c = ax.scatter(np.log10(d1s),np.log10(d2s),dip,c=ci_col,cmap=cmap,vmin=-1.,vmax=1.)
ax.view_init(azim=30,elev=20)
plt.show()
ax.set_zlim((0,1))
cbar = plt.colorbar(c,label='Combination Index',fraction=.03,pad=-.1)
cbar.set_ticks([-1.,0,1.])

d1_proj = np.unique(d1s);d2_proj = np.unique(d2s)
ci_proj_fun = lambda d,Dm,m: 1/(1+(d/Dm)**m)
dip_proj_fun = lambda d,h,C,E0,Emx: Emx+(E0-Emx)/(1+(d/C)**h)
dip_proj_x = dip_proj_fun(d1_proj,h1,C1,E0,E1)
dip_proj_y = dip_proj_fun(d2_proj,h2,C2,E0,E2)

ci_proj_x = ci_proj_fun(d1_proj,Dm1,m1)
ci_proj_y = ci_proj_fun(d2_proj,Dm2,m2)
ax.plot3D(np.log10(np.ones(len(d1_proj))*min(d1s)),np.log10(d2_proj),ci_proj_y,c='k',linewidth=2)
ax.plot3D(np.log10(d1_proj),np.log10(np.ones(len(d2_proj))*min(d2s)),ci_proj_x,c='k',linewidth=2)

ax.scatter(np.log10(np.ones(len(d1_proj))*min(d1s)),np.log10(d2_proj),dip_proj_y,c='grey',linewidth=2)
ax.scatter(np.log10(d1_proj),np.log10(np.ones(len(d2_proj))*min(d2s)),dip_proj_x,c='grey',linewidth=2)

ax.set_xticks([-3,0,3])
ax.set_yticks([-3,0,3])
ax.set_zticks([0,.4,1.])
ax.set_xlabel('log(d1)',labelpad=-5)
ax.set_ylabel('log(d2)',labelpad=-5)
ax.set_zlabel('% effect',labelpad=-5)
ax.tick_params(axis='both',pad=-3)
plt.tight_layout()
plt.savefig('ci_hill_bias_1.pdf')

# =============================================================================
# Hill bias h>1
# =============================================================================
#Parameters
h1=2.
h2=2.

d1 = []
d2 = []
dip = []
for r in range(rep):
    DD1,DD2 = np.meshgrid(np.concatenate(([0],np.logspace(-3,3,N))),np.concatenate(([0],np.logspace(-3,3,N))))
    d1_tmp = DD1.reshape((-1,))
    d2_tmp = DD2.reshape((-1,))
    dip_tmp = Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
    dip_tmp = np.random.randn(len(dip_tmp))*noise+dip_tmp
    d1 = np.concatenate((d1,d1_tmp))
    d2 = np.concatenate((d2,d2_tmp))
    dip = np.concatenate((dip,dip_tmp))
d1s = d1.copy();d2s = d2.copy()
d1s[d1s==0]=min(d1s[d1s!=0])/10.
d2s[d2s==0]=min(d2s[d2s!=0])/10.
CI,d1_full,d2_full,Dm1,Dm2,m1,m2 = combination_index(d1,d2,dip)
ci_col = np.zeros(len(dip))*np.nan
for e in range(len(d1)):
    if d1[e] in d1_full and d2[e] in d2_full:
        ci_col[e]=-np.log10(CI[(d1_full==d1[e])&(d2_full==d2[e])])
 
cmap = plt.set_cmap('seismic')
cmap = cm.get_cmap()
cmap = cmap.set_bad(color='grey')

plt.figure(figsize = (3,3))
ax = plt.subplot(111,projection='3d')
c = ax.scatter(np.log10(d1s),np.log10(d2s),dip,c=ci_col,cmap=cmap,vmin=-.25,vmax=.25)
ax.view_init(azim=30,elev=20)
plt.show()
ax.set_zlim((0,1))
cbar = plt.colorbar(c,label='Combination Index',fraction=.03,pad=-.1)
cbar.set_ticks([-.25,0,.25])

d1_proj = np.unique(d1s);d2_proj = np.unique(d2s)
ci_proj_fun = lambda d,Dm,m: 1/(1+(d/Dm)**m)
dip_proj_fun = lambda d,h,C,E0,Emx: Emx+(E0-Emx)/(1+(d/C)**h)
dip_proj_x = dip_proj_fun(d1_proj,h1,C1,E0,E1)
dip_proj_y = dip_proj_fun(d2_proj,h2,C2,E0,E2)

ci_proj_x = ci_proj_fun(d1_proj,Dm1,m1)
ci_proj_y = ci_proj_fun(d2_proj,Dm2,m2)
ax.plot3D(np.log10(np.ones(len(d1_proj))*min(d1s)),np.log10(d2_proj),ci_proj_y,c='k',linewidth=2)
ax.plot3D(np.log10(d1_proj),np.log10(np.ones(len(d2_proj))*min(d2s)),ci_proj_x,c='k',linewidth=2)

ax.scatter(np.log10(np.ones(len(d1_proj))*min(d1s)),np.log10(d2_proj),dip_proj_y,c='grey',linewidth=2)
ax.scatter(np.log10(d1_proj),np.log10(np.ones(len(d2_proj))*min(d2s)),dip_proj_x,c='grey',linewidth=2)

ax.set_xticks([-3,0,3])
ax.set_yticks([-3,0,3])
ax.set_zticks([0,.4,1.])
ax.set_xlabel('log(d1)',labelpad=-5)
ax.set_ylabel('log(d2)',labelpad=-5)
ax.set_zlabel('% effect',labelpad=-5)
ax.tick_params(axis='both',pad=-3)
plt.tight_layout()
plt.savefig('ci_hill_bias_2.pdf')



# =============================================================================
# Isobol figures
# =============================================================================
fig = plt.figure(figsize=(1.5,4))
ax = plt.subplot(311)
np.random.seed(123)
#Parameters
N = 25
h1=1.
h2=1.

DD1,DD2 = np.meshgrid(np.concatenate(([0],np.linspace(10**-3,10**0,N))),np.concatenate(([0],np.linspace(10**-3,10**0,N))))
d1_tmp = DD1.reshape((-1,))
d2_tmp = DD2.reshape((-1,))
dip_tmp = Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
dip = dip_tmp.reshape(DD1.shape)

c = ax.pcolor(DD1,DD2,dip,cmap=cm.PRGn,vmin=0.,vmax=1.)
c = ax.contour(DD1,DD2,dip,levels=[.4,.5,.6,.7],colors=['k','k','k','k'])
manual=[]
for l in c.collections:
    x,y = l.get_paths()[0].vertices.T
    f = interp1d(x-y,x)
    manual.append((f(0),f(0)))
ax.clabel(c, fmt='%0.1f', colors='k', fontsize=8,manual=manual)
ax.set_xticks([])
ax.set_yticks([])
#ax.set_title(r'$h_1=h_2=1$'+'\n'+r'$\alpha_{12}=\alpha_{21}=0$')


ax2 = plt.subplot(312)
h1=.5
h2=.5
dip_tmp = Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
dip = dip_tmp.reshape(DD1.shape)
c = ax2.pcolor(DD1,DD2,dip,cmap=cm.PRGn,vmin=0.,vmax=1.)
c = ax2.contour(DD1,DD2,dip,levels=[.5])
x, y = c.collections[0].get_paths()[0].vertices.T

ax2.fill_between(x,y,1-x,alpha=.25,color='r')
ax2.plot(x,1-x,color='r',linestyle='--')

ax2.clabel(c, fmt='%0.1f', colors='k', fontsize=8,manual=[(.25,.25)])
ax2.set_xticks([])
ax2.set_yticks([])
#ax2.set_title(r'$h_1=h_2=0.3$'+'\n'+r'$\alpha_{12}=\alpha_{21}=0$')



ax3 = plt.subplot(313)
h1=2
h2=2
dip_tmp = Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
dip = dip_tmp.reshape(DD1.shape)
cs = ax3.pcolor(DD1,DD2,dip,cmap=cm.PRGn,vmin=0.,vmax=1.)
c = ax3.contour(DD1,DD2,dip,levels=[.5])
x, y = c.collections[0].get_paths()[0].vertices.T

ax3.fill_between(x,y,1-x,alpha=.25,color='r')
ax3.plot(x,1-x,color='r',linestyle='--')
ax3.clabel(c, fmt='%0.1f', colors='k', fontsize=8,manual=[(.75,.75)])
ax3.set_xticks([])
ax3.set_yticks([])
ax3.set_xlabel('[d2]')
ax3.set_ylabel('[d1]')

#ax3.set_title(r'$h_1=h_2=2.0$'+'\n'+r'$\alpha_{12}=\alpha_{21}=0$')
#cb = plt.colorbar(cs,label='Drug Effect',ticks=[0,1])
#cb.outline.set_visible(False)
plt.subplots_adjust(wspace=0,hspace=0)
plt.savefig('isobol_figure.pdf')

# =============================================================================
# Find experimental examples of the biases above
# =============================================================================

# =============================================================================
# Find example of efficacy bias
# =============================================================================
#Begin with Cancer combination analysis
#Run from the MuSyC_Theory/Figures/Figure_Conflate_3/ folder
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)
T['geo_h'] = np.sqrt(T['h1']*T['h2'])

for k in T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
    
sT = T[((T[['log_alpha1','log_alpha2']]<-1).all(axis=1))&((T[['E2_ci_lw','E1_ci_lw']]>0.3).all(axis=1))&((T[['E2_ci_up','E1_ci_up']]<0.8).all(axis=1))]

i = 6186 #5-fu_mk-4827_MDAMB436
for i in [6186]:
    sub_T = sT.loc[i]
    expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1'] = data['drug1'].str.lower()
    data['drug2'] = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    sub_T['save_direc'] = os.getcwd()
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
    dip_sd[:] = 0
    
    fig = plt.figure(figsize=(2.2,2.4))
    ax = plt.subplot(111,projection='3d')
    try:
        CI,d1_full,d2_full,Dm1,Dm2,m1,m2 = combination_index(d1,d2,dip)
    except ValueError:
        continue
    ci_dip = []
    for i,j in zip(d1_full,d2_full):
        ci_dip.append(dip[(d1==i)&(d2==j)])
    cmap = plt.set_cmap('seismic')
    cmap = cm.get_cmap()
    cmap = cmap.set_bad(color='grey')
    c = ax.scatter(np.log10(d1_full),np.log10(d2_full),np.array(ci_dip),c=-np.log10(CI.reshape(-1,)),vmin=-1,vmax=1,edgecolor='k')
    ax.view_init(azim=30,elev=20)
    d1_proj = np.unique(d1);d2_proj = np.unique(d2)
    dip_z = dip[((d1==0)&(d2!=0))|((d1!=0)&(d2==0))|((d1==0)&(d2==0))]
    d1s = d1[((d1==0)&(d2!=0))|((d1!=0)&(d2==0))|((d1==0)&(d2==0))]
    d2s = d2[((d1==0)&(d2!=0))|((d1!=0)&(d2==0))|((d1==0)&(d2==0))]
    d1s[d1s==0] = min(d1s[d1s!=0])/10.
    d2s[d2s==0] = min(d2s[d2s!=0])/10.
    ax.scatter(np.log10(d1s),np.log10(d2s),dip_z,c='grey')
    ci_proj_fun = lambda d,Dm,m: 1/(1+(d/Dm)**m)
    ci_proj_x = ci_proj_fun(d1_proj,Dm1,m1)
    ci_proj_y = ci_proj_fun(d2_proj,Dm2,m2)
    ax.plot3D(np.log10(np.ones(len(d1_proj))*min(d1s)),np.log10(d2_proj),ci_proj_y,c='k',linewidth=2)
    ax.plot3D(np.log10(d1_proj),np.log10(np.ones(len(d2_proj))*min(d2s)),ci_proj_x,c='k',linewidth=2)
    ax.set_zlim((0,1.1))
    ax.view_init(elev=20,azim=19)
    #format axis
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.xaxis.set_tick_params(pad=-4)
    ax.yaxis.set_tick_params(pad=-4)
    ax.zaxis.set_tick_params(pad=-2)

    cbar = plt.colorbar(c,label='-log(CI)',fraction=.03,pad=-.1)
    cbar.set_ticks([-1.,0,1.])
    ax.set_zticks([0,1.1])
    ax.set_zlabel('% Effect')
    ax.set_ylabel('log('+drug2_name+')')
    ax.set_xlabel('log('+drug1_name+')')
    ax.set_title(sample)
    plt.savefig(drug1_name+'_'+drug2_name+'_'+sample+'combination_index.pdf')



    popt3 = [sub_T['E0'],sub_T['E2'],sub_T['E1'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C2'],10**sub_T['log_C1'],10**sub_T['log_h2'],10**sub_T['log_h1'],10**sub_T['log_alpha2'],10**sub_T['log_alpha1']]
    sub_T['E1'],sub_T['E2']=sub_T['E2'],sub_T['E1']
    sub_T['h1'],sub_T['h2']=sub_T['h2'],sub_T['h1']
    sub_T['log_C1'],sub_T['log_C2']=sub_T['log_C2'],sub_T['log_C1']
    sub_T['log_alpha1'],sub_T['log_alpha2']=sub_T['log_alpha2'],sub_T['log_alpha1']
    sub_T['min_conc_d1'],sub_T['min_conc_d2']=sub_T['min_conc_d2'],sub_T['min_conc_d1']
    sub_T['max_conc_d1'],sub_T['max_conc_d2']=sub_T['max_conc_d2'],sub_T['max_conc_d1']
    d1,d2=d2,d1
    sub_T['drug1_name'],sub_T['drug2_name']=sub_T['drug2_name'],sub_T['drug1_name']
    DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
    title = 'Loewe @ EC50:%.2f'%sub_T['loewe_fit_ec50'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
    matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.0,1.1), zero_conc=1,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))




# =============================================================================
# Find example of hill bias <1
# =============================================================================
#Begin with Cancer combination analysis
#Run from the MuSyC_Theory/Figures/Figure_Conflate_3/ folder
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)
T['geo_h'] = np.sqrt(T['h1']*T['h2'])

for k in T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
    
sT = T[((T[['log_alpha1','log_alpha2']]<-2).all(axis=1))&((T[['h1','h2']]<0.5).all(axis=1))]

i = 16329 #lapatinib_mk-8776_ZR751
for i in sT.index:
    sub_T = sT.loc[i]
    expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1'] = data['drug1'].str.lower()
    data['drug2'] = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    sub_T['save_direc'] = os.getcwd()
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
    dip_sd[:] = 0
    
    fig = plt.figure(figsize=(2.2,2.4))
    ax = plt.subplot(111,projection='3d')
    try:
        CI,d1_full,d2_full,Dm1,Dm2,m1,m2 = combination_index(d1,d2,dip)
    except ValueError:
        continue
    ci_dip = []
    for i,j in zip(d1_full,d2_full):
        ci_dip.append(dip[(d1==i)&(d2==j)])
    cmap = plt.set_cmap('seismic')
    cmap = cm.get_cmap()
    cmap = cmap.set_bad(color='yellow')
    c = ax.scatter(np.log10(d1_full),np.log10(d2_full),np.array(ci_dip),c=-np.log10(CI.reshape(-1,)),vmin=-1,vmax=1,edgecolor='k')
    ax.view_init(azim=30,elev=20)
    d1_proj = np.unique(d1);d2_proj = np.unique(d2)
    dip_z = dip[((d1==0)&(d2!=0))|((d1!=0)&(d2==0))|((d1==0)&(d2==0))]
    d1s = d1[((d1==0)&(d2!=0))|((d1!=0)&(d2==0))|((d1==0)&(d2==0))]
    d2s = d2[((d1==0)&(d2!=0))|((d1!=0)&(d2==0))|((d1==0)&(d2==0))]
    d1s[d1s==0] = min(d1s[d1s!=0])/10.
    d2s[d2s==0] = min(d2s[d2s!=0])/10.
    ax.scatter(np.log10(d1s),np.log10(d2s),dip_z,c='grey')
    ci_proj_fun = lambda d,Dm,m: 1/(1+(d/Dm)**m)
    ci_proj_x = ci_proj_fun(d1_proj,Dm1,m1)
    ci_proj_y = ci_proj_fun(d2_proj,Dm2,m2)
    ax.plot3D(np.log10(np.ones(len(d1_proj))*min(d1s)),np.log10(d2_proj),ci_proj_y,c='k',linewidth=2)
    ax.plot3D(np.log10(d1_proj),np.log10(np.ones(len(d2_proj))*min(d2s)),ci_proj_x,c='k',linewidth=2)
    ax.set_zlim((0,1.1))
    ax.view_init(elev=20,azim=19)
    #format axis
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.xaxis.set_tick_params(pad=-4)
    ax.yaxis.set_tick_params(pad=-4)
    ax.zaxis.set_tick_params(pad=-2)


    cbar = plt.colorbar(c,label='-log(CI)',fraction=.03,pad=-.1)
    cbar.set_ticks([-1.,0,1.])
    ax.set_zticks([0,1.1])
    ax.set_zlabel('% Effect')
    ax.set_ylabel('log('+drug2_name+')')
    ax.set_xlabel('log('+drug1_name+')')
    ax.set_title(sample)

    plt.savefig(drug1_name+'_'+drug2_name+'_'+sample+'combination_index.pdf')



    popt3 = [sub_T['E0'],sub_T['E2'],sub_T['E1'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C2'],10**sub_T['log_C1'],10**sub_T['log_h2'],10**sub_T['log_h1'],10**sub_T['log_alpha2'],10**sub_T['log_alpha1']]
    sub_T['E1'],sub_T['E2']=sub_T['E2'],sub_T['E1']
    sub_T['h1'],sub_T['h2']=sub_T['h2'],sub_T['h1']
    sub_T['log_C1'],sub_T['log_C2']=sub_T['log_C2'],sub_T['log_C1']
    sub_T['log_alpha1'],sub_T['log_alpha2']=sub_T['log_alpha2'],sub_T['log_alpha1']
    sub_T['min_conc_d1'],sub_T['min_conc_d2']=sub_T['min_conc_d2'],sub_T['min_conc_d1']
    sub_T['max_conc_d1'],sub_T['max_conc_d2']=sub_T['max_conc_d2'],sub_T['max_conc_d1']
    d1,d2=d2,d1
    sub_T['drug1_name'],sub_T['drug2_name']=sub_T['drug2_name'],sub_T['drug1_name']
    DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
    title = 'Loewe @ EC50:%.2f'%sub_T['loewe_fit_ec50'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
    matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.0,1.1), zero_conc=1,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))




# =============================================================================
# Show hill bias h>2
# =============================================================================
#Begin with Cancer combination analysis
#Run from the MuSyC_Theory/Figures/Figure_Conflate_3/ folder
i = 'mott_antimalaria'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/mott_antimalaria'
T['model_level']=1
T.reset_index(drop=True,inplace=True)
T['geo_h'] = np.sqrt(T['h1']*T['h2'])

for k in T.columns:
    if k.endswith('_ci') and k!='asym_ci':
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
        
     
sT = T[((T[['log_alpha1','log_alpha2']]<-2).all(axis=1))&((T[['h1','h2']]>2).all(axis=1))]

i = 381 #amodiaquine_artenimol_PLASMODIUM_FALCIPARUM_HB3
for i in [381]:
    sub_T = sT.loc[i]
    expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1'] = data['drug1'].str.lower()
    data['drug2'] = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    sub_T['save_direc'] = os.getcwd()
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
    dip_sd[:] = 0
    
    fig = plt.figure(figsize=(2.2,2.4))
    ax = plt.subplot(111,projection='3d')
    CI,d1_full,d2_full,Dm1,Dm2,m1,m2 = combination_index(d1,d2,dip/100.)
    ci_dip = []
    for i,j in zip(d1_full,d2_full):
        ci_dip.append(dip[(d1==i)&(d2==j)])
    cmap = plt.set_cmap('seismic')
    cmap = cm.get_cmap()
    cmap = cmap.set_bad(color='grey')
    c = ax.scatter(np.log10(d1_full),np.log10(d2_full),np.array(ci_dip),c=-np.log10(CI.reshape(-1,)),vmin=-1,vmax=1,edgecolor='k')
    ax.view_init(azim=30,elev=20)
    d1_proj = np.unique(d1);d2_proj = np.unique(d2)
    dip_z = dip[((d1==0)&(d2!=0))|((d1!=0)&(d2==0))|((d1==0)&(d2==0))]
    d1s = d1[((d1==0)&(d2!=0))|((d1!=0)&(d2==0))|((d1==0)&(d2==0))]
    d2s = d2[((d1==0)&(d2!=0))|((d1!=0)&(d2==0))|((d1==0)&(d2==0))]
    d1s[d1s==0] = min(d1s[d1s!=0])/10.
    d2s[d2s==0] = min(d2s[d2s!=0])/10.
    ax.scatter(np.log10(d1s),np.log10(d2s),dip_z,c='grey')
    ci_proj_fun = lambda d,Dm,m: 100./(1+(d/Dm)**m)
    ci_proj_x = ci_proj_fun(d1_proj,Dm1,m1)
    ci_proj_y = ci_proj_fun(d2_proj,Dm2,m2)
    ax.plot3D(np.log10(np.ones(len(d2_proj))*min(d1s)),np.log10(d2_proj),ci_proj_y,c='k',linewidth=2)
    ax.plot3D(np.log10(d1_proj),np.log10(np.ones(len(d1_proj))*min(d2s)),ci_proj_x,c='k',linewidth=2)
    ax.set_zlim((0,110))
    ax.view_init(elev=20,azim=19)
    #format axis
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.xaxis.set_tick_params(pad=-4)
    ax.yaxis.set_tick_params(pad=-4)
    ax.zaxis.set_tick_params(pad=-2)


    cbar = plt.colorbar(c,label='-log(CI)',fraction=.03,pad=-.1)
    cbar.set_ticks([-1.,0,1.])
    ax.set_zticks([0,1.1])
    ax.set_zlabel('% Effect')
    ax.set_ylabel('log('+drug2_name+')')
    ax.set_xlabel('log('+drug1_name+')')
    ax.set_title(sample)
    
    plt.savefig(drug1_name+'_'+drug2_name+'_'+sample+'combination_index.pdf')
    
    popt3 = [sub_T['E0'],sub_T['E2'],sub_T['E1'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C2'],10**sub_T['log_C1'],10**sub_T['log_h2'],10**sub_T['log_h1'],10**sub_T['log_alpha2'],10**sub_T['log_alpha1']]
    sub_T['E1'],sub_T['E2']=sub_T['E2'],sub_T['E1']
    sub_T['h1'],sub_T['h2']=sub_T['h2'],sub_T['h1']
    sub_T['log_C1'],sub_T['log_C2']=sub_T['log_C2'],sub_T['log_C1']
    sub_T['log_alpha1'],sub_T['log_alpha2']=sub_T['log_alpha2'],sub_T['log_alpha1']
    sub_T['min_conc_d1'],sub_T['min_conc_d2']=sub_T['min_conc_d2'],sub_T['min_conc_d1']
    sub_T['max_conc_d1'],sub_T['max_conc_d2']=sub_T['max_conc_d2'],sub_T['max_conc_d1']
    d1,d2=d2,d1
    sub_T['drug1_name'],sub_T['drug2_name']=sub_T['drug2_name'],sub_T['drug1_name']
    DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
    title = 'Loewe @ EC50:%.2f'%sub_T['loewe_fit_ec50'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
    matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.0,110), zero_conc=1,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))







