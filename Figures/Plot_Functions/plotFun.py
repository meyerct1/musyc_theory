#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 12:52:30 2019

@author: xnmeyer
"""

import numpy as np
import pandas as pd
#Plotting functions for the conflation figure:
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
from matplotlib.collections import PolyCollection
from matplotlib.ticker import FormatStrFormatter
from matplotlib.colors import colorConverter
#from matplotlib.patches import FancyArrowPatch
from matplotlib.colors import ListedColormap
import matplotlib.cm as cm
from matplotlib import rc
import scipy.stats as st
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from mpl_toolkits.mplot3d import Axes3D

def moving_window_percentiles(df, xaxis, yaxis, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True):
    """
    Calculates moving window percentiles.
    The first window begins at df[xaxis].min()-window_size/2
    The final window ends at df[xaxis].max()+window_size/2
    The window step_size is calculated so that the result will have n_steps many values
    x values for each window are the midpoint of xmin,xmax of that window
    """
    dff = pd.DataFrame(df)[xaxis]
    if logx: dff = np.log(dff)
    XMIN, XMAX = dff.min()-window_size/2., dff.max()+window_size/2.
    step_size = (XMAX - XMIN - window_size) / (1.*(n_steps-1))
    w_df = pd.DataFrame(index = range(n_steps), columns=percentiles + ['xmin','xmax','xmid','n_rows'])
    
    for i in w_df.index:
        xmin = XMIN + step_size*i
        xmax = xmin + window_size
        xmid = xmin + window_size/2.
        if logx: w_df.loc[i,['xmin','xmax','xmid']] = np.exp(xmin), np.exp(xmax), np.exp(xmid)
        else: w_df.loc[i,['xmin','xmax','xmid']] = xmin, xmax, xmid
        sub_df = df.loc[(dff >= xmin) & (dff <= xmax)]
        w_df.loc[i,"n_rows"] = sub_df.shape[0]
        w_df.loc[i, percentiles] = sub_df[yaxis].quantile(percentiles)

    return w_df

def surfGen(ax,yy,tt,zz,zlim):
    zmin = zlim[0]
    zmax = zlim[1]
    # Plot the surface for real, with appropriate alpha
    alpha = 1
    surf = ax.plot_surface(np.log10(tt), np.log10(yy), zz, cstride=1, rstride=1, alpha=alpha, cmap = cm.PRGn, vmin=zmin, vmax=zmax, linewidth=0)
    # colored curves on left and right
    lw = 5
    ax.plot(np.log10(tt.min()*np.ones(len(tt))),np.log10(yy[0,:]),zz[0,:],linewidth=lw)
#    ax.plot(np.log10(tt.max()*np.ones(d1.shape)),np.log10(yy[0,:]),zz[-1,:],linewidth=lw)
    ax.plot(np.log10(tt[:,0]),np.log10(yy.min()*np.ones(len(yy))),zz[:,0],linewidth=lw)
#    ax.plot(np.log10(tt[:,0]),np.log10(yy.max()*np.ones(len(yy))),zz[:,-1],linewidth=lw)
    for i in range(len(tt)):
        if i%5==0:
            ax.plot(np.log10(tt[i,0]*np.ones(len(yy))),np.log10(yy[0,:]),zz[i,:],'-k',linewidth=1,alpha=.1)
    for i in range(len(yy)):
        if i%5==0:        
            ax.plot(np.log10(tt[:,0]),np.log10(yy[0,i]*np.ones(len(tt))),zz[:,i],'-k',linewidth=1,alpha=.1)
    # Set the view
    ax.view_init(elev=19, azim=27)
    ax.set_ylabel("log(d1)",labelpad=-17)
    ax.set_xlabel("log(d2)",labelpad=-17)
    ax.set_zlabel("Effect")
    return ax

def plot_quantile_bias(window_df, xaxis, ax, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True, alpha_lim=(0.2,0.8), color='gold', bias=None, xlim=None, ylim=None, plot_midline=True):
    """
    Plot the bias of df[yaxis] as a function of df['xmid']
    Assumes window_df has been returned as window_df=moving_window_percentiles(df, ...)
    """
    
    x = np.float64(window_df['xmid'])
    
    n_color_shades = len(percentiles)/2
    
    for i in range(n_color_shades):
        alpha = alpha_lim[0] + (alpha_lim[1] - alpha_lim[0])*i/(n_color_shades-1.)
        y1 = np.float64(window_df[percentiles[i+1]])
        y2 = np.float64(window_df[percentiles[i]])
        ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
        
        if i<n_color_shades-1 or len(percentiles)%2==1:
            y1 = np.float64(window_df[percentiles[-i-1]])
            y2 = np.float64(window_df[percentiles[-i-2]])
            ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
    if bias is not None: ax.plot(bias[0], bias[1],'b-',lw=2,zorder=1000,label='Predicted Bias')
    ax.plot([x.min(), x.max()], [0,0],'k--')
    if plot_midline: ax.plot(x, window_df[percentiles[n_color_shades]], 'r-',label='Median') # Draw the median, (assuming 0.5 is the middle percentile)
    
    if xlim is not None: ax.set_xlim(xlim)
    if ylim is not None: ax.set_ylim(ylim)
    if logx: ax.set_xscale('log')
    
    
    

#fname=None; zlim=(-.1,1.1); zero_conc=1;title=None
def matplotlibArbitrarySurface(tt,yy,zz,d1,d2,dip_rate,dip_sd,drug1_name,drug2_name,fname=None,metric_name='Percent',zlim=None,zero_conc=1,title=None,d1lim=None,d2lim=None,fmt='pdf',plt_zeropln=True,plt_data=True,figsize_surf=(3.5,3)):
    N=15;elev=20;azim=19;alpha = 0.9;
    if d1lim is not None:
        d1_min=d1lim[0];d1_max=d1lim[1]
    if d2lim is not None:
        d2_min=d2lim[0];d2_max=d2lim[1]    
        
    fig1 = plt.figure(figsize=figsize_surf,facecolor='w')
    ax = fig1.gca(projection='3d')
    #ax.set_axis_off()
    d2_min=np.min(tt)+zero_conc;d1_min=np.min(yy)+zero_conc
    d2_max=np.max(tt)+zero_conc;d1_max=np.max(yy)+zero_conc
    y = yy[0,:]
    t = tt[:,0]
    if zlim is None:
        zmin = np.min(zz)
        zmax = np.max(zz)
        if np.abs(zmin) > np.abs(zmax): zmax = np.abs(zmin)
        else: zmin = -np.abs(zmax)
    else:
        zmin = zlim[0]
        zmax = zlim[1]
        
    # Plot it once on a throwaway axis, to get cbar without alpha problems
    my_cmap_rgb = plt.get_cmap('PRGn')(np.arange(256))

    for i in range(3): # Do not include the last column!
        my_cmap_rgb[:,i] = (1 - alpha) + alpha*my_cmap_rgb[:,i]
    my_cmap = ListedColormap(my_cmap_rgb, name='my_cmap')
    surf = ax.plot_surface(yy, tt, zz, cstride=1, rstride=1, cmap = my_cmap, vmin=zmin, vmax=zmax, linewidth=0)
#    cbar_ax = fig.add_axes([0.1, 0.9, 0.6, 0.05])
    cbar = fig1.colorbar(surf,ticks=[zmin, 0, zmax],pad=-.08,fraction=.025)
    cbar.ax.set_yticklabels(['%.2f'%zmin, 0, '%.2f'%zmax])
    cbar.solids.set_rasterized(True)
    cbar.solids.set_edgecolor('face')
    cbar.outline.set_visible(False)
    
    plt.cla()
    ax.set_zlim(zmin,zmax)
    # Plot the surface for real, with appropriate alpha
    surf = ax.plot_surface(tt, yy, zz, cstride=1, rstride=1, alpha=alpha, cmap = cm.PRGn, vmin=zmin, vmax=zmax, linewidth=0)
    
    lw=5 
    ax.plot(d2_min*np.ones(y.shape)-zero_conc, y, zz[0,:], linewidth=lw,color='b')
    ax.plot(d2_max*np.ones(y.shape), y, zz[-1,:], linewidth=lw,color='b',linestyle='--')

    ax.plot(t, d1_min*np.ones(y.shape)-zero_conc, zz[:,0], linewidth=lw,color='r')
    ax.plot(t, d1_max*np.ones(y.shape), zz[:,-1], linewidth=lw,color='r',linestyle='--')
        
    
    # Set the view
    ax.view_init(elev=elev, azim=azim)
    ax.set_ylabel("log(%s)[M]"%drug1_name,labelpad=-5)   
    ax.set_xlabel("log(%s)[M]"%drug2_name,labelpad=-5)
    ax.set_zlabel(metric_name,labelpad=-3)

    if plt_data:
        scat_d1 = d1.copy()
        scat_d2 = d2.copy()
        scat_dip = dip_rate.copy()
        scat_erb = dip_sd.copy()
        scat_d1 = np.log10(scat_d1)
        scat_d2 = np.log10(scat_d2)
        if d1lim is not None:
            scat_d1[scat_d1==-np.inf] = d1_min
        else:
            scat_d1[scat_d1==-np.inf] = scat_d1[scat_d1!=-np.inf].min()-zero_conc
        if d2lim is not None:
            scat_d2[scat_d2==-np.inf] = d2_min
        else:
            scat_d2[scat_d2==-np.inf] = scat_d2[scat_d2!=-np.inf].min()-zero_conc
        
        #Remove values which are outside bounds
        mask = ~np.isnan(scat_d1)
        if d1lim is not None:
            mask=(mask) & ((scat_d1>=d1_min) & (scat_d1<=d1_max))
        if d2lim is not None:
            mask=(mask) & ((scat_d2>=d2_min) & (scat_d2<=d2_max))
        scat_d1=scat_d1[mask];scat_d2=scat_d2[mask];
        scat_erb=scat_erb[mask];scat_dip=scat_dip[mask];
        
        ax.scatter(scat_d2, scat_d1, scat_dip, s=10,c='k', depthshade=False)
        # Plot error bars
        for _d1, _d2, _dip, _erb in zip(scat_d1, scat_d2, scat_dip, scat_erb):
            ax.plot([_d2,_d2], [_d1,_d1], [_dip-_erb, _dip+_erb], 'k-', alpha=0.3,linewidth=1)

    #format axis
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))

 
    ax.set_zticks([zmin,0,zmax])
    ax.xaxis.set_tick_params(pad=-4)
    ax.yaxis.set_tick_params(pad=-4)
    ax.zaxis.set_tick_params(pad=-2)


    # Plane of DIP=0
    if plt_zeropln:
        c_plane = colorConverter.to_rgba('k', alpha=0.3)
        verts = [np.array([(d2_min-zero_conc,d1_min-zero_conc), (d2_min-zero_conc,d1_max), (d2_max,d1_max), (d2_max,d1_min-zero_conc), (d2_min-zero_conc,d1_min-zero_conc)])]
        poly = PolyCollection(verts, facecolors=c_plane)
        #ax.add_collection3d(poly, zs=[max(e1,e2)], zdir='z')
        ax.add_collection3d(poly, zs=[0], zdir='z')

        # Plot intersection of surface with DIP=0 plane
        #plt.contour(tt,yy,zz,levels=[max(e1,e2)], linewidths=3, colors='k')
        plt.contour(tt,yy,zz,levels=[0], linewidths=3, colors='k')

    # If needed, manually set zlim
    if zlim is not None: ax.set_zlim(zlim)
    plt.tight_layout()

    if title is not None:      
        plt.title(title,fontsize=8)

    # Save plot, or show it
    if fname is None: plt.show()
    else: plt.savefig(fname+'.'+fmt,pad_inches=0.,format=fmt);plt.close()
    return fig1,ax
    ############
    