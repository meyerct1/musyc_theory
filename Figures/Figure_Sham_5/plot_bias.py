#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
#Code to plot resulting hill bias
"""
# =============================================================================
# Import packages
# =============================================================================
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rc
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from pythontools.synergy import synergy_tools
import numpy as np
# =============================================================================
# Functions to plot bias
# =============================================================================
def get_loewe_bias(h1, h2, d1, d2, C1, C2, E0, E1, E2, E3=None, alpha=0.):
    """
    Calculates Loewe for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    l = -np.log10(synergy_tools.loewe(d1, d2, E, E0, E1, E2, h1, h2, C1, C2))
    return np.nanmedian(l)

def get_schindler_bias(h1, h2, d1, d2, C1, C2, E0, E1, E2, E3=None, alpha=0.):
    """
    Calculates schindler for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    l = synergy_tools.schindler(d1, d2, E, E0, E1, E2, h1, h2, C1, C2)
    return np.nanmedian(l)


def moving_window_percentiles(df, xaxis, yaxis, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True):
    """
    Calculates moving window percentiles.
    The first window begins at df[xaxis].min()-window_size/2
    The final window ends at df[xaxis].max()+window_size/2
    The window step_size is calculated so that the result will have n_steps many values
    x values for each window are the midpoint of xmin,xmax of that window
    """
    dff = pd.DataFrame(df)[xaxis]
    if logx: dff = np.log(dff)
    XMIN, XMAX = dff.min()-window_size/2., dff.max()+window_size/2.
    step_size = (XMAX - XMIN - window_size) / (1.*(n_steps-1))
    w_df = pd.DataFrame(index = range(n_steps), columns=percentiles + ['xmin','xmax','xmid','n_rows'])
    
    for i in w_df.index:
        xmin = XMIN + step_size*i
        xmax = xmin + window_size
        xmid = xmin + window_size/2.
        if logx: w_df.loc[i,['xmin','xmax','xmid']] = np.exp(xmin), np.exp(xmax), np.exp(xmid)
        else: w_df.loc[i,['xmin','xmax','xmid']] = xmin, xmax, xmid
        sub_df = df.loc[(dff >= xmin) & (dff <= xmax)]
        w_df.loc[i,"n_rows"] = sub_df.shape[0]
        w_df.loc[i, percentiles] = sub_df[yaxis].quantile(percentiles)

    return w_df
        
def plot_quantile_bias(window_df, xaxis, ax, percentiles = [0.2, 0.4, 0.5, 0.6, 0.8], window_size=0.05, n_steps=50, logx=True, alpha_lim=(0.2,0.8), color='gold', bias=None, xlim=None, ylim=None, plot_midline=True):
    """
    Plot the bias of df[yaxis] as a function of df['xmid']
    Assumes window_df has been returned as window_df=moving_window_percentiles(df, ...)
    """
    
    x = np.float64(window_df['xmid'])
    
    n_color_shades = len(percentiles)/2
    
    for i in range(n_color_shades):
        alpha = alpha_lim[0] + (alpha_lim[1] - alpha_lim[0])*i/(n_color_shades-1.)
        y1 = np.float64(window_df[percentiles[i+1]])
        y2 = np.float64(window_df[percentiles[i]])
        ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
        
        if i<n_color_shades-1 or len(percentiles)%2==1:
            y1 = np.float64(window_df[percentiles[-i-1]])
            y2 = np.float64(window_df[percentiles[-i-2]])
            ax.fill_between(x, y1, y2=y2, alpha=alpha, color=color, lw=0)
    if bias is not None: ax.plot(bias[0], bias[1],'b-',lw=2,zorder=1000,label='Predicted Bias')
    ax.plot([x.min(), x.max()], [0,0],'k--')
    if plot_midline: ax.plot(x, window_df[percentiles[n_color_shades]], 'r-',label='Median') # Draw the median, (assuming 0.5 is the middle percentile)
    
    if xlim is not None: ax.set_xlim(xlim)
    if ylim is not None: ax.set_ylim(ylim)
    if logx: ax.set_xscale('log')
    
# =============================================================================
# Correct the bias
# =============================================================================
# Read the data (or load a version that was already calculated)
# Include only drugs with doses spanning ec50 (within the multiplicative factor of edge_buffer, e.g. dmin > ec50/buffer, and dmax < ec50*buffer
data = pd.read_csv("merck_synergy_full.csv")
data = data[data['converge_mc_nlls']==1.]
data = data[(data['C1']<data['max_conc_d1'])&(data['C2']<data['max_conc_d2'])]
data.reset_index(drop=True,inplace=True)

# Calculate an "average bias" as a function of h
# "average bias" is the median synergy, assuming a null MuSyC surface, across doses ranging from ec50/10 to ec50*10
xaxis = "hill_geom"
hmin = data[xaxis].min()
hmax = data[xaxis].max()
hmax = 33.
hrange = np.logspace(np.log10(hmin), np.log10(hmax), 50)
loewe_y = [];schindler_y = []

C1,C2 = 1e-4, 1e-4
d1min, d1max = C1/10., C1*10.
d2min, d2max = C2/10., C2*10.
d1 = np.logspace(np.log10(d1min), np.log10(d1max), 20)
d2 = np.logspace(np.log10(d2min), np.log10(d2max), 20)
DD1, DD2 = np.meshgrid(d1, d2)
E0, E1, E2, E3 = 1., 0., 0., 0.

for h in hrange:
    loewe_y.append(get_loewe_bias(h, h, DD1, DD2, C1, C2, E0, E1, E2)) # This is the average bias
    schindler_y.append(get_schindler_bias(h, h, DD1, DD2, C1, C2, E0, E1, E2)) # This is the average bias
    

avg_bias = [loewe_y,schindler_y]
for e1,method in enumerate(['loewe','schindler']):
    # Calculate the moving windows of the scattered data
    percentiles = [i/10. for i in range(1,10)]
    n_steps=200
    window_size=0.1 # This is in h-space
    
    window_df = moving_window_percentiles(data, xaxis, method, percentiles = percentiles, n_steps=n_steps, window_size=window_size)
    corrected_window_df = moving_window_percentiles(data, xaxis, method+"_corrected", percentiles = percentiles, n_steps=n_steps, window_size=window_size)
    x = np.float64(window_df['xmid'])
    
    xlim=(0.35,10)
    ylim=(-1,1.2)
    fig = plt.figure(figsize=(3.75,3),facecolor='w')
    ax = []
    ax.append(plt.subplot2grid((5,1),(0,0),rowspan=1))
    ax.append(plt.subplot2grid((5,1),(1,0),rowspan=2))
    ax.append(plt.subplot2grid((5,1),(3,0),rowspan=2))
    
    # Plot the number of points within each window
    ax[0].plot(x, window_df['n_rows'],lw=3,c='k')
    ax[0].set_xscale('log')
    ax[0].set_xticklabels([])
    ax[0].set_xlim(xlim)
    ax[0].set_yscale('log')
    ax[0].set_ylabel("N-points")
    ax[0].spines['top'].set_visible(False)
    ax[0].spines['right'].set_visible(False)
    ax[0].xaxis.set_ticks_position('bottom')
    ax[0].yaxis.set_ticks_position('left')
    ax[0].set_yticks([1,100,10000])
    
    # Plot the loewe bias
    ax[1].scatter(data[xaxis],data[method],alpha=0.05, s=.1,rasterized=True)
    plot_quantile_bias(window_df, xaxis, ax[1], color='darkorange', percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=(hrange,avg_bias[e1]), xlim=xlim, ylim=ylim)
    ax[1].set_xticklabels([])
    if e1==0:
        ax[1].set_ylabel("-log10(Loewe)")
        ax[1].set_ylim([1.,1.])
        ax[1].set_yticks([-.5,0,.5])
        ax[1].text(2,.9,method + ' biased by hill slope',ha='center',va='center')

    else:
        ax[1].set_ylabel("Schindler")
        ax[1].set_ylim([-.5,.5])
        ax[1].set_yticks([-.25,0,.25])
        ax[1].text(2,.35,method + ' biased by hill slope',ha='center',va='center')
        
    ax[1].spines['top'].set_visible(False)
    ax[1].spines['right'].set_visible(False)
    ax[1].xaxis.set_ticks_position('bottom')
    ax[1].yaxis.set_ticks_position('left')
    
    # Plot the corrected Loewe
    ax[2].scatter(data[xaxis],data[method+'_corrected'],alpha=0.05, s=.1,rasterized=True)
    plot_quantile_bias(corrected_window_df, xaxis, ax[2], color='darkorange', percentiles=percentiles, n_steps=n_steps, window_size=window_size, xlim=xlim, ylim=ylim)
    if e1==0:
        ax[2].set_ylabel("-log10(Loewe)")
        ax[2].set_ylim([1.,1.])
        ax[2].set_yticks([-.5,0,.5])
        ax[2].text(2,.9,method + ' corrected for hill bias',ha='center',va='center')
    else:
        ax[2].set_ylabel("Schindler")
        ax[2].set_ylim([-.5,.5])
        ax[2].set_yticks([-.25,0,.25])
        ax[2].text(2,.35,method + ' corrected for hill bias',ha='center',va='center')
        
    ax[2].set_xlabel(r"$\sqrt{h_1 h_2}$",labelpad=-10)
    ax[2].spines['top'].set_visible(False)
    ax[2].spines['right'].set_visible(False)
    ax[2].xaxis.set_ticks_position('bottom')
    ax[2].yaxis.set_ticks_position('left')
    
    #plt.tight_layout()
    plt.savefig(method+"_bias.pdf",bbox_inches = 'tight',pad_inches = 0)


# =============================================================================
# Find examples where the error matters
# =============================================================================
from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.calcOtherSynergyMetrics import loewe
from SynergyCalculator.NDHillFun import Edrug2D_NDB
import os

i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)
T['hill_geom'] = np.sqrt(T['h1']*T['h2'])

T['loewe_fit_ec50_fix'] = np.nan

for ind in T.index:
    print ind
    h1,h2,d1,d2,C1,C2,E0,E1,E2,E3,alpha1,alpha2 = T.loc[ind,'h1'],T.loc[ind,'h2'],T.loc[ind,'C1'],T.loc[ind,'C2'],T.loc[ind,'C1'],T.loc[ind,'C2'],T.loc[ind,'E0'],T.loc[ind,'E1'],T.loc[ind,'E2'],T.loc[ind,'E3'],10**T.loc[ind,'log_alpha1'],10**T.loc[ind,'log_alpha2']
    E_ec50 = Edrug2D_NDB((C1,C2),E0,E1,E2,E3,100.,100.,C1,C2,h1,h2,alpha1,alpha2)
    T.loc[ind,'loewe_fit_ec50_fix'] = -np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2)) - get_loewe_bias(h1, h2, d1, d2, C1, C2, E0, E1, E2,E3=E3)

for k in T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
        
#Find examples
T[((T[['beta_obs','beta','log_alpha1','log_alpha2']]<0).all(axis=1)) & (T['loewe_fit_ec50_fix']<0.) & (T['loewe_fit_ec50']>0.) & (T['hill_geom']<1) ][['beta_obs','beta','log_alpha1_ci_up','log_alpha2_ci_up','loewe_fit_ec50','hill_geom']]

sub_T = T[(T['sample']=='MDAMB436')&(T['drug1_name']=='doxorubicin')&(T['drug2_name']=='mk-4827')].to_dict(orient='record')[0]
sub_T = T.loc[6273]
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
sub_T['save_direc'] = os.getcwd()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:] = 0
popt3 = [sub_T['E0'],sub_T['E2'],sub_T['E1'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C2'],10**sub_T['log_C1'],10**sub_T['log_h2'],10**sub_T['log_h1'],10**sub_T['log_alpha2'],10**sub_T['log_alpha1']]
sub_T['E1'],sub_T['E2']=sub_T['E2'],sub_T['E1']
sub_T['h1'],sub_T['h2']=sub_T['h2'],sub_T['h1']
sub_T['log_C1'],sub_T['log_C2']=sub_T['log_C2'],sub_T['log_C1']
sub_T['log_alpha1'],sub_T['log_alpha2']=sub_T['log_alpha2'],sub_T['log_alpha1']
sub_T['min_conc_d1'],sub_T['min_conc_d2']=sub_T['min_conc_d2'],sub_T['min_conc_d1']
sub_T['max_conc_d1'],sub_T['max_conc_d2']=sub_T['max_conc_d2'],sub_T['max_conc_d1']
d1,d2=d2,d1
sub_T['drug1_name'],sub_T['drug2_name']=sub_T['drug2_name'],sub_T['drug1_name']
DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
title =  'Loewe @ EC50:%.2f'%sub_T['loewe_fit_ec50']+'\n'+r'$\sqrt{h2*h2}=%.2f$'%sub_T['hill_geom']+'\n'+'Corrected Loewe:%.2f'%sub_T['loewe_fit_ec50_fix']+'\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.0,1.1), zero_conc=0,title=None,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
