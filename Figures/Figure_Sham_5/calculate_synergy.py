#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
#Code to calculate other synergy parameters for metrics which results in a hill bias
"""
#Code to calculate loewe, combination index, schindler to show hill bias
import numpy as np
import pandas as pd
from SynergyCalculator.calcOtherSynergyMetrics import loewe,schindler,combination_index
from SynergyCalculator.gatherData import subset_data
from pythontools.synergy import synergy_tools

# =============================================================================
# Function to calculate Loewe/schindler bias
# =============================================================================
def get_loewe_bias(h1, h2, d1, d2, ec50_1, ec50_2, E0, E1, E2, E3=None, alpha=0.):
    """
    Calculates Loewe for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    l = -np.log10(synergy_tools.loewe(d1, d2, E, E0, E1, E2, h1, h2, C1, C2))
    return np.nanmedian(l)

def get_schindler_bias(h1, h2, d1, d2, ec50_1, ec50_2, E0, E1, E2, E3=None, alpha=0.):
    """
    Calculates schindler for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    l = schindler(d1, d2, E, E0, E1, E2, h1, h2, ec50_1, ec50_2)
    return np.nanmedian(l)



def get_ci_bias(h1, h2, d1, d2, ec50_1, ec50_2, E0, E1, E2, E3=None, alpha=0.):
    """
    Calculates schindler for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    try:
        l,_,_,_,_,_,_ = combination_index(d1, d2, E)
    except ValueError:
        l = np.nan
    return np.nanmedian(l)



# =============================================================================
# Calculate loewe and schindler at each dose and also the corrected bias
# =============================================================================
fit_files = ['oneil_anticancer/MasterResults_mcnlls.csv','mott_antimalaria/MasterResults_mcnlls.csv']
data_files = ['oneil_anticancer/merck_perVia_10-29-2018.csv','mott_antimalaria/mott_compiled_01302020.csv']
to_write = ['merck_synergy_full.csv','mott_synergy_full.csv']
divisor = [1.,100.]

for e in [0]:
    
    fit_fil = fit_files[e]
    dat_fil = data_files[e]
    
    #File with MuSyC fits
    T = pd.read_csv("../../Data/" + fit_fil)
    #File with raw data
    df = pd.read_csv("../../Data/" + dat_fil)
#    # Filter out any weird datapoints, or points that aren't real combinations
#    df['schindler'] = np.nan
#    df['loewe'] = np.nan
#    df['loewe_bias'] =np.nan
#    df['loewe_corrected'] = np.nan
#    df['schindler_bias'] = np.nan
#    df['combination_index_bias'] = np.nan
#    df['schindler_corrected'] = np.nan    
#    
#    df['h1'] = np.nan
#    df['h2'] = np.nan
#    df['hill_geom'] = np.nan
#    df['E0'] = np.nan
#    df['E1'] = np.nan
#    df['E2'] = np.nan
#    df['C1'] = np.nan
#    df['C2'] = np.nan
#    df['max_conc_d1']=np.nan
#    df['max_conc_d2']=np.nan
#    df['converge_mc_nlls'] = np.nan
#    df['drugunique'] = np.nan
    
    df['drug1'] = [i.lower() for i in df['drug1']]
    df['drug2'] = [i.lower() for i in df['drug2']]
    df['sample'] = [i.upper() for i in df['sample']]
    
    df['effect'] = df['effect']/divisor[e]
    for k in ['E0','E1','E2','E3']:
        T[k] = T[k]/divisor[e]
    
    prog = 0
    count = 0
    n_rows = T.shape[0]
    
    df_master = pd.DataFrame()
    #For each drug
    for i in T.index:
        count += 100 # Print progress, counting up to 100
        if count/n_rows > prog:
            prog = count/n_rows
            print prog
    
        # Read drug parameters
        cline, drug1, drug2, h1, h2, E0, E1, E2, E3, C1, C2, mxd1, mxd2, cmn, un = T.loc[i, ['sample', 'drug1_name', 'drug2_name', 'h1', 'h2', 'E0', 'E1', 'E2','E3', 'log_C1', 'log_C2','max_conc_d1','max_conc_d2','converge_mc_nlls','drugunique']]
        C1 = np.power(10., C1)
        C2 = np.power(10., C2)
        
        # Subset the data to just combinations of those drugs
        if e==1:
            df_sub = df.loc[(df['drug1']==drug1) & (df['drug2']==drug2) & (df['sample']==cline)][['drug1','drug2','drug1.conc','drug2.conc','effect','sample']]
            df_sub.reset_index(drop=True,inplace=True)
        elif e==0:
            d1c, d2c, E, _ = subset_data(df,drug1,drug2,cline)
            df_sub = pd.DataFrame({'drug1.conc':d1c,'drug2.conc':d2c,'effect':E})
            df_sub['drug1']=drug1
            df_sub['drug2']=drug2
            df_sub['sample']=cline
            
        # Read the data
        d1c = df_sub['drug1.conc']
        d2c = df_sub['drug2.conc']
        E = df_sub['effect']
        
        # Calculate synergy and population dataframe
        sc = schindler(d1c, d2c, E, E0, E1, E2, h1, h2, C1, C2)
        df_sub["schindler"] = sc
        lw = -np.log10(loewe(d1c, d2c, E, E0, E1, E2, h1, h2, C1, C2))
        df_sub["loewe"] = lw
        try:
            ci,d1_ci,d2_ci,ci_C1,ci_C2,ci_h1,ci_h2 = combination_index(d1c,d2c,E)
            tmp = pd.DataFrame({'combination_index':ci.reshape(-1),'drug1.conc':d1_ci,'drug2.conc':d2_ci})
            tmp['sample'] = cline
            tmp['drug1'] = drug1
            tmp['drug2'] = drug2
            tmp['ci_h1'] = ci_h1
            tmp['ci_h2'] = ci_h2
            tmp['ci_C1'] = ci_C1
            tmp['ci_C2'] = ci_C2            
            df_sub = df_sub.merge(tmp,how='outer',on=['drug1','drug2','drug1.conc','drug2.conc','sample'],sort=False)
        except ValueError:
            for k in ['ci_h1','ci_h2','ci_C1','ci_C2','combination_index']:
                df_sub[k]=np.nan

        
        #Save information about the single fits based on MuSyC
        df_sub['h1'] = h1
        df_sub['h2'] = h2
        df_sub['C1'] = C1
        df_sub['C2'] = C2
        df_sub['E0'] = E0
        df_sub['E1'] = E1
        df_sub['E2'] = E2
        
        df_sub['max_conc_d1'] = mxd1
        df_sub['max_conc_d2'] = mxd2
        df_sub['converge_mc_nlls'] = cmn
        
        df_sub['drugunique'] = un
        
        df_sub['hill_geom'] = np.sqrt(h1*h2)
        
        loewe_bias = get_loewe_bias(h1, h2, d1c, d2c, C1, C2, E0, E1, E2, E3=E3)
        schindler_bias = get_schindler_bias(h1, h2, d1c, d2c, C1, C2, E0, E1, E2, E3=E3)
        combination_index_bias = get_ci_bias(h1, h2, d1c, d2c, C1, C2, E0, E1, E2, E3=E3)
        
        df_sub["loewe_bias"] =  loewe_bias
        df_sub["schindler_bias"] =  schindler_bias
        df_sub["combination_index_bias"] =  combination_index_bias
        
        df_sub["loewe_corrected"] =  lw - loewe_bias
        df_sub["schindler_corrected"] =  sc - schindler_bias    
        df_sub['combination_index_corrected'] = df_sub['combination_index']-df_sub['combination_index_bias']
        if i == 0:
            df_sub.to_csv(to_write[e],index=False)
        else:
            df_sub.to_csv(to_write[e],mode='a',header=False,index=False)
    
    #df_master.to_csv(to_write[e],index=None)
    #df = df.loc[~pd.isnull(df['schindler'])]
    #df = df.loc[~pd.isnull(df['loewe'])]
    #df.to_csv("merck_synergy.csv",index=None)
    
