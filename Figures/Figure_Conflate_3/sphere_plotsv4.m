cSet defaults for plotting
set(0,'DefaultAxesFontSize',8)
set(0,'DefaultLineLinewidth',2)
set(0, 'DefaultAxesFontWeight','normal')
set(0, 'DefaultAxesLineWidth',2)
set(0, 'DefaultFigureColor','w')
set(0, 'DefaultTextFontSize',8)
set(0, 'DefaultTextFontWeight','normal')
set(0, 'defaultTextFontName','arial')
set(0, 'defaultAxesFontName','arial')

radius = [.65,1,2];
%Solve the 2D hill equation at the ec50 of both drugs with the following
%values
syms r1 r1_1 r2 r2_1 h1 h2 d1 d2 alpha1 alpha2 positive
syms E0 E1 E2 E3 real
syms C1 C2
r1 = r2;
C1 = 1;C2 = 1;
d1 = C1; d2 = C2;
h1 = 1;  h2 = 1;
E0 = 1;  E1 = 0;  E2 = 0;
r1_1 = C1^h1*r1; r2_1 = C2^h2*r2;
T = [-(r1*d1^h1+r2*d2^h2),r1_1,r2_1,0;
    r1*d1^h1,-(r1_1+r2*(alpha1*d2)^h2),0,r2_1;
    r2*d2^h2,0,-(r2_1+r1*(alpha2*d1)^h1),r1_1;
    1,1,1,1];
Eqn = [E0,E1,E2,E3] * inv(T)*[0;0;0;1];
eq1 = simplify(Eqn);
eq2 = combine(eq1);
%Equation is of the form:
%(alpha1/2+alpha2/2+(E3*alpha1)/2 + (E3*alpha2)/2 + E3*alpha1*alpha2 +1)/
%(2*alpha1 + 2*alpha2 + alpha1*alpha2 + 3)

iso_surf_vec_l = zeros([3,3]);
iso_surf_vec_h = zeros([3,3]);

label = {'Loewe','Bliss','HSA'};
for i=1:3
    %Solve for loewe,bliss,and hsa over a 1x1x1 grid for alpha1,alpha2,beta,
    [x3,y3,z3] = meshgrid(linspace(-radius(i),radius(i),25));
    e3 = -z3;%Beta is equal to -E3 when E0=1,E1=E2=0
    %Solve for the expected effect
    ed = (10.^x3/2 + 10.^y3/2 + (e3.*10.^x3)/2 + (e3.*10.^y3)/2 + e3.*10.^x3.*10.^y3 + 1)./(2*10.^x3 + 2*10.^y3 + 10.^x3.*10.^y3 + 3);
    %Matrix to store loewe bliss and hsa calc
    f1 = zeros([size(x3),3]);
    %Calculate loewe
    %Where loewe is undefined log10 returns complex numbers.  Just take the
    %real component and set all places where infinite to nan
    tmp = real(-log10(2/(1./ed-1)));
    tmp(isinf(tmp))=nan;
    tmp(ed<0) = nan;
    f1(:,:,:,1) =tmp;
    %Calculate Bliss  Expected effect at ec50 for both is .5*.5=.25
    f1(:,:,:,2) = 1/4-ed;
    %Calculate hsa at ec50 max(e1,e2)=1/2
    f1(:,:,:,3) = 1/2-ed;
    %Define sphere to plot each metric on
    f2 = sqrt((x3).^2+(y3).^2+(z3).^2);
    %For each metric
    
    % Find contour where synergy metric is zero
    p1 = isosurface(x3, y3, z3, f1(:,:,:,i), 0);
    %Some nan vertices have been observed before... remove them along with
    %the faces they touch
    newVertices = p1.vertices;
    if ~isempty(newVertices)
        verticesToRemove = [find(isnan(mean(p1.vertices')))];
        newVertices(verticesToRemove,:) = [];
        % Find the new index for each of the new vertices
        [~, newVertexIndex] = ismember(p1.vertices, newVertices, 'rows');
        % Find any faces that used the vertices that we removed and remove them
        facesToRemove = [];
        cnt=1;
        for j=1:size(p1.faces,1)
            if sum(ismember(p1.faces(j,:),verticesToRemove))>0
                facesToRemove(cnt) = j;
                cnt=cnt+1;
            end
        end
        newFaces = p1.faces;
        newFaces(facesToRemove,:)=[];
        % Now update the vertex indices to the new ones
        newFaces = newVertexIndex(newFaces);
        p1.vertices = newVertices;
        p1.faces = newFaces;
    end

    %Create a sphere
    p2 = isosurface(x3, y3, z3, f2, radius(i));
    %Create Figure
    fig = figure('Color','white','rend','painters');
    fig.PaperUnits = 'inches';
    fig.Units = 'inches';
    fig.Position=[1,1,2.5,2.5];
    %Create 2 axes
    ax2 = axes('Position',[0.0 0.0 0.35 0.35]);    ax1 = axes('Position',[0.0 0.0 1 1]);
    axes(ax1)
    hold on
    %Plot the sphere and color based on the value of each synergy metric
    p = patch(p2);
    isonormals(x3,y3,z3,f2,p)
    isocolors(x3,y3,z3,f1(:,:,:,i),p)
    p.FaceColor = 'interp';
    p.EdgeColor = 'none';
    %Set aspect ratio and view
    daspect([1 1 1])
    view(55,30)
    %Add and modify colorbar and change color max and min so zero is centered
    h = colorbar('East','AxisLocation','out');
    h.Label.String = strcat('<--Ant',{'    '},label(i), '     Syn-->');
    cmax = max(p.FaceVertexCData); cmin=min(p.FaceVertexCData);
%     if abs(cmin)>cmax
%         cmax=-cmin;
%     else
%         cmin=-cmax;
%     end
    caxis([cmin,cmax])
    if min(h.Ticks)>=0
        h.Ticks = [min(h.Ticks),max(h.Ticks)];
    else
        h.Ticks = [min(h.Ticks),0,max(h.Ticks)];
    end
    h.Position = [0.87,0.25,0.03,0.55];
    h.Label.Position = [2.2,0,0];

    %Remove background axis
    axis('off')
    %Freeze colors to prevent the colorbar from changing with new plot
    freezeColors
    %Find the intersection of the sphere and the zero contour surfaces
    if ~isempty(newVertices)
        try
            [~, s12] = SurfaceIntersection(p1, p2);
            %Plot as black line
            S=s12; trisurf(S.faces, S.vertices(:,1),S.vertices(:,2),S.vertices(:,3),'EdgeColor', 'k', 'FaceColor', 'none','LineWidth',1.5);
        end
    end
    %Create x,y,z planes for marking axes
    ps1 = isosurface(x3,y3,z3,x3,0);
    ps2 = isosurface(x3,y3,z3,y3,0);
    ps3 = isosurface(x3,y3,z3,z3,0);
    patch(ps1,'FaceColor',[.5,.5,.5],'FaceAlpha',.5,'EdgeColor','none');
    patch(ps2,'FaceColor',[.5,.5,.5],'FaceAlpha',.5,'EdgeColor','none');
    patch(ps3,'FaceColor',[.5,.5,.5],'FaceAlpha',.5,'EdgeColor','none');

    
    %Calculate two points for iso surfaces
    x1 = cosd(45)*radius(i);
    y1 = cosd(45)*radius(i);
    z1 = 0;
    %Solve for the expected effect
    ed = (10.^x1/2 + 10.^y1/2 + (z1.*10.^x1)/2 + (z1.*10.^y1)/2 + z1.*10.^x1.*10.^y1 + 1)./(2*10.^x1 + 2*10.^y1 + 10.^x1.*10.^y1 + 3);
    %Matrix to store loewe bliss and hsa calc
    f = zeros([3,1]);
    %Calculate loewe
    %Where loewe is undefined log10 returns complex numbers.  Just take the
    %real component and set all places where infinite to nan
    tmp = real(-log10(2/(1./ed-1)));
    tmp(isinf(tmp))=nan;
    tmp(ed<0) = nan;
    f(1) =tmp;
    %Calculate Bliss  Expected effect at ec50 for both is .5*.5=.25
    f(2) = 1/4-ed;
    %Calculate hsa at ec50 max(e1,e2)=1/2
    f(3) = 1/2-ed;
    % Find contour where synergy metric is zero
    p3 = isosurface(x3, y3, z3, f1(:,:,:,i), f(i));
    %Some nan vertices have been observed before... remove them along with
    %the faces they touch
    newVertices = p3.vertices;
    if ~isempty(newVertices)
        verticesToRemove = [find(isnan(mean(p3.vertices')))];
        newVertices(verticesToRemove,:) = [];
        % Find the new index for each of the new vertices
        [~, newVertexIndex] = ismember(p3.vertices, newVertices, 'rows');
        % Find any faces that used the vertices that we removed and remove them
        facesToRemove = [];
        cnt=1;
        for j=1:size(p3.faces,1)
            if sum(ismember(p3.faces(j,:),verticesToRemove))>0
                facesToRemove(cnt) = j;
                cnt=cnt+1;
            end
        end
        newFaces = p3.faces;
        newFaces(facesToRemove,:)=[];
        % Now update the vertex indices to the new ones
        newFaces = newVertexIndex(newFaces);
        p3.vertices = newVertices;
        p3.faces = newFaces;
    end    
    if ~isempty(newVertices)
        try
            [~, s32] = SurfaceIntersection(p3, p2);
            X = sortrows(s32.vertices,3);
            tmp1 = X(1,:);
            tmp2 = X(end,:);
            scatter3(tmp2(1),tmp2(2),tmp2(3),100,'filled','MarkerFaceColor',[.25 .25 .25],'MarkerEdgeColor',[0 0 0]);
            scatter3(tmp1(1),tmp1(2),tmp1(3),100,'filled','MarkerFaceColor',[1 0 0],'MarkerEdgeColor',[0 0 0]);
        end
    end    
    iso_surf_vec_l(i,:) = tmp1;
    iso_surf_vec_h(i,:) = tmp2;
    
    %Create axis which axis labels
    axes(ax2)
    mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
    mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
    mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
    xlim([-1,1]);    ylim([-1,1]);    zlim([-1,1]);
    %Set view
    view(55,30)
    axis('off')
    %Add labels
    text(0,-1.8,-1,'log(\alpha_1)','fontsize',10,'fontweight','bold')
    text(-2.1,0.3,-1,'log(\alpha_2)','fontsize',10,'fontweight','bold')
    text(-1.3,-1.3,-.3,'\beta','fontsize',10,'fontweight','bold')
    text(-1,-1,1.1,num2str(radius(i)),'fontsize',10)

    %Stop clipping to avoid loosing text
    ax2.Clipping='off';
    tightfig
    %Beautify
    camlight
    lighting 'gouraud'
    export_fig(strcat(label{i},'_sphere'),'-m3')
    for j=0:2:360
        axes(ax1)
        view(55+j,30)
        axes(ax2)
        view(55+j,30)
        export_fig(strcat('movie_plots/',label{i},'_',int2str(j)),'-m3')
    end
    close(fig)
    im_cnt = j;
    x = [linspace(radius(i),2.05,round((2.05-radius(i))/.05+1)),linspace(2.05,.5,32),linspace(.5,radius(i),round((radius(i)-.5)/.05+1))];
    for r=x
        %Solve for loewe,bliss,and hsa over a 1x1x1 grid for alpha1,alpha2,beta,
        [x3,y3,z3] = meshgrid(linspace(-r,r,25));
        e3 = -z3;%Beta is equal to -E3 when E0=1,E1=E2=0
        %Solve for the expected effect
        ed = (10.^x3/2 + 10.^y3/2 + (e3.*10.^x3)/2 + (e3.*10.^y3)/2 + e3.*10.^x3.*10.^y3 + 1)./(2*10.^x3 + 2*10.^y3 + 10.^x3.*10.^y3 + 3);
        %Matrix to store loewe bliss and hsa calc
        f1 = zeros([size(x3),3]);
        %Calculate loewe
        %Where loewe is undefined log10 returns complex numbers.  Just take the
        %real component and set all places where infinite to nan
        tmp = real(-log10(2/(1./ed-1)));
        tmp(isinf(tmp))=nan;
        tmp(ed<0) = nan;
        f1(:,:,:,1) =tmp;
        %Calculate Bliss  Expected effect at ec50 for both is .5*.5=.25
        f1(:,:,:,2) = 1/4-ed;
        %Calculate hsa at ec50 max(e1,e2)=1/2
        f1(:,:,:,3) = 1/2-ed;
        %Define sphere to plot each metric on
        f2 = sqrt((x3).^2+(y3).^2+(z3).^2);
        %For each metric


        % Find contour where synergy metric is zero
        p1 = isosurface(x3, y3, z3, f1(:,:,:,i), 0);
        %Some nan vertices have been observed before... remove them along with
        %the faces they touch
        newVertices = p1.vertices;
        if ~isempty(newVertices)
            verticesToRemove = [find(isnan(mean(p1.vertices')))];
            newVertices(verticesToRemove,:) = [];
            % Find the new index for each of the new vertices
            [~, newVertexIndex] = ismember(p1.vertices, newVertices, 'rows');
            % Find any faces that used the vertices that we removed and remove them
            facesToRemove = [];
            cnt=1;
            for j=1:size(p1.faces,1)
                if sum(ismember(p1.faces(j,:),verticesToRemove))>0
                    facesToRemove(cnt) = j;
                    cnt=cnt+1;
                end
            end
            newFaces = p1.faces;
            newFaces(facesToRemove,:)=[];
            % Now update the vertex indices to the new ones
            newFaces = newVertexIndex(newFaces);
            p1.vertices = newVertices;
            p1.faces = newFaces;
        end

        %Create a sphere
        p2 = isosurface(x3, y3, z3, f2, r);
        %Create Figure
        fig = figure('Color','white','rend','painters');
        fig.PaperUnits = 'inches';
        fig.Units = 'inches';
        fig.Position=[1,1,2.5,2.5];
        %Create 2 axes
        ax2 = axes('Position',[0.0 0.0 0.35 0.35]);    ax1 = axes('Position',[0.0 0.0 1 1]);
        axes(ax1)
        hold on
        %Plot the sphere and color based on the value of each synergy metric
        p = patch(p2);
        isonormals(x3,y3,z3,f2,p)
        isocolors(x3,y3,z3,f1(:,:,:,i),p)
        p.FaceColor = 'interp';
        p.EdgeColor = 'none';
        %Set aspect ratio and view
        xlim([-r,r]);    ylim([-r,r]);    zlim([-r,r]);
        daspect([1 1 1])
        view(55,30)
        %Add and modify colorbar and change color max and min so zero is centered
        h = colorbar('East','AxisLocation','out');
%         h.Label.String = strcat('<--Ant',{'    '},label(i), '     Syn-->');
%         cmax = max(p.FaceVertexCData); cmin=min(p.FaceVertexCData);
    %     if abs(cmin)>cmax
    %         cmax=-cmin;
    %     else
    %         cmin=-cmax;
    %     end
        caxis([cmin,cmax])
        h.Ticks = [0];
        h.Position = [0.87,0.25,0.03,0.55];
%         h.Label.Position = [2.2,0,0];

        %Remove background axis
        axis('off')
        %Freeze colors to prevent the colorbar from changing with new plot
        freezeColors
        %Find the intersection of the sphere and the zero contour surfaces
        if ~isempty(newVertices)
            try
                [~, s12] = SurfaceIntersection(p1, p2);
                %Plot as black line
                S=s12; trisurf(S.faces, S.vertices(:,1),S.vertices(:,2),S.vertices(:,3),'EdgeColor', 'k', 'FaceColor', 'none','LineWidth',1.5);
            end
        end
        %Create x,y,z planes for marking axes
        ps1 = isosurface(x3,y3,z3,x3,0);
        ps2 = isosurface(x3,y3,z3,y3,0);
        ps3 = isosurface(x3,y3,z3,z3,0);
        patch(ps1,'FaceColor',[.5,.5,.5],'FaceAlpha',.5,'EdgeColor','none');
        patch(ps2,'FaceColor',[.5,.5,.5],'FaceAlpha',.5,'EdgeColor','none');
        patch(ps3,'FaceColor',[.5,.5,.5],'FaceAlpha',.5,'EdgeColor','none');

        %Create axis which axis labels
        axes(ax2)
        mArrow3([-1,-1,-1],[1,-1,-1], 'color', 'red', 'stemWidth', 0.03)
        mArrow3([-1,-1,-1],[-1,-1,1], 'color', 'red', 'stemWidth', 0.03)
        mArrow3([-1,-1,-1],[-1,1,-1], 'color', 'red', 'stemWidth', 0.03)
        xlim([-1,1]);    ylim([-1,1]);    zlim([-1,1]);
        %Set view
        view(55,30)
        axis('off')
        %Add labels
        text(0,-1.8,-1,'log(\alpha_1)','fontsize',10,'fontweight','bold')
        text(-2.1,0.3,-1,'log(\alpha_2)','fontsize',10,'fontweight','bold')
        text(-1.3,-1.3,-.3,'\beta','fontsize',10,'fontweight','bold')
        text(-1,-1,1.1,num2str(r),'fontsize',10)
        %Stop clipping to avoid loosing text
        ax2.Clipping='off';
        tightfig
        %Beautify
        camlight
        lighting 'gouraud'
        export_fig(strcat('movie_plots/',label{i},'_sphere_',num2str(im_cnt)),'-m3')
        im_cnt = im_cnt + 2;
        close(fig)
    end
end





%Solve the 2D hill equation at the ec50 of both drugs with the following
%values
syms r1 r1_1 r2 r2_1 h1 h2 d1 d2 alpha1 alpha2 positive
syms E0 E1 E2 E3 real
syms C1 C2
r1 = r2;
C1 = 1;C2 = 1;
h1 = 1;  h2 = 1;
E0 = 1;  E1 = 0;  E2 = 0;
r1_1 = C1^h1*r1; r2_1 = C2^h2*r2;
T = [-(r1*d1^h1+r2*d2^h2),r1_1,r2_1,0;
    r1*d1^h1,-(r1_1+r2*(alpha1*d2)^h2),0,r2_1;
    r2*d2^h2,0,-(r2_1+r1*(alpha2*d1)^h1),r1_1;
    1,1,1,1];
Eqn = [E0,E1,E2,E3] * inv(T)*[0;0;0;1];
eq1 = simplify(Eqn);
eq2 = combine(eq1);
%Equation is of the form:
%(alpha1/2+alpha2/2+(E3*alpha1)/2 + (E3*alpha2)/2 + E3*alpha1*alpha2 +1)/
%(2*alpha1 + 2*alpha2 + alpha1*alpha2 + 3)





beta	beta_ci	beta_obs	beta_obs_ci	converge_mc_nlls	converge_percnt_mc_nlls	drug1_name	drug1_units	drug2_name	drug2_units	drugunique	E0	E0_ci	E1	E1_ci	E1_obs	E1_obs_ci	E2	E2_ci	E2_obs	E2_obs_ci	E3	E3_ci	E3_obs	E3_obs_ci	expt	expt_date	fit_method	h1	h2	init_rndm_seed	log_alpha1	log_alpha1_ci	log_alpha2	log_alpha2_ci	log_C1	log_C1_ci	log_C2	log_C2_ci	log_h1	log_h1_ci	log_h2	log_h2_ci	log_like_mc_nlls	max_conc_d1	max_conc_d2	metric_name	min_conc_d1	min_conc_d2	num_parms_fit	R2	sample	time



[d1,d2] = meshgrid([0 logspace(-5,5,20)],[0 logspace(-5,5,20)]);
label = {'Loewe','Bliss','HSA'};
for i=1:3
    alpha1 = 10^iso_surf_vec_l(i,1);
    alpha2 = 10^iso_surf_vec_l(i,2);
    E3 = -iso_surf_vec_l(i,3);
    ed = (alpha1*d2 + alpha2*d1 + E3*alpha1*d1.*d2 + E3*alpha2*d1.*d2 + E3*alpha1*alpha2*d1.*d2.^2 + E3*alpha1*alpha2*d1.^2.*d2 + 2)./(2*d1 + 2*d2 + alpha1*d2 + alpha2*d1 + alpha1*d2.^2 + alpha2*d1.^2 + 2*alpha1*d1.*d2 + 2*alpha2*d1.*d2 + alpha1*alpha2*d1.*d2.^2 + alpha1*alpha2*d1.^2.*d2 + 2);
    s = cell(size(d1(:)));
    s(:) = strcat(label(i),'_low');
    d = cell(size(d1(:)));
    d(:) = {'drug1'};
    dd = cell(size(d1(:)));
    dd(:) = {'drug2'};
    if i==1
        T = table(d,dd,s,d1(:),d2(:),ed(:),ones(size(d1(:),1),1)*.01,ones(size(d1(:),1),1));
        T.Properties.VariableNames = {'drug1','drug2','sample','drug1_conc','drug2_conc','effect','effect_95ci','expt_date'};
    else
    T_sub = table(d,dd,s,d1(:),d2(:),ed(:),ones(size(d1(:),1),1)*.01,ones(size(d1(:),1),1));
    T_sub.Properties.VariableNames = {'drug1','drug2','sample','drug1_conc','drug2_conc','effect','effect_95ci','expt_date'};
    end
    

    
    
    fig = figure('Color','white','rend','painters');
    s = surf(log10(d1),log10(d2),ed,'EdgeColor','interp','FaceColor','interp','FaceLighting','gouraud');
    %Set aspect ratio and view
    xlim([-5,5]);    ylim([-5,5]);    zlim([-1,1]);
    view(72,35)

    
    
    
% %Export figures while maintaining resolution and size
% export_fig HSA_sphere -m3
% close(gcf)
% export_fig Bliss_sphere -m3
% close(gcf)
% export_fig Loewe_sphere -m3
% close(gcf)
