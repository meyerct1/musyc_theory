#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 16 10:23:56 2019

@author: xnmeyer
"""
##############################################################################
##############################################################################
#Code to create plots for conflation figure 
import pandas as pd
from conflation_plots import plotViolin
from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data

import os
import sys
sys.path.insert(0,'../Figure_Sham_5/')
from pythontools.synergy import synergy_tools
import numpy as np
###############################################################################
###############################################################################
#Panels B,C
##############################################################################
##############################################################################
#Begin with Cancer combination analysis
#Run from the MuSyC_Theory/Figures/Figure_Conflate_3/ folder
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)
T['geo_h'] = np.sqrt(T['h1']*T['h2'])

for k in T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
        
        
#Select out the important conditions
key = 'loewe'
mask = []
#sub_T = sub_T[(sub_T['E1']<.1)&(sub_T['E2']<.1)&(sub_T['E3']<.1)]
#Correct the hill bias for Loewe
#T = T[~T[key+'_fit_ec50'].isna()]

from SynergyCalculator.NDHillFun import Edrug2D_NDB
#
from SynergyCalculator.calcOtherSynergyMetrics import loewe
#
def get_loewe_bias(h1, h2, d1, d2, C1, C2, E0, E1, E2, E3=None, alpha=0.):
    """
    Calculates Loewe for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False,r1=100.)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False,r1=100.)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    l = -np.log10(synergy_tools.loewe(d1, d2, E, E0, E1, E2, h1, h2, C1, C2))
    return np.nanmedian(l)
#
T['loewe_fit_ec50_fix'] = np.nan
T['loewe_fit_ec10_fix'] = np.nan
T['loewe_fit_ec25_fix'] = np.nan

from SynergyCalculator.calcOtherSynergyMetrics import hill_1D_inv
for ind in T.index:
    print ind
    h1,h2,d1,d2,C1,C2,E0,E1,E2,E3,alpha1,alpha2 = T.loc[ind,'h1'],T.loc[ind,'h2'],T.loc[ind,'C1'],T.loc[ind,'C2'],T.loc[ind,'C1'],T.loc[ind,'C2'],T.loc[ind,'E0'],T.loc[ind,'E1'],T.loc[ind,'E2'],T.loc[ind,'E3'],10**T.loc[ind,'log_alpha1'],10**T.loc[ind,'log_alpha2']
    E_ec50 = Edrug2D_NDB((C1,C2),E0,E1,E2,E3,100.,100.,C1,C2,h1,h2,alpha1,alpha2)
    T.loc[ind,'loewe_fit_ec50_fix'] = -np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2)) - get_loewe_bias(h1, h2, d1, d2, C1, C2, E0, E1, E2,E3=E3)
#    EC10_1 = hill_1D_inv(E0-(E0-E1)*.1,E0,E1,h1,C1) #concentration for this effect
#    EC10_2 = hill_1D_inv(E0-(E0-E2)*.1,E0,E2,h2,C2) #concentration for this effect
#    E_ec10 = Edrug2D_NDB((EC10_1,EC10_2),E0,E1,E2,E3,100.,100.,C1,C2,h1,h2,alpha1,alpha2)
#    T.loc[ind,'loewe_fit_ec10_fix'] = -np.log10(loewe(EC10_1, EC10_1, E_ec10, E0, E1, E2, h1, h2, C1, C2)) - get_loewe_bias(h1, h2, EC10_1, EC10_2, C1, C2, E0, E1, E2,E3=E3)
#    EC25_1 = hill_1D_inv(E0-(E0-E1)*.25,E0,E1,h1,C1)
#    EC25_2 = hill_1D_inv(E0-(E0-E2)*.25,E0,E2,h2,C2)
#    E_ec25 = Edrug2D_NDB((EC25_1,EC25_2),E0,E1,E2,E3,100.,100.,C1,C2,h1,h2,alpha1,alpha2)
#    T.loc[ind,'loewe_fit_ec25_fix'] = -np.log10(loewe(EC25_1, EC25_2, E_ec50, E0, E1, E2, h1, h2, C1, C2)) - get_loewe_bias(h1, h2, EC25_1, EC25_2, C1, C2, E0, E1, E2,E3=E3)

T['loewe_fit_ec50_orig']=T['loewe_fit_ec50']
T['loewe_fit_ec50']=T['loewe_fit_ec50_fix']

sub_T = T[~T[key+'_fit_ec50'].isna()]
sub_T.reset_index(inplace=True,drop=False)
mask=[]
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']<0.))
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']>0.))
mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']>0.)&(sub_T['beta']>0.))

xlim = (-.3,.7)
yticklabs=['$\\alpha_1,\\alpha_2<0$\n$\\beta<0$',
           '$\\alpha_1,\\alpha_2<0$\n$\\beta>0$',
           '$\\alpha_1,\\alpha_2>0$\n$\\beta>0$',
           ]
ax = plotViolin(sub_T,mask,key,xlim,yticklabs)

sub_T = T[(T['loewe_fit_ec50']>0.)&(T['beta']<0.)&(T['beta_obs']<0.)&(T['log_alpha1_ci_up']<0.2)&(T['log_alpha2_ci_up']<0.2)&(T['log_alpha1']<0.)&(T['log_alpha2']<0.)]

#T[T['drugunique']=='methotrexate_erlotinib_UWB1289BRCA1']
sub_T = T.loc[15582]
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
sub_T['save_direc'] = os.getcwd()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:] = 0
popt3 = [sub_T['E0'],sub_T['E2'],sub_T['E1'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C2'],10**sub_T['log_C1'],10**sub_T['log_h2'],10**sub_T['log_h1'],10**sub_T['log_alpha2'],10**sub_T['log_alpha1']]
sub_T['E1'],sub_T['E2']=sub_T['E2'],sub_T['E1']
sub_T['h1'],sub_T['h2']=sub_T['h2'],sub_T['h1']
sub_T['log_C1'],sub_T['log_C2']=sub_T['log_C2'],sub_T['log_C1']
sub_T['log_alpha1'],sub_T['log_alpha2']=sub_T['log_alpha2'],sub_T['log_alpha1']
sub_T['min_conc_d1'],sub_T['min_conc_d2']=sub_T['min_conc_d2'],sub_T['min_conc_d1']
sub_T['max_conc_d1'],sub_T['max_conc_d2']=sub_T['max_conc_d2'],sub_T['max_conc_d1']
d1,d2=d2,d1
sub_T['drug1_name'],sub_T['drug2_name']=sub_T['drug2_name'],sub_T['drug1_name']
DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
title = 'Loewe @ EC50:%.2f'%sub_T['loewe_fit_ec50'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.0,1.1), zero_conc=0,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))


# =============================================================================
# For supplemental figure example
# =============================================================================
sub_T = T[(T['loewe_fit_ec50']>0.)&(T['beta']>0.)&(T['beta_obs']>0.)&(T['log_alpha1_ci_up']<0.0)&(T['log_alpha2_ci_up']<0.0)&(T['log_alpha1']<0.)&(T['log_alpha2']<0.)]

#T[T['drugunique']=='methotrexate_erlotinib_UWB1289BRCA1']
sub_T = T.loc[2174]
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
sub_T['save_direc'] = os.getcwd()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:] = 0
popt3 = [sub_T['E0'],sub_T['E1'],sub_T['E2'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C1'],10**sub_T['log_C2'],10**sub_T['log_h1'],10**sub_T['log_h2'],10**sub_T['log_alpha1'],10**sub_T['log_alpha2']]
DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
title = 'Loewe @ EC50:%.2f'%sub_T['loewe_fit_ec50'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.0,1.1), zero_conc=0,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))


# =============================================================================
# Panels H,I
# =============================================================================
#Select out the important conditions
key = 'hsa'
mask = []
sub_T = T[~T[key+'_fit_ec50'].isna()]
sub_T = sub_T.reset_index(drop=True)
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']>0.))
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.)&(sub_T['beta']<0.))
mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']>0.)&(sub_T['beta']>0.))
xlim = (-.3,.7)
yticklabs=['$\\alpha_1,\\alpha_2<0$\n$\\beta>0$',
           '$\\alpha_1,\\alpha_2<0$\n$\\beta<0$',
           '$\\alpha_1,\\alpha_2>0$\n$\\beta>0$'
           ]
ax = plotViolin(sub_T,mask,key,xlim,yticklabs)

#Find antagonistic by hsa synergistic by beta antagonistic by alpha
sub_T = T[(T['hsa_fit_ec50']<0.)&(T['beta']>.2)&(T['beta_obs']>.2)&(T['log_alpha1_ci_up']<-.25)&(T['log_alpha2_ci_up']<-.25)]
sub_T = sub_T[['hsa_fit_ec50','beta','beta_obs','log_alpha1','log_alpha2','drug1_name','drug2_name','sample']]
print sub_T.sort_values('hsa_fit_ec50')

#dexamethasone_mk-8669_DLD1
sub_T = T.loc[2652]
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
sub_T['save_direc'] = os.getcwd()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:] = 0
popt3 = [sub_T['E0'],sub_T['E1'],sub_T['E2'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C1'],10**sub_T['log_C2'],10**sub_T['log_h1'],10**sub_T['log_h2'],10**sub_T['log_alpha1'],10**sub_T['log_alpha2']]
DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
title = 'HSA @ EC50:%.2f'%sub_T['hsa_fit_ec50'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
fname=drug1_name+'_'+drug2_name+'_'+sample
[fig1,fig2],[ax_surf,ax1,ax2],[tt,yy,zz],surf = matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=None, zlim=(0.5,1.1), zero_conc=0,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
ax1.set_xticks([-2,0])
ax2.set_xticks([-5,-3])
ax1.set_ylim([.5,1.1])
ax2.set_ylim([.5,1.1])

fig1.savefig(fname+'.pdf',pad_inches=0.,format='pdf');
fig2.savefig(fname+'_slices.pdf',pad_inches=0.,format='pdf');

#cell line characteristics: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4350329/
#Dexamethasone glucocorticoid: https://www.ncbi.nlm.nih.gov/pubmed/12204894
#Dexamethasone and mtor http://www.jbc.org/content/281/51/39128.full     https://www.sciencedirect.com/science/article/pii/S1550413111000027

##############################################################################
##############################################################################
#Panesl E,F
##############################################################################
##############################################################################
# Malaria combination analysis
i = 'mott_antimalaria'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/mott_antimalaria'
T['model_level']=1
T.reset_index(drop=True,inplace=True)
from SynergyCalculator.calcOtherSynergyMetrics import bliss
from SynergyCalculator.NDHillFun import Edrug2D_NDB
for i in T.index:
    E_ec50 = Edrug2D_NDB((T.loc[i,'C1'],T.loc[i,'C2']),T.loc[i,'E0'],T.loc[i,'E1'],T.loc[i,'E2'],T.loc[i,'E3'],T.loc[i,'r1'],T.loc[i,'r2'],T.loc[i,'C1'],T.loc[i,'C2'],10**T.loc[i,'log_h1'],10**T.loc[i,'log_h2'],10**T.loc[i,'log_alpha1'],10**T.loc[i,'log_alpha2'])
    T.loc[i,'bliss_fit_ec50'] = bliss(T.loc[i,'E0']/100.-(T.loc[i,'E0']-T.loc[i,'E1'])/200., T.loc[i,'E0']/100.-(T.loc[i,'E0']-T.loc[i,'E2'])/200., E_ec50/100.)
for k in T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
        
#Select out the important conditions
key = 'bliss'
mask = []
sub_T = T[~T[key+'_fit_ec50'].isna()]
sub_T = sub_T[(sub_T['E1']<10)&(sub_T['E2']<10)&(sub_T['E3']<10)]
sub_T = sub_T.reset_index(drop=True)
mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']<0.) | (sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']>0.))
mask.append((sub_T['log_alpha1']<0.)&(sub_T['log_alpha2']<0.))
mask.append((sub_T['log_alpha1']>0.)&(sub_T['log_alpha2']>0.))
    
ylim = (-.25,.35)
xticklabs =  ['$\\alpha_1<0$\n$\\alpha_2>0$',
              '$\\alpha_1,\\alpha_2<0$',
              '$\\alpha_1,\\alpha_2>0$',
                     ]
plotViolin(sub_T,mask,key,ylim,xticklabs)

##################################################################################
#Find antagonistic by bliss synergistic by beta antagonistic by alpha
sub_T = T[(T['bliss_fit_ec50']>0.)&(((T['log_alpha1_ci_lw']>0.)&(T['log_alpha2_ci_up']<0))|((T['log_alpha1_ci_up']<0.)&(T['log_alpha2_ci_lw']>0)))]
sub_T = sub_T[['bliss_fit_ec50','log_alpha1','log_alpha2','drug1_name','drug2_name','sample']]
print sub_T.sort_values('bliss_fit_ec50')


#mefloquine_artesunate_PLASMODIUM_FALCIPARUM_DD2
sub_T = T[(T['drug1_name']=='mefloquine')&(T['drug2_name']=='halofantrine')&(T['sample']=='PLASMODIUM_FALCIPARUM_HB3')].to_dict(orient='record')[0]

expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
sub_T['save_direc'] = os.getcwd()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:] = 0
popt3 = [sub_T['E0'],sub_T['E1'],sub_T['E2'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C1'],10**sub_T['log_C2'],10**sub_T['log_h1'],10**sub_T['log_h2'],10**sub_T['log_alpha1'],10**sub_T['log_alpha2']]
DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
title = 'Bliss @ EC50:%.2f'%sub_T['bliss_fit_ec50'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,110), zero_conc=0,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))


#https://www.ncbi.nlm.nih.gov/pubmed/8866337
#https://www.webmd.com/drugs/2/drug-6081/mefloquine-oral/details/list-interaction-details/dmid-350/dmtitle-antimalarials-halofantrine/intrtype-drug
#https://en.wikipedia.org/wiki/Mefloquine

#https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2801676/



## =============================================================================
## Calculate how many combinations have intermediate effects
## =============================================================================
#i = ['oneil_anticancer','mott_antimalaria']
#for f in i:
#    T = pd.read_csv('../../../Data/' + f + '/MasterResults_noGamma_otherSynergyMetricsCalculation.csv')
#    T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
#    T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
#    T.reset_index(drop=True,inplace=True)
#    tmp = np.sum((T[['E1','E2']]>.15).any(axis=1) & (T[['E1','E2']]<.50).any(axis=1))
#    print tmp/float(len(T))
#    
#from scipy.stats import gaussian_kde
#plt.figure()    
#l1=[]
#l2=[]
#drgs = np.unique(np.concatenate((T['drug1_name'].unique(),T['drug2_name'].unique())))
#for d in drgs:
#    tmp = np.array(np.concatenate((T[T['drug1_name']==d]['log_alpha1'].values,T[T['drug2_name']==d]['log_alpha2'].values)))
#    if len(tmp) > 5:
#        l1.append(np.median(tmp))
#        l2.append(d)
#plt.figure()
#x = np.sort(l1)
#y = [l2[i] for i in np.argsort(l1)]
#plt.bar(range(len(x)),x)
#plt.xticks(range(len(x)),y,rotation=90)
#plt.xticklabels(y)