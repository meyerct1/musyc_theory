

dir = getDirectory("Choose a Directory");
setBatchMode(true);
count = 0;
run("Colors...", "foreground=white background=white selection=yellow");
n=0;
list = getFileList(dir)
for (i=0; i<list.length;i++)
	action(dir,list[i]);
setBatchMode(false);
function action(dir,path) {
	if (endsWith(path,".png")) {
		open(dir+path);
		run("Canvas Size...", "width=800 height=800 position=Center");		
		saveAs("png",dir+path);
	}
}

