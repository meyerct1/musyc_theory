#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  8 11:25:36 2019

@author: xnmeyer
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.pyplot import cm
from matplotlib import rc
#Adjust text from  https://github.com/Phlya/adjustText
#Can be installed with pip
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)


data_sets = ['oneil_anticancer','mott_antimalaria']
colors = ['red','purple','green']
plt.figure(facecolor='w',figsize=(6,1.8))
ax = []
ax.append(plt.subplot(121))
ax.append(plt.subplot(122))
p = []
ylim = [100,15000,300]
    
for j,ds in enumerate(data_sets):
    plt.sca(ax[j])
    df1 = pd.read_csv('../../../Data/'+ds+'/MasterResults_gamma.csv')
    df2 = pd.read_csv('../../../Data/'+ds+'/MasterResults_noGamma.csv')
    df1 = df1[(df1['converge_nlls']==1)&(df1['R2']>.7)]
    df1['C1'] = 10**df1['log_C1'];df1['C2']=10**df1['log_C2']
    df1 = df1[(df1['C1']<df1['max_conc_d1'])&(df1['C2']<df1['max_conc_d2'])]
    df2 = df2[(df2['converge_nlls']==1)&(df2['R2']>.7)]
    df2['C1'] = 10**df2['log_C1'];df2['C2']=10**df2['log_C2']
    df2 = df2[(df2['C1']<df2['max_conc_d1'])&(df2['C2']<df2['max_conc_d2'])]

    df1['AIC'] = 2*12-2*df1['log_like_nlls']
    df2['AIC'] = 2*10-2*df2['log_like_nlls']
        
    df1.reset_index(drop=True,inplace=True)
    df2.reset_index(drop=True,inplace=True)

    df = df1.merge(df2,on='drugunique')
    
    df['dAIC'] = df['AIC_x'].values-df['AIC_y']
    df = df[(df['dAIC']>-ylim[j])&(df['dAIC']<ylim[j])]
    print sum(df['dAIC']<0)/float(len(df))
    p=[]
    p.append(plt.violinplot(df['dAIC'].values,positions=[0],widths=.75,showextrema=False,showmedians=True))
    print df['dAIC'].mean()
    print np.exp(-df['dAIC'].mean()/2)
    for e,bp in enumerate(p):
        for pc in bp['bodies']:
            pc.set_facecolor('k')
            pc.set_edgecolor(colors[j]) 
            pc.set_linewidth(2)
            pc.set_alpha(.8)
        bp['cmedians'].set_color(colors[j])
    plt.xlim(-.5,0.5)
    plt.ylabel(r'$AIC_{\gamma}-AIC_{no \gamma}$')        
    plt.title(ds)
    plt.ylim((-ylim[j],ylim[j]))
plt.tight_layout()
plt.savefig('supplement_fit_gamma.pdf')