#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jun  7 14:28:32 2019

@author: meyerct6
"""

#Import packages
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from matplotlib.collections import PolyCollection
from matplotlib.patches import Rectangle
from matplotlib.colors import Normalize,colorConverter
from matplotlib.colorbar import ColorbarBase
import matplotlib.cm as cm
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
# Based on code written by Alex Lubbuck for the PyDRC package
from scipy.stats import ttest_ind
import pandas as pd

from SynergyCalculator.NDHillFun import Edrug1D,Edrug2D_NDB
from SynergyCalculator.calcOtherSynergyMetrics import get_params,bliss,zimmer,zimmer_effective_dose,combination_index
from SynergyCalculator.initialFit import init_fit
from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data

import os
import sys
sys.path.insert(0, '../../Plot_Functions')
from plotFun import surfGen
from plotFun import matplotlibArbitrarySurface

# =============================================================================
# REad in data
# =============================================================================

i = 'oneil_anticancer'
T = pd.read_csv('../../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)
T['geo_h'] = np.sqrt(T['h1']*T['h2'])

for k in T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
        
df = T.copy()     
################################################################################
################################################################################
#Zimmer and CI errors
xaxis = "avg_Emax"
df[xaxis] = df[['E1','E2']].mean(axis=1)

xlim=(0.,1.)
ylim=(-2,2)

df[((df[['E1_ci_up','E2_ci_up']]<.55).any(axis=1))&((df[['E1_ci_lw','E2_ci_lw']]>.45).any(axis=1))&(df['zimmer_R2']<.15)&(df['zimmer_R2']>0)&(df['beta']>.20)][['drug1_name','drug2_name','zimmer_R2']]
elev=20;azim=70

i = 13963
sub_T       = df.loc[i]
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:]=0.01
title = r'MuSyC R2:%.2f'%sub_T['R2'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$E1$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['E1'],sub_T['E1_ci_up'],sub_T['E1_ci_lw']) +'\n'+r'$E2$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['E2'],sub_T['E2_ci_up'],sub_T['E2_ci_lw'])
[fig1,fig2],[ax_surf,ax1,ax2],[tt,yy,zz],surf = matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=None, zlim=(0.0,1.1), zero_conc=0,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))

ax_surf.view_init(elev=elev,azim=azim)
fname = drug1_name + '_' + drug2_name + '_' + sample +'.pdf'
fig1.savefig(fname,pad_inches=0.,format='pdf')
fname = drug1_name + '_' + drug2_name + '_' + sample + '_slices.pdf'
fig2.savefig(fname,pad_inches=0.,format='pdf')

E_fix = None
E_bnd = [[.99,0.,0.,0.,0.],[1.01,2.5,2.5,2.5]]
E_fx = [np.nan,np.nan,np.nan,np.nan] if E_fix is None else E_fix
E_bd = [[-np.inf,-np.inf,-np.inf,-np.inf],[np.inf,np.inf,np.inf,np.inf]] if E_bnd is None else E_bnd
   
sub_T = init_fit(sub_T.to_dict(),d1,d2,dip,dip_sd,drug1_name,drug2_name,E_fx,E_bd)  
C1 = 10**sub_T['log_C1']
C2 = 10**sub_T['log_C2']
h1 = 10**sub_T['log_h1']
h2 = 10**sub_T['log_h2']
C1,C2,h1,h2,a12, a21, _, R2,loglike = zimmer(d1,d2,dip,sigma=dip_sd,p0=[C1,C2,h1,h2,0.,0.],vector_input=True)

zero_conc = 0
N=100
d1_min=np.log10(sub_T['min_conc_d1']);d1_max=np.log10(sub_T['max_conc_d1'])
d2_min=np.log10(sub_T['min_conc_d2']);d2_max=np.log10(sub_T['max_conc_d2'])
y = np.linspace(d1_min-zero_conc, d1_max ,N)
t = np.linspace(d2_min-zero_conc, d2_max ,N)
yy,tt=np.meshgrid(y,t)
zz = zimmer_effective_dose(10**yy, 10**tt, C1, C2, h1, h2, a12, a21, logspace=False)
title = r'Zimmer R2:%.2f'%sub_T['zimmer_R2']+'\n'+r'$a12=%.2f$'%(sub_T['zimmer_raw_a1'])+ '\n' + r'$a21 = %.2f$'%(sub_T['zimmer_raw_a2'])
fig1,ax = matplotlibArbitrarySurface(tt,yy,zz,d1,d2,dip,dip_sd,drug1_name,drug2_name,fname=None,metric_name='Percent',zlim=(0.0,1.1),zero_conc=zero_conc,title=title,d1lim=None,d2lim=None,fmt='pdf',plt_zeropln=False,plt_data=True,figsize_surf=(2.2,2.4))
fname = drug1_name+'_'+drug2_name+'_'+sample+'_zimmer.pdf'
elev=20;azim=70
ax.view_init(elev=elev,azim=azim)
fig1.savefig(fname,pad_inches=0.,format='pdf')

# =============================================================================
# Now combination index
# =============================================================================


i = 8679#117
sub_T       = df.loc[i]
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:]=0.01
title = r'MuSyC R2:%.2f'%sub_T['R2'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
[fig1,fig2],[ax_surf,ax1,ax2],[tt,yy,zz],surf = matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=None, zlim=(0.0,1.1), zero_conc=0,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))

ax_surf.view_init(elev=elev,azim=azim)
fname = drug1_name + '_' + drug2_name + '_' + sample +'.pdf'
fig1.savefig(fname,pad_inches=0.,format='pdf')
fname = drug1_name + '_' + drug2_name + '_' + sample + '_slices.pdf'
fig2.savefig(fname,pad_inches=0.,format='pdf')



###Now show combination index plots
CI,d1_full,d2_full,Dm1,Dm2,m1,m2  = combination_index(d1,d2,dip)
def ll2(d,h,C):
    return 1/(1+(d/C)**h)
from SynergyCalculator.NDHillFun import Edrug1D

plt.figure(figsize=(3,2))
ax1=plt.subplot(121)
ax2=plt.subplot(122)
lw=2;sz=20
d1_tmp = d1[d2==0];d2_tmp=d2[d1==0]
d1_tmp[d1_tmp==0]=min(d1[d1!=0])/10.; d2_tmp[d2_tmp==0]=min(d2[d2!=0])/10.
ax1.scatter(np.log10(d1_tmp),dip[d2==0],s=sz,c='k')
d1_tmp = np.logspace(np.log10(min(d1_tmp)),np.log10(max(d1_tmp)),100)
ax1.plot(np.log10(d1_tmp),ll2(d1_tmp,m1,Dm1),c='r',label='CI',lw=lw)
ax1.plot(np.log10(d1_tmp),Edrug1D(d1_tmp,*[sub_T[i] for i in ['E0','E1','C1','h1']]),c='b',label='MuSyC',lw=lw)
ax1.set_ylim((0,1.1))
ax1.set_xlabel('log(%s)'%drug1_name)
ax1.set_ylabel('% Viability')
ax1.spines['top'].set_visible(False)
ax1.spines['right'].set_visible(False)

ax2.scatter(np.log10(d2_tmp),dip[d1==0],s=sz,c='k')
d2_tmp = np.logspace(np.log10(min(d2_tmp)),np.log10(max(d2_tmp)),100)
ax2.plot(np.log10(d2_tmp),ll2(d2_tmp,m2,Dm2),c='r',label='CI',lw=lw)
ax2.plot(np.log10(d2_tmp),Edrug1D(d2_tmp,*[sub_T[i] for i in ['E0','E2','C2','h2']]),c='b',label='MuSyC',lw=lw)
ax2.set_ylim((.0,1.1))
ax2.legend()
ax2.set_xlabel('log(%s)'%drug2_name)
ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)
ax2.set_yticklabels([])
plt.tight_layout()
plt.savefig('CI_misfits.pdf')







