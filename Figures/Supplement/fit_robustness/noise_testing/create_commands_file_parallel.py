#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Aug  9 13:08:25 2019

@author: meyerct6
"""

import glob

fils = glob.glob('SynCode_template_*.py')
for f in fils:
    cstr = 'python < ' + f + ' && rm ' + f +'\n'
    with open('commands.txt','a') as cfile:
        cfile.write(cstr)
        
