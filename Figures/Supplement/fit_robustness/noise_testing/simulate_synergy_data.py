#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 31 09:21:03 2020

@author: meyerct6
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Sep  8 13:43:32 2018

@author: xnmeyer
Designing experiment to test how noise, coverage of the surface, and density of sampling 
impact parameter certainty
"""
# =============================================================================
# Import packages
# =============================================================================
import numpy as np
import pandas as pd
import os
import matplotlib.pylab as plt
from SynergyCalculator.NDHillFun import Edrug2D_NDB_hill

#Vary noise, sampling density, number of replicates, maximal tested dose, beta, alpha, and gamma
noise = [.1,.01,.001]
data_mat = [5,10,20]
sample_type = ['matrix','edge','radial','css']
noise_type = ['homoscedastic','heteroscedastic']
beta = np.linspace(-.5,.5,5)
alpha = np.logspace(-2,2,5)
gamma = np.logspace(-1,1,3)
num_samp = 10

#Parameters that are the same for all experiments
E0 = 1.;E1 = .334;E2 = .334
h1 = 1.;h2 = 1.
C1 = 1e-5;C2 = 1e-5
r1 = 100.;r2 = 100.


cnt = 0;
for N in data_mat:
    for st in sample_type:
        for n in noise:
            for nt in noise_type:
                print(cnt/float(54000))
                for b in beta:
                    for a in alpha:
                        for g in gamma:
                            for rep in range(num_samp):
                                cnt = cnt + 1
                                for rp in range(3):
                                    df_tmp = pd.DataFrame()  
                                    E3 = -b*(E0-min(E1,E2))+min(E1,E2)
                                    alpha1 = a
                                    alpha2 = a
                                    
                                    gamma1 = g
                                    gamma2 = g
                                    if st == 'matrix':
                                        DD1,DD2 = np.meshgrid(np.concatenate(([0],np.logspace(-10,0,N-1))),np.concatenate(([0],np.logspace(-10,0,N-1))))
                                        d1 = DD1.reshape((-1,))
                                        d2 = DD2.reshape((-1,))
                                    elif st == 'edge':
                                        d1 = np.concatenate((np.zeros(int(N**2/4)),np.logspace(-10,0,int(N**2/4)),np.ones(int(N**2/4))*1.,np.logspace(0,-10,int(N**2/4))))
                                        d2 = np.concatenate((np.logspace(-10,0,int(N**2/4)),np.ones(int(N**2/4))*1.,np.logspace(0,-10,int(N**2/4)),np.zeros(int(N**2/4))))
                                    elif st == 'radial':
                                        d1 = np.logspace(-10,0,int(N**2/5))
                                        d2 = np.zeros(int(N**2/5))
                                        d1 = np.concatenate((d1,np.zeros(int(N**2/5))))
                                        d2 = np.concatenate((d2,np.logspace(-10,0.,int(N**2/5))))
                                        d1 = np.concatenate((d1,np.logspace(-10,0.,int(N**2/5))))
                                        d2 = np.concatenate((d2,np.logspace(-10,0.,int(N**2/5))))
                                        d1 = np.concatenate((d1,1/2.*np.logspace(-10,0.,int(N**2/5))))
                                        d2 = np.concatenate((d2,np.logspace(-10,0.,int(N**2/5))))
                                        d1 = np.concatenate((d1,np.logspace(-10,0.,int(N**2/5))))
                                        d2 = np.concatenate((d2,1/2.*np.logspace(-10,0.,int(N**2/5))))   
                                    elif st == 'css':
                                        d1 = np.logspace(-10,0,int(N**2/4))
                                        d2 = np.zeros(int(N**2/4))
                                        d1 = np.concatenate((d1,np.zeros(int(N**2/4))))
                                        d2 = np.concatenate((d2,np.logspace(-10,0.,int(N**2/4))))
                                        d1 = np.concatenate((d1,np.ones(int(N**2/4))*C1))
                                        d2 = np.concatenate((d2,np.logspace(-10,0.,int(N**2/4))))
                                        d2 = np.concatenate((d2,np.ones(int(N**2/4))*C2))
                                        d1 = np.concatenate((d1,np.logspace(-10,0.,int(N**2/4))))                                    
                                                             
                        
                                    dip = Edrug2D_NDB_hill((d1,d2), E0, E1, E2, E3, r1, r2, C1, C2, h1, h2, alpha1, alpha2, gamma1, gamma2)
                            
                                    if nt == 'homoscedastic':                                            
                                        dip = np.random.randn(len(dip))*n+dip
                                    if nt == 'heteroscedastic':
                                        dip = np.random.randn(len(dip))*n*abs(dip)+dip    
                                                                                    
                                    df_tmp['drug1.conc']=d1
                                    df_tmp['drug2.conc']=d2
                                    df_tmp['effect'] = dip
                                    df_tmp['sample'] = str(N)+'_'+st+'_'+str(n)+'_'+nt+'_'+str(b)+'_'+str(a)+'_'+str(g)+'_'+str(rep)
                                    df_tmp['expt.date'] = '00-00-0000'
                                    df_tmp['drug1']='d1'
                                    df_tmp['drug2']='d2'                 
                                    df_tmp['drug1.units'] = 'uM'
                                    df_tmp['drug2.units'] = 'uM'
                                    df_tmp['effect.95ci'] = .00001
                                    if rp==0:
                                        df_tmp.to_csv('data/testing_data_'+str(cnt)+'.csv',index=False)
                                    else:
                                        df_tmp.to_csv('data/testing_data_'+str(cnt)+'.csv',index=False,mode='a',header=False)
                                    
    
      
    
