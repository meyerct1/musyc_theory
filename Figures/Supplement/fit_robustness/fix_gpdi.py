#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 12 07:23:11 2020

@author: meyerct6
"""



import pandas as pd
import numpy as np
from SynergyCalculator.gatherData import subset_data

df = pd.read_csv('MasterResults.csv')
df['gpdi_bliss_mean_ab'] = np.nan
df['gpdi_bliss_mean_ba'] = np.nan
df['gpdi_loewe_mean_ab'] = np.nan
df['gpdi_loewe_mean_ba'] = np.nan

for i in df.index:
    print(i)
    expt = df.loc[i,'expt']
    data = pd.read_csv(expt)
    data['sample']=data['sample'].str.upper()
    d1,d2,_,_=subset_data(data,df.loc[i,'drug1_name'],df.loc[i,'drug2_name'],df.loc[i,'sample'])
    d1s = d1[(d1!=0)&(d2!=0)]
    d2s = d2[(d1!=0)&(d2!=0)]
    EIAB_bliss = df.loc[i,'gdpi_bi_eab']
    EIBA_bliss = df.loc[i,'gdpi_bi_eba']
    EIAB_loewe = df.loc[i,'gdpi_la_eab']
    EIBA_loewe = df.loc[i,'gdpi_la_eba']

    IAB_bliss = df.loc[i,'gdpi_bi_iab']
    IBA_bliss = df.loc[i,'gdpi_bi_iba']
    IAB_loewe = df.loc[i,'gdpi_la_iab']
    IBA_loewe = df.loc[i,'gdpi_la_iba']
    
    df.loc[i,'gpdi_bliss_mean_ab'] = np.nanmean(1.+(IAB_bliss*d2s**1)/(EIAB_bliss**1+d2s**1))
    df.loc[i,'gpdi_bliss_mean_ba'] = np.nanmean(1.+(IBA_bliss*d1s**1)/(EIBA_bliss**1+d1s**1))
    df.loc[i,'gpdi_loewe_mean_ab'] = np.nanmean(1.+(IAB_loewe*d2s**1)/(EIAB_loewe**1+d2s**1))
    df.loc[i,'gpdi_loewe_mean_ba'] = np.nanmean(1.+(IBA_loewe*d1s**1)/(EIBA_loewe**1+d1s**1))
    

df.to_csv('MasterResults.csv',index=False)


