#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 12:33:16 2020

@author: meyerct6
"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
import matplotlib.cm as cm
from matplotlib import rc

rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

import pandas as pd
import numpy as np 
df = pd.read_csv('MasterResults.csv')

df['num_doses'] = [int(i.split('_')[0]) for i in df['sample']]
df['sample_design'] = [str(i.split('_')[1]) for i in df['sample']]
df['noise'] = [float(i.split('_')[2]) for i in df['sample']]
df['noise_profile'] = [str(i.split('_')[3]) for i in df['sample']]
df['beta_true'] = [float(i.split('_')[4]) for i in df['sample']]
df['alpha_true'] = [float(i.split('_')[5]) for i in df['sample']]
df['gamma_true'] = [float(i.split('_')[6]) for i in df['sample']]
df['sample_id'] = ['_'.join((i.split('_')[4:7])) for i in df['sample']]
df['sample_id_full'] = ['_'.join((i.split('_')[0:4])) for i in df['sample']]
df['hsa_fit_ec50'] = df['hsa_fit_ec50'].fillna(-1000)
df['hsa_fit_ec50'] = [eval(i)[0] if not i==-1000 else np.nan for i in df['hsa_fit_ec50']]

for k in ['gpdi_bliss_mean_ab','gpdi_loewe_mean_ab','gpdi_bliss_mean_ba','gpdi_loewe_mean_ba']:
    df[k] = np.log10(df[k])
    df.loc[np.isinf(df[k]),k]=np.nan
df.loc[df['ci_raw_mean']>200.,'ci_raw_mean']=np.nan

zscore = lambda x: abs(x - x.mean(skipna=True)) / x.std(skipna=True)

keys = ['beta','log_alpha1','log_gamma1','ci_raw_mean','zip_raw','zimmer_raw_a1','brd_delta','brd_kappa','gpdi_bliss_mean_ab','gpdi_loewe_mean_ab']
labels = [r'$\beta$',r'log($\alpha$)',r'log($\gamma$)','CI','ZIP','Eff. Dose',r'$\Delta$ BRAID',r'$\kappa$ BRAID',r'$GDPI_{Loewe}$',r'$GDPI_{Bliss}$']
color = cm.nipy_spectral(np.linspace(0,1,len(keys)))
plt.figure(figsize=(2.5,9))
ax = []
for e,k in enumerate(keys):
    ax.append(plt.subplot(len(keys),1,e+1))
    df[k+'_zscore'] = np.nan
    for i in df['sample_id'].unique():
        df.loc[df['sample_id']==i,k+'_zscore'] = df.loc[df['sample_id']==i,k].transform(zscore)
    leg = True if e==0 else False
    df.groupby(['noise','noise_profile'])[k+'_zscore'].mean().unstack(1).plot(kind='barh',ax=ax[-1],legend=leg,color=['g','r'],alpha=1)
    plt.xlim((0,2))
    if e!=14:
        plt.xticks([])
    else:
        plt.xticks([0,1,2])
        plt.xlabel('Avg. Z-score\n<---More robust       Less robust----->')
    if e!=0:
        plt.yticks([])
        plt.ylabel(labels[e],labelpad=30)
    else:
        plt.ylabel(labels[e])

plt.tight_layout()
plt.subplots_adjust(wspace=0,hspace=0)
plt.savefig('robustness_noise.pdf')


plt.figure(figsize=(2.5,9))
ax = []
for e,k in enumerate(keys):
    ax.append(plt.subplot(len(keys),1,e+1))
    df[k+'_zscore'] = np.nan
    for i in df['sample_id'].unique():
        df.loc[df['sample_id']==i,k+'_zscore'] = df.loc[df['sample_id']==i,k].transform(zscore)
    leg = False
    df.groupby(['num_doses'])[k+'_zscore'].mean().plot(kind='barh',ax=ax[-1],legend=leg,color=[color[e,:]])
    plt.xlim((0,2))
    if e!=14:
        plt.xticks([])
    else:
        plt.xticks([0,1,2])
        plt.xlabel('Avg. Z-score\n<---More robust       Less robust----->')
    if e!=0:
        plt.yticks([])
        plt.ylabel(labels[e],labelpad=30)
    else:
        plt.ylabel(labels[e])

plt.tight_layout()
plt.subplots_adjust(wspace=0,hspace=0)
plt.savefig('robustness_sample.pdf')


plt.figure(figsize=(2.5,9))
ax = []
for e,k in enumerate(keys):
    ax.append(plt.subplot(len(keys),1,e+1))
    df[k+'_zscore'] = np.nan
    for i in df['sample_id'].unique():
        df.loc[df['sample_id']==i,k+'_zscore'] = df.loc[df['sample_id']==i,k].transform(zscore)
    leg = False
    df.groupby(['sample_design'])[k+'_zscore'].mean().plot(kind='barh',ax=ax[-1],legend=leg,color=[color[e,:]])
    plt.xlim((0,2))
    if e!=14:
        plt.xticks([])
    else:
        plt.xticks([0,1,2])
        plt.xlabel('Avg. Z-score\n<---More robust       Less robust----->')
    if e!=0:
        plt.yticks([])
        plt.ylabel(labels[e],labelpad=30)
    else:
        plt.ylabel(labels[e])

plt.tight_layout()
plt.subplots_adjust(wspace=0,hspace=0)
plt.savefig('robustness_sampledesign.pdf')




# =============================================================================
# R2 and convergence plots
# =============================================================================

keys = ['R2',
        'skip','skip','zimmer_R2','brd_R2',
        'gpdi_bi_r2','gpdi_la_r2']

ckeys = ['converge_nlls',
        'ci_raw_mean','zip_raw','zimmer_raw_a1','brd_delta',
        'gpdi_bliss_mean_ab','gpdi_loewe_mean_ab']

labels = [r'MuSyC','CI','ZIP','Eff. Dose',r'BRAID',
          r'$GDPI_{Bliss}$',r'$GDPI_{Loewe}$']
color = cm.gist_rainbow(np.linspace(0,1,len(keys)))

df.loc[df['converge_nlls']==0,'converge_nlls']=np.nan
for e,k in enumerate(keys):
    if k!='skip':
        df.loc[df[k]<0.3,ckeys[e]]=np.nan
        df.loc[df[k]<0.3,k] = np.nan
    
    
plt.figure(figsize=(2,2.5))
ax = plt.subplot(111)
for e,k in enumerate(keys):
    if k=='skip':
        plt.text(e,.5,'NA')
    else:
        data = df[k]
        data = data[~((np.isnan(data))|(np.isinf(data)))&(~df[ckeys[e]].isna())]
        vp = plt.violinplot(data,positions=[e],showextrema=False,showmedians=True)
	print('Mean R2 value for ' + labels[e] + ': ' + str(np.nanmean(data)))
        vp['bodies'][0].set_color(color[e,:])
        vp['bodies'][0].set_alpha(1)
        vp['cmedians'].set_color('k')
        
        
plt.xlim((-.5,len(keys)))
plt.xticks(range(len(keys)))
ax.set_xticklabels(labels,rotation=90)
plt.tight_layout()
plt.title('R2 distribution')
plt.savefig('r2_distribution.pdf')



keys = ['converge_nlls',
        'ci_raw_mean','zip_raw','zimmer_raw_a1','brd_delta',
        'gpdi_bliss_mean_ab','gpdi_loewe_mean_ab']
labels = [r'MuSyC','CI','ZIP','Eff. Dose',r'BRAID',
          r'$GDPI_{Bliss}$',r'$GDPI_{Loewe}$']

df.loc[df['converge_nlls']==0,'converge_nlls']=np.nan

plt.figure(figsize=(2,2.5))
ax = plt.subplot(111)
for e,k in enumerate(keys):
	plt.bar(e,sum(~df[k].isna())/float(len(df)),color=color[e,:])
	print('% Convergence for ' + labels[e] + ': ' + str(sum(~df[k].isna())/float(len(df)))) 
plt.xlim((-.5,len(keys)))
plt.xticks(range(len(keys)))
ax.set_xticklabels(labels,rotation=90)
plt.tight_layout()
plt.title('Percent Convergence')
plt.savefig('percent_convergence.pdf')



