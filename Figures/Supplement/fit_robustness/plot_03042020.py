#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 11:53:32 2020

@author: meyerct6
"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
import matplotlib.cm as cm
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

beta = np.linspace(-.5,.5,5)
alpha = np.logspace(-2,2,5)
gamma = np.logspace(-1,1,3)


X,Y,Z = np.meshgrid(beta,alpha,gamma)
X = X.ravel()
Y = Y.ravel()
Z = Z.ravel()
plt.figure(figsize=(3,3))
ax = plt.subplot(111,projection='3d')
ax.scatter(X,np.log10(Y),np.log10(Z))
ax.view_init(azim=50,elev=13)
ax.set_xticks([-.5,0,.5])
ax.set_yticks([-2,0,2])
ax.set_zticks([-1,0,1])
ax.set_xlabel(r'$\beta$')
ax.set_ylabel(r'$log(\alpha)$')
ax.set_zlabel(r'$log(\gamma)$')

plt.savefig('surfaces_tested.pdf')