#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 05:57:02 2020

@author: meyerct6
"""

import pandas as pd
import numpy as np
from scipy.stats import ttest_ind
from glob import glob
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import rc
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from scipy import stats
from matplotlib.ticker import FormatStrFormatter

datasets = ['oneil_anticancer','mott_antimalaria','cokol_antifungal','holbeck_anticancer','tan_antihiv']

fig = plt.figure(figsize=(4,8))
ax = []
for e,ds in enumerate(datasets):
    df = pd.read_csv(ds+'_bias.csv')
    x = np.array(df['bliss'] )
    y = np.array(df['bliss'] - df['bliss_bias'])
    x[np.isinf(x)]=np.nan;y[np.isinf(y)]=np.nan
    xt = x[(~np.isnan(x))&(~np.isnan(y))]
    yt = y[(~np.isnan(x))&(~np.isnan(y))]
    y = yt
    x = xt
    
#    xy = np.vstack([x,y])
#    z = stats.gaussian_kde(xy)(xy)
#    idx = z.argsort()
#    x,y,z = x[idx],y[idx],z[idx]
    ax.append(plt.subplot2grid((5,2),(e,0)))
    if e==2 or e==3:
        ax[-1].text(0,0,'NA',va='center',ha='center')
    else:
        sc = ax[-1].scatter(x,y,edgecolor='',rasterized=True),#c=z,cmap='plasma',edgecolor='')
    if e ==0:
        plt.title('Bliss Bias',fontsize=8)
    plt.ylabel(ds+'\nCorrected Bliss')
    if e==4:
        plt.xlabel('Bliss')
    else:
        plt.xticks([])
    ax[-1].set_aspect('equal','box')
    xlim = ax[-1].get_xlim()
    ylim = ax[-1].get_ylim()
    lim = list(xlim)+list(ylim)
    ax[-1].set_xlim((-max(abs(np.array(lim))),max(abs(np.array(lim)))))
    ax[-1].set_ylim((-max(abs(np.array(lim))),max(abs(np.array(lim)))))
    ax[-1].plot(ax[-1].get_xlim(),ax[-1].get_xlim(),'r--')

    ax[-1].set_yticks([-np.floor(max(abs(np.array(lim)))),0,np.floor(max(abs(np.array(lim))))])
    ax[-1].set_xticks([-np.floor(max(abs(np.array(lim)))),0,np.floor(max(abs(np.array(lim))))])

    
    x = np.array(df['loewe'] )
    y = np.array(df['loewe'] - df['loewe_bias'])
    x[np.isinf(x)]=np.nan;y[np.isinf(y)]=np.nan
    xt = x[(~np.isnan(x))&(~np.isnan(y))]
    yt = y[(~np.isnan(x))&(~np.isnan(y))]
    y = yt
    x = xt
#    xy = np.vstack([x,y])
#    z = stats.gaussian_kde(xy)(xy)
#    idx = z.argsort()
#    x,y,z = x[idx],y[idx],z[idx]
    ax.append(plt.subplot2grid((5,2),(e,1)))
    sc = ax[-1].scatter(x,y,edgecolor='',rasterized=True),#c=z,cmap='plasma',edgecolor='')
    if e ==0:
        plt.title('Loewe Bias',fontsize=8)
    plt.ylabel('Corrected Loewe')
    if e==4:
        plt.xlabel('Loewe')
    else:
        plt.xticks([])
    ax[-1].set_aspect('equal','box')
    xlim = ax[-1].get_xlim()
    ylim = ax[-1].get_ylim()
    lim = list(xlim)+list(ylim)
    ax[-1].set_xlim((-max(abs(np.array(lim))),max(abs(np.array(lim)))))
    ax[-1].set_ylim((-max(abs(np.array(lim))),max(abs(np.array(lim)))))
    ax[-1].plot(ax[-1].get_xlim(),ax[-1].get_xlim(),'r--')

    ax[-1].set_yticks([-np.floor(max(abs(np.array(lim)))),0,np.floor(max(abs(np.array(lim))))])
    ax[-1].set_xticks([-np.floor(max(abs(np.array(lim)))),0,np.floor(max(abs(np.array(lim))))])

   
plt.tight_layout()
plt.savefig('error_magnitude.pdf')