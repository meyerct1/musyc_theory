#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
#Code to calculate other synergy parameters for metrics which results in a hill bias
"""
#Code to calculate loewe, combination index, schindler to show hill bias
import numpy as np
import pandas as pd
from SynergyCalculator.calcOtherSynergyMetrics import loewe,combination_index,bliss
from pythontools.synergy import synergy_tools

# =============================================================================
# Function to calculate Loewe/schindler bias
# =============================================================================
def get_loewe_bias(h1, h2, d1, d2, ec50_1, ec50_2, E0, E1, E2, E3=None, alpha=0.):
    """
    Calculates Loewe for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    l = -np.log10(synergy_tools.loewe(d1, d2, E, E0, E1, E2, h1, h2, C1, C2))
    return l

def get_bliss_bias(h1, h2, d1, d2, ec50_1, ec50_2, E0, E1, E2, E3=None, alpha=1.):
    """
    Calculates schindler for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False)

    if E3 is None: E3 = min(E1, E2)

    E =  synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    e1 = synergy_tools.hill_1D(d1,E0,E1,h1,C1)
    e2 = synergy_tools.hill_1D(d2,E0,E2,h2,C2) 
    b =  synergy_tools.bliss(e1,e2,E)
    return b


# =============================================================================
# Calculate loewe and schindler at each dose and also the corrected bias
# =============================================================================
    
folders = ['oneil_anticancer','mott_antimalaria','cokol_antifungal','holbeck_anticancer','tan_antihiv']
fit_fil = 'MasterResults_mcnlls.csv'
data_files = ['merck_perVia_10-29-2018.csv','mott_compiled_01302020.csv','compiled_cokol_antifungal.csv','almanac_reformatted.csv','tan_anti-hiv-data.csv']
divisor = [1.,100.,np.nan,np.nan,1.]
for e in [len(folders)-1]:
    
    dat_fil = data_files[e]
    
    #File with MuSyC fits
    T = pd.read_csv("../../../Data/" + folders[e]+'/'+fit_fil)
    T = T.loc[T['converge_mc_nlls']==1]
    T = T.loc[T['R2']>.3]
    
    #File with raw data
    df = pd.read_csv("../../../Data/" + folders[e]+'/'+dat_fil)
    
    df = df.loc[(df['drug1.conc']!=0)&(df['drug2.conc']!=0)]
    
    df['drug1'] = [i.lower() for i in df['drug1']]
    df['drug2'] = [i.lower() for i in df['drug2']]
    df['sample'] = [i.upper() for i in df['sample']]
    
    if not np.isnan(divisor[e]):
        df['effect'] = df['effect']/divisor[e]
        for k in ['E0','E1','E2','E3']:
            T[k] = T[k]/divisor[e]
    
    prog = 0
    count = 0
    n_rows = T.shape[0]
    
    df_master = pd.DataFrame()
    #For each drug
    for vv,i in enumerate(T.index):
        count += 100 # Print progress, counting up to 100
        if count/n_rows > prog:
            prog = count/n_rows
            print prog
    
        # Read drug parameters
        cline, drug1, drug2, h1, h2, E0, E1, E2, E3, C1, C2, mxd1, mxd2, cmn, un = T.loc[i, ['sample', 'drug1_name', 'drug2_name', 'h1', 'h2', 'E0', 'E1', 'E2','E3', 'log_C1', 'log_C2','max_conc_d1','max_conc_d2','converge_mc_nlls','drugunique']]
        C1 = np.power(10., C1)
        C2 = np.power(10., C2)
        
        df_sub = df.loc[(df['drug1']==drug1) & (df['drug2']==drug2) & (df['sample']==cline)][['drug1','drug2','drug1.conc','drug2.conc','effect','sample']]
        df_sub.reset_index(drop=True,inplace=True)

        # Read the data
        d1c = df_sub['drug1.conc']
        d2c = df_sub['drug2.conc']
        E = df_sub['effect']
        
        # Calculate synergy and population dataframe
        lw = -np.log10(loewe(d1c, d2c, E, E0, E1, E2, h1, h2, C1, C2))
        if not np.isnan(divisor[e]):
            bl = bliss(synergy_tools.hill_1D(d1c,E0,E1,h1,C1),synergy_tools.hill_1D(d2c,E0,E2,h2,C2),E)
        else:
            bl = np.nan * np.zeros(len(d1c))
        df_sub["loewe"] = lw
        df_sub['bliss'] = bl
        
        df_sub['drugunique'] = un
                
        loewe_bias = get_loewe_bias(h1, h2, d1c, d2c, C1, C2, E0, E1, E2, E3=E3)
        if not np.isnan(divisor[e]):
            bliss_bias = get_bliss_bias(h1, h2, d1c, d2c, C1, C2, E0, E1, E2, E3=E3)
        else:
            bliss_bias = np.nan*np.zeros(len(d1c))

        
        df_sub["loewe_bias"] =  loewe_bias
        df_sub["bliss_bias"] =  bliss_bias
        
        if vv == 0:
            df_sub.to_csv(folders[e]+'_bias.csv',index=False)
        else:
            df_sub.to_csv(folders[e]+'_bias.csv',mode='a',header=False,index=False)
    

    
