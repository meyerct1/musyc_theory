#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  8 08:30:05 2019

@author: xnmeyer
"""

import numpy as np
import pandas as pd
from SynergyCalculator.NDHillFun import Edrug2D_NDB_hill
from SynergyCalculator.doseResponseSurfPlot import plotDoseResponseSurf, matplotlibDoseResponseSurface,matplotlibDoseResponseSlices
import matplotlib.pyplot as plt
log_g_ran = (-.5,.4)
log_a_ran = (-3,3)
b_ran     = (-.5,1.)

test_alpha = np.concatenate((np.ones(2),[10**i for i in log_a_ran],np.ones(2))) 
test_gamma = np.concatenate((np.ones(2),np.ones(2),[10**i for i in log_g_ran])) 
test_beta  = np.concatenate((b_ran,np.zeros(2),np.zeros(2)))

alpha = np.concatenate((np.ones(1),np.logspace(0,log_a_ran[1],50),10**(log_a_ran[1])*np.ones(15),np.logspace(log_a_ran[1],log_a_ran[0],100),10**log_a_ran[0]*np.ones(15),np.logspace(log_a_ran[0],0,50)))
gamma = np.concatenate((np.ones(1),np.logspace(0,log_g_ran[1],50),10**(log_g_ran[1])*np.ones(15),np.logspace(log_g_ran[1],log_g_ran[0],100),10**log_g_ran[0]*np.ones(15),np.logspace(log_g_ran[0],0,50)))
beta  = np.concatenate((np.zeros(0),np.linspace(0,1,50),np.ones(15),np.linspace(1,-.5,100),-.5*np.ones(15),np.linspace(-.5,0,50)))
    
len_alpha = len(alpha)
len_gamma = len(gamma)
len_beta = len(beta)

alpha = np.concatenate((np.ones(len_beta),alpha,np.ones(len_gamma)))
gamma = np.concatenate((np.ones(len_beta),np.ones(len_alpha),gamma))
beta  = np.concatenate((beta,np.zeros(len_alpha),np.zeros(len_gamma)))


#Testing
def genData(a,b,g,cnt):
    E0 = 0.05; E1 = 0.00; E2 = 0.03
    h1 = .8; h2 = .8
    C1 = 1e-5; C2 = 1e-5
    noise = .01;    N = 7;    rep = 1
    df = {}
    df['drug1_name']='d1'
    df['drug2_name']='d2'
    df['expt'] = 'cartoon'
    df['sample']='cell_line'
    df['E0']=E0; df['E1']=E1
    df['E2']=E2; df['E3']=-b*(E0-min(E1,E2))+min(E1,E2);
    df['h1']=h1;   df['h2']=h2
    df['C1']=C1;df['C2']=C2
    df['log_C1']=np.log10(C1);df['log_C2']=np.log10(C2)
    df['r1']=100.; df['r2']=100.
    
    df['log_alpha1']=0; df['log_alpha2']=np.log10(a)
    df['log_gamma1']=0; df['log_gamma2']=np.log10(g)
    d1 = [];d2 = [];dip = []
    for r in range(rep):
        DD1,DD2 = np.meshgrid(np.concatenate(([0],np.logspace(-10,0,N))),np.concatenate(([0],np.logspace(-10,0,N))))
        d1_tmp = DD1.reshape((-1,))
        d2_tmp = DD2.reshape((-1,))
        dip_tmp = Edrug2D_NDB_hill((d1_tmp,d2_tmp),df['E0'],df['E1'],df['E2'],df['E3'],
                                   df['r1'],df['r2'],10**df['log_C1'],10**df['log_C2'],df['h1'],df['h2'],
                                   10**df['log_alpha1'],10**df['log_alpha2'],10**df['log_gamma1'],10**df['log_gamma2'])
        dip_tmp = np.random.randn(len(dip_tmp))*noise+dip_tmp
        d1 = np.concatenate((d1,d1_tmp))
        d2 = np.concatenate((d2,d2_tmp))
        dip = np.concatenate((dip,dip_tmp))
    dip_sd = np.zeros(len(dip))+noise*np.random.randn(len(dip))
    df['min_conc_d1']=min(d1[d1!=0]);df['min_conc_d2']=min(d2[d2!=0])
    df['max_conc_d1']=max(d1) ;df['max_conc_d2']=max(d2)
    df['save_direc'] = 'surf_movie'
    df['metric_name'] = 'Effect'
    title = ''
    if b>0:
        title = title + 'Synergistic Efficacy '
    if b<0:
        title = title + 'Antagonistic Efficacy '
    if a>1:
        title = title + 'Synergistic Potency '
    if a<1:
        title = title + 'Antagonistic Potency '
    if g>1:
        title = title + 'Synergistic Cooperativity '
    if g<1:
        title = title + 'Antagonistic Cooperativity '
    matplotlibDoseResponseSurface(pd.DataFrame([df]),d1,d2,dip,dip_sd,fname=None, zlim=(-0.05,0.05), zero_conc=1,title=title,d1lim=None,d2lim=None,fmt='png',plt_slices=False,plt_zeropln=False,plt_data=False,incl_gamma=True)
    plt.title(title)
    plt.savefig('surf_movie/surf_'+str(cnt)+'.png',pad_inches=0.,format='png');plt.close()
    cnt = cnt + 1
    return cnt

#Testing 
cnt = 0
for a,b,g in zip(test_alpha,test_beta,test_gamma):
    cnt = genData(a,b,g,cnt) 

cnt = 0
for a,b,g in zip(alpha,beta,gamma):
    cnt = genData(a,b,g,cnt)
    
    

alpha = [1000.,1000.,.001,.001]
gamma = [1,1,1,1]
beta  = [1,-.5,-.5,1]
cnt = 0
for a,b,g in zip(alpha,beta,gamma):
    cnt = genData(a,b,g,cnt)
    