#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 23 12:50:20 2019

@author: xnmeyer
"""

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  3 16:42:04 2018

@author: xnmeyer
"""
#Import packages
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
from matplotlib.collections import PolyCollection
from matplotlib.patches import Rectangle
from matplotlib.colors import Normalize,colorConverter
from matplotlib.colorbar import ColorbarBase
import matplotlib.cm as cm
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
# Based on code written by Alex Lubbuck for the PyDRC package
from scipy.stats import ttest_ind
import pandas as pd

from SynergyCalculator.NDHillFun import Edrug1D,Edrug2D_NDB

import sys
sys.path.insert(0, '../Plot_Functions/')
from plotFun import surfGen

# =============================================================================
# Show the theoretical bias in bliss due to percent error
# =============================================================================
#Set parameters for surfaces
d1 = np.logspace(-5,0,50); d2 = np.logspace(-5,0,50);
h1 = 1.; h2 = 1.;
C1 = .005; C2 = .001;
E0 = 1.; E1 = .5; E2 = .5;E3=.5;
alpha1 = 1.; alpha2 = 1.;
r1 = 100.*E0; r2 = 100.*E0;

fig = plt.figure(figsize=(5,2))
zlim = (0.,1)
yy,tt = np.meshgrid(d1,d2)
ax = []
for e1,E in enumerate([.1,.5,.9]):
    ax.append(fig.add_subplot(1,3,e1+1,projection='3d',))
    bl = np.array(np.matrix(Edrug1D(d2,E0,E,C2,h2)).T*np.matrix(Edrug1D(d1,E0,E,C1,h1)))
    ax[-1] = surfGen(ax[-1],yy,tt,bl,zlim)
    ax[-1].set_zlim(zlim);ax[-1].set_xticks([]);ax[-1].set_yticks([])
    ax[-1].set_zticks([0,.5,1.])

for e1 in [1,2]:
    ax[e1].set_zticklabels([]);ax[e1].set_zlabel('')
    ax[e1].set_xlabel('');ax[e1].set_ylabel('')
plt.subplots_adjust(wspace=-.1)

plt.show()
plt.savefig('bliss_theory_bias.pdf', bbox_inches = 'tight',pad_inches = 0)


N=50
mat = np.ones((N,N))
for e1,E1 in enumerate(np.linspace(0,1,N)):
    for e2,E2 in enumerate(np.linspace(0,1,N)):
            E3 = min(E1,E2)
            bl = np.array(np.matrix(Edrug1D(d2,E0,E2,C2,h2)).T*np.matrix(Edrug1D(d1,E0,E1,C1,h1)))
            mat[e1,e2]=E3-bl[-1,-1]

plt.figure(figsize=(2.5,2))
X,Y = np.meshgrid(np.linspace(0,1,N),np.linspace(0,1,N))
ax = plt.subplot(111)
plt.pcolor(X,Y,mat,cmap=cm.plasma)
plt.colorbar()
ax.set_xlabel('E1')
ax.set_ylabel('E2')
#ax.set_title(r'$\Delta$=Null Emax (MuSyC)'+'\n-\nNull Emax (Bliss)')
plt.tight_layout()
plt.savefig('bliss_theory_bias_full.pdf', bbox_inches = 'tight',pad_inches = 0)


# =============================================================================
# Now show bias is in data
# =============================================================================
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)


from SynergyCalculator.calcOtherSynergyMetrics import bliss
for i in T.index:
    E_mxd1d2 = Edrug2D_NDB((T.loc[i,'max_conc_d1'],T.loc[i,'max_conc_d2']),T.loc[i,'E0'],T.loc[i,'E1'],T.loc[i,'E2'],T.loc[i,'E3'],T.loc[i,'r1'],T.loc[i,'r2'],T.loc[i,'C1'],T.loc[i,'C2'],10**T.loc[i,'log_h1'],10**T.loc[i,'log_h2'],10**T.loc[i,'log_alpha1'],10**T.loc[i,'log_alpha2'])
    T.loc[i,'bliss_fit_mxd1d2'] = bliss(T.loc[i,'E1_obs'],T.loc[i,'E2_obs'],E_mxd1d2)


df = T
df = df[~np.isnan(df[['bliss_fit_mxd1d2']].mean(axis=1))]
df = df[(df['E1']<1.)&(df['E2']<1.)]
df= df.reset_index(drop=True)

from matplotlib.path import Path
m1 = Path([(.35,.35),(.35,.65),(.65,.65),(.65,.35)])
m2 = Path([(0.1,0.1),(0.1,.9),(.9,.9),(.9,.1)])

msk1 = m1.contains_points(np.array(df[['E1','E2']]))
msk2 = (m2.contains_points(np.array(df[['E1','E2']]))&~msk1)
msk3 = ~msk1&~msk2

z1 = df['bliss_fit_mxd1d2'].loc[msk1].median()
z2 = df['bliss_fit_mxd1d2'].loc[msk2].median()
z3 = df['bliss_fit_mxd1d2'].loc[msk3].median()
z_k = [z1,z2,z3]
norm = Normalize(vmin=min(z_k), vmax=max(z_k))
cols = cm.cool(norm(z_k))

plt.figure(figsize=(2.25,2.5),facecolor='w')
ax = plt.subplot2grid((20,1),(0,0),rowspan=17,projection='3d')
cax = plt.subplot2grid((20,1),(19,0))
ax.xaxis.set_major_locator(plt.MaxNLocator(3))
ax.yaxis.set_major_locator(plt.MaxNLocator(3))
ax.zaxis.set_major_locator(plt.MaxNLocator(3))
ax.set_zlim((min(z_k),max(z_k)))
ax.set_xlim((0,df[['E1','E2']].max(axis=0).mean()))
ax.set_ylim((0,df[['E1','E2']].max(axis=0).mean()))

ax.add_collection3d(Poly3DCollection([[(.25,.25,z1),(.25,.75,z1),(.75,.75,z1),(.75,.25,z1)]],facecolor=cols[0,:],alpha=.75))
ax.add_collection3d(Poly3DCollection([[(.9,.9,z2),(.1,.9,z2),(.1,.1,z2),(.25,.25,z2),(.25,.75,z2),(.75,.75,z2),(.75,.25,z2),(.25,.25,z2),(.1,.1,z2),(.9,.1,z2),(.9,.9,z2)]],facecolor=cols[1,:],alpha=.75))
ax.add_collection3d(Poly3DCollection([[(1,1,z3),(0,1,z3),(0,0,z3),(.1,.1,z3),(.1,.9,z3),(.9,.9,z3),(.9,.1,z3),(.1,.1,z3),(0,0,z3),(1,0,z3),(1,1,z3)]],facecolor=cols[2,:],alpha=.75))
#ax.scatter(df['E1'],df['E2'],df['bliss_fit_mxd1d2'],s=.1,c=df['bliss_fit_mxd1d2'],alpha=.2,)
cb1 = ColorbarBase(cax, 
                   cmap=cm.cool,
                   norm=norm,
                   orientation='horizontal')
cb1.set_label('<---Ant  Syn--->\nMedian Bliss @ max(d1,d2)',labelpad=8)
cb1.set_ticks([0])
ax.set_title('ONeil et al.\nAnti Cancer Combinations',fontsize=8)
ax.set_xlabel('E1 (% viable)',labelpad=-6)
ax.set_ylabel('E2 (% viable)',labelpad=-6)
ax.set_zlabel('Bliss',labelpad=-2)
#ax.plot([ax.get_xlim()[0],ax.get_xlim()[0]],ax.get_ylim(),[0,0],'k')
#ax.plot(ax.get_xlim(),[ax.get_ylim()[1],ax.get_ylim()[1]],[0,0],'k')
ax.tick_params(pad=0)
ax.tick_params(axis='x',pad=-4)
ax.tick_params(axis='y',pad=-4)
ax.view_init(26,-12)
plt.tight_layout()
plt.savefig('bliss_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)

################################################################################
# Panel C
################################################################################
cell_line = []
drug =[]
emax=[]
emax_sd=[]
ec50=[]
ec50_sd=[]
bliss = []

for c in df['sample'].unique():
    sub_df = df[df['sample']==c]
    dgls = np.unique(list(sub_df['drug2_name'].unique())+list(sub_df['drug1_name'].unique()))
    for d in dgls:
        emax.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['E1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['E2'].mean()))))
        cell_line.append(c)
        drug.append(d)
        ec50.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['C1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['C2'].mean()))))

sensitivity_map = pd.DataFrame({'sample':cell_line,'drug':drug,'emax':emax}) 
        
sens_map = sensitivity_map.groupby('drug')[['emax']].mean().merge(pd.read_csv('drug_classes.csv'),left_on='drug',right_on='drug_name')
sens_map = sens_map.sort_values('emax').reset_index(drop=False)

grp_cols = ['#fffe40','aqua','springgreen']
fig = plt.figure(figsize=(6,3.5),facecolor='w')
ax = []
ax.append(plt.subplot2grid((13,14),(2,2),colspan=7,rowspan=7))
ax.append(plt.subplot2grid((13,14),(0,2),colspan=7,rowspan=2))
ax.append(plt.subplot2grid((13,14),(2,9),colspan=2,rowspan=7))
ax.append(plt.subplot2grid((13,14),(5,11),colspan=2,rowspan=5))
ax.append(plt.subplot2grid((13,14),(9,2),colspan=3,rowspan=3))
ax.append(plt.subplot2grid((13,14),(3,1),colspan=1,rowspan=5))

mat = np.zeros((len(sens_map),len(sens_map)))*np.nan
for e1,d1 in enumerate(sens_map['drug_name']):
    for e2,d2 in enumerate(sens_map['drug_name']):
        if e1!=e2:
            msk = ((df['drug1_name']==d1)&(df['drug2_name']==d2))|((df['drug1_name']==d2)&(df['drug2_name']==d1))
            mat[e1,e2] = df.loc[msk,'bliss_fit_mxd1d2'].median()

to_remove = np.where(np.sum(np.isnan(mat),axis=0)>15)[0]
mat=np.delete(mat,to_remove,axis=1)
mat=np.delete(mat,to_remove,axis=0)

sens_map = sens_map.loc[~np.in1d(sens_map.index.values,to_remove)]
sens_map.reset_index(drop=True,inplace=True)
norm = Normalize(np.nanmin(mat),np.nanmax(mat))
hm = ax[0].pcolor(mat,cmap=cm.seismic,vmin=np.nanmin(mat), vmax=np.nanmax(mat))
hm.cmap.set_under('k')
plt.sca(ax[0])
plt.axis('off')

cb1 = ColorbarBase(ax[-1], 
                   cmap=cm.seismic,
                   norm=norm,
                   orientation='vertical',ticklocation='left')
cb1.set_label('<---Ant  Syn--->\nMedian Bliss @ max(d1,d2)',labelpad=1)
cb1.set_ticks([-.1,0,.1])

mask = []
mask.append((sens_map['emax']>.35)&(sens_map['emax']<.65))
mask.append((sens_map['emax']>.1)&(sens_map['emax']<.9) & ~mask[-1])
mask.append((~mask[0])&(~mask[1]))
p=[]
for e,m in enumerate(mask):
    x = np.where(m)[0][0]
    w = np.where(m)[0][-1]-np.where(m)[0][0]+1
    p.append(Rectangle((x,x),w,w,fill=False,color=grp_cols[e],linewidth=3,zorder=100))
    ax[0].add_patch(p[-1])

un_class = list(sens_map['class'].unique())
cols = cm.nipy_spectral(np.linspace(0,1,len(sens_map['class'].unique())))
col_list = []
for ind in sens_map.index:
   col_list.append(cols[un_class.index(sens_map.loc[ind,'class'])])

ax[1].bar(range(len(sens_map)),sens_map['emax'],width=1,color=col_list,edgecolor=col_list,lw=0)
ax[1].set_xticks(np.arange(len(sens_map)))
ax[1].set_xlim((0,len(sens_map)-.5))
ax[1].set_xticklabels([])
ax[1].set_ylabel("%-viability",labelpad=0)
ax[1].set_title("Avg Emax Across Cell Line Panel\n<---Sensitive   Resistant--->",fontsize=8)
ax[1].set_ylim((0,1))
ax[1].set_yticks([0,.5,1])
ax[1].spines['top'].set_visible(False)
ax[1].spines['right'].set_visible(False)
ax[1].xaxis.set_ticks_position('bottom')
ax[1].yaxis.set_ticks_position('left')

ax[2].barh(range(len(sens_map)),sens_map['emax'],height=1,color=col_list,edgecolor=col_list,lw=0)
ax[2].set_yticks(np.arange(len(sens_map)))
ax[2].set_ylim((0,len(sens_map)-.5))
ax[2].set_yticklabels([])
ax[2].set_xlabel("%-viability",labelpad=0)
ax[2].set_xlim((0,1))
ax[2].set_xticks([0,.5,1])
ax[2].spines['top'].set_visible(False)
ax[2].spines['right'].set_visible(False)
ax[2].xaxis.set_ticks_position('bottom')
ax[2].yaxis.set_ticks_position('left')


ax[3].patch.set_facecolor('w')
ax[3].patch.set_alpha(0.)
ax[3].spines['top'].set_visible(False)
ax[3].spines['right'].set_visible(False)
ax[3].spines['bottom'].set_visible(False)
ax[3].spines['left'].set_visible(False)
ax[3].set_xticks([])
ax[3].set_yticks([])
ax[3].set_ylim((0,1.1))
ax[3].set_xlim((0,1))

for e,i in enumerate(un_class):
    ax[3].scatter(.05,1-(e)/10.,s=50,marker='s',c=cols[e])
    ax[3].text(.25,1-(e)/10.,i,ha='left',va='center')

m1 = Path([(.35,.35),(.35,.65),(.65,.65),(.65,.35)])
m2 = Path([(0.1,0.1),(0.1,.9),(.9,.9),(.9,.1)])
mask=[]
mask.append(m1.contains_points(np.array(df[['E1','E2']])))
mask.append(m2.contains_points(np.array(df[['E1','E2']]))&~mask[0])
mask.append(~mask[0]&~mask[1])
p = []
for e in range(len(mask)):
    p.append(ax[4].boxplot(df['bliss_fit_mxd1d2'].loc[mask[e]].values,positions=[e],notch=False,patch_artist=True,widths=.75,showfliers=False,zorder=20))

for e,bp in enumerate(p):
    plt.setp(bp['medians'],color='r',linewidth=2)
    plt.setp(bp['whiskers'],color='k')
    plt.setp(bp['caps'],color='k')
    plt.setp(bp['boxes'],edgecolor='k')
    
    for patch in bp['boxes']:
        patch.set(facecolor=grp_cols[e]) 

ax[4].set_xticks([])
ax[4].set_xlim((-.5,2.5))
ax[4].set_xticklabels([])
ax[4].set_ylabel("Bliss\nmax(d1,d2)",labelpad=-2)
ax[4].set_ylim((-.4,.2))
ax[4].set_yticks([-.2,0.,.2])
ax[4].spines['top'].set_visible(False)
ax[4].spines['right'].set_visible(False)
ax[4].spines['bottom'].set_position('zero')
ax[4].xaxis.set_ticks_position('bottom')
ax[4].yaxis.set_ticks_position('left')
ax[4].tick_params(axis='x',direction='out')
labs = ['A1 = (x E 0.35<Emax<0.65)','A2 = (x E 0.10<Emax<0.90)|(x ~E A1)','(x ~A1 | x ~A2)']
ax[4].legend([bp['boxes'][0] for bp in p], labs,ncol=1,bbox_to_anchor=(1.05, 1.0))
ax[4].plot((0,1),(-.3,-.3),color='k',linewidth=2,clip_on=False)
ax[4].plot((1,2),(-.4,-.4),color='k',linewidth=2,clip_on=False)
ax[4].plot((0,2),(-.5,-.5),color='k',linewidth=2,clip_on=False)

for e in range(2):
    print(ttest_ind(df['bliss_fit_mxd1d2'].loc[mask[e+1]].values,df['bliss_fit_mxd1d2'].loc[mask[e]].values).pvalue/2.)
print(ttest_ind(df['bliss_fit_mxd1d2'].loc[mask[2]].values,df['bliss_fit_mxd1d2'].loc[mask[0]].values).pvalue/2.)
plt.savefig('bliss_bias_drugClass.pdf',bbox_inches = 'tight',pad_inches = 0)








# =============================================================================
# Panel F
# =============================================================================
from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
import os
sub_T = T[(T['E1']<.6)&(T['E1']>.4)&(T['E2']<.6)&(T['E2']>.4)&(T['beta']>.25)&(T['bliss_fit_mxd1d2']<-0.05)&(T['log_alpha1']>0)&(T['log_alpha2']>0)]
for k in sub_T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
        
#T[T['drugunique']=='paclitaxel_mk-2206_KPL1]
sub_T = T.loc[5150]
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
sub_T['save_direc'] = os.getcwd()
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:] = 0
popt3 = [sub_T['E0'],sub_T['E1'],sub_T['E2'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C1'],10**sub_T['log_C2'],10**sub_T['log_h1'],10**sub_T['log_h2'],10**sub_T['log_alpha1'],10**sub_T['log_alpha2']]
DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
title = 'Bliss @ max(d1,d2):%.2f'%sub_T['bliss_fit_mxd1d2'] + '\n'+r'$\beta$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['beta'],sub_T['beta_ci_up'],sub_T['beta_ci_lw']) + '\n'+r'$log(\alpha_1)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha1'],sub_T['log_alpha1_ci_up'],sub_T['log_alpha1_ci_lw']) +'\n'+r'$log(\alpha_2)$=%.2f (95%%CI:[%.2f,%.2f])'%(sub_T['log_alpha2'],sub_T['log_alpha2_ci_up'],sub_T['log_alpha2_ci_lw'])
[fig1,fig2],[ax_surf,ax1,ax2],[tt,yy,zz],surf = matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=None, zlim=(0.0,1.1), zero_conc=0,title=None,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
fname=drug1_name+'_'+drug2_name+'_'+sample

c_plane = colorConverter.to_rgba('k', alpha=0.3)
xl = ax_surf.get_xlim();yl=ax_surf.get_ylim()
verts = [np.array([(xl[0],yl[0]),(xl[0],yl[1]),(xl[1],yl[1]),(xl[1],yl[0])])]
poly = PolyCollection(verts, facecolors='k',alpha=.4)
ax_surf.add_collection3d(poly, zs=[sub_T['E1_obs']*sub_T['E2_obs']], zdir='z')

fig1.savefig(fname+'.pdf',pad_inches=0.,format='pdf');
fig2.savefig(fname+'_slices.pdf',pad_inches=0.,format='pdf');






#
#
#
#
###########################################################################################
###########################################################################################
##Show the problems with Loewe
#fit_fil = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
#dat_fil = 'oneil_anticancer/merck_perVia_10-29-2018.csv'
#T = pd.read_csv("../../Data/" + fit_fil)
#T = T[(T['selected_fit_alg']=='nlls')&(T['R2']>.9)]
#T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
#T.reset_index(drop=True,inplace=True)
#df = T
#
#xaxis = "avg_Emax"
#df[xaxis] = df[['E1','E2']].mean(axis=1)
#
## Calculate the moving windows of the scattered data
#percentiles = [i/10. for i in range(1,10)]
#n_steps=20
#window_size=0.05 # This is in h-space
#window_df2 = moving_window_percentiles(df, xaxis, "loewe_fit_perUnd", percentiles = percentiles, n_steps=n_steps, window_size=window_size)
#x = np.float64(window_df2['xmid'])
#
#xlim=(-.1,1)
#ylim=(-.1,1)
#
#fig = plt.figure(figsize=(3.5,1.5),facecolor='w')
#ax = []
#ax.append(plt.subplot(111))
#ax[0].scatter(df[xaxis],df['loewe_fit_perUnd'],alpha=0.05, s=.5,rasterized=True)
#plot_quantile_bias(window_df2, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
#ax[0].set_xlabel('Avg(E1,E2)')
#ax[0].set_xlim((0,1))
#ax[0].set_xticks([0.,.5,1.])
#ax[0].set_ylim((0,1))
#ax[0].set_title("% Conditions\nUndefined by Loewe",fontsize=8)
#ax[0].set_yticks([0,1])
#ax[0].spines['top'].set_visible(False)
#ax[0].spines['right'].set_visible(False)
#ax[0].xaxis.set_ticks_position('bottom')
#ax[0].yaxis.set_ticks_position('left')
#ax[0].set_ylabel('Percent',labelpad=-1)
#plt.subplots_adjust(hspace=.0)
#plt.savefig('loewe_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)
#
########
#df = df[(df['loewe_fit_perUnd']>.5)&(df['beta']>.5)&(df['E1']<.9)&(df['E2']<.9)]
#
#sub_T =df.loc[270]
#sub_T['to_save_plots'] = 1
#expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
#drug1_name  = sub_T['drug1_name']
#drug2_name  = sub_T['drug2_name']
#sample      = sub_T['sample']
#data        = pd.read_table(expt, delimiter=',')        
#data['drug1'] = data['drug1'].str.lower()
#data['drug2'] = data['drug2'].str.lower()
#data['sample'] = data['sample'].str.upper()
#d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
#title = '%% undefined Loewe:%.2f'%sub_T['loewe_fit_perUnd']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 
#[[fig_surf,fig_slice],[ax_surf,ax_slice1,ax_slice2],[tt,yy,zz],surf] = matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,plt_zeropln=False,fname=None, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
#
#c_plane = colorConverter.to_rgba('k', alpha=0.3)
#d2_min,d2_max = ax_surf.get_xlim()
#d1_min,d1_max = ax_surf.get_ylim()
#zero_conc=0
#verts = [np.array([(d2_min-zero_conc,d1_min-zero_conc), (d2_min-zero_conc,d1_max), (d2_max,d1_max), (d2_max,d1_min-zero_conc), (d2_min-zero_conc,d1_min-zero_conc)])]
#poly = PolyCollection(verts, facecolors=c_plane)
##ax.add_collection3d(poly, zs=[max(e1,e2)], zdir='z')
#ax_surf.add_collection3d(poly, zs=[max(sub_T['E1_obs'],sub_T['E2_obs'])], zdir='z')
#plt.sca(ax_surf)
## Plot intersection of surface with DIP=0 plane
##plt.contour(tt,yy,zz,levels=[max(e1,e2)], linewidths=3, colors='k')
#plt.contour(tt,yy,zz,levels=[max(sub_T['E1_obs'],sub_T['E2_obs'])], linewidths=3, colors='k')
##    
##from matplotlib import colors
### define the bins and normalize
##cmap = colors.ListedColormap(['green','blue'])
##boundaries = [-np.inf,max(sub_T['E1_obs'],sub_T['E2_obs']),np.inf]
##norm = colors.BoundaryNorm(boundaries, cmap.N, clip=True)
##
##surf.set(cmap=cmap,norm=norm)
#
#fig_surf.savefig(drug1_name+'_'+drug2_name+'_'+sample+'.pdf')
#fig_slice.savefig(drug1_name+'_'+drug2_name+'_'+sample+'_slices.pdf')
#
#################################################################################
#################################################################################
##Show schindler is not a fix
##Show the problems with Loewe
#fit_fil1 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation_allDoses.csv'
#fit_fil2 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
#T1 = pd.read_csv("../../Data/" + fit_fil1)
#T2 = pd.read_csv("../../Data/" + fit_fil2)
#T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.9)]
#T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]
#
#T = T1.merge(T2,on='drugunique')
#T = T[~np.isnan(T['schindler'])]
#df = T
#
#df= df.reset_index(drop=True)
##    
#xaxis = "delta_Emax"
#df[xaxis] = np.abs(df['E1']-df['E2'])
#
#
## Calculate the moving windows of the scattered data
#percentiles = [i/10. for i in range(1,10)]
#n_steps=100
#window_size=0.01 # This is in h-space
#window_df = moving_window_percentiles(df, xaxis, "schindler", percentiles = percentiles, n_steps=n_steps, window_size=window_size,logx=False)
#x = np.float64(window_df['xmid'])
#
#xlim=(0.,1.1)
#ylim=(-2,2)
#
#fig = plt.figure(figsize=(3.5,1.5),facecolor='w')
#ax = []
#ax.append(plt.subplot(111))
#ax[0].scatter(df[xaxis],df['schindler'],alpha=0.03, s=.1,rasterized=True)
#plot_quantile_bias(window_df, xaxis, ax[0], color='darkorange',logx=False, percentiles=percentiles, n_steps=n_steps, window_size=window_size, bias=None, xlim=xlim, ylim=ylim)
#ax[0].set_xlabel(r'$\Delta$(E1,E2)')
#ax[0].set_xlim(xlim)
#ax[0].set_xticks([0.,.5,1.])
#ax[0].set_ylabel("Schindler",labelpad=-4)
#ax[0].set_ylim((-.5,.5))
#ax[0].set_yticks([-.5,0,.25])
#ax[0].set_ylim((-.25,.35))
#ax[0].spines['top'].set_visible(False)
#ax[0].spines['right'].set_visible(False)
#ax[0].xaxis.set_ticks_position('bottom')
#ax[0].yaxis.set_ticks_position('left')
#ax[0].set_title('Schindler biased by difference in Emax',fontsize=8)
#plt.subplots_adjust(hspace=.0)
#plt.savefig('Schindler_trend_merck.pdf', bbox_inches = 'tight',pad_inches = 0)
#
#
##Find a particularily extreme example
#T2["delta_Emax"] = np.abs(T2['E1']-T2['E2'])
#T2 = T2.merge(df[['schindler','drugunique']].groupby(['drugunique']).sum(),on='drugunique')
#sub_T = T2[(T2[['log_alpha1','log_alpha2','beta']]<0).all(axis=1) & (T2['delta_Emax']>.9) & (T2[['E1','E2']]<1).all(axis=1)]
#
#
#sub_T[['delta_Emax','schindler','beta','log_alpha1','log_alpha2']].sort_values('schindler')
#
#import os
#sub_T =T2.loc[10646]
#sub_T['to_save_plots'] = 1
#expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
#drug1_name  = sub_T['drug1_name']
#drug2_name  = sub_T['drug2_name']
#sample      = sub_T['sample']
#data        = pd.read_table(expt, delimiter=',')        
#data['drug1'] = data['drug1'].str.lower()
#data['drug2'] = data['drug2'].str.lower()
#data['sample'] = data['sample'].str.upper()
#d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
#title = r'$\Sigma$ Schindler:%.2f'%sub_T['loewe_fit_perUnd']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$E_1=%.2f\pm%.2f$'%(sub_T['E1'],sub_T['E1_std']) + '\n' + r'$E_2=%.2f\pm%.2f$'%(sub_T['E2'],sub_T['E2_std']) 
#matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,plt_zeropln=False,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
#
#################################################################################
#################################################################################
##Zimmer and CI errors
#fit_fil1 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation_allDoses.csv'
#fit_fil2 = 'oneil_anticancer/MasterResults_noGamma_otherSynergyMetricsCalculation.csv'
#T2 = pd.read_csv("../../Data/" + fit_fil2)
#T2 = T2[(T2['selected_fit_alg']=='nlls')&(T2['R2']>.9)]
#T2 = T2[(T2['C1']<T2['max_conc_d1'])&(T2['C2']<T2['max_conc_d2'])]
#T2 = T2[(~np.isnan(T2['zimmer_raw_a1']) & (~np.isnan(T2['zimmer_raw_a2'])))]
#T2.reset_index(drop=True,inplace=True)
#df = T2
#xaxis = "avg_Emax"
#df[xaxis] = df[['E1','E2']].mean(axis=1)
#
#xlim=(0.,1.)
#ylim=(-2,2)
#
#df[(df['zimmer_R2']<0.2)&(df['E1']<.7)&(df['E2']<.7)][['beta','log_alpha1','log_alpha2']]
#
#from plotFun import matplotlibArbitrarySurface
#from SynergyCalculator.calcOtherSynergyMetrics import get_params,bliss,zimmer,zimmer_effective_dose
#from SynergyCalculator.initialFit import init_fit
#
#elev=20;azim=70
#
#
#i = 164#117
#sub_T       = df.loc[i].to_dict()
#expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
#drug1_name  = sub_T['drug1_name']
#drug2_name  = sub_T['drug2_name']
#sample      = sub_T['sample']
#data        = pd.read_table(expt, delimiter=',')        
#data['drug1'] = data['drug1'].str.lower()
#data['drug2'] = data['drug2'].str.lower()
#data['sample'] = data['sample'].str.upper()
#d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
#title = r'MuSyC R2:%.2f'%sub_T['R2']+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$log(\alpha_1)=%.2f\pm%.2f$'%(sub_T['log_alpha1'],sub_T['log_alpha1_std']) + '\n' + r'$log(\alpha_2)=%.2f\pm%.2f$'%(sub_T['log_alpha2'],sub_T['log_alpha2_std']) 
#[fig1,fig2],[ax_surf,ax1,ax2],[tt,yy,zz],surf = matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,plt_zeropln=False,fname=None, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
#
#ax_surf.view_init(elev=elev,azim=azim)
#fname = drug1_name + '_' + drug2_name + '_' + sample +'.pdf'
#fig1.savefig(fname,pad_inches=0.,format='pdf')
#
#
#E_fix = None
#E_bnd = [[.9,0.,0.,0.,0.],[1.1,2.5,2.5,2.5]]
#E_fx = [np.nan,np.nan,np.nan,np.nan] if E_fix is None else E_fix
#E_bd = [[-np.inf,-np.inf,-np.inf,-np.inf],[np.inf,np.inf,np.inf,np.inf]] if E_bnd is None else E_bnd
#   
#sub_T = init_fit(sub_T,d1,d2,dip,dip_sd,drug1_name,drug2_name,E_fx,E_bd)  
#C1 = 10**sub_T['log_C1']
#C2 = 10**sub_T['log_C2']
#h1 = 10**sub_T['log_h1']
#h2 = 10**sub_T['log_h2']
#C1, C2, h1, h2, a12, a21, _, R2  = zimmer(d1, d2, dip, logspace=False, sigma=None, vector_input=True,p0=[C1,C2,h1,h2,0,0])   
#zero_conc = 0
#N=15
#d1_min=np.log10(sub_T['min_conc_d1']);d1_max=np.log10(sub_T['max_conc_d1'])
#d2_min=np.log10(sub_T['min_conc_d2']);d2_max=np.log10(sub_T['max_conc_d2'])
#y = np.linspace(d1_min-zero_conc, d1_max ,N)
#t = np.linspace(d2_min-zero_conc, d2_max ,N)
#yy,tt=np.meshgrid(y,t)
#zz = zimmer_effective_dose(10**yy,10**tt,C1,C2,h1,h2,a12,a21)
#title = r'Zimmer R2:%.2f'%sub_T['zimmer_R2']+'\n'+r'$a12=%.2f$'%(sub_T['zimmer_raw_a1'])+ '\n' + r'$a21 = %.2f$'%(sub_T['zimmer_raw_a2'])
#
#fig1,ax = matplotlibArbitrarySurface(tt,yy,zz,d1,d2,dip,dip_sd,drug1_name,drug2_name,fname=None,metric_name='Percent',zlim=(0,1.1),zero_conc=0,title=title,d1lim=None,d2lim=None,fmt='pdf',plt_zeropln=False,plt_data=True,figsize_surf=(2.2,2.4))
#
#fname = drug1_name+'_'+drug2_name+'_'+sample+'_zimmer.pdf'
#elev=20;azim=70
#ax.view_init(elev=elev,azim=azim)
#fig1.savefig(fname,pad_inches=0.,format='pdf')
#
#
#
#
#
#df[((df[['E1','E2']]<.7).all(axis=1))&((df[['E1','E2']]>.4).all(axis=1))&((df[['E1_std','E2_std']]<.01).all(axis=1))][['beta_obs']]
#
#
#df[((df[['E1','E2']]<.7).all(axis=1))&((df[['E1','E2']]>.6).all(axis=1))&(df['beta']>1)][['beta_obs']]
#i = 9918
#sub_T       = df.loc[i].to_dict()
#expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
#drug1_name  = sub_T['drug1_name']
#drug2_name  = sub_T['drug2_name']
#sample      = sub_T['sample']
#data        = pd.read_table(expt, delimiter=',')        
#data['drug1'] = data['drug1'].str.lower()
#data['drug2'] = data['drug2'].str.lower()
#data['sample'] = data['sample'].str.upper()
#d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
#
#from SynergyCalculator.calcOtherSynergyMetrics import combination_index
#
####Now show combination index plots
#CI,d1_full,d2_full,Dm1,Dm2,m1,m2  = combination_index(d1,d2,dip)
#def ll2(d,h,C):
#    return 1/(1+(d/C)**h)
#from SynergyCalculator.NDHillFun import Edrug1D
#
#plt.figure(figsize=(3,2))
#ax1=plt.subplot(121)
#ax2=plt.subplot(122)
#lw=2;sz=20
#d1_tmp = d1[d2==0];d2_tmp=d2[d1==0]
#d1_tmp[d1_tmp==0]=min(d1[d1!=0])/10.; d2_tmp[d2_tmp==0]=min(d2[d2!=0])/10.
#ax1.scatter(np.log10(d1_tmp),dip[d2==0],s=sz,c='k')
#d1_tmp = np.logspace(np.log10(min(d1_tmp)),np.log10(max(d1_tmp)),100)
#ax1.plot(np.log10(d1_tmp),ll2(d1_tmp,m1,Dm1),c='r',label='CI',lw=lw)
#ax1.plot(np.log10(d1_tmp),Edrug1D(d1_tmp,*[sub_T[i] for i in ['E0','E1','C1','h1']]),c='b',label='MuSyC',lw=lw)
#ax1.set_ylim((0,1.1))
#ax1.set_xlabel('log(%s)'%drug1_name)
#ax1.set_ylabel('% Viability')
#ax1.spines['top'].set_visible(False)
#ax1.spines['right'].set_visible(False)
#
#ax2.scatter(np.log10(d2_tmp),dip[d1==0],s=sz,c='k')
#d2_tmp = np.logspace(np.log10(min(d2_tmp)),np.log10(max(d2_tmp)),100)
#ax2.plot(np.log10(d2_tmp),ll2(d2_tmp,m2,Dm2),c='r',label='CI',lw=lw)
#ax2.plot(np.log10(d2_tmp),Edrug1D(d2_tmp,*[sub_T[i] for i in ['E0','E2','C2','h2']]),c='b',label='MuSyC',lw=lw)
#ax2.set_ylim((.0,1.1))
#ax2.legend()
#ax2.set_xlabel('log(%s)'%drug2_name)
#ax2.spines['top'].set_visible(False)
#ax2.spines['right'].set_visible(False)
#ax2.set_yticklabels([])
#plt.tight_layout()
#plt.savefig('CI_misfits.pdf')
#title = r'$Median CI:%.2f'%np.nanmedian(CI)+'\n'+r'$\beta=%.2f\pm%.2f$'%(sub_T['beta'],sub_T['beta_std']) + '\n' + r'$E_1=%.2f\pm%.2f$'%(sub_T['E1'],sub_T['E1_std']) + '\n' + r'$E_2=%.2f\pm%.2f$'%(sub_T['E2'],sub_T['E2_std']) 
#
#matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,plt_zeropln=False,fname=drug1_name+'_'+drug2_name+'_'+sample, zlim=(0.,1.1), zero_conc=0,title=title,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
#







#
#
#cell_line = []
#drug =[]
#emax=[]
#emax_sd=[]
#ec50=[]
#ec50_sd=[]
#zimmer = []
#
#df = df[abs(df['zimmer_raw_a1'])<100]
#for c in df['sample'].unique():
#    sub_df = df[df['sample']==c]
#    dgls = np.unique(list(sub_df['drug2_name'].unique())+list(sub_df['drug1_name'].unique()))
#    for d in dgls:
#        emax.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['E1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['E2'].mean()))))
#        cell_line.append(c)
#        drug.append(d)
#        ec50.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['C1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['C2'].mean()))))
#        zimmer.append(np.nanmean((np.array(sub_df[sub_df['drug1_name']==d]['zimmer_raw_a1'].mean()),np.array(sub_df[sub_df['drug2_name']==d]['zimmer_raw_a1'].mean()))))
#
#sensitivity_map = pd.DataFrame({'sample':cell_line,'drug':drug,'emax':emax,'zimmer':zimmer}) 
#        
#sens_map = sensitivity_map.groupby('sample')[['emax','zimmer']].mean().merge(pd.read_csv('cell_lines_classification.csv'),on='sample')
#sens_map = sens_map.sort_values('emax').reset_index(drop=False)
#
#un_class = list(sens_map['class'].unique())
#cols = cm.nipy_spectral(np.linspace(0,1,len(sens_map['class'].unique())))
#col_list = []
#for ind in sens_map.index:
#   col_list.append(cols[un_class.index(sens_map.loc[ind,'class'])])
#
#fig = plt.figure(figsize=(6,3.5),facecolor='w')
#ax = []
#ax.append(plt.subplot2grid((3,5),(0,0),colspan=4))
#ax.append(plt.subplot2grid((3,5),(1,0),colspan=4))
#ax.append(plt.subplot2grid((3,5),(2,0),colspan=4))
#ax.append(plt.subplot2grid((3,5),(0,4),rowspan=3))
#
#ax[0].bar(range(len(sens_map)),sens_map['emax'],width=1,color=col_list,edgecolor=col_list,lw=0)
#ax[0].set_xticks(np.arange(len(sens_map))+.5)
#ax[0].set_xticklabels([])
#ax[0].set_ylabel("%-viability",labelpad=-2)
#ax[0].set_title("Avg Efficacy Across Cell Panel\n<---Sensitive   Resistant--->",fontsize=8)
#ax[0].set_ylim((0,1))
#ax[0].set_yticks([0,.5,1])
#ax[0].spines['top'].set_visible(False)
#ax[0].spines['right'].set_visible(False)
#ax[0].xaxis.set_ticks_position('bottom')
#ax[0].yaxis.set_ticks_position('left')
#
#ax[1].bar(range(len(sens_map)),sens_map['zimmer'],width=1,color=col_list,edgecolor=col_list,lw=0)
#ax[1].set_xticks(np.arange(len(sens_map))+.5)
#ax[1].set_xticklabels([])
#ax[1].set_ylabel("zimmer",labelpad=-2)
#ax[1].text(20,.01,"Avg zimmer Across\nDrug Combination Panel",ha='center')
#ax[1].set_ylim((-.1,.01))
#ax[1].set_yticks([-.1,0.])
#ax[1].spines['top'].set_visible(False)
#ax[1].spines['right'].set_visible(False)
#ax[1].spines['bottom'].set_position('zero')
#ax[1].xaxis.set_ticks_position('bottom')
#ax[1].yaxis.set_ticks_position('left')
#ax[1].tick_params(axis='x',direction='out')
#
#mask1 = []
#mask1.append(sens_map['emax']<.15)
#mask1.append((sens_map['emax']>.15)&(sens_map['emax']<.45))
#mask1.append((sens_map['emax']>.45)&(sens_map['emax']<.75))
#mask1.append((sens_map['emax']>.75))
#labs = ['Emax<15%','15%<Emax<45%','45%<Emax<75%','Emax>75%']
#mask = []
#for j in range(4):
#    mask.append(np.in1d(df['drug1_name'],sens_map['drug_name'].loc[mask1[j]]) | np.in1d(df['drug2_name'],sens_map['drug_name'].loc[mask1[j]]))
#
#p = []
#for e in range(len(mask)):
#    p.append(ax[2].boxplot(df['bliss_fit_mxd1d2'].loc[mask[e]].values,positions=[np.mean(np.where(mask1[e])[0])],notch=False,patch_artist=True,widths=.75,showfliers=False,zorder=20))
#
#for e,bp in enumerate(p):
#    plt.setp(bp['medians'],color='r',linewidth=2)
#    plt.setp(bp['whiskers'],color='k')
#    plt.setp(bp['caps'],color='k')
#    plt.setp(bp['boxes'],edgecolor='k')
#    
#    for patch in bp['boxes']:
#        patch.set(facecolor=cols[e]) 
#
#ax[2].set_xticks([])
#ax[2].set_xlim((0,len(sens_map)))
#ax[2].set_xticklabels([])
#ax[2].set_ylabel("Bliss @ max(d1,d2)",labelpad=-2)
#ax[2].set_ylim((-.2,.1))
#ax[2].set_yticks([-.1,0.])
#ax[2].spines['top'].set_visible(False)
#ax[2].spines['right'].set_visible(False)
#ax[2].spines['bottom'].set_position('zero')
#ax[2].xaxis.set_ticks_position('bottom')
#ax[2].yaxis.set_ticks_position('left')
#ax[2].tick_params(axis='x',direction='out')
#ax[2].legend([bp['boxes'][0] for bp in p], labs,ncol=1,bbox_to_anchor=(1.35, 1.2))
#
#ax[3].patch.set_facecolor('w')
#ax[3].patch.set_alpha(0.)
#ax[3].spines['top'].set_visible(False)
#ax[3].spines['right'].set_visible(False)
#ax[3].spines['bottom'].set_visible(False)
#ax[3].spines['left'].set_visible(False)
#ax[3].set_xticks([])
#ax[3].set_yticks([])
#ax[3].set_ylim((0,1.1))
#ax[3].set_xlim((0,1))
#
#for e,i in enumerate(un_class):
#    ax[3].scatter(.05,1-(e)/10.,s=50,marker='s',c=cols[e])
#    ax[3].text(.25,1-(e)/10.,i,ha='left',va='center')
#
#plt.tight_layout()
