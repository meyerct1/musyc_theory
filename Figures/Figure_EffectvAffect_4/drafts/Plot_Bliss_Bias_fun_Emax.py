#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 10:57:17 2019

@author: meyerct6
"""

#Import packages
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from matplotlib import rc
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

def Edrug_2D_full(d1,d2,E0,E1,E2,beta,C1,C2,h1,h2,r1,r2,alpha1,alpha2,gamma1,gamma2):
    E3 = -beta*(E0-min(E1,E2))+min(E1,E2);
    return (C1**(2*gamma1*h1)*E2*d2**h2*r1 + C2**(2*gamma2*h2)*E1*d1**h1*r2 + C1**(2*gamma1*h1)*C2**(gamma2*h2)*E0*r1 + C1**(gamma1*h1)*C2**(2*gamma2*h2)*E0*r2 + E3*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + E3*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*E2*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + C2**(gamma2*h2)*E1*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1 + C1**(gamma1*h1)*C2**(gamma2*h2)*E0*r1*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*C2**(gamma2*h2)*E0*r2*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*E2*d1**h1*r1*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*E3*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + C2**(gamma2*h2)*E1*d2**h2*r2*(alpha2*d1)**(gamma2*h1) + C2**(gamma2*h2)*E3*d1**h1*r2*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*C2**(gamma2*h2)*E1*d1**h1*r1 + C1**(gamma1*h1)*C2**(gamma2*h2)*E2*d2**h2*r2)/(C1**(2*gamma1*h1)*C2**(gamma2*h2)*r1 + C1**(gamma1*h1)*C2**(2*gamma2*h2)*r2 + C1**(2*gamma1*h1)*d2**h2*r1 + C2**(2*gamma2*h2)*d1**h1*r2 + C1**(gamma1*h1)*C2**(gamma2*h2)*r1*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*C2**(gamma2*h2)*r2*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*d1**h1*r1*(alpha1*d2)**(gamma1*h2) + C1**(gamma1*h1)*d2**h2*r1*(alpha2*d1)**(gamma2*h1) + C2**(gamma2*h2)*d1**h1*r2*(alpha1*d2)**(gamma1*h2) + C2**(gamma2*h2)*d2**h2*r2*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*C2**(gamma2*h2)*d1**h1*r1 + C1**(gamma1*h1)*C2**(gamma2*h2)*d2**h2*r2 + alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1*(alpha1*d2)**(gamma1*h2) + alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2*(alpha2*d1)**(gamma2*h1) + C1**(gamma1*h1)*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2 + C2**(gamma2*h2)*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1);
def Edrug1D(d,E0,Em,C,h):
    return Em + (E0-Em) / (1 + (d/C)**h)
#Set parameters for surfaces
d1 = np.logspace(-5,0,100);
d2 = np.logspace(-5,0,100);
h1 = 1;
h2 = 1;
C1 = .005;
C2 = .001;
E0 = 1;
E1 = .5;
E2 = .5;
beta = 0;
alpha1 = 1;
alpha2 = 1;
gamma1 = 1;
gamma2 = 1;
r1 = 100*E0;
r2 = 100*E0;
yy,tt = np.meshgrid(d1,d2)
mat = np.zeros((100,100))
for e1,E1 in enumerate(np.linspace(0,1,100)):
    for e2,E2 in enumerate(np.linspace(0,1,100)):
            ed = Edrug_2D_full(yy.flatten(),tt.flatten(),E0,E1,E2,beta,C1,C2,h1,h2,r1,r2,alpha1,alpha2,gamma1,gamma2).reshape(yy.shape)
            bl = np.array(np.matrix(Edrug1D(d2,E0,E2,C2,h2)).T*np.matrix(Edrug1D(d1,E0,E1,C1,h1)))
            mat[e1,e2]=ed[-1,-1]-bl[-1,-1]

plt.figure(figsize=(2,2))
X,Y = np.meshgrid(np.linspace(0,1,100),np.linspace(0,1,100))
ax = plt.subplot(111)
plt.pcolor(X,Y,mat,cmap=cm.plasma)
ax.set_xlabel('E1',labelpad=-7)
ax.set_ylabel('E2',labelpad=-7)
ax.set_zlabel('$\Delta$',labelpad=-3,rotation=90)
ax.set_title(r'$\Delta$=Null Emax (MuSyC)'+'\n-\nNull Emax (Bliss)')
ax.tick_params(axis='x',pad=-3)
ax.tick_params(axis='y',pad=-3)
ax.tick_params(axis='z',pad=-2)
ax.view_init(azim=-110.,elev=50)

plt.figure(figsize=(2,2))
ax = plt.subplot(111)
plt.pcolor(X,Y,mat,cmap=cm.bwr)
ax.set_xlabel('E1',labelpad=-1)
ax.set_ylabel('E2',labelpad=-1)
ax.set_title('Null Emax (MuSyC)'+'\n-\nNull Emax (Bliss)')
ax.tick_params(axis='x',pad=0)
ax.tick_params(axis='y',pad=0)












import pandas as pd
i = 'oneil_anticancer'
T = pd.read_csv('../../Data/' + i + '/MasterResults_mcnlls.csv')
T = T[(T['converge_mc_nlls']==1.)&(T['R2']>.7)]
T['C1'] = 10**T['log_C1'];T['C2']=10**T['log_C2']
T = T[(T['C1']<T['max_conc_d1'])&(T['C2']<T['max_conc_d2'])]
T['r1'] = 100.;T['r2']=100.;T['r1_std']=0.;T['r2_std']=0.
T['save_direc'] = '/home/meyerct6/Repos/MuSyC_Theory/Data/oneil_anticancer'
T['model_level']=1
T.reset_index(drop=True,inplace=True)


from SynergyCalculator.calcOtherSynergyMetrics import bliss
from SynergyCalculator.NDHillFun import Edrug2D_NDB
for i in T.index:
    E_mxd1d2 = Edrug2D_NDB((T.loc[i,'max_conc_d1'],T.loc[i,'max_conc_d2']),T.loc[i,'E0'],T.loc[i,'E1'],T.loc[i,'E2'],T.loc[i,'E3'],T.loc[i,'r1'],T.loc[i,'r2'],T.loc[i,'C1'],T.loc[i,'C2'],10**T.loc[i,'log_h1'],10**T.loc[i,'log_h2'],10**T.loc[i,'log_alpha1'],10**T.loc[i,'log_alpha2'])
    T.loc[i,'bliss_fit_mxd1d2'] = bliss(T.loc[i,'E1_obs'],T.loc[i,'E2_obs'],E_mxd1d2)



z = T.loc[~T['bliss_fit_mxd1d2'].isna(),'bliss_fit_mxd1d2']
x = T.loc[~T['bliss_fit_mxd1d2'].isna(),'E1']
y = T.loc[~T['bliss_fit_mxd1d2'].isna(),'E2']

import matplotlib._cntr as cntr
from matplotlib.path import Path
scon = []
stp = .1
cnt = 1
for i in range(6):
    bol = True
    stp = .1
    while bol:
        con = np.max(mat)-stp*cnt
        if con<np.min(mat):
            stp=stp/(1.1*(i+1))
        else:
            cs = cntr.Cntr(X,Y,mat)
            p = Path(cs.trace(con,con,0)[0])
            val2 = sum(p.contains_points(np.vstack((x,y)).T))
            if val2>(i+1)*3000 and val2<(i+1)*3020:
                print('Success')
                scon.append(con)
                bol = False
            elif val2>(i+1)*3020:
                print(val2)
                stp=stp/(1.1*(i+1))
            elif val2<(i+1)*3020:
                cnt+=1


