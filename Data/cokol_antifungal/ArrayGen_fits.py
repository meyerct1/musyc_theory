#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Code for creating an array of SynCode_dipDB_template.py for fitting the dose response surfaces
One file for each concentration of drug each cell line.
Edit the expFile and filename variables to match the directory where the 
files should be written.  Code assumes SynCode is in this same folder.


Code commented and up to date 11/01/2018
@author: Christian Meyer 
@email: christian.t.meyer@vanderbilt.edu
"""
import os
###pydrc analysis of cellavista data 
#os.sys.path.append('/home/xnmeyer/Documents/Lab/Repos/pyDRC')
import numpy as np
import pandas as pd
import glob

experiments = glob.glob('data/*AUGC.csv') #List of experimental files
metric_name = 'AUGC' #What is the metric of drug effect called
to_plot     = 0#Should the output be plotted?
to_save     = 0#Should the trace/iterations for pso and mcmc be saved?
hill_orient = 1#What is the orientation of the hill curve.  1 means Emax<E0.  0 means Emax>E0
#Fit Algorithm to use
fit_alg = 'nlls'
E_fix=None #Fix Effects.  None if fitting beta
E_bnd='[[-np.inf,0,0,0],[np.inf,np.inf,np.inf,np.inf]]' #bounds of effects.  overrulled by E_fix.  make a string
find_opt=True#Find optimal model granularity
fit_gamma=True#Fit cooperativity synergy
musyc_path = '/home/xnmeyer/Documents/Lab/Repos/MuSyC_Code/'

#Function to switch the index in target file
def StringSwitch(string,file_string,val):
    idx1 = file_string.find(string)
    idx2 = file_string.find('#',idx1)
    file_string = file_string[0:idx1+len(string)] + str(val) + file_string[idx2:]
    return file_string    
    
#Functions to generate array files by reading in the file name and saving an editted version with
#a new index corresponding to a new drug combination.
def AccreArrayGen(filename,sample,expt,drug1_name,drug2_name,metric_name,cnt):
    fil = str(filename) + '.py'
    f = open(musyc_path + fil, 'r')
    file_string = f.read()
    string = 'fit_alg = '
    file_string = StringSwitch(string,file_string,'"%s"' % fit_alg)  
    string = 'expt = '
    file_string = StringSwitch(string,file_string,'"%s"' % expt)  
    string = 'metric_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % metric_name)  
    string = 'to_plot = '
    file_string = StringSwitch(string,file_string,to_plot)
    string = 'to_save = '
    file_string = StringSwitch(string,file_string,to_save)
    string = 'hill_orient = '
    file_string = StringSwitch(string,file_string,hill_orient)
    string = 'sample = '
    file_string = StringSwitch(string,file_string,'"%s"' % sample)
    string = 'find_opt = '
    file_string = StringSwitch(string,file_string,str(find_opt))
    string = 'fit_gamma = '
    file_string = StringSwitch(string,file_string,str(fit_gamma))
    string = 'drug1_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % drug1_name)
    string = 'drug2_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % drug2_name)  
    string = 'E_fix = '
    file_string = StringSwitch(string,file_string,str(E_fix))
    string = 'E_bnd = '
    file_string = StringSwitch(string,file_string,E_bnd)
    bol = True
    while bol:
        if os.path.isfile(filename + '_' + str(cnt) + '.py'):
            cnt = cnt + 1
        else:
            f = open(filename + '_' + str(cnt) + '.py', 'w')
            f.write(file_string)
            bol = False
    return cnt

#File to replicate
filename = 'SynCode_template'
cnt = 1 
for expt in experiments:                
    #Read in the data into a pandas data frame
    data = pd.read_table(expt, delimiter=',')
    for sample in np.unique(data['sample']):
        #Subset by target cell line
        sub_data = data[data['sample']==sample]
        drug_combinations = sub_data[list(['drug1','drug2'])].drop_duplicates()
        #Remove the double control condition...
        drug_combinations = drug_combinations[np.logical_and(drug_combinations['drug1']!='control', drug_combinations['drug2']!='control')]
        drug_combinations = drug_combinations.reset_index()
        #For all unique drug combinations:
        for e in drug_combinations.index:
            drug1_name = drug_combinations['drug1'][e]
            drug2_name = drug_combinations['drug2'][e]
            cnt = AccreArrayGen(filename,sample,expt,drug1_name,drug2_name,metric_name,cnt)              
            if drug1_name.lower()=='qmy' and drug2_name.lower()=='ben':
                print cnt
            
            

        
