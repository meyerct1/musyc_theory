#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 13:47:01 2018

@author: xnmeyer
"""

import pandas as pd
import numpy as np
from scipy.integrate import simps
import glob
import os
fils = glob.glob('*-*.txt')
dx = [];dy=[]
df_comp = pd.DataFrame()
for f in fils:
    df = pd.read_csv(f,sep='\t',header=None)
    df = df.loc[:,0:63] #Remove the last row of nans...
    if f[0:3]!=f[4:7]:
        area = np.zeros((len(df.T),))
        for i in range(len(df.T)):
            area[i] = simps(df.loc[10:len(df),i],dx=15)
        area = area.reshape((8,8))
        #To plot doses on log space
        t_d1 = np.array([1./2.**e for e in range(7)] + [0])[::-1]
        t_d2 = np.array([1./2.**e for e in range(7)] + [0])[::-1]
        X, Y = np.meshgrid(t_d1, t_d2)
        df_tmp = pd.DataFrame()
        df_tmp['drug2.conc'] = X.flatten()
        df_tmp['drug1.conc'] = Y.flatten()
        df_tmp['effect'] = area.flatten()
        df_tmp['effect.95ci'] = max(area.flatten())/10.
        df_tmp['drug1'] = f[4:7]
        df_tmp['drug2'] = f[0:3]
        df_tmp['sample'] = 'yeast'
        df_tmp['expt'] = '06-20-2018'
        df_comp = df_comp.append(df_tmp,ignore_index=True)
        
df_comp.to_csv('compiled_combination_yeast_06202018_integral.csv',sep=',')

from scipy.stats import linregress
fils = glob.glob('*-*.txt')
dx = [];dy=[]
df_comp = pd.DataFrame()
for f in fils:
    df = pd.read_csv(f,sep='\t',header=None)
    df = df.loc[:,0:63] #Remove the last row of nans...
    if f[0:3]!=f[4:7]:
        slope = np.zeros((len(df.T),))
        uncer = np.zeros((len(df.T),))
        for i in range(len(df.T)):
            t = linregress(np.linspace(15*10,15*(len(df)-1),len(df)-10),np.log2(df.loc[10:len(df),i]))
            slope[i] = t[0]
            uncer[i] = t[-1]
        slope = slope.reshape((8,8))
        uncer = uncer.reshape((8,8))
        t_d1 = np.array([1./2.**e for e in range(7)] + [0])[::-1]
        t_d2 = np.array([1./2.**e for e in range(7)] + [0])[::-1]
        X, Y = np.meshgrid(t_d1, t_d2)
        df_tmp = pd.DataFrame()
        df_tmp['drug2.conc'] = X.flatten()
        df_tmp['drug1.conc'] = Y.flatten()
        df_tmp['effect'] = slope.flatten()
        df_tmp['effect.95ci'] = uncer.flatten()
        df_tmp['drug1'] = f[4:7]
        df_tmp['drug2'] = f[0:3]
        df_tmp['sample'] = 'yeast'
        df_tmp['expt'] = '06-20-2018'
        df_comp = df_comp.append(df_tmp,ignore_index=True)
df_comp = df_comp.loc[~np.isnan(df_comp['effect'])]
df_comp.to_csv('compiled_combination_yeast_06202018_slope.csv',sep=',')


filename = 'SynCode_preCalcDIP_template'

experiments = ['compiled_combination_yeast_06202018_integral.csv']
metric_name = ['Area Under Growth Curve']
max_nest=7
min_nest=7
hill_orient = 1
filename = 'SynCode_preCalcDIP_template'

#Function to switch the index in SynCode.py
def StringSwitch(string,file_string,val):
    idx1 = file_string.find(string)
    idx2 = file_string.find('#',idx1)
    file_string = file_string[0:idx1+len(string)] + str(val) + file_string[idx2:]
    return file_string    
    
#Functions to generate array files by reading in the file name and saving an editted version with
#a new index corresponding to a new drug combination.
def Accre_Array_gen_darren(filename,target_cell_line,expt,drug1_name,drug2_name,metric_name,cnt):
    fil = str(filename) + '.py'
    f = open(fil, 'r')
    file_string = f.read()
    string = 'expt = '
    file_string = StringSwitch(string,file_string,'"%s"' % expt)  
    string = 'metric_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % metric_name)  
    string = 'min_nest = '
    file_string = StringSwitch(string,file_string,min_nest)
    string = 'max_nest = '
    file_string = StringSwitch(string,file_string,max_nest)
    string = 'hill_orient = '
    file_string = StringSwitch(string,file_string,hill_orient)  
    string = 'target_cell_line = '
    file_string = StringSwitch(string,file_string,'"%s"' % target_cell_line)
    string = 'drug1_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % drug1_name)
    string = 'drug2_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % drug2_name)  
    bol = True
    while bol:
        if os.path.isfile(filename + '_' + str(cnt) + '.py'):
            cnt = cnt + 1
        else:
            f = open(filename + '_' + str(cnt) + '.py', 'w')
            f.write(file_string)
            bol = False
    return cnt

cnt = 200
for e1,expt in enumerate(experiments):                
    #Read in the data into a pandas data frame
    data = pd.read_table(expt, delimiter=',')
    for target_cell_line in np.unique(data['sample']):
        #Subset by target cell line
        sub_data = data[data['sample']==target_cell_line]
        drug_combinations = sub_data[list(['drug1','drug2'])].drop_duplicates()
        #Remove the double control condition...
        drug_combinations = drug_combinations[np.logical_and(drug_combinations['drug1']!='control', drug_combinations['drug2']!='control')]
        drug_combinations = drug_combinations.reset_index()
        #For all unique drug combinations:
        for e in drug_combinations.index:
            drug1_name = drug_combinations['drug1'][e]
            drug2_name = drug_combinations['drug2'][e]
            cnt = Accre_Array_gen_darren(filename,target_cell_line,expt,drug1_name,drug2_name,metric_name[e1],cnt)          

        