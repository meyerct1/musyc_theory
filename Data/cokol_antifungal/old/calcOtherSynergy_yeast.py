#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 15 12:56:42 2018

@author: xnmeyer
"""
from SynergyCalculator.gatherData import subset_data
import pandas as pd
import numpy as np
from SynergyCalculator.calcOtherSynergyMetrics import calcOtherSynergy
import os
import warnings
warnings.simplefilter("ignore")

T = pd.read_csv('../../Data/cokol_antifungal/MasterResults_yeast.csv')
E_fix = None
key = ['loewe_fit_ec50','loewe_fit_ec10','loewe_fit_ec25','loewe_fit_all','loewe_fit_perUnd','zip_fit','hsa_raw_all','hsa_fit_all','hsa_fit_ec50','bliss_fit_all','bliss_fit_ec50','zimmer_fit_a1','zimmer_fit_a2','ci_fit_all','ci_fit_ec50','schindler_fit_ec50','schindler_fit_all','loewe_raw_all','loewe_raw_perUnd','schindler_raw_ec50','zimmer_raw_a1','zimmer_raw_a2','bliss_raw_all','zip_raw','ci_raw','brd_raw','brd_fit']
for k in key:
    T[k]=np.nan
for ind in T.index:
    print ind
    sub_T       = T.loc[ind].to_dict()
    expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1'] = data['drug1'].str.lower()
    data['drug2'] = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
    E_fix = [max(dip),108,108,108]
    try:
        x = calcOtherSynergy(sub_T,d1,d2,dip,dip_sd,percent=False,E_fix=E_fix,calc_brd_zip=False)
        for e,k in enumerate(key):
            T.loc[ind,k]=x[e]
    except:
        continue
       
       
T.to_csv('../../Data/cokol_antifungal/MasterResults_otherSynergyMetrics_yeast.csv')

