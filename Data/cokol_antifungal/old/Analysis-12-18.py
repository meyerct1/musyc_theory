#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 18 11:38:18 2017

@author: xnmeyer
"""

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from scipy.integrate import simps
from mpl_toolkits.mplot3d import axes3d, Axes3D #<-- Note the capitalization! 
from matplotlib import cm

df = pd.read_csv('5FU-5FU.txt',sep='\t',header=None)
df = df.loc[:,0:63] #Remove the last row of nans...
area = np.zeros((len(df.T),))
for i in range(len(df.T)):
    area[i] = simps(df.loc[10:len(df),i],dx=15)
area = area.reshape((8,8))

#To plot doses on log space
t_d1 = np.linspace(0,1,8)
t_d2 = np.linspace(0,1,8)
t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10
t_d2[t_d2==0] = min(t_d2[t_d2!=0])/10
X, Y = np.meshgrid(t_d1, t_d2)
#
#fig = plt.figure()
#ax = Axes3D(fig) #<-- Note the difference from your original code...
#ax.plot_surface(np.log10(X),np.log10(Y),area,cmap=cm.coolwarm)



fig = plt.figure()
ax = plt.subplot(111)
plt.plot(np.log10(Y[:,0]),area[:,0])

df = pd.read_csv('5FU-Ben.txt',sep='\t',header=None)
df = df.loc[:,0:63] #Remove the last row of nans...
area = np.zeros((len(df.T),))
for i in range(len(df.T)):
    area[i] = simps(df.loc[10:len(df),i],dx=15)

area = area.reshape((8,8))
#To plot doses on log space
t_d1 = np.linspace(0,1,8)
t_d2 = np.linspace(0,1,8)
t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10
t_d2[t_d2==0] = min(t_d2[t_d2!=0])/10
X, Y = np.meshgrid(t_d1, t_d2)
#fig = plt.figure()
#ax = Axes3D(fig) #<-- Note the difference from your original code...
#ax.plot_surface(np.log10(X),np.log10(Y),area,cmap=cm.coolwarm)




5fu is y


plt.plot(np.log10(Y[:,0]),area[:,0])
plt.plot(np.log10(X[0,:]),area[0,:])



df = pd.read_csv('Ben-Ben.txt',sep='\t',header=None)
df = df.loc[:,0:63] #Remove the last row of nans...
area = np.zeros((len(df.T),))
for i in range(len(df.T)):
    area[i] = simps(df.loc[10:len(df),i],dx=15)


area = area.reshape((8,8))

#To plot doses on log space
t_d1 = np.linspace(0,1,8)
t_d2 = np.linspace(0,1,8)
t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10
t_d2[t_d2==0] = min(t_d2[t_d2!=0])/10
X, Y = np.meshgrid(t_d1, t_d2)
#fig = plt.figure()
#ax = Axes3D(fig) #<-- Note the difference from your original code...
#ax.plot_surface(np.log10(X),np.log10(Y),area,cmap=cm.coolwarm)


df = pd.read_csv('5FU-5FU.txt',sep='\t',header=None)
df = df.loc[:,0:63] #Remove the last row of nans...
area = np.zeros((len(df.T),))
for i in range(len(df.T)):
    area[i] = simps(df.loc[10:len(df),i],dx=15)
area = area.reshape((8,8))
#To plot doses on log space
t_d1 = np.linspace(0,1,8)
t_d2 = np.linspace(0,1,8)
t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10
t_d2[t_d2==0] = min(t_d2[t_d2!=0])/10
X, Y = np.meshgrid(t_d1, t_d2)
fig = plt.figure()
ax = plt.subplot(111)
plt.plot(np.log10(Y[:,0]),area[:,0],label='5FU-5FU-Y')
plt.plot(np.log10(X[0,:]),area[0,:],label='5FU-5FU-X')

df = pd.read_csv('5FU-Ben.txt',sep='\t',header=None)
df = df.loc[:,0:63] #Remove the last row of nans...
area = np.zeros((len(df.T),))
for i in range(len(df.T)):
    area[i] = simps(df.loc[10:len(df),i],dx=15)
area = area.reshape((8,8))
#To plot doses on log space
t_d1 = np.linspace(0,1,8)
t_d2 = np.linspace(0,1,8)
t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10
t_d2[t_d2==0] = min(t_d2[t_d2!=0])/10
X, Y = np.meshgrid(t_d1, t_d2)
plt.plot(np.log10(Y[:,0]),area[:,0],label='5FU-Ben-Y')
plt.plot(np.log10(X[0,:]),area[0,:],label='5FU-Ben-X')


df = pd.read_csv('Ben-Ben.txt',sep='\t',header=None)
df = df.loc[:,0:63] #Remove the last row of nans...
area = np.zeros((len(df.T),))
for i in range(len(df.T)):
    area[i] = simps(df.loc[10:len(df),i],dx=15)
area = area.reshape((8,8))
#To plot doses on log space
t_d1 = np.linspace(0,1,8)
t_d2 = np.linspace(0,1,8)
t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10
t_d2[t_d2==0] = min(t_d2[t_d2!=0])/10
X, Y = np.meshgrid(t_d1, t_d2)
plt.plot(np.log10(Y[:,0]),area[:,0],label='Ben-Ben-Y')
plt.plot(np.log10(X[0,:]),area[0,:],label='Ben-Ben-X')
plt.legend()




 


####Misc code:###

#
#plt.figure()
#ax = plt.subplot(111)
#plt.plot(df.loc[:,0])
#d = np.diff(df.loc[:,0])
#plt.figure()
#ax = plt.subplot(111)
#plt.plot(d)
