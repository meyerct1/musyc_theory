#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Code for creating an array of SynCode_dipDB_template.py for fitting the dose response surfaces
One file for each concentration of drug each cell line.
Edit the expFile and filename variables to match the directory where the 
files should be written.  Code assumes SynCode is in this same folder.


Code commented and up to date 11/01/2018
@author: Christian Meyer 
@email: christian.t.meyer@vanderbilt.edu
"""
import os
###pydrc analysis of cellavista data 
#os.sys.path.append('/home/xnmeyer/Documents/Lab/Repos/pyDRC')
import numpy as np
import pandas as pd
import glob

experiments = glob.glob('data/*AUGC.csv') #List of experimental files

#File to replicate
filename = 'SynCode_template'
cnt = 1 
for e,expt in enumerate(experiments):                
    #Read in the data into a pandas data frame
    data = pd.read_table(expt, delimiter=',')
    if e == 0:
	data.to_csv('compiled_cokol_antifungal.csv',index=False)
    else:
	data.to_csv('compiled_cokol_antifungal.csv',mode='a',header=False,index=False)

