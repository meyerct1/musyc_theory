#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 28 14:24:01 2018

@author: xnmeyer
"""
#List of Synthetic lethal interactions
t = ['lat_ter',    'lat_tac',    'dyc_lat',    'sta_tac',    'fen_lat',    'rad_ter',    'hal_lat',    'lit_tac',    'fen_sta',    'dyc_sta',    'rap_ter',    'ben_pen',    'hal_sta',    'aba_wor',    'hyg_myr',    'myr_rad',    'ben_rad',    'lit_rad',    'rad_rap',    'qnn_rad',    'myr_tac',    'lit_sta',    'hyg_rad',    'lat_rap',    'hyg_sta',    'lit_rap',    'lat_tun',    'clo_rad',    'hyg_rap',    'sta_wor',    'myr_qnn',    'aba_lit',    'ben_lat',    'ben_cal',    'hyg_lat',    'rad_sta',    'lat_rad',    'cyc_rad']
ind = []
for i in t:
    d1 = i.split('_')[1]
    d2 = i.split('_')[0]
    ind.append(T[(T['drug1_name']==d1)&(T['drug2_name']==d2)].index[0])
    