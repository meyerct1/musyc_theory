#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 13:47:01 2018

@author: xnmeyer
"""

import pandas as pd
import numpy as np
from scipy.integrate import simps
import glob
import os
fils = glob.glob('*-*.txt')
dx = [];dy=[]
df_comp = pd.DataFrame()
#Doses according to Table 1 doi:10.1038/msb.2011.71
#Unit is in ug/ml
D = {'5FU':28,
     'ABA':280,
     'AMB':.49,
     'ANI':3.5,
     'BEN':28,
     'BRO':490,
     'C3P':21,
     'CAL':2.1,
     'CAN':140,
     'CHL':350,
     'CIS':80,
     'CLO':105,
     'CYC':.91,
     'DYC':49,
     'FEN':1.54,
     'HAL':56,
     'HYG':7,
     'LAT':14,
     'LIT':4500,
     'MET':1000,
     'MMS':175,
     'MYR':.35,
     'PEN':70,
     'QNN':1000,
     'QMY':30,
     'RAD':56,
     'RAP':.0049,
     'STA':1.26,
     'TAC':110,
     'TAM':2.8,
     'TER':10.5,
     'TUN':.35,
     'WOR':280}

#convert to g/ml
for k in D.keys():
    D[k]=D[k]/1e6
    
for f in fils:
    df = pd.read_csv(f,sep='\t',header=None)
    df = df.loc[:,0:63] #Remove the last row of nans...
    if f[0:3]!=f[4:7]:
        area = np.zeros((len(df.T),))
        for i in range(len(df.T)):
            area[i] = simps(df.loc[10:len(df),i],dx=15)
        area = area.reshape((8,8))
        #To plot doses on log space
        mxd1 = D[f[4:7].upper()]
        mxd2 = D[f[0:3].upper()]
        t_d1 = np.linspace(0,mxd1,8)
        t_d2 = np.linspace(0,mxd2,8)
        X, Y = np.meshgrid(t_d1, t_d2)
        df_tmp = pd.DataFrame()
        df_tmp['drug2.conc'] = X.flatten()
        df_tmp['drug1.conc'] = Y.flatten()
        df_tmp['effect'] = area.flatten()
        df_tmp['effect'] = df_tmp['effect']-df_tmp['effect'].min() #Remove the background for each experiment
        df_tmp['effect.95ci'] = max(area.flatten())/10.
        df_tmp['drug1'] = f[4:7]
        df_tmp['drug2'] = f[0:3]
        df_tmp['sample'] = 'yeast'
        df_tmp['expt'] = '06-20-2018'
        df_tmp.to_csv(f[4:7]+'_'+f[0:3]+'_AUGC.csv')
        
        
