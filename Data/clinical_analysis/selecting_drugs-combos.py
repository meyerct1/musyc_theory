#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 29 13:57:05 2019

@author: meyerct6
"""


import os
import pandas as pd
import numpy as np
from glob import glob

lst_fnd = pd.read_csv('matching_drug_names-11-29-2019_final.csv')
look_up_table = {}
for i in lst_fnd.index:
    tmp = lst_fnd.iloc[i,0]
    if tmp==1 or tmp==4:
        look_up_table[lst_fnd.loc[i,'screen_name']] = lst_fnd.loc[i,'cls_clinid_sort']
    elif tmp==2:
        look_up_table[lst_fnd.loc[i,'screen_name']] = lst_fnd.loc[i,'cls_clinid_set']
    elif tmp==3:
        look_up_table[lst_fnd.loc[i,'screen_name']] = lst_fnd.loc[i,'other_id']
        
#This fixes the names for the anti-fungal drugs
lk_table = {'5fu':'5 fluorouracil',
            'aba':'aureoblasidin a',
            'amb':'aureoblasidin b',
            'ani':'anisomycin',
            'ben':'benomyl',
            'bro':'bromopyruvate',
            'c3p':'cccp',
            'cal':'calyculin a',
            'can':'cantharidin',
            'chl':'chlorzoxazone',
            'cis':'cisplatin',
            'clo':'clozapine',
            'cyc':'cycloheximide',
            'dyc':'dyclonine',
            'fen':'fenpropimorph',
            'hal':'haloperidol',
            'hyg':'hygromycin',
            'lat':'latrunculin b',
            'lit':'lithium',
            'met':'methotrexate',
            'mms':'methyl methanesulfonate',
            'myr':'myriocin',
            'pen':'pentamidine',
            'qmy':'quinine',
            'qnn':'quinomycin',
            'rad':'radicicol',
            'rap':'rapamycin',
            'sta':'staurosporine',
            'tac':'tacrolimus',
            'tam':'tamoxifen',
            'ter':'terbinafine',
            'tun':'tunicamycin',
            'wor':'wortmannin'}

#For all datasets I have fit grab the drug names and the synergistic efficacy
fils = glob('raw_data/*.csv')
T = pd.DataFrame([])
for f in fils:
    print(f)
    tmp = pd.read_csv(f)
    if f.endswith('_0.csv'):
        tmp['expt']='mott_antimalaria'
    elif f.endswith('_AUGC.csv'):
        tmp['expt']='cokol_antifungal'
    elif f.endswith('10-29-2018.csv'):
        tmp['expt']='oneil_anticancer'
    elif f.endswith('reformatted.csv'):
        tmp['expt']='holbeck_anticancer'
    elif f.endswith('hiv-data.csv'):
        tmp['expt']='tan_antihiv'
    elif f.endswith('data_formatted.csv'):
        tmp['expt']='menden_anticancer'
    else:
        print('Error')
    #Fix the drug names
    tmp['drug1']=tmp['drug1'].str.lower()
    tmp['drug2']=tmp['drug2'].str.lower()
          
            
    T=T.append(tmp,ignore_index=True)

T.to_csv('all_data.csv')

d1 = T['drug1'].unique();d2=T['drug1'].unique()
for i in d1:
    if i in lk_table.keys():
        T.loc[T['drug1']==i,'drug1'] = lk_table[i]
for i in d2:
    if i in lk_table.keys():
        T.loc[T['drug2']==i,'drug2'] = lk_table[i]  
            
T.to_csv('all_data.csv')
#Read in the drug combinations
df_combo = pd.read_table('DCDB/DRUG_COMBINATION.txt',sep='\t')
#Add information about usage
df_combo =  df_combo.merge(pd.read_table('DCDB/DC_USAGE.txt',sep='\t').merge(pd.read_table('DCDB/DC_TO_DCU.txt',sep='\t'),on='DCU_ID',how='outer'),on='DC_ID')
cnt = 0
T['barcode'] = T['drug1']+'_'+T['drug2']

barcode_lst = []
#Go through the combinations in DCDB and find any matches to data MuSyC has analyzed
lk_tbl =  pd.DataFrame([look_up_table]).T
for i in df_combo.index:
    #If more than one single drug key is in the certified matches
    if sum(np.in1d(df_combo.loc[i,'DCC_ID'].split('/'),lk_tbl[0]))>1:
        print(i)
        tmp = []
        #Get the drg ids of the combination
        for k in np.array(df_combo.loc[i,'DCC_ID'].split('/'))[np.in1d(df_combo.loc[i,'DCC_ID'].split('/'),lk_tbl[0])]:
            tmp = tmp + list(lk_tbl[lk_tbl[0]==k].index)
        #Make all possible barcodes
        for e1 in np.arange(0,len(tmp)):
            for e2 in np.arange(e1+1,len(tmp)):
                barcode_lst.append(tmp[e1]+'_'+tmp[e2])
                barcode_lst.append(tmp[e2]+'_'+tmp[e1])


import pickle
pickle.dump(barcode_lst,open('barcodes_to_save.p','wb'))


