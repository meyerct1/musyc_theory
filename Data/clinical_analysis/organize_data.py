#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 09:13:29 2019

@author: meyerct6
"""

import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz
from scipy.stats import ttest_ind
from glob import glob

#Compile drug combination database entries  Download from (http://www.cls.zju.edu.cn/dcdb/searchdc.jsf) Date: 11-21-2019
#Identifiers
#DC is for each combination
#DCC is for each single drug
#DCU is drug combination usage with information about approval/stage
#DSY is the single drug synonyms


#Get a list of single drugs (i.e. components)
df_sin = pd.read_table('COMPONENTS.txt',sep='\t')
#Add the synonyms and brand names to a look up table
tmp = pd.read_table('DRUG_SYNONYMS.txt',sep='\t').merge(pd.read_table('DCC_TO_DRUGSYNONYMS.txt',sep='\t'),on='DSY_ID',how='outer')
tmp1 = pd.read_table('BRANDNAME.txt',sep='\t').merge(pd.read_table('DCC_TO_BRANDNAME.txt',sep='\t'),on='DBN_ID')
lk_up_tbl = {}
for i in df_sin.index:
    l = [df_sin.loc[i,'GENERIC_NAME']]
    if sum(df_sin.loc[i,'DCC_ID']==tmp1['DCC_ID'])>0:
        l = l + list(tmp1.loc[tmp1['DCC_ID']==df_sin.loc[i,'DCC_ID'],'BRANDNAME'])
    if sum(df_sin.loc[i,'DCC_ID']==tmp['DCC_ID'])>0:
        l = l + list(tmp.loc[tmp['DCC_ID']==df_sin.loc[i,'DCC_ID'],'SYNONYMS'])
    lk_up_tbl[df_sin.loc[i,'DCC_ID']] = l
    
#For all datasets I have fit grab the drug names and the synergistic efficacy
fils = glob('MasterResults*')
T = pd.DataFrame([])
for f in fils:
    tmp = pd.read_csv(f)
    tmp['study'] = f.split('_')[1].split('.')[0]
    T = T.append(tmp,ignore_index=True)

#This fixes the names for the anti-fungal drugs
lk_table = {'5fu':'5 fluorouracil',
            'aba':'aureoblasidin a',
            'amb':'aureoblasidin b',
            'ani':'anisomycin',
            'ben':'benomyl',
            'bro':'bromopyruvate',
            'c3p':'cccp',
            'cal':'calyculin a',
            'can':'cantharidin',
            'chl':'chlorzoxazone',
            'cis':'cisplatin',
            'clo':'clozapine',
            'cyc':'cycloheximide',
            'dyc':'dyclonine',
            'fen':'fenpropimorph',
            'hal':'haloperidol',
            'hyg':'hygromycin',
            'lat':'latrunculin b',
            'lit':'lithium',
            'met':'methotrexate',
            'mms':'methyl methanesulfonate',
            'myr':'myriocin',
            'pen':'pentamidine',
            'qmy':'quinine',
            'qnn':'quinomycin',
            'rad':'radicicol',
            'rap':'rapamycin',
            'sta':'staurosporine',
            'tac':'tacrolimus',
            'tam':'tamoxifen',
            'ter':'terbinafine',
            'tun':'tunicamycin',
            'wor':'wortmannin'}
for i in T.index:
    if T.loc[i,'drug1_name'] in lk_table.keys():
        T.loc[i,'drug1_name'] = lk_table[T.loc[i,'drug1_name']]
    if T.loc[i,'drug2_name'] in lk_table.keys():
        T.loc[i,'drug2_name'] = lk_table[T.loc[i,'drug2_name']]

#Find the unique set of names for all the drugs
search = list(set(list(T['drug1_name'])+list(T['drug2_name'])))
search = np.sort(search)
#Look through the look up table to find matches between the datasets
lst_fnd = {}
#for each drug
for s in search:
    #Look through all the look up table elements to find if there is a match
    for i in lk_up_tbl.keys():
        for k in lk_up_tbl[i]:
            #Use fuzzywuzzy (https://www.datacamp.com/community/tutorials/fuzzy-string-python)
            r = fuzz.token_sort_ratio(s.lower(),k.lower())
            if r>85: #Must match ~85%
                lst_fnd[i] = [k.lower(),s.lower()]
                print(k.lower()+':'+s.lower())
                break
            
#Read in the drug combinations
df_combo = pd.read_table('DRUG_COMBINATION.txt',sep='\t')
#Add information about usage
df_combo =  df_combo.merge(pd.read_table('DC_USAGE.txt',sep='\t').merge(pd.read_table('DC_TO_DCU.txt',sep='\t'),on='DCU_ID',how='outer'),on='DC_ID')
cnt = 0
T['barcode'] = T['drug1_name']+'_'+T['drug2_name']
data_index = {}
drg_lst = []
#Go through the combinations in DCDB and find any matches to data MuSyC has analyzed
for i in df_combo.index:
    #If more than one single drug key is in the certified matches
    if sum(np.in1d(df_combo.loc[i,'DCC_ID'].split('/'),lst_fnd.keys()))>1:
        tmp = []
        #Get the drg ids of the combination
        for k in np.array(df_combo.loc[i,'DCC_ID'].split('/'))[np.in1d(df_combo.loc[i,'DCC_ID'].split('/'),lst_fnd.keys())]:
            tmp.append(lst_fnd[k][1])
        brcd = []
        #Make all possible barcodes
        for e1 in range(len(tmp)):
            for e2 in range(len(tmp)-e1-1):
                brcd.append(tmp[e1]+'_'+tmp[e2+1])
                brcd.append(tmp[e2+1]+'_'+tmp[e1])
        #If the barcode is in MuSyC data add the information
        if sum(np.in1d(brcd,T['barcode']))>0:
            cnt = cnt+1
            data_index[i]= list(T.index[np.in1d(T['barcode'], brcd)])
            t = list(np.array(brcd)[np.in1d(brcd,T['barcode'])])
            for t in list(np.array(brcd)[np.in1d(brcd,T['barcode'])]):
                drg_lst.append(t.split('_')[0])
                drg_lst.append(t.split('_')[1])
        

#Split by Efficacious and non-efficacious and unknown efficacy
indx1=[]
indx2=[]
indx3=[]

for k in data_index.keys():
    if df_combo.loc[k,'EFFICACY'].startswith('Eff'):
        indx1 = indx1 + list(data_index[k])
    elif df_combo.loc[k,'EFFICACY'].startswith('Non-'):
        indx2 = indx2 + list(data_index[k])
    elif df_combo.loc[k,'EFFICACY'].startswith('Need'):
        indx3 = indx3 + list(data_index[k])
    else:
        print('error')
        
 
        
from scipy.stats import ttest_ind
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import rc
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)

plt.figure(facecolor='w',figsize=(7.5,2))
ax = []
ax.append(plt.subplot2grid((1,3),(0,0)))
ax.append(plt.subplot2grid((1,3),(0,1)))
ax.append(plt.subplot2grid((1,3),(0,2)))

plt.sca(ax[2])
labels = list(pd.DataFrame(df_combo.loc[data_index.keys(),'EFFICACY']).groupby('EFFICACY').size().index)
sizes = np.array(pd.DataFrame(df_combo.loc[data_index.keys(),'EFFICACY']).groupby('EFFICACY').size())
colors = ['purple', 'grey', 'green']
plt.pie(sizes,labels=labels, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)

plt.sca(ax[1])
labels = list(pd.DataFrame(df_combo.loc[data_index.keys(),'STAGE']).groupby('STAGE').size().index)
sizes = np.array(pd.DataFrame(df_combo.loc[data_index.keys(),'STAGE']).groupby('STAGE').size())
colors = cm.terrain(np.linspace(0,1,len(sizes)))
plt.pie(sizes,labels=labels, colors=colors,autopct='%.1f%%', shadow=True, startangle=140)

ind_all = np.array(T.index[(np.in1d(T['drug1_name'],drg_lst))&(np.in1d(T['drug2_name'],drg_lst))])
ind_all = list(ind_all[(np.in1d(ind_all,indx1))|(np.in1d(ind_all,indx2))])
labels = list(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size().index)
number_of_unique_conditions = np.array((pd.DataFrame(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size()).groupby('study').sum()))
for e,l in enumerate(labels):
    labels[e] = l+'\nn='+str(number_of_unique_conditions[e][0])
sizes = np.array(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size())
plt.sca(ax[0])
colors = cm.rainbow(np.linspace(0,1,len(sizes)))
plt.pie(sizes,labels=labels, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)
plt.tight_layout()
plt.savefig('Clinical_FigA.pdf')




plt.figure(facecolor='w',figsize=(8.5,2))
ax = []
ax.append(plt.subplot2grid((1,6),(0,0)))
ax.append(plt.subplot2grid((1,6),(0,1)))
ax.append(plt.subplot2grid((1,6),(0,2)))
ax.append(plt.subplot2grid((1,6),(0,3)))
ax.append(plt.subplot2grid((1,6),(0,4)))
ax.append(plt.subplot2grid((1,6),(0,5)))

labels = ['beta','log_alpha','loewe_fit_ec50','bliss_fit_ec50','hsa_fit_ec50','ci_raw_mean']
long_labels = [r'$\beta$',r'log($\alpha_{12/21}$)','Loewe','Bliss','HSA','CI']
ylim = None
style = 'violin'

for e1,l in enumerate(labels):
    plt.sca(ax[e1])
    
    if l is not 'log_alpha':      
        x1 = list(T.loc[indx1,l]);x1 = [eval(i) if isinstance(i,str) else i for i in x1];x1 = np.array([i[0] if isinstance(i,list) else i for i in x1]);x1_nancnt=sum(np.isnan(x1));x1=x1[~np.isnan(x1)]
        x2 = list(T.loc[indx2,l]);x2 = [eval(i) if isinstance(i,str) else i for i in x2];x2 = np.array([i[0] if isinstance(i,list) else i for i in x2]);x2_nancnt=sum(np.isnan(x2));x2=x2[~np.isnan(x2)]
        x3 = list(T.loc[indx3,l]);x3 = [eval(i) if isinstance(i,str) else i for i in x3];x3 = np.array([i[0] if isinstance(i,list) else i for i in x3]);x3_nancnt=sum(np.isnan(x3));x3=x3[~np.isnan(x3)]

    else:
        x1 = np.array(T.loc[indx1,['log_alpha1','log_alpha2']]).ravel()
        x2 = np.array(T.loc[indx2,['log_alpha1','log_alpha2']]).ravel()
        x3 = np.array(T.loc[indx3,['log_alpha1','log_alpha2']]).ravel()
                
    cols = ['purple', 'green', 'grey']

    if style=='violin':
        p1 = ax[e1].violinplot([x1,x2,x3],positions=[1,2,3],showmeans=False,showmedians=False,showextrema=False)
        for e,pc in enumerate(p1['bodies']):
            pc.set_facecolor(cols[e])
            pc.set_edgecolor('black')
            pc.set_alpha(1)
    
        ylim = [100,-100]
        p1 = plt.boxplot([x1,x2,x3],positions=[1,2,3],notch=False,patch_artist=True,widths=.05,showfliers=False,showcaps=False)
        for e,bp in enumerate(p1['boxes']):
            plt.setp(bp,edgecolor='k')
            plt.setp(bp,facecolor='k')
        for e,bp in enumerate(p1['medians']):
            plt.setp(bp,color='w',linewidth=2)
        for e,bp in enumerate(p1['whiskers']):    
            plt.setp(bp,color='k')
            lim = np.sort(bp.get_ydata())
            if lim[1]>ylim[1]:
                ylim[1]=lim[1]
            if lim[0]<ylim[0]:
                ylim[0]=lim[0]
                
    
    plt.xlim((.25,3.75))
    if ylim is not None:
        plt.ylim(ylim)
    plt.xticks([])
    plt.title(long_labels[e1])
    print(l)
    x = [x1,x2,x3]
    for k1 in range(2):
        for k2 in np.arange(k1+1,3):
            v = ttest_ind(x[k1],x[k2])
            if v.statistic>0:
                pv = v.pvalue/2
            else:
                pv = 1-v.pvalue/2
            print str(k1+1)+'-'+str(k2+1) + ' ' + str(pv)
plt.subplots_adjust(wspace=.5)

plt.savefig('1B.pdf')



        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
#Split by Efficacious and non-efficacious and unknown efficacy
indx1=[]
indx2=[]
indx3=[]

for k in data_index.keys():
    if df_combo.loc[k,'EFFICACY'].startswith('Eff'):
        indx1 = indx1 + list(data_index[k])
    else:
        indx2 = indx2 + list(data_index[k])
        
indx3 = np.array(T.index[(np.in1d(T['drug1_name'],drg_lst))&(np.in1d(T['drug2_name'],drg_lst))])
indx3 = list(indx3[~(np.in1d(indx3,indx1))&~(np.in1d(indx3,indx2))])
ttest_ind(T.loc[indx1,'beta'],T.loc[indx2,'beta'])

import matplotlib.pyplot as plt
plt.figure()
plt.boxplot(T.loc[indx1,'beta'],positions=[0.])
plt.boxplot(T.loc[indx2,'beta'],positions=[1.])
plt.boxplot(T.loc[indx3,'beta'],positions=[2.])
plt.xlim(-.5,2.5)


ttest_ind(T.loc[indx1,'beta'],T.loc[indx3,'beta'])
T.loc[indx3,'beta']

#        
#
#
#from matplotlib import cm
#from matplotlib import rc
#font = {'family' : 'normal',
#        'weight':'normal',
#        'size'   : 8}
#axes = {'linewidth': 2}
#rc('font', **font)
#rc('axes',**axes)
#
#plt.figure(facecolor='w',figsize=(7.5,2))
#ax = []
#ax.append(plt.subplot2grid((1,3),(0,0)))
#ax.append(plt.subplot2grid((1,3),(0,1)))
#ax.append(plt.subplot2grid((1,3),(0,2)))
#
#plt.sca(ax[2])
#labels = list(pd.DataFrame(df_combo.loc[data_index.keys(),'EFFICACY']).groupby('EFFICACY').size().index)
#sizes = np.array(pd.DataFrame(df_combo.loc[data_index.keys(),'EFFICACY']).groupby('EFFICACY').size())
#colors = ['purple', 'grey', 'green']
#plt.pie(sizes,labels=labels, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)
#
#plt.sca(ax[1])
#labels = list(pd.DataFrame(df_combo.loc[data_index.keys(),'STAGE']).groupby('STAGE').size().index)
#sizes = np.array(pd.DataFrame(df_combo.loc[data_index.keys(),'STAGE']).groupby('STAGE').size())
#colors = cm.terrain(np.linspace(0,1,len(sizes)))
#plt.pie(sizes,labels=labels, colors=colors,autopct='%.1f%%', shadow=True, startangle=140)
#
#
#ind_all = np.array(T.index[(np.in1d(T['drug1_name'],drg_lst))&(np.in1d(T['drug2_name'],drg_lst))])
#ind_all = list(ind_all[(np.in1d(ind_all,indx1))|(np.in1d(ind_all,indx2))])
#labels = list(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size().index)
#number_of_unique_conditions = np.array((pd.DataFrame(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size()).groupby('study').sum()))
#for e,l in enumerate(labels):
#    labels[e] = l+'\nn='+str(number_of_unique_conditions[e][0])
#sizes = np.array(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size())
#plt.sca(ax[0])
#colors = cm.rainbow(np.linspace(0,1,len(sizes)))
#plt.pie(sizes,labels=labels, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)
#plt.tight_layout()
#plt.savefig('Clinical_FigA.pdf')
#
#
#from scipy.stats import ttest_ind
#plt.figure(facecolor='w',figsize=(7.5,2))
#ax = []
#ax.append(plt.subplot2grid((1,7),(0,0)))
#ax.append(plt.subplot2grid((1,7),(0,1)))
#ax.append(plt.subplot2grid((1,7),(0,2)))
#ax.append(plt.subplot2grid((1,7),(0,3)))
#ax.append(plt.subplot2grid((1,7),(0,4)))
#ax.append(plt.subplot2grid((1,7),(0,5)))
#ax.append(plt.subplot2grid((1,7),(0,6)))
#
#labels = ['beta','log_alpha1','log_alpha2','loewe_fit_ec50','bliss_fit_ec50','hsa_fit_ec50','ci_raw_mean']
#long_labels = [r'$\beta$',r'log($\alpha_1$)',r'log($\alpha_2$)','Loewe','Bliss','HSA','CI']
#ylim = None
#for e1,l in enumerate(labels):
#    plt.sca(ax[e1])
#    x1 = list(T.loc[indx1,l]);x1 = [eval(i) if isinstance(i,str) else i for i in x1];x1 = np.array([i[0] if isinstance(i,list) else i for i in x1]);x1_nancnt=sum(np.isnan(x1));x1=x1[~np.isnan(x1)]
#    x2 = list(T.loc[indx2,l]);x2 = [eval(i) if isinstance(i,str) else i for i in x2];x2 = np.array([i[0] if isinstance(i,list) else i for i in x2]);x2_nancnt=sum(np.isnan(x2));x2=x2[~np.isnan(x2)]
#    x3 = list(T.loc[indx3,l]);x3 = [eval(i) if isinstance(i,str) else i for i in x3];x3 = np.array([i[0] if isinstance(i,list) else i for i in x3]);x3_nancnt=sum(np.isnan(x3));x3=x3[~np.isnan(x3)]
#    p1 = plt.boxplot([x1,x2,x3],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False)
#    cols = [[0,.566,0],[1.,1.,0],[0,0,1.]]
#    for e,bp in enumerate(p1['boxes']):
#        plt.setp(bp,edgecolor='k')
#        plt.setp(bp,facecolor=cols[e])
#    for e,bp in enumerate(p1['medians']):
#        plt.setp(bp,color='r',linewidth=2)
#    for e,bp in enumerate(p1['whiskers']):    
#        plt.setp(bp,color='k')
#    for e,bp in enumerate(p1['caps']):
#        plt.setp(bp,color='k')
#    
#    plt.xlim((.25,3.75))
#    if ylim is not None:
#        plt.ylim(ylim)
#    plt.xticks([])
#    plt.title(long_labels[e1])
#    print(l)
#    print '1-2: ' + str(ttest_ind(x1,x2).pvalue) 
#    print '1-3: ' + str(ttest_ind(x1,x3).pvalue)
#    print '2-3: ' + str(ttest_ind(x2,x3).pvalue)
#
#plt.savefig('1B.pdf')
#
#
#



from scipy.stats import ttest_ind
plt.figure(facecolor='w',figsize=(7.5,2))
ax = []
ax.append(plt.subplot2grid((1,7),(0,0)))
ax.append(plt.subplot2grid((1,7),(0,1)))
ax.append(plt.subplot2grid((1,7),(0,2)))
ax.append(plt.subplot2grid((1,7),(0,3)))
ax.append(plt.subplot2grid((1,7),(0,4)))
ax.append(plt.subplot2grid((1,7),(0,5)))
ax.append(plt.subplot2grid((1,7),(0,6)))

labels = ['beta','log_alpha1','log_alpha2','loewe_fit_ec50','bliss_fit_ec50','hsa_fit_ec50','ci_raw_mean']
long_labels = [r'$\beta$',r'log($\alpha_1$)',r'log($\alpha_2$)','Loewe','Bliss','HSA','CI']
ylim = None
for e1,l in enumerate(labels):
    plt.sca(ax[e1])
    x1 = list(T.loc[indx1,l]);x1 = [eval(i) if isinstance(i,str) else i for i in x1];x1 = np.array([i[0] if isinstance(i,list) else i for i in x1]);x1_nancnt=sum(np.isnan(x1));x1=x1[~np.isnan(x1)]
    x2 = list(T.loc[indx2,l]);x2 = [eval(i) if isinstance(i,str) else i for i in x2];x2 = np.array([i[0] if isinstance(i,list) else i for i in x2]);x2_nancnt=sum(np.isnan(x2));x2=x2[~np.isnan(x2)]
    p1 = plt.boxplot([x1,x2],positions=[1,2],notch=False,patch_artist=True,widths=.75,showfliers=False)
    cols = [[1.,1.,0],[0,0,1.]]
    for e,bp in enumerate(p1['boxes']):
        plt.setp(bp,edgecolor='k')
        plt.setp(bp,facecolor=cols[e])
    for e,bp in enumerate(p1['medians']):
        plt.setp(bp,color='r',linewidth=2)
    for e,bp in enumerate(p1['whiskers']):    
        plt.setp(bp,color='k')
    for e,bp in enumerate(p1['caps']):
        plt.setp(bp,color='k')
    
    plt.xlim((.25,2.75))
    if ylim is not None:
        plt.ylim(ylim)
    plt.xticks([])
    plt.title(long_labels[e1])
    print(l)
    print '1-2: ' + str(ttest_ind(x1,x2).pvalue) 

plt.savefig('1B.pdf')
