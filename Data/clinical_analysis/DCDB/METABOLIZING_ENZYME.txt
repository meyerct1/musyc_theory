EZ_ID	NAME	GENE_SYMBOL	GENE_ID	GENEBANK_PROTEIN	UNIPROT_ACCESSIONNUMBER	HGNC_ID	ORGANISM
ENZ00001	Prostaglandin G/H synthase 2	PTGS2	291988	L15326	P35354	HGNC:9605	Homo sapiens
ENZ00002	Cytosolic phospholipase A2	PLA2G4A	190007	M72393	P47712	HGNC:9035	Homo sapiens
ENZ00003	Xanthine dehydrogenase/oxidase	XDH	10336525	D11456	P47989	HGNC:12805	Homo sapiens
ENZ00004	Cytochrome P450 3A4	CYP3A4		M18907	P08684	HGNC:2637	Homo sapiens
ENZ00005	Cytochrome P450 2E1	CYP2E1	181360	J02625	P05181	GNC:2631	Homo sapiens
ENZ00006	Cytochrome P450 19A1	CYP19A1	179002	M22246	P11511	HGNC:2594	Homo sapiens
ENZ00007	Cytochrome P450 3A5	CYP3A5	181346	J04813	P20815	HGNC:2638	Homo sapiens
ENZ00008	Cytochrome P450 2D6	CYP2D6	181350	M20403	P10635	HGNC:2625	Homo sapiens
ENZ00009	Cytochrome P450 2C9	CYP2C9		AY341248	P11712	HGNC:2623	Homo sapiens
ENZ00010	Cytochrome P450 2C8	CYP2C8		M17397	P10632	HGNC:2622	Homo sapiens
ENZ00011	Cytochrome P450 2C19	CYP2C19	181344	M61854	P33261	GNC:2621	Homo sapiens
ENZ00012	Cytochrome P450 3A7	CYP3A7	220149	D00408	P24462	GNC:2640	Homo sapiens
ENZ00013	Cob(I)yrinic acid a,c-diamide adenosyltransferase, mitochondrial	MMAB	26284726	AF550404	Q96EY8	HGNC:19331	Homo sapiens
ENZ00014	Cholinesterase	BCHE	1311630	M32391	P06276	HGNC:983	Homo sapiens
ENZ00015	Prostaglandin G/H synthase 1	PTGS1	387018	M31822	P23219	HGNC:9604	Homo sapiens
ENZ00016	Cytochrome P450 1A2	CYP1A2		Z00036	P05177	HGNC:2596	Homo sapiens
ENZ00017	Cytochrome P450 1A1	CYP1A1	181276	K03191	P04798	GNC:2595	Homo sapiens
ENZ00018	Cytochrome P450 2B6	CYP2B6	181296	M29874	P20813	GNC:2615	Homo sapiens
ENZ00019	Cytochrome P450 1B1	CYP1B1	501031	U03688	Q16678	HGNC:2597	Homo sapiens
ENZ00020	Cytochrome P450 2A6	CYP2A6		X13897	P11509	HGNC:2610	Homo sapiens
ENZ00021	UDP-glucuronosyltransferase 1-1	UGT1A1	184473	M57899	P22309	GNC:12530	Homo sapiens
ENZ00022	Serum paraoxonase/lactonase 3	PON3	12751374	AF320003	Q15166	GNC:9206	Homo sapiens
ENZ00023	UDP-glucuronosyltransferase 1-3	UGT1A3	340135	M84127	P35503	GNC:12535	Homo sapiens
ENZ00024	UDP-glucuronosyltransferase 2B7	UGT2B7	340080	J05428	P16662	GNC:12554	Homo sapiens
ENZ00025	Cytochrome P450 11B1, mitochondrial	CYP11B1	181333	M32879	P15538	HGNC:2591	Homo sapiens
ENZ00026	Cytochrome P450 2C18	CYP2C18		M61853	P33260	GNC:2620	Homo sapiens
ENZ00027	Cholesterol side-chain cleavage enzyme, mitochondrial	CYP11A1	181376	M14565	P05108	GNC:2590	Homo sapiens
ENZ00028	Aromatic-L-amino-acid decarboxylase	DDC	181521	M76180	P20711	HGNC:2719	Homo sapiens
ENZ00029	Adenosine deaminase	ADA	28380	X02994	P00813	HGNC:186	Homo sapiens
ENZ00030	UDP-glucuronosyltransferase 2B4	UGT2B4	37589	Y00317	P06133	GNC:12553	Homo sapiens
ENZ00031	UDP-glucuronosyltransferase 1-8	UGT1A8	2613044	AF030310	Q9HAW9	GNC:12540	Homo sapiens
ENZ00032	UDP-glucuronosyltransferase 2B15				P54855		
ENZ00033	Nucleoside diphosphate kinase A	NME1		X75598	P15531	HGNC:7849	Homo sapiens
ENZ00034	Nucleoside diphosphate kinase B	NME2		X58965	P22392	HGNC:7850	Homo sapiens
ENZ00035	Adenylate kinase 2, mitochondrial	AK2			P54819		Homo sapiens
ENZ00036	"GTP:AMP phosphotransferase AK4, mitochondrial "				P27144		
ENZ00037	NADPH--cytochrome P450 reductase	POR	247307	S90469	P16435	HGNC:9208	Homo sapiens
ENZ00038	UDP-glucuronosyltransferase 1-9	UGT1A9	7690346	S55985	O60656	GNC:12541	Homo sapiens
ENZ00039	UDP-glucuronosyltransferase 1-4	UGT1A4	184475	M57951	P22310	GNC:12536	Homo sapiens
ENZ00040	UDP-glucuronosyltransferase 1-10	UGT1A10	2039362	U89508	Q9HAW8	GNC:12531	Homo sapiens
ENZ00041	UDP-glucuronosyltransferase 1-6				P19224		
ENZ00042	Bile salt sulfotransferase	SULT2A1	306702	L20000	Q06520	HGNC:11458	Homo sapiens
ENZ00043	Sulfotransferase 1A1	SULT1A1	179042	L10819	P50225	HGNC:11453	Homo sapiens
ENZ00044	Estrogen sulfotransferase	SULT1E1	488283	U08098	P49888	HGNC:11377	Homo sapiens
ENZ00045	Arylamine N-acetyltransferase 2	NAT2	219412	D90040	P11245	GNC:7646	Homo sapiens
ENZ00046	Sulfotransferase 1A3/1A4				P50224		
ENZ00047	Liver carboxylesterase 1	CES1		M73499	P23141	HGNC:1863	Homo sapiens
ENZ00048	Dimethylaniline monooxygenase [N-oxide-forming] 3	FMO3	188631	M83772	P31513	GNC:3771	Homo sapiens
ENZ00049	Aldehyde oxidase	AOX1	438656	L11005	Q06278	GNC:553	Homo sapiens
ENZ00050	Acetylcholinesterase	ACHE	177975	M55040	P22303	HGNC:108	Homo sapiens
ENZ00051	Amine oxidase [flavin-containing] A	MAOA	187353	M68840	P21397	HGNC:6833	Homo sapiens
ENZ00052	Beta-lactamase	SHV-7	684934	U20270	Q46759		Escherichia coli
ENZ00053	Thymidine kinase, cytosolic	TK1	339709	K02581	P04183	HGNC:11830	Homo sapiens
ENZ00054	Thymidine phosphorylase	TYMP	189701	M63193	P19971	HGNC:3148	Homo sapiens
ENZ00055	Thiopurine S-methyltransferase	TPMT	386420	S62904	P51580	HGNC:12014	Homo sapiens
ENZ00056	Cytidine deaminase	CDA		L27943	P32320	HGNC:1712	Homo sapiens
ENZ00057	Deoxycytidine kinase	DCK		M60527	P27707	HGNC:2704	Homo sapiens
ENZ00058	Cytochrome P450 4A11	CYP4A11	181397	L04751	Q02928	HGNC:2642	Homo sapiens
ENZ00059	CYP2B protein	CYP2B	181294	M29873	Q14097		Homo sapiens
ENZ00060	Catechol O-methyltransferase	COMT	180920	M65212	P21964	HGNC:2228	Homo sapiens
ENZ00061	Dimethylaniline monooxygenase [N-oxide-forming] 1	FMO1	182671	M64082	Q01740	GNC:3769	Homo sapiens
ENZ00062	Glutathione S-transferase Mu 1	GSTM1	31924	X08020	P09488	HGNC:4632	Homo sapiens
ENZ00063	Glutathione S-transferase P	GSTP1	31946	M24485	P09211	HGNC:4638	Homo sapiens
ENZ00064	Glutathione S-transferase theta-1	GSTT1	510905	X79389	P30711	HGNC:4641	Homo sapiens
ENZ00065	Myeloperoxidase	MPO	189040	J02694	P05164	HGNC:7218	Homo sapiens
ENZ00066	NAD(P)H dehydrogenase [quinone] 1	NQO1	189246	J03934	P15559	HGNC:2874	Homo sapiens
ENZ00067	Superoxide dismutase [Cu-Zn]	SOD1		L44139	P00441	HGNC:11179	Homo sapiens
ENZ00068	Arylamine N-acetyltransferase	nat			P0A5L8		
ENZ00069	Metallothionein-1A				P04731		
ENZ00070	Metallothionein-2				P02795		
ENZ00071	Methylenetetrahydrofolate reductase	MTHFR	6139053	U09806	P42898	HGNC:7436	Homo sapiens
ENZ00072	Thymidylate synthase	TYMS	37479	X02308	P04818	HGNC:12441	Homo sapiens
ENZ00073	Amidophosphoribosyltransferase	PPAT	219459	D13757	Q06203	HGNC:9238	Homo sapiens
ENZ00074	Dihydropyrimidine dehydrogenase [NADP+]	DPYD	558305	U09178	Q12882	HGNC:3012	Homo sapiens
ENZ00075	Uridine phosphorylase 1	UPP1		X90858	Q16831	HGNC:12576	Homo sapiens
ENZ00076	Uridine phosphorylase 2	UPP2		AY225131	O95045	HGNC:23061	Homo sapiens
ENZ00077	"Uridine 5'-monophosphate synthase"	UMPS			P11172		Homo sapiens
ENZ00078	Gamma-glutamyl hydrolase	GGH	2951931	U55206	Q92820	HGNC:4248	Homo sapiens
ENZ00079	Dihydrofolate reductase	DHFR	182724	J00140	P00374	HGNC:2861	Homo sapiens
ENZ00080	Folylpolyglutamate synthase, mitochondrial	FPGS	292029	M98045	Q05932	HGNC:3824	Homo sapiens
ENZ00081	Bifunctional purine biosynthesis protein PURH [Includes: Phosphoribosylaminoimidazolecarboxamide formyltransferase	ATIC	1263196	U37436	P31939	HGNC:794	Homo sapiens
ENZ00085	Amiloride-sensitive amine oxidase [copper-containing]	ABP1			Q9TRC7		
ENZ00086	Amine oxidase [flavin-containing] B	MAOB	398415	S62734	P27338	HGNC:6834	Homo sapiens
ENZ00087	Equilibrative nucleoside transporter 1	SLC29A1	1845345	U81375	Q99808	HGNC:11003	Homo sapiens
ENZ00088	UDP-glucuronosyltransferase 2B17	UGT2B17	3287473	U59209	O75795	GNC:12547	Homo sapiens
ENZ00089	"Aminoglycoside 2'-N-acetyltransferase"	aac		U72714	P0A5N0		Mycobacterium tuberculosis
ENZ00082	6-phosphogluconate dehydrogenase, decarboxylating	PGD	984325	U30255	P52209	GNC:8891	Homo sapiens
ENZ00083	Cytochrome P450 3A43	CYP3A43	12642642	AF319634	Q9HB55	GNC:17450	Homo sapiens
ENZ00084	Amiloride-sensitive amine oxidase [copper-containing]	ABP1	177960	M55602	P19801	HGNC:80	Homo sapiens
ENZ00090	UDP-glucuronosyltransferase 1-7				Q9HAW7		
ENZ00091	Cocaine esterase				O00748		
ENZ00092	Choline-phosphate cytidylyltransferase A	PCYT1A	575486	L28957	P49585	HGNC:8754	Homo sapiens
ENZ00093	UMP-CMP kinase	CMPK1	6578133	AF070416	P30085	HGNC:18170	Homo sapiens
ENZ00094	Phosphoglycerate kinase 1	PGK1			P00558		Homo sapiens
ENZ00095	Ethanolamine-phosphate cytidylyltransferase				Q99447		
ENZ00096	"5'(3')-deoxyribonucleotidase, cytosolic type "				Q8TCD5		
ENZ00097	Cytochrome P450 11B2, mitochondrial	CYP11B2	35200	X54741	P19099	GNC:2592	Homo sapiens
ENZ00098	Beta-lactamase PSE-2	bla	1019897	U37105	P14489		Pseudomonas aeruginosa
ENZ00099	Adenosine kinase	ADK	1224125	U50196	P55263	GNC:257	Homo sapiens
ENZ00100	Fatty-acid amide hydrolase	FAAH	2149156	U82535	O00519	HGNC:3553	Homo sapiens
ENZ00101	Chloramphenicol acetyltransferase	cat	4104539	AF036933	P26841		Pseudomonas aeruginosa
ENZ00102	Histamine N-methyltransferase	HNMT		D16224	P50135	HGNC:5028	Homo sapiens
ENZ00103	"5'-nucleotidase"	NT5E	23897	X55740	P21589	HGNC:8021	Homo sapiens
ENZ00104	Deoxycytidylate deaminase				P32321		
ENZ00105	Branched-chain-amino-acid aminotransferase, cytosolic	BCAT1	1036780	U21551	P54687	HGNC:976	Homo sapiens
ENZ00106	Nitric oxide synthase, inducible	NOS2	292242	L09210	P35228	HGNC:7873	Homo sapiens
ENZ00107	Nitric-oxide synthase, brain	NOS1	642526	U17327	P29475	HGNC:7872	Homo sapiens
ENZ00108	NADH dehydrogenase [ubiquinone] iron-sulfur protein 3, mitochondrial	NDUFS3	3337441	AF067139	O75489	HGNC:7710	Homo sapiens
ENZ00109	Nitric-oxide synthase, endothelial	NOS3	189212	M93718	P29474	HGNC:7876	Homo sapiens
ENZ00110	NADH dehydrogenase [ubiquinone] iron-sulfur protein 7, mitochondrial	NDUFS7	3342734	AC005329	O75251	HGNC:7714	Homo sapiens
ENZ00111	Aldo-keto reductase family 1 member C3	AKR1C3	4261711	S68288	P42330	HGNC:386	Homo sapiens
ENZ00112	NADH dehydrogenase [ubiquinone] iron-sulfur protein 2, mitochondrial	NDUFS2	3540239	AF013160	O75306	HGNC:7708	Homo sapiens
ENZ00113	Carbonyl reductase [NADPH] 1	CBR1		J04056	P16152	HGNC:1548	Homo sapiens
ENZ00114	Alcohol dehydrogenase [NADP+]	AKR1A1	178481	J04794	P14550	GNC:380	Homo sapiens
ENZ00115	Carbonyl reductase [NADPH] 3				O75828		
ENZ00116	Arylamine N-acetyltransferase 1	NAT1	219414	D90041	P18440	GNC:7645	Homo sapiens
ENZ00117	Alcohol dehydrogenase 6	ADH6	193787401	AK092768	P28332	GNC:255	Homo sapiens
ENZ00118	Heparanase	HPSE	5616197	AF152376	Q9Y251	HGNC:5164	Homo sapiens
ENZ00119	Glutamine synthetase	GLUL	31833	Y00387	P15104	HGNC:4341	Homo sapiens
ENZ00120	Kanamycin nucleotidyltransferase	knt	46496	X03408	P05057		Staphylococcus aureus
ENZ00121	"Aminoglycoside 3'-phosphotransferase"	aphA1	43026	V00359	P00551		Escherichia coli
ENZ00122	Cytochrome P450 17A1	CYP17A1	181342	M14564	P05093	HGNC:2593	Homo sapiens
ENZ00123	Cytochrome P450 24A1, mitochondrial	CYP24A1	306704	L13286	Q07973	HGNC:2602	Homo sapiens
ENZ00124	Leukotriene-B(4) omega-hydroxylase 1				P78329		
ENZ00125	Sphingosine kinase 1				Q9NYA1		
ENZ00126	Cytochrome P450 4F12				Q9HCS2		
ENZ00127	Cytochrome P450 2R1	CYP2R1	33591222	AY323817	Q6VVX0	HGNC:20580	Homo sapiens
ENZ00128	Cytochrome P450 27, mitochondrial	CYP27A1	181292	M62401	Q02318	HGNC:2605	Homo sapiens
ENZ00129	Cytochrome P450 2J2	CYP2J2	18254513	U37143	P51589	GNC:2634	Homo sapiens
ENZ00130	3 beta-hydroxysteroid dehydrogenase/Delta 5-->4-isomerase type II	HSD3B2	184401	M67466	P26439	HGNC:5218	Homo sapiens
ENZ00132	Prolyl endopeptidase	PREP	558596	X74496	P48147	HGNC:9358	Homo sapiens
ENZ00134	Glutathione S-transferase A2	GSTA2	306811	M16594	P09210	HGNC:4627	Homo sapiens
ENZ00135	Glutathione S-transferase A1	GSTA1	306809	M15872	P08263	HGNC:4626	Homo sapiens
ENZ00136	25-hydroxyvitamin D-1 alpha hydroxylase, mitochondrial	CYP27B1	2612976	AF027152	O15528	HGNC:2606	Homo sapiens
ENZ00137	Cytochrome P450 26A1	CYP26A1		AF005418	O43174	HGNC:2603	Homo sapiens
ENZ00142	Cytochrome P450 2A13	CYP2A13	11494143	AF209774	Q16696	GNC:2608	Homo sapiens
ENZ00143	Bleomycin hydrolase				Q13867		
ENZ00145	Carboxylesterase 1A1				Q6LAP9		
ENZ00146	Aldehyde dehydrogenase, mitochondrial	ALDH2	28606	X05409	P05091	HGNC:404	Homo sapiens
ENZ00147	Cytochrome P450 4B1	CYP4B1			P13584		
ENZ00149	Mannitol dehydrogenase	mtlD		AF007800	O08355		Pseudomonas fluorescens
ENZ00152	Microsomal glutathione S-transferase 2	MGST2	1747521	U77604	Q99735	HGNC:7063	Homo sapiens
ENZ00153	Cytochrome P450 51A1	CYP51A1	1698396	U23942	Q16850	HGNC:2649	Homo sapiens
ENZ00158	Neprilysin	MME	34758	X07166	P08473	HGNC:7154	Homo sapiens
ENZ00159	Dipeptidyl peptidase 4	DPP4	535388	U13735	P27487	HGNC:3009	Homo sapiens
ENZ00161	Steryl-sulfatase	STS	338565	J04964	P08842	HGNC:11425	Homo sapiens
