#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 30 15:03:21 2019

@author: meyerct6
"""
# =============================================================================
# Import packages
# =============================================================================
import pandas as pd
import numpy as np
from scipy.stats import ttest_ind
from glob import glob
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import rc
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
# =============================================================================
# Read in MuSyC data fits from the different databases
# =============================================================================
fils = glob('data/*/MasterResults.csv')
T = pd.DataFrame([])
for f in fils:
    tmp = pd.read_csv(f)
    tmp['study'] = f.split('/')[1]
    T = T.append(tmp,ignore_index=True)
#Drop samples which did not converge.
T = T[T['converge_nlls']==1].reset_index(drop=True)
#This fixes the names for the anti-fungal drugs
lk_table = {'5fu':'5 fluorouracil',
            'aba':'aureoblasidin a',
            'amb':'aureoblasidin b',
            'ani':'anisomycin',
            'ben':'benomyl',
            'bro':'bromopyruvate',
            'c3p':'cccp',
            'cal':'calyculin a',
            'can':'cantharidin',
            'chl':'chlorzoxazone',
            'cis':'cisplatin',
            'clo':'clozapine',
            'cyc':'cycloheximide',
            'dyc':'dyclonine',
            'fen':'fenpropimorph',
            'hal':'haloperidol',
            'hyg':'hygromycin',
            'lat':'latrunculin b',
            'lit':'lithium',
            'met':'methotrexate',
            'mms':'methyl methanesulfonate',
            'myr':'myriocin',
            'pen':'pentamidine',
            'qmy':'quinine',
            'qnn':'quinomycin',
            'rad':'radicicol',
            'rap':'rapamycin',
            'sta':'staurosporine',
            'tac':'tacrolimus',
            'tam':'tamoxifen',
            'ter':'terbinafine',
            'tun':'tunicamycin',
            'wor':'wortmannin'}
for i in T.index:
    if T.loc[i,'drug1_name'] in lk_table.keys():
        T.loc[i,'drug1_name'] = lk_table[T.loc[i,'drug1_name']]
    if T.loc[i,'drug2_name'] in lk_table.keys():
        T.loc[i,'drug2_name'] = lk_table[T.loc[i,'drug2_name']]
#Normalize HSA
T['hsa_fit_ec50_norm']=np.nan
for expt in T['study'].unique():
    sub_T = T[T['study']==expt]
    e0 = sub_T['E0'].max()
    em = sub_T[['E1','E2','E3']].min().min()
    hs = sub_T['hsa_fit_ec50'].values
    hs = list(sub_T['hsa_fit_ec50']);hs = [eval(i) if isinstance(i,str) else i for i in hs];hs = np.array([i[0] if isinstance(i,list) else i for i in hs]);
    hs=hs/(e0-em)
    T.loc[T['study']==expt,'hsa_fit_ec50_norm']=hs 
        

# =============================================================================
# Read in table matching drug names between DCDB and MuSyC data...
# =============================================================================
lst_fnd = pd.read_csv('matching_drug_names-11-29-2019_final.csv')
look_up_table = {}
for i in lst_fnd.index:
    tmp = lst_fnd.iloc[i,0]
    if tmp==1 or tmp==4:
        look_up_table[lst_fnd.loc[i,'screen_name']] = lst_fnd.loc[i,'cls_clinid_sort']
    elif tmp==2:
        look_up_table[lst_fnd.loc[i,'screen_name']] = lst_fnd.loc[i,'cls_clinid_set']
    elif tmp==3:
        look_up_table[lst_fnd.loc[i,'screen_name']] = lst_fnd.loc[i,'other_id']
#Convert to a dataframe  
lk_tbl =  pd.DataFrame([look_up_table]).T
      
# =============================================================================
# Read in the DCDB data 
# =============================================================================
#Compile drug combination database entries  Download from (http://www.cls.zju.edu.cn/dcdb/searchdc.jsf) Date: 11-21-2019
#Identifiers
#DC is for each combination
#DCC is for each single drug
#DCU is drug combination usage with information about approval/stage
#DSY is the single drug synonyms
#Read in the drug combinations
df_combo = pd.read_table('DRUG_COMBINATION.txt',sep='\t')
#Add information about usage
df_combo =  df_combo.merge(pd.read_table('DC_USAGE.txt',sep='\t').merge(pd.read_table('DC_TO_DCU.txt',sep='\t'),on='DCU_ID',how='outer'),on='DC_ID')

# =============================================================================
# Find matches in DCDB to MuSyc data.
# =============================================================================
mtc_num = 2  #If 2 look for combinations of only 2 drugs with exact match.  if 3 look for combinations of 2 or more which have at least 2 drugs from the musyc dataset

cnt = 0 #counter for number of combinations
#drug combination barcode for musyc data
T['barcode'] = T['drug1_name']+'_'+T['drug2_name']
#Store the index of the musyc data with matching combinations for each combination in DCDB in a dictonary
data_index = {}
#For each combination in DCDB, look for matches to musyc data
for i in df_combo.index:
    if mtc_num==3:
        cont = sum(np.in1d(df_combo.loc[i,'DCC_ID'].split('/'),lk_tbl[0]))>1
    elif mtc_num==2:
        cont = (sum(np.in1d(df_combo.loc[i,'DCC_ID'].split('/'),lk_tbl[0]))==2) and (len(df_combo.loc[i,'DCC_ID'].split('/'))==2)
    else:
        print('error: wrong number for mtc_num')
    if cont:
        tmp = []
        #Get the drug ids of the combination
        for k in np.array(df_combo.loc[i,'DCC_ID'].split('/')):
            tmp = tmp + list(lk_tbl[lk_tbl[0]==k].index)
        #Make all possible barcodes
        barcode_lst=[]
        for e1 in np.arange(0,len(tmp)):
            for e2 in np.arange(e1+1,len(tmp)):
                barcode_lst.append(tmp[e1]+'_'+tmp[e2])
                barcode_lst.append(tmp[e2]+'_'+tmp[e1])
        #If the barcode exists in the musyc data, store index of matches.
        if sum(np.in1d(T['barcode'],barcode_lst))>0:
            cnt = cnt+1
            data_index[i]= list(T.index[np.in1d(T['barcode'], barcode_lst)])

# =============================================================================
# Now sort the drug combinations by whether they were efficacious or non-efficacious.
# =============================================================================
indx1=[]
indx2=[]
indx3=[]
for k in data_index.keys():
    if df_combo.loc[k,'EFFICACY'].startswith('Eff'):
        indx1 = indx1 + list(data_index[k])
    elif df_combo.loc[k,'EFFICACY'].startswith('Non-'):
        indx2 = indx2 + list(data_index[k])
    elif df_combo.loc[k,'EFFICACY'].startswith('Need'):
        indx3 = indx3 + list(data_index[k])
    else:
        print('error')
        
        

# =============================================================================
# Make figure describing clinical combinations
# =============================================================================
plt.figure(facecolor='w',figsize=(7.5,2))
ax = []
ax.append(plt.subplot2grid((1,3),(0,0)))
ax.append(plt.subplot2grid((1,3),(0,1)))
ax.append(plt.subplot2grid((1,3),(0,2)))

plt.sca(ax[2])
labels = list(pd.DataFrame(df_combo.loc[data_index.keys(),'EFFICACY']).groupby('EFFICACY').size().index)
sizes = np.array(pd.DataFrame(df_combo.loc[data_index.keys(),'EFFICACY']).groupby('EFFICACY').size())
colors = ['purple', 'grey', 'green']
plt.pie(sizes,labels=labels, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)

plt.sca(ax[1])
labels = list(pd.DataFrame(df_combo.loc[data_index.keys(),'STAGE']).groupby('STAGE').size().index)
sizes = np.array(pd.DataFrame(df_combo.loc[data_index.keys(),'STAGE']).groupby('STAGE').size())
colors = cm.terrain(np.linspace(0,1,len(sizes)))
plt.pie(sizes,labels=labels, colors=colors,autopct='%.1f%%', shadow=True, startangle=140)

ind_all = np.array(T.index)
ind_all = list(ind_all[(np.in1d(ind_all,indx1))|(np.in1d(ind_all,indx2))])
labels = list(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size().index)
number_of_unique_conditions = np.array((pd.DataFrame(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size()).groupby('study').sum()))
for e,l in enumerate(labels):
    labels[e] = l+'\nn='+str(number_of_unique_conditions[e][0])
sizes = np.array(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size())
plt.sca(ax[0])
colors = cm.rainbow(np.linspace(0,1,len(sizes)))
plt.pie(sizes,labels=labels, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)
plt.tight_layout()
plt.savefig('Clinical_FigA.pdf')


# =============================================================================
# Analyze synergy metrics
# =============================================================================

plt.figure(facecolor='w',figsize=(8.5,2))
ax = []
ax.append(plt.subplot2grid((1,6),(0,0)))
ax.append(plt.subplot2grid((1,6),(0,1)))
ax.append(plt.subplot2grid((1,6),(0,2)))
ax.append(plt.subplot2grid((1,6),(0,3)))
ax.append(plt.subplot2grid((1,6),(0,4)))
ax.append(plt.subplot2grid((1,6),(0,5)))

labels = ['beta','log_alpha','loewe_fit_ec50','bliss_fit_ec50','hsa_fit_ec50_norm','ci_raw_mean']
long_labels = [r'$\beta$',r'log($\alpha_{12/21}$)','Loewe','Bliss','HSA','CI']
ylim = None
style = 'violin'

for e1,l in enumerate(labels):
    plt.sca(ax[e1])
    
    if l is not 'log_alpha':  
        #Get the values for each type and remove nans.  This long command is necessary because HSA array is strangely formated.
        x1 = list(T.loc[indx1,l]);x1 = [eval(i) if isinstance(i,str) else i for i in x1];x1 = np.array([i[0] if isinstance(i,list) else i for i in x1]);x1_nancnt=sum(np.isnan(x1));x1=x1[~np.isnan(x1)]
        x2 = list(T.loc[indx2,l]);x2 = [eval(i) if isinstance(i,str) else i for i in x2];x2 = np.array([i[0] if isinstance(i,list) else i for i in x2]);x2_nancnt=sum(np.isnan(x2));x2=x2[~np.isnan(x2)]
        x3 = list(T.loc[indx3,l]);x3 = [eval(i) if isinstance(i,str) else i for i in x3];x3 = np.array([i[0] if isinstance(i,list) else i for i in x3]);x3_nancnt=sum(np.isnan(x3));x3=x3[~np.isnan(x3)]
    #Combine alpha12,21
    else:
        x1 = np.array(T.loc[indx1,['log_alpha1','log_alpha2']]).ravel()
        x2 = np.array(T.loc[indx2,['log_alpha1','log_alpha2']]).ravel()
        x3 = np.array(T.loc[indx3,['log_alpha1','log_alpha2']]).ravel()
                
    #Remove outliers
    q1,q3 =np.quantile(np.concatenate((x1,x2,x3)),[.25,.75])
    x1 = x1[(x1>(q1-1.5*np.abs(q1-q3))) & (x1<(q3+1.5*np.abs(q1-q3)))]
    x2 = x2[(x2>(q1-1.5*np.abs(q1-q3))) & (x2<(q3+1.5*np.abs(q1-q3)))]
    x3 = x3[(x3>(q1-1.5*np.abs(q1-q3))) & (x3<(q3+1.5*np.abs(q1-q3)))]

    cols = ['purple', 'green', 'grey']

    if style=='violin':
        p1 = ax[e1].violinplot([x1,x2,x3],positions=[1,2,3],showmeans=False,showmedians=False,showextrema=False)
        for e,pc in enumerate(p1['bodies']):
            pc.set_facecolor(cols[e])
            pc.set_edgecolor('black')
            pc.set_alpha(1)
    
        ylim = [100,-100]
        p1 = plt.boxplot([x1,x2,x3],positions=[1,2,3],notch=False,patch_artist=True,widths=.05,showfliers=False,showcaps=False)
        for e,bp in enumerate(p1['boxes']):
            plt.setp(bp,edgecolor='k')
            plt.setp(bp,facecolor='k')
        for e,bp in enumerate(p1['medians']):
            plt.setp(bp,color='w',linewidth=2)
        for e,bp in enumerate(p1['whiskers']):    
            plt.setp(bp,color='k')
            lim = np.sort(bp.get_ydata())
            if lim[1]>ylim[1]:
                ylim[1]=lim[1]
            if lim[0]<ylim[0]:
                ylim[0]=lim[0]
                
    plt.xlim((.25,3.75))
    if ylim is not None:
        plt.ylim(ylim)
    plt.xticks([])
    plt.title(long_labels[e1])
    print(l)
    x = [x1,x2,x3]
    #Run 1-sided t-test
    for k1 in range(2):
        for k2 in np.arange(k1+1,3):
            v = ttest_ind(x[k1],x[k2])
            if v.statistic>0:
                pv = v.pvalue/2
            else:
                pv = 1-v.pvalue/2
            print str(k1+1)+'-'+str(k2+1) + ' ' + str(pv)
plt.subplots_adjust(wspace=.5)

plt.savefig('1B.pdf')



        
        
        
