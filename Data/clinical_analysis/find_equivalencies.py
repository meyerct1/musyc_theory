#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 27 09:03:51 2019

@author: meyerct6
"""
#Code to find the best matches between the in vitro screens and the DCDB

import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz
from scipy.stats import ttest_ind
from glob import glob

#Compile drug combination database entries  Download from (http://www.cls.zju.edu.cn/dcdb/searchdc.jsf) Date: 11-21-2019
#Identifiers
#DC is for each combination
#DCC is for each single drug
#DCU is drug combination usage with information about approval/stage
#DSY is the single drug synonyms


#Get a list of single drugs (i.e. components)
df_sin = pd.read_table('DCDB/COMPONENTS.txt',sep='\t')
#Add the synonyms and brand names to a look up table
tmp = pd.read_table('DCDB/DRUG_SYNONYMS.txt',sep='\t').merge(pd.read_table('DCDB/DCC_TO_DRUGSYNONYMS.txt',sep='\t'),on='DSY_ID',how='outer')
tmp1 = pd.read_table('DCDB/BRANDNAME.txt',sep='\t').merge(pd.read_table('DCDB/DCC_TO_BRANDNAME.txt',sep='\t'),on='DBN_ID')
lk_up_tbl = {}
for i in df_sin.index:
    l = [df_sin.loc[i,'GENERIC_NAME']]
    if sum(df_sin.loc[i,'DCC_ID']==tmp1['DCC_ID'])>0:
        l = l + list(tmp1.loc[tmp1['DCC_ID']==df_sin.loc[i,'DCC_ID'],'BRANDNAME'])
    if sum(df_sin.loc[i,'DCC_ID']==tmp['DCC_ID'])>0:
        l = l + list(tmp.loc[tmp['DCC_ID']==df_sin.loc[i,'DCC_ID'],'SYNONYMS'])
    lk_up_tbl[df_sin.loc[i,'DCC_ID']] = l
    
#For all datasets I have fit grab the drug names and the synergistic efficacy
fils = glob('raw_data/*.csv')
T = []
for f in fils:
    tmp = pd.read_csv(f)[['drug1','drug2']]
    T = T + list(np.unique(np.array(tmp)))

#This fixes the names for the anti-fungal drugs
lk_table = {'5fu':'5 fluorouracil',
            'aba':'aureoblasidin a',
            'amb':'aureoblasidin b',
            'ani':'anisomycin',
            'ben':'benomyl',
            'bro':'bromopyruvate',
            'c3p':'cccp',
            'cal':'calyculin a',
            'can':'cantharidin',
            'chl':'chlorzoxazone',
            'cis':'cisplatin',
            'clo':'clozapine',
            'cyc':'cycloheximide',
            'dyc':'dyclonine',
            'fen':'fenpropimorph',
            'hal':'haloperidol',
            'hyg':'hygromycin',
            'lat':'latrunculin b',
            'lit':'lithium',
            'met':'methotrexate',
            'mms':'methyl methanesulfonate',
            'myr':'myriocin',
            'pen':'pentamidine',
            'qmy':'quinine',
            'qnn':'quinomycin',
            'rad':'radicicol',
            'rap':'rapamycin',
            'sta':'staurosporine',
            'tac':'tacrolimus',
            'tam':'tamoxifen',
            'ter':'terbinafine',
            'tun':'tunicamycin',
            'wor':'wortmannin'}
T = [i.lower() for i in T]
for i in range(len(T)):
    if T[i] in lk_table.keys():
        T[i] = lk_table[T[i]]
        
#Find the unique set of names for all the drugs
search = list(set(T))
search = np.sort(search)
#Look through the look up table to find matches between the datasets
lst_fnd = pd.DataFrame([search])
lst_fnd=lst_fnd.T
lst_fnd.columns = ['screen_name']

lst_fnd['cls_clinid_set']=np.nan
lst_fnd['cls_clinname_set']=np.nan
lst_fnd['cls_clinid_sort']=np.nan
lst_fnd['cls_clinname_sort']=np.nan
lst_fnd['score_set_r'] = 0.
lst_fnd['score_sort_r'] = 0.

#For each drug find the closest match...
for indx in lst_fnd.index:
    s = lst_fnd.loc[indx,'screen_name']
    #look through all the look up table elements to find if there is a match
    for i in lk_up_tbl.keys():
        for k in lk_up_tbl[i]:
            r1 = fuzz.token_sort_ratio(s.lower(),k.lower())
            r2 = fuzz.token_set_ratio(s.lower(),k.lower())
            if r1>lst_fnd.loc[indx,'score_sort_r']:
                lst_fnd.loc[indx,'cls_clinid_sort']=i
                lst_fnd.loc[indx,'cls_clinname_sort']=k.lower()
                lst_fnd.loc[indx,'score_sort_r']=r1
            if r2>lst_fnd.loc[indx,'score_set_r']:
                lst_fnd.loc[indx,'cls_clinid_set']=i
                lst_fnd.loc[indx,'cls_clinname_set']=k.lower()
                lst_fnd.loc[indx,'score_set_r']=r2
                
    print(s + ':' + lst_fnd.loc[indx,'cls_clinname_sort'] + ' ' + str(lst_fnd.loc[indx,'score_sort_r']))                

#Write a first guess
lst_fnd.to_csv('matching_drug_names.csv')
#Manually go in and select which drugs are correct match.
#1=cls_clinid_sort is  correct match
#2=cls_clinid_set is correct match
#3=alternate id
#4=structural analogue
#0=neither are a match


