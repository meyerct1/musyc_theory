#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 30 15:03:21 2019

@author: meyerct6
"""
# =============================================================================
# Import packages
# =============================================================================
import pandas as pd
import numpy as np
from scipy.stats import ttest_ind
from glob import glob
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib import rc
font = {'family' : 'normal',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)
from matplotlib_venn import venn2
import os

from SynergyCalculator.gatherData import subset_data, subset_expt_info
from SynergyCalculator.calcOtherSynergyMetrics import loewe, hsa, bliss, combination_index, get_params
from SynergyCalculator.NDHillFun import Edrug2D_NDB_hill
# =============================================================================
# Read in MuSyC data fits from the different databases
# =============================================================================
fils = glob('../*/MasterResults_mcnlls.csv')
T = pd.DataFrame([])
for f in fils:
    tmp = pd.read_csv(f)
    tmp['study'] = f.split('/')[1]
    T = T.append(tmp,ignore_index=True)
#Drop samples which did not converge.
T = T[T['converge_mc_nlls']==1].reset_index(drop=True)
#This fixes the names for the anti-fungal drugs
lk_table = {'5fu':'5 fluorouracil',
            'aba':'aureoblasidin a',
            'amb':'aureoblasidin b',
            'ani':'anisomycin',
            'ben':'benomyl',
            'bro':'bromopyruvate',
            'c3p':'cccp',
            'cal':'calyculin a',
            'can':'cantharidin',
            'chl':'chlorzoxazone',
            'cis':'cisplatin',
            'clo':'clozapine',
            'cyc':'cycloheximide',
            'dyc':'dyclonine',
            'fen':'fenpropimorph',
            'hal':'haloperidol',
            'hyg':'hygromycin',
            'lat':'latrunculin b',
            'lit':'lithium',
            'met':'methotrexate',
            'mms':'methyl methanesulfonate',
            'myr':'myriocin',
            'pen':'pentamidine',
            'qmy':'quinine',
            'qnn':'quinomycin',
            'rad':'radicicol',
            'rap':'rapamycin',
            'sta':'staurosporine',
            'tac':'tacrolimus',
            'tam':'tamoxifen',
            'ter':'terbinafine',
            'tun':'tunicamycin',
            'wor':'wortmannin'}
for i in T.index:
    if T.loc[i,'drug1_name'] in lk_table.keys():
        T.loc[i,'drug1_name'] = lk_table[T.loc[i,'drug1_name']]
    if T.loc[i,'drug2_name'] in lk_table.keys():
        T.loc[i,'drug2_name'] = lk_table[T.loc[i,'drug2_name']]

#Add the almanac cell line info
cell_lines = pd.read_csv('all_samples.csv',header=0)
cell_lines.columns = ['cells','type']
T = T.merge(cell_lines,right_on='cells',left_on='sample',how='outer')

T = T.reset_index(drop=True)        
# =============================================================================
# Read in table matching drug names between DCDB and MuSyC data...
# =============================================================================
lst_fnd = pd.read_csv('matching_drug_names-11-29-2019_final.csv')
look_up_table = {}
for i in lst_fnd.index:
    tmp = lst_fnd.iloc[i,0]
    if tmp==1 or tmp==4:
        look_up_table[lst_fnd.loc[i,'screen_name']] = lst_fnd.loc[i,'cls_clinid_sort']
    elif tmp==2:
        look_up_table[lst_fnd.loc[i,'screen_name']] = lst_fnd.loc[i,'cls_clinid_set']
    elif tmp==3:
        look_up_table[lst_fnd.loc[i,'screen_name']] = lst_fnd.loc[i,'other_id']
#Convert to a dataframe  
lk_tbl =  pd.DataFrame([look_up_table]).T
      
# =============================================================================
# Read in the DCDB data 
# =============================================================================
#Compile drug combination database entries  Download from (http://www.cls.zju.edu.cn/dcdb/searchdc.jsf) Date: 11-21-2019
#Identifiers
#DC is for each combination
#DCC is for each single drug
#DCU is drug combination usage with information about approval/stage
#DSY is the single drug synonyms
#Read in the drug combinations
df_combo = pd.read_table('DCDB/DRUG_COMBINATION.txt',sep='\t')
#Add information about usage
df_combo =  df_combo.merge(pd.read_table('DCDB/DC_USAGE.txt',sep='\t').merge(pd.read_table('DCDB/DC_TO_DCU.txt',sep='\t'),on='DCU_ID',how='outer'),on='DC_ID')

# =============================================================================
# Find matches in DCDB to MuSyc data.
# =============================================================================
mtc_num = 2  #If 2 look for combinations of only 2 drugs with exact match.  if 3 look for combinations of 2 or more which have at least 2 drugs from the musyc dataset

cnt = 0 #counter for number of combinations
#drug combination barcode for musyc data
T['barcode'] = T['drug1_name']+'_'+T['drug2_name']
#Store the index of the musyc data with matching combinations for each combination in DCDB in a dictonary
data_index = {}
#For each combination in DCDB, look for matches to musyc data
for i in df_combo.index:
    if mtc_num==3:
        cont = sum(np.in1d(df_combo.loc[i,'DCC_ID'].split('/'),lk_tbl[0]))>1
    elif mtc_num==2:
        cont = (sum(np.in1d(df_combo.loc[i,'DCC_ID'].split('/'),lk_tbl[0]))==2) and (len(df_combo.loc[i,'DCC_ID'].split('/'))==2)
    else:
        print('error: wrong number for mtc_num')
    if cont:
        tmp = []
        #Get the drug ids of the combination
        for k in np.array(df_combo.loc[i,'DCC_ID'].split('/')):
            tmp = tmp + list(lk_tbl[lk_tbl[0]==k].index)
        #Make all possible barcodes
        barcode_lst=[]
        for e1 in np.arange(0,len(tmp)):
            for e2 in np.arange(e1+1,len(tmp)):
                barcode_lst.append(tmp[e1]+'_'+tmp[e2])
                barcode_lst.append(tmp[e2]+'_'+tmp[e1])
        #If the barcode exists in the musyc data, store index of matches.
        if 'idarubicin_hydrochloride_daunorubicin_hydrochloride' in barcode_lst:
            print(i)
        if sum(np.in1d(T['barcode'],barcode_lst))>0:
            cnt = cnt+1
            data_index[i]= list(T.index[np.in1d(T['barcode'], barcode_lst)])

# =============================================================================
# Now sort the drug combinations by whether they were efficacious or non-efficacious.
# =============================================================================
indx1=[]
indx2=[]
indx3=[]
for k in data_index.keys():
    if df_combo.loc[k,'EFFICACY'].startswith('Eff'):
        indx1 = indx1 + list(data_index[k])
    elif df_combo.loc[k,'EFFICACY'].startswith('Non-'):
        indx2 = indx2 + list(data_index[k])
    elif df_combo.loc[k,'EFFICACY'].startswith('Need'):
        indx3 = indx3 + list(data_index[k])
    else:
        print('error')
        
indx1 = list(set(indx1))
indx2 = list(set(indx2))
indx3 = list(set(indx3))
# 
#indx1 = np.array(indx1);t_indx1 = indx1[~np.in1d(indx1,indx2)];
#indx2 = np.array(indx2);t_indx2 = indx2[~np.in1d(indx2,indx1)];

# =============================================================================
# Make figure describing clinical combinations
# =============================================================================
df_combo_reorg = []
keys = list(data_index.keys())
to_ignore = []
for k1 in range(len(keys)):
    if k1 not in to_ignore:
        tmp = {keys[k1]:df_combo.loc[keys[k1]]}
        for k2 in np.arange(k1+1,len(keys)):
            if k2 not in to_ignore:
                if sum(np.in1d(data_index[keys[k1]],data_index[keys[k2]]))==len(data_index[keys[k1]]):
                        to_ignore.append(k1)
                        to_ignore.append(k2)
                        to_ignore = list(set(to_ignore))
                        tmp[keys[k2]] = df_combo.loc[keys[k2]]
        df_combo_reorg.append(tmp.copy())
T['combo_id'] = np.nan    
for v in range(len(df_combo_reorg)):
    key_list = []
    for k in df_combo_reorg[v].keys():
        key_list = key_list + data_index[k]
    key_list = list(set(key_list))
    T.loc[key_list,'combo_id'] = int(v)
    

#Recalculate bliss loewe, hsa, and CI for the identified combos
fil_list = ['merck_perVia_10-29-2018.csv','almanac_reformatted.csv','tan_anti-hiv-data.csv','compiled_cokol_antifungal.csv','mott_compiled_01302020.csv']
scale = [[0,1],[-100,100],[0,1], [np.nan,np.nan], [0,100]]
for e1,expt in enumerate(T['study'].unique()[:-1]):
    print(expt)
    sub_T = T[(T['study']==expt) & (~T['combo_id'].isna())]
    print(len(sub_T))

    data = pd.read_table('..' + os.sep + expt + os.sep + fil_list[e1],delimiter=',')
    data['drug1'] = data['drug1'].str.lower(); data['drug2'] = data['drug2'].str.lower();  data['sample'] = data['sample'].str.upper();
    for idd in sub_T.index:
        E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, C1, C2, alpha1, alpha2,gamma1,gamma2, d1min, d1max, d2min, d2max, drug1_name, drug2_name, expt = get_params(pd.DataFrame([sub_T.loc[idd]]))
        r1 = 100.; r2 = 100.;
        E_ec50 = Edrug2D_NDB_hill((C1,C2),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
        #Subset data for doses for fitting
        sample = sub_T.loc[idd,'sample']
        d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)


        if scale[e1][0] is not np.nan:
            E0 = (E0-scale[e1][0])/(scale[e1][1]-scale[e1][0]);E1 = (E1-scale[e1][0])/(scale[e1][1]-scale[e1][0]);E2 = (E2-scale[e1][0])/(scale[e1][1]-scale[e1][0]);E3 = (E3-scale[e1][0])/(scale[e1][1]-scale[e1][0]);
            dip = (dip-scale[e1][0])/(scale[e1][1]-scale[e1][0]); E_ec50 = (E_ec50-scale[e1][0])/(scale[e1][1]-scale[e1][0])

            loewe_fit_ec50 = -np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2))
            hsa_fit_ec50 = hsa([E0-(E0-E1)/2],[E0-(E0-E2)/2],E_ec50)[0]
            bliss_fit_ec50 = bliss(E0-(E0-E1)/2, E0-(E0-E2)/2, E_ec50)
            try:
                ci_raw,_,_,ci_C1,ci_C2,ci_h1,ci_h2=combination_index(d1,d2,dip)
                ci_raw_mean  = np.log10(np.nanmean(ci_raw.reshape((-1,))))
            except ValueError:
                ci_raw_mean = np.nan

        else:
            loewe_fit_ec50 = -np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2))
            hsa_fit_ec50 = hsa([E0-(E0-E1)/2],[E0-(E0-E2)/2],E_ec50)
            bliss_fit_ec50 = np.nan
            ci_raw_mean  = np.nan

        T.loc[idd,'loewe_fit_ec50'] = loewe_fit_ec50;
        T.loc[idd,'bliss_fit_ec50'] = bliss_fit_ec50;
        T.loc[idd,'hsa_fit_ec50'] = hsa_fit_ec50;
        T.loc[idd,'ci_raw_mean'] = ci_raw_mean;


#Normalize HSA
T['hsa_fit_ec50_norm']=np.nan
for expt in T['study'].unique():
    sub_T = T[T['study']==expt]
    e0 = sub_T['E0'].max()
    em = sub_T[['E1','E2','E3']].min().min()
    hs = sub_T['hsa_fit_ec50'].values
    hs = list(sub_T['hsa_fit_ec50']);hs = [eval(i) if isinstance(i,str) else i for i in hs];hs = np.array([i[0] if isinstance(i,list) else i for i in hs]);
    hs=hs/(e0-em)
    T.loc[T['study']==expt,'hsa_fit_ec50_norm']=hs


# =============================================================================
# Drug combination information
# =============================================================================
plt.figure(facecolor='w',figsize=(7.5,2))
ax = []
ax.append(plt.subplot2grid((1,3),(0,0)))
ax.append(plt.subplot2grid((1,3),(0,1)))
ax.append(plt.subplot2grid((1,3),(0,2)))

plt.sca(ax[2])
labels = list(pd.DataFrame(df_combo.loc[data_index.keys(),'EFFICACY']).groupby('EFFICACY').size().index)
sizes = np.array(pd.DataFrame(df_combo.loc[data_index.keys(),'EFFICACY']).groupby('EFFICACY').size())
colors = ['purple', 'grey', 'green']
plt.pie(sizes,labels=labels, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)

plt.sca(ax[1])
labels = list(pd.DataFrame(df_combo.loc[data_index.keys(),'STAGE']).groupby('STAGE').size().index)
sizes = np.array(pd.DataFrame(df_combo.loc[data_index.keys(),'STAGE']).groupby('STAGE').size())
colors = cm.terrain(np.linspace(0,1,len(sizes)))
plt.pie(sizes,labels=labels, colors=colors,autopct='%.1f%%', shadow=True, startangle=140)

ind_all = np.array(T.index)
ind_all = list(ind_all[(np.in1d(ind_all,indx1))|(np.in1d(ind_all,indx2))])
labels = list(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size().index)
number_of_unique_conditions = np.array((pd.DataFrame(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size()).groupby('study').sum()))
for e,l in enumerate(labels):
    labels[e] = l+'\nn='+str(number_of_unique_conditions[e][0])
sizes = np.array(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size())
plt.sca(ax[0])
colors = cm.rainbow(np.linspace(0,1,len(sizes)))
plt.pie(sizes,labels=labels, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)
plt.tight_layout()
plt.savefig('Clinical_B.pdf')


ind_all = np.array(T.index)
ind_all = list(ind_all[(np.in1d(ind_all,indx1))|(np.in1d(ind_all,indx2))])
labels = list(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size().index)
number_of_unique_conditions = np.array((pd.DataFrame(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size()).groupby('study').sum()))
for e,l in enumerate(labels):
    labels[e] = l+'\nn='+str(number_of_unique_conditions[e][0])
sizes = np.array(T.loc[ind_all,['barcode','study']].groupby(['barcode','study']).size().groupby('study').size())
plt.sca(ax[0])
colors = cm.rainbow(np.linspace(0,1,len(sizes)))
plt.pie(sizes,labels=labels, colors=colors,autopct='%1.1f%%', shadow=True, startangle=140)
plt.tight_layout()

# =============================================================================
# Analyze synergy metrics
# =============================================================================

for style in ['violin']:
    plt.figure(facecolor='w',figsize=(10,2))
    ax = []
    ax.append(plt.subplot2grid((1,6),(0,0)))
    ax.append(plt.subplot2grid((1,6),(0,1)))
    ax.append(plt.subplot2grid((1,6),(0,2)))
    ax.append(plt.subplot2grid((1,6),(0,3)))
    ax.append(plt.subplot2grid((1,6),(0,4)))
    ax.append(plt.subplot2grid((1,6),(0,5)))
    
    labels = ['beta','log_alpha','loewe_fit_ec50','bliss_fit_ec50','hsa_fit_ec50_norm','ci_raw_mean']
    long_labels = [r'$\beta$',r'log($\alpha_{12/21}$)','Loewe','Bliss','HSA','CI']
    ylim = None

    for e1,l in enumerate(labels):
        plt.sca(ax[e1])
        
        if l is not 'log_alpha':  
            #Get the values for each type and remove nans.  This long command is necessary because HSA array is strangely formated.
            x1 = list(T.loc[indx1,l]);x1 = [eval(i) if isinstance(i,str) else i for i in x1];x1 = np.array([i[0] if isinstance(i,list) else i for i in x1]);x1_nancnt=sum(np.isnan(x1));x1=x1[~np.isnan(x1)]
            x2 = list(T.loc[indx2,l]);x2 = [eval(i) if isinstance(i,str) else i for i in x2];x2 = np.array([i[0] if isinstance(i,list) else i for i in x2]);x2_nancnt=sum(np.isnan(x2));x2=x2[~np.isnan(x2)]
            x3 = list(T.loc[indx3,l]);x3 = [eval(i) if isinstance(i,str) else i for i in x3];x3 = np.array([i[0] if isinstance(i,list) else i for i in x3]);x3_nancnt=sum(np.isnan(x3));x3=x3[~np.isnan(x3)]
        #Combine alpha12,21
        else:
            x1 = np.array(T.loc[indx1,['log_alpha1','log_alpha2']]).ravel()
            x2 = np.array(T.loc[indx2,['log_alpha1','log_alpha2']]).ravel()
            x3 = np.array(T.loc[indx3,['log_alpha1','log_alpha2']]).ravel()
                    
        #Remove outliers
        q1,q3 =np.quantile(np.concatenate((x1,x2,x3)),[.25,.75])
        ylim = [q1-1.5*np.abs(q1-q3),q3+1.5*np.abs(q1-q3)]
        x1 = x1[(x1>(q1-1.5*np.abs(q1-q3))) & (x1<(q3+1.5*np.abs(q1-q3)))]
        x2 = x2[(x2>(q1-1.5*np.abs(q1-q3))) & (x2<(q3+1.5*np.abs(q1-q3)))]
        x3 = x3[(x3>(q1-1.5*np.abs(q1-q3))) & (x3<(q3+1.5*np.abs(q1-q3)))]
    
        cols = ['purple', 'green', 'grey']
    
        if style=='violin':
            p1 = ax[e1].violinplot([x1,x2,x3],positions=[1,2,3],showmeans=False,showmedians=False,showextrema=False,widths=.7)
            for e,pc in enumerate(p1['bodies']):
                pc.set_facecolor(cols[e])
                pc.set_edgecolor('black')
                pc.set_alpha(1)
        
            p1 = plt.boxplot([x1,x2,x3],positions=[1,2,3],notch=False,patch_artist=True,widths=.05,showfliers=False,showcaps=False)
            for e,bp in enumerate(p1['boxes']):
                plt.setp(bp,edgecolor='k')
                plt.setp(bp,facecolor='k')
            for e,bp in enumerate(p1['medians']):
                plt.setp(bp,color='w',linewidth=2)
            for e,bp in enumerate(p1['whiskers']):    
                plt.setp(bp,color='k')
                    
        elif style=='box':
            p1 = plt.boxplot([x1,x2,x3],positions=[1,2,3],notch=False,patch_artist=True,widths=.75,showfliers=False,showcaps=False)
            for e,bp in enumerate(p1['boxes']):
                plt.setp(bp,edgecolor='k')
                plt.setp(bp,facecolor=cols[e])
            for e,bp in enumerate(p1['medians']):
                plt.setp(bp,color='gray',linewidth=2)
            for e,bp in enumerate(p1['whiskers']):    
                plt.setp(bp,color='k')
                
            
        elif style=='bar':
            for e,x in enumerate([x1,x2,x3]):
                plt.bar(e+1,np.mean(x),color=cols[e])
                plt.errorbar(e+1,np.mean(x),yerr=np.std(x),color='k')
            
        #plt.xlim((.25,3.75))    
        plt.xlim((.5,2.5))
        if ylim is not None:
            plt.ylim(ylim)
        plt.xticks([])
        plt.title(long_labels[e1])
        tks = plt.yticks()[0]
        plt.yticks((tks[0],0,tks[-1]))
        print(l)
        x = [x1,x2,x3]
        #Run 1-sided t-test
        for k1 in range(2):
            for k2 in np.arange(k1+1,3):
                v = ttest_ind(x[k1],x[k2])
                if v.statistic>0:
                    pv = v.pvalue/2
                else:
                    pv = 1-v.pvalue/2
                print(str(k1+1)+'-'+str(k2+1) + ' ' + str(pv))
    plt.subplots_adjust(wspace=.6)

    plt.savefig('fig_'+style+'.pdf')


# =============================================================================
# Looking for good example of Loewe bias.
# =============================================================================
indx2 = np.array(indx2);t_indx2 = indx2[~np.in1d(indx2,np.array(indx1))];
sub_T = T.loc[indx2]
sub_T['geo_h']=np.sqrt(sub_T['h1']*sub_T['h2'])
gpby = sub_T.groupby(['type','barcode'])
sub_T = pd.DataFrame(gpby[['beta','loewe_fit_ec50','geo_h','combo_id']].mean())[(gpby['beta'].mean()<0)&(gpby['loewe_fit_ec50'].mean()>0)&(gpby['geo_h'].mean()<1)]

for i in range(len(sub_T)):
    print(sub_T.index[i])
    tmp = df_combo_reorg[int(sub_T.iloc[i,-1])]
    for v in tmp.keys():
        print(tmp[v]['DISEASE'])
        print(tmp[v]['STAGE'])
    print('')

sub_T = T.loc[indx2]

sub_T = sub_T[sub_T['study']=='holbeck_anticancer']
#Add the almanac cell line info
sub_T['geo_h']=np.sqrt(sub_T['h1']*sub_T['h2'])
gpby = sub_T.groupby(['type','barcode'])
x3 = pd.DataFrame(gpby['geo_h'].mean(),columns=['geo_h'])
x1 = pd.DataFrame(gpby['beta'].mean(),columns=['beta'])
x2 = pd.DataFrame(gpby['loewe_fit_ec50'].mean(),columns=['loewe_fit_ec50'])
sub_T = pd.concat([x1,x2,x3],axis=1)
sub_T = sub_T[(sub_T['loewe_fit_ec50']>T.loc[indx1,'loewe_fit_ec50'].mean())&(sub_T['beta']<0)&(sub_T['geo_h']<1)]
barcode_clinical_list = np.array([i[1] for i in sub_T.index])


#sub_T = pd.DataFrame(sub_T.groupby(['type','barcode'])[['geo_h','beta','loewe_fit_ec50']].mean())        
#sub_T[(sub_T['loewe_fit_ec50']>0)&(sub_T['beta']<0)&(sub_T['geo_h']<1)]
# =============================================================================
# Picked docetaxel_gemcitabine_hydrochloride in Prostate cancer
# https://www.ncbi.nlm.nih.gov/pubmed/19882157?dopt=Abstract
# https://onlinelibrary.wiley.com/doi/full/10.1002/cncr.25457
# =============================================================================
sub_T = T.copy()
sub_T = sub_T[sub_T['study']=='holbeck_anticancer']
sub_T['geo_h']=np.sqrt(sub_T['h1']*sub_T['h2'])
sub_T = sub_T[(sub_T['type']=='Prostate')&(sub_T['barcode']=='docetaxel_gemcitabine_hydrochloride')]

from SynergyCalculator.doseResponseSurfPlot import DosePlots_PLY, matplotlibDoseResponseSurface
from SynergyCalculator.gatherData import subset_data
import os
for k in sub_T.columns:
    if k.endswith('_ci'):
            T[k] = T[k].str.strip('[]')
            T[k+'_lw']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[0]
            T[k+'_up']= T[k].str.strip('[]').str.split(',',expand=True).astype('float')[1]
        
sub_T['save_direc'] = os.getcwd() + '/data/holbeck_anticancer' 
sub_T = sub_T.loc[4856]
expt        = sub_T['save_direc'] + os.sep+ sub_T['expt']
drug1_name  = sub_T['drug1_name']
drug2_name  = sub_T['drug2_name']
sample      = sub_T['sample']
data        = pd.read_table(expt, delimiter=',')        
data['drug1'] = data['drug1'].str.lower()
data['drug2'] = data['drug2'].str.lower()
data['sample'] = data['sample'].str.upper()
sub_T['r1'] = 100.
sub_T['r2'] = 100.
d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
dip_sd[:] = 0
sub_T['save_direc'] = os.getcwd()
#Flip d1 and d2
d1,d2 = d2,d1
drug1_name,drug2_name=drug2_name,drug1_name
sub_T['E1'],sub_T['E2'],sub_T['log_C1'],sub_T['log_C2'],sub_T['log_h1'],sub_T['log_h2'],sub_T['log_alpha1'],sub_T['log_alpha2']=sub_T['E2'],sub_T['E1'],sub_T['log_C2'],sub_T['log_C1'],sub_T['log_h2'],sub_T['log_h1'],sub_T['log_alpha2'],sub_T['log_alpha1']
for i in ['C1','C2','h1','h2']:
    sub_T[i] = 10**sub_T['log_'+i]
popt3 = [sub_T['E0'],sub_T['E1'],sub_T['E2'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C1'],10**sub_T['log_C2'],10**sub_T['log_h1'],10**sub_T['log_h2'],10**sub_T['log_alpha1'],10**sub_T['log_alpha2']]
DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=1)     
title = 'Loewe:%.2f'%sub_T['loewe_fit_ec50'] + '\n'+r'$\beta$=%.2f '%(sub_T['beta']) + '\n'+r'$\sqrt(h1*h2)$=%.2f'%(np.sqrt(sub_T['h1']*sub_T['h2']))
[fig1,fig2],[ax_surf,ax1,ax2],[tt,yy,zz],surf = matplotlibDoseResponseSurface(pd.DataFrame([sub_T]),d1,d2,dip,dip_sd,fname=None, zlim=(0.0,120.0), zero_conc=0,title=title,plt_zeropln=False,figsize_surf=(2.2,2.4),figsize_slice=(2,1.3))
fname=drug1_name+'_'+drug2_name+'_'+sample
fig1.savefig(fname+'.pdf',pad_inches=0.,format='pdf');
fig2.savefig(fname+'_slices.pdf',pad_inches=0.,format='pdf');


from pythontools.synergy import synergy_tools
def get_loewe_bias(h1, h2, d1, d2, ec50_1, ec50_2, E0, E1, E2, E3=None, alpha=0.):
    """
    Calculates Loewe for a combination, assuming the MuSyC model. By default, the MuSyC model is assumed with beta=0, alpha1=alpha2=1.
    """
    r1,r1r = synergy_tools.rates(C1, h1, logspace=False)
    r2,r2r = synergy_tools.rates(C2, h2, logspace=False)

    if E3 is None: E3 = min(E1, E2)

    E = synergy_tools.hill_2D(d1, d2, E0, E1, E2, E3, h1, h2, alpha, alpha, r1, r1r, r2, r2r)
    l = -np.log10(synergy_tools.loewe(d1, d2, E, E0, E1, E2, h1, h2, C1, C2))
    return np.nanmedian(l)




cline, drug1, drug2, h1, h2, E0, E1, E2, E3, C1, C2, mxd1, mxd2, cmn, un = sub_T[['sample', 'drug1_name', 'drug2_name', 'h1', 'h2', 'E0', 'E1', 'E2','E3', 'log_C1', 'log_C2','max_conc_d1','max_conc_d2','converge_mc_nlls','drugunique']]
C1 = np.power(10., C1)
C2 = np.power(10., C2)
print('corrected loewe:' + str(sub_T['loewe_fit_ec50'] - get_loewe_bias(h1,h2,C1,C2,C1,C2,E0,E1,E2,E3)))





























        
# plt.figure(facecolor='w',figsize=(2.5,2.5))
# venn2(subsets=(7132-len(df_combo_reorg),1793-len(df_combo_reorg),len(df_combo_reorg)))
# plt.savefig('Clinical_A.pdf')


#
#colors = cm.gist_rainbow(np.linspace(0,1,len(T['study'].unique())))
#exp = list(T['study'].unique())
#label = []
#plt.figure(facecolor='w',figsize=(7.5,6))
#ax = []
#ax.append(plt.subplot2grid((3,1),(0,0)))
#ax.append(plt.subplot2grid((3,1),(1,0)))
#ax.append(plt.subplot2grid((3,1),(2,0)))
#
#plt.sca(ax[2])
#srt_keys = pd.DataFrame({'index':range(len(df_combo_reorg))});
#srt_keys['length']=np.nan
#for i in range(len(df_combo_reorg)):
#    kys = df_combo_reorg[i].keys()
#    h=0
#    for k in kys:
#        h = h + len(data_index[k])
#    srt_keys.loc[i,'length']=int(h)
#    
#srt_keys = srt_keys.sort_values('length').set_index('index').reset_index()
#
#for v in range(len(df_combo_reorg)):
#    kys = df_combo_reorg[v].keys()
#    tmp = pd.DataFrame()
#    for k in kys:
#        tmp = tmp.append(T.loc[set(data_index[k])],ignore_index=True)
#    tmp = tmp.groupby('study').count()['barcode']
#    plt.bar(srt_keys.index[srt_keys['index']==v]+1,tmp.sum(),color='k',width=1)
#plt.xlim((.5,v+.5))
#plt.xticks([1,v])
#plt.ylim((0,170))
#plt.xlabel('Clinical Drug Combination #')
#plt.ylabel('Disease Models')
#
#
#plt.sca(ax[0])
#for v in range(len(df_combo_reorg)):
#    kys = df_combo_reorg[v].keys()
#    tmp = pd.DataFrame()
#    for k in kys:
#        tmp = tmp.append(T.loc[set(data_index[k])],ignore_index=True)
#    tmp = tmp.groupby('study').count()['barcode']
#    tmp = tmp/float(tmp.sum())
#    h = 0
#    for i in tmp.index:
#        plt.bar(srt_keys.index[srt_keys['index']==v],tmp.loc[i],bottom=h,color=colors[exp.index(i)],width=1)
#        h=h+tmp.loc[i]
#        
#plt.xlim((.5,v+.5))
#plt.xticks([])
#plt.yticks([0.,.5,1.00])
#plt.ylabel('Percent of study')
#
#
#plt.sca(ax[1])
#stgs = list(df_combo.loc[data_index.keys(),'STAGE'].unique())
#stgs_order = [3,5,6,0,1,4,2]
#eff_colors = ['purple', 'grey', 'green']
#eff = list(df_combo.loc[data_index.keys(),'EFFICACY'].unique())
#for v in range(len(df_combo_reorg)):
#    for tmp in df_combo_reorg[v].keys():
#        y = stgs_order[stgs.index(df_combo_reorg[v][tmp]['STAGE'])]
#        plt.scatter(srt_keys.index[srt_keys['index']==v],y,20,c=eff_colors[eff.index(df_combo_reorg[v][tmp]['EFFICACY'])])
#
#        
#plt.xlim((.5,v+.5))
#plt.xticks([])
#plt.ylabel('Clinical Trial Phase')
#plt.yticks(range(len(stgs_order)))
#ax[1].set_yticklabels(['No Info','Pre-clinic','Phase 0','Phase 1','Phase 1/2','Phase 2','Phase 3'])
#
#plt.savefig('Clinical_B.pdf')
