#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Code for creating an array of SynCode_dipDB_template.py for fitting the dose response surfaces
One file for each concentration of drug each cell line.
Edit the expFile and filename variables to match the directory where the 
files should be written.  Code assumes SynCode is in this same folder.


Code commented and up to date 01/26/2019
@author: Christian Meyer 
@email: christian.t.meyer@vanderbilt.edu
"""
import os
import numpy as np
import pandas as pd
import glob

##############################################################################
#Parameters specifying fitting details
##############################################################################
experiments = ['merck_perVia_10-29-2018.csv'] #List of experimental files
metric_name = 'Percent Effect' #What is the metric of drug effect called
to_plot     = 0#Should the output be plotted?
to_save     = 0#Should the trace/iterations for pso and mcmc be saved?
hill_orient = 1#What is the orientation of the hill curve.  1 means Emax<E0.  0 means Emax>E0

#Fit Algorithm to use
fit_alg = 'nlls'
E_fix=None #Fix Effects.  None if fitting beta
E_bnd='[[.9,0.,0.,0.,0.],[1.1,2.5,2.5,2.5]]' #bounds of effects.  overrulled by E_fix.  make a string
find_opt=False#Find optimal model granularity
fit_gamma=False#Fit cooperativity synergy

#Details needed if fitting other synergy frameworks
musyc_path = '/home/xnmeyer/Documents/Lab/Repos/MuSyC_Code/'
calc_other_metrics = True
metric_percent = True
metric_max = 1.
calc_brd_zip = True

#script to replicate for each combination to fit
filename = 'SynCode_template'
##############################################################################
#Functions for generating scripts
##############################################################################
#Function to switch the index in target file
def StringSwitch(string,file_string,val):
    idx1 = file_string.find(string)
    idx2 = file_string.find('#',idx1)
    file_string = file_string[0:idx1+len(string)] + str(val) + file_string[idx2:]
    return file_string    
    
#Functions to generate array files by reading in the file name and saving an editted version with
#a new index corresponding to a new drug combination.
def AccreArrayGen(sample,expt,drug1_name,drug2_name,cnt):
    f = open(musyc_path + filename +'.py', 'r')
    file_string = f.read()
    string = 'fit_alg = '
    file_string = StringSwitch(string,file_string,'"%s"' % fit_alg)  
    string = 'expt = '
    file_string = StringSwitch(string,file_string,'"%s"' % expt)  
    string = 'metric_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % metric_name)  
    string = 'to_plot = '
    file_string = StringSwitch(string,file_string,to_plot)
    string = 'to_save = '
    file_string = StringSwitch(string,file_string,to_save)
    string = 'hill_orient = '
    file_string = StringSwitch(string,file_string,hill_orient)
    string = 'sample = '
    file_string = StringSwitch(string,file_string,'"%s"' % sample)
    string = 'E_fix = '
    file_string = StringSwitch(string,file_string,str(E_fix))
    string = 'E_bnd = '
    file_string = StringSwitch(string,file_string,E_bnd)
    string = 'find_opt = '
    file_string = StringSwitch(string,file_string,str(find_opt))
    string = 'fit_gamma = '
    file_string = StringSwitch(string,file_string,str(fit_gamma))
    string = 'drug1_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % drug1_name)
    string = 'drug2_name = '
    file_string = StringSwitch(string,file_string,'"%s"' % drug2_name)  
    string = 'musyc_path = '
    file_string = StringSwitch(string,file_string,'"%s"' % musyc_path)
    string = 'calc_other_metrics = '
    file_string = StringSwitch(string,file_string,str(calc_other_metrics))
    string = 'metric_percent = '
    file_string = StringSwitch(string,file_string,str(metric_percent))
    string = 'metric_max = '
    file_string = StringSwitch(string,file_string,metric_max)
    string = 'calc_brd_zip = '
    file_string = StringSwitch(string,file_string,str(calc_brd_zip))
    bol = True
    while bol:
        if os.path.isfile(filename + '_' + str(cnt) + '.py'):
            cnt = cnt + 1
        else:
            f = open(filename + '_' + str(cnt) + '.py', 'w')
            f.write(file_string)
            bol = False
    return cnt+1


##############################################################################
#For each combination in each experiment file create a script
##############################################################################
cnt = 1 
for expt in experiments:                
    #Read in the data into a pandas data frame
    data = pd.read_table(expt, delimiter=',')
    for sample in np.unique(data['sample']):
        #Subset by sample
        sub_data = data[data['sample']==sample]
        drug_combinations = sub_data[list(['drug1','drug2'])].drop_duplicates()
        #Remove the double control condition...
        drug_combinations = drug_combinations[np.logical_and(drug_combinations['drug1']!='control', drug_combinations['drug2']!='control')]
        drug_combinations = drug_combinations.reset_index()
        #For all unique drug combinations:
        for e in drug_combinations.index:
            drug1_name = drug_combinations['drug1'][e]
            drug2_name = drug_combinations['drug2'][e]
            cnt = AccreArrayGen(sample,expt,drug1_name,drug2_name,cnt)      
            #Used to find a particular job
            if drug1_name.lower()=='myr' and drug2_name.lower()=='hyg':
                print cnt
            
            

        
