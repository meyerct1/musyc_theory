
#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 24 11:53:25 2018

@author: xnmeyer
"""

import pandas as pd
import numpy as np
#Read in data (both single and combinations)
sa = pd.read_excel('156849_1_supp_0_w2lh45_singleAgent.xlsx')
co = pd.read_excel('156849_1_supp_1_w2lrww_combinations.xls')
#Calculate the uncertainty in values
sa['effect.95ci'] = np.nanstd(sa[['viability1','viability2','viability3','viability4','viability5','viability6']],axis=1)*2*1.96
co['effect.95ci'] = np.nanstd(co[['viability1','viability2','viability3','viability4']],axis=1)*2*1.96
#Format Data
sa = sa.drop(['viability1','viability2','viability3','viability4','viability5','viability6','BatchID','mu/muMax'],axis=1)
co = co.drop(['viability1','viability2','viability3','viability4','BatchID','mu/muMax','combination_name'],axis=1)
sa = sa.rename(index = str,columns={'drug_name':'drugA_name','Drug_concentration (uM)':'drugA Conc (uM)'})
sa['drugB Conc (uM)'] = 0
sa['drugB_name'] = 'control'
#Combine the data sets
data = co.append(sa,ignore_index=True)
data = data.rename(index=str,columns={'drugA_name':'drug1','drugB_name':'drug2','cell_line':'sample','drugB Conc (uM)':'drug2.conc','drugA Conc (uM)':'drug1.conc','X/X0':'effect'})
for s in data['sample'].unique():
    data = data.append(pd.DataFrame([{'drug1':'control','drug2':'control','drug1.conc':0.,'drug2.conc':0.,'sample':s,'effect':1.,'effect.95ci':.01}]),ignore_index=True)
    data = data.append(pd.DataFrame([{'drug1':'control','drug2':'control','drug1.conc':0.,'drug2.conc':0.,'sample':s,'effect':1.,'effect.95ci':.01}]),ignore_index=True)
data['effect'] = np.round(data['effect'],decimals=3)
data['drug1.units'] = 'uM'
data['drug2.units'] = 'uM'
data['expt.date'] = '00-00-0000'
data.to_csv('merck_perVia_10-29-2018.csv',sep=',',index=False)
