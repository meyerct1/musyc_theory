#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Code to calculate the synergy by other methods for the three datasets fit by MuSyC
"""
#Import packages
import pandas as pd
import numpy as np

from SynergyCalculator.gatherData import subset_data
from SynergyCalculator.calcOtherSynergyMetrics import calcOtherSynergy,get_params
from SynergyCalculator.calcOtherSynergyMetrics import loewe,combination_index,schindler,bliss,hsa
from SynergyCalculator.NDHillFun import Edrug2D_NDB_hill

import os
import warnings
warnings.simplefilter("ignore")

###################################################################################################################################
#Calculate for each dataset

data_sets = ['oneil_anticancer/MasterResults_noGamma.csv',
             'cokol_antifungal/MasterResults_noGamma.csv',
             'mott_antimalaria/MasterResults_noGamma.csv']

#Is the dataset a percent?  If true what is the scaling factor (i.e. 100 will divide all the effects by 100.)
percent = [(True,1.), (False,np.nan), (True,100.)]
      
#Bounds used for fits for each dataset
bnds = [
        [[.9,0.,0.,0.,0.],[1.1,2.5,2.5,2.5]],
        [[-np.inf,0,0,0],[np.inf,np.inf,np.inf,np.inf]],
        [[.9,0.,0.,0.],[1.1,2.0,2.0,2.0]]
        ]
    
#Bounds used for fits for each dataset
for ds,per,bds in zip(data_sets,percent,bnds):
    T = pd.read_csv(ds)
    key = ['loewe_fit_ec50','loewe_fit_ec10','loewe_fit_ec25','loewe_fit_all','loewe_fit_perUnd','zip_fit','hsa_raw_all','hsa_fit_all','hsa_fit_ec50','bliss_fit_all','bliss_fit_ec50','zimmer_fit_a1','zimmer_fit_a2','ci_fit_all','ci_fit_ec50','schindler_fit_ec50','schindler_fit_all','loewe_raw_all','loewe_raw_perUnd','schindler_raw_ec50','zimmer_raw_a1','zimmer_raw_a2','bliss_raw_all','zip_raw','ci_raw','brd_raw','brd_fit','drugunique','zimmer_R2','bliss_fit_mxd1d2','loewe_fit_mxd1d2']
    for k in key:
        T[k]=np.nan
    if per[0]:
        for k in ['E0','E1','E2','E3']:
            T[k]=T[k]/per[1]
    T = T.reset_index(drop=True)           
    df = pd.DataFrame([])
    for ind in T.index:
        print ind
        sub_T       = T.loc[ind].to_dict()
        expt        = sub_T['save_direc']+os.sep+sub_T['expt']
        drug1_name  = sub_T['drug1_name']
        drug2_name  = sub_T['drug2_name']
        sample      = sub_T['sample']
        data        = pd.read_table(expt, delimiter=',')        
        data['drug1'] = data['drug1'].str.lower()
        data['drug2'] = data['drug2'].str.lower()
        data['sample'] = data['sample'].str.upper()
        
        d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
        if per[0]:
            dip = dip/per[1]
            dip_sd=dip_sd/per[1]
        
        try:
            x = calcOtherSynergy(sub_T,d1,d2,dip,dip_sd,percent=per[0],E_fix=None,E_bnd=bds,calc_brd_zip=False)
            key = ['loewe_fit_ec50','loewe_fit_ec10','loewe_fit_ec25','loewe_fit_all','loewe_fit_perUnd','zip_fit','hsa_raw_all','hsa_fit_all','hsa_fit_ec50','bliss_fit_all','bliss_fit_ec50','zimmer_fit_a1','zimmer_fit_a2','ci_fit_all','ci_fit_ec50','schindler_fit_ec50','schindler_fit_all','loewe_raw_all','loewe_raw_perUnd','schindler_raw_ec50','zimmer_raw_a1','zimmer_raw_a2','bliss_raw_all','zip_raw','ci_raw','brd_raw','brd_fit','zimmer_R2','bliss_fit_mxd1d2','loewe_fit_mxd1d2']
            for e,k in enumerate(key):
                T.loc[ind,k]=x[e]
        except:
            continue
        
        #File with raw data
        df_tmp = pd.DataFrame({'drug1.conc':d1,'drug2.conc':d2,'effect':dip,'effect.95ci':dip_sd})
        df_tmp['drug1'] = drug1_name.lower(); df_tmp['drug2'] = drug2_name.lower();
        # Filter out any weird datapoints, or points that aren't real combinations
        key = ['schindler','loewe','ci','bliss','hsa','drugunique']
        for k in key:
            df_tmp[k]=np.nan
        
        # Read drug parameters
        E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, C1, C2, alpha1, alpha2,gamma1,gamma2, d1min, d1max, d2min, d2max, drug1_name, drug2_name, expt = get_params(pd.DataFrame([sub_T]))
        DD1, DD2 = np.meshgrid(np.unique(d1),np.unique(d2))       
        E = Edrug2D_NDB_hill((DD1.reshape((-1,)),DD2.reshape((-1,))),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2).reshape(DD2.shape)   
        dd1 = DD1.reshape((-1,));dd2=DD2.reshape((-1,));ee=E.reshape((-1,))
        
        # Calculate synergy and population dataframe
        mask = (df_tmp['drug1.conc']>0.) & (df_tmp['drug2.conc']>0.)
        d1_tmp = np.array(df_tmp.loc[mask,'drug1.conc']);d2_tmp=np.array(df_tmp.loc[mask,'drug2.conc']);dip_tmp=np.array(df_tmp.loc[mask,'effect'])
        df_tmp.loc[mask,"schindler"] = schindler(d1_tmp, d2_tmp, dip_tmp, E0, E1, E2, h1, h2, C1, C2)
        df_tmp.loc[mask,"loewe"] = -np.log10(loewe(d1_tmp, d2_tmp, dip_tmp, E0, E1, E2, h1, h2, C1, C2))
        
        #min(e_drug1_alone, e_drug2_alone) - e_combination
        df_tmp.loc[mask,"hsa"] = np.min((Edrug2D_NDB_hill((d1_tmp,np.zeros(len(d1_tmp))),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2),Edrug2D_NDB_hill((np.zeros(len(d1_tmp)),d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)),axis=0)-Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
       
        if per[0]:#If a percent 
            mask = (df_tmp['effect']>=0.) & (df_tmp['effect']<=1.)
            d1_tmp = np.array(df_tmp.loc[mask,'drug1.conc']);d2_tmp=np.array(df_tmp.loc[mask,'drug2.conc']);dip_tmp=np.array(df_tmp.loc[mask,'effect'])

            if sum(d1_tmp==0)>3 and sum(d2_tmp==0)>3:
                ci,d1_full,d2_full = combination_index(d1_tmp,d2_tmp,dip_tmp)
                for (x1,x2,e) in zip(d1_full,d2_full,ci):
                    tmp = df_tmp[(df_tmp['drug1.conc']==x1) & (df_tmp['drug2.conc']==x2)].index
                    if len(tmp)>0:
                        df_tmp.loc[tmp,'ci']=-np.log10(e)

            mask = (df_tmp['effect']>=0.) & (df_tmp['effect']<=1.) &  (df_tmp['drug1.conc']>0.) & (df_tmp['drug2.conc']>0.)
            d1_tmp = np.array(df_tmp.loc[mask,'drug1.conc']);d2_tmp=np.array(df_tmp.loc[mask,'drug2.conc']);dip_tmp=np.array(df_tmp.loc[mask,'effect'])
            df_tmp.loc[mask,'bliss'] = (Edrug2D_NDB_hill((d1_tmp,np.zeros(len(d1_tmp))),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)*Edrug2D_NDB_hill((np.zeros(len(d1_tmp)),d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2))-Edrug2D_NDB_hill((d1_tmp,d2_tmp),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)

                                         
        df_tmp['drugunique'] = "%s_%s_%s"%(sample, drug1_name, drug2_name)
        df = df.append(df_tmp,ignore_index=True)

        T.loc[ind,'drugunique'] = "%s_%s_%s"%(sample, drug1_name, drug2_name)

        
    df.to_csv(ds.split('.csv')[0]+'_otherSynergyMetricsCalculation_allDoses.csv')
    T.to_csv(ds.split('.csv')[0]+'_otherSynergyMetricsCalculation.csv')
