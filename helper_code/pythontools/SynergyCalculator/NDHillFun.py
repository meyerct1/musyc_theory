#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 09:57:33 2018

@author: xnmeyer
"""

from uncertainties import ufloat
import numpy as np
#from numba import jit

##############################################################################
#2D hill curve functions and class of MCMC model tiers to fit
##############################################################################
##Fitting functions   
# 1D Hill function for fitting
def Edrug1D(d,E0,Em,C,h):
    return Em + (E0-Em) / (1 + (d/C)**h)
    
#2D dose response surface function not assuming detail balance but assuming proliferation 
# is << than the rate of transition between the different states.
#@jit(nopython=True)
def Edrug2D_NDB(d,E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2):
    d1 = d[0]
    d2 = d[1]
    Ed = (E3*r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
          E3*r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
          C2**h2*E0*r1*(C1*alpha2*d1)**h1 + 
          C1**h1*E0*r2*(C2*alpha1*d2)**h2 + 
          C2**h2*E1*r1*(alpha2*d1**2)**h1 + 
          C1**h1*E2*r2*(alpha1*d2**2)**h2 + 
          E3*d2**h2*r1*(C1*alpha2*d1)**h1 + 
          E3*d1**h1*r2*(C2*alpha1*d2)**h2 + 
          C1**(2*h1)*C2**h2*E0*r1 + 
          C1**h1*C2**(2*h2)*E0*r2 + 
          C1**(2*h1)*E2*d2**h2*r1 + 
          C2**(2*h2)*E1*d1**h1*r2 + 
          E1*r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
          E2*r1*(C1*d1)**h1*(alpha1*d2)**h2 +
          C2**h2*E1*r1*(C1*d1)**h1 +
          C1**h1*E2*r2*(C2*d2)**h2)/  \
         (r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
          r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
          C2**h2*r1*(C1*alpha2*d1)**h1 +
          C1**h1*r2*(C2*alpha1*d2)**h2 + 
          C2**h2*r1*(alpha2*d1**2)**h1 + 
          C1**h1*r2*(alpha1*d2**2)**h2 + 
          d2**h2*r1*(C1*alpha2*d1)**h1 + 
          d1**h1*r2*(C2*alpha1*d2)**h2 + 
          C1**(2*h1)*C2**h2*r1 + 
          C1**h1*C2**(2*h2)*r2 + 
          C1**(2*h1)*d2**h2*r1 + 
          C2**(2*h2)*d1**h1*r2 + 
          r1*(C1*d1)**h1*(alpha1*d2)**h2 + 
          r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
          C2**h2*r1*(C1*d1)**h1 + 
          C1**h1*r2*(C2*d2)**h2)
         
    return Ed

#For propogating error in the non-detail balance case
#assumes h1,h2,c1,c2,alpha1,alpha2 come in as log10()
def err_Edrug2D_NDB(d,x):
    d1      = d[0]
    d2      = d[1]
    E0      = ufloat(x[0],x[1])
    E1      = ufloat(x[2],x[3])
    E2      = ufloat(x[4],x[5])
    E3      = ufloat(x[6],x[7])
    r1      = ufloat(x[8],x[9])
    r2      = ufloat(x[10],x[11])
    C1      = 10**ufloat(x[12],x[13])
    C2      = 10**ufloat(x[14],x[15])
    h1      = 10**ufloat(x[16],x[17])
    h2      = 10**ufloat(x[18],x[19])
    alpha1  = 10**ufloat(x[20],x[21])
    alpha2  = 10**ufloat(x[22],x[23])
    try:
        if d1==0 and d2!=0:
            Ed = Edrug1D(d2,E0,E2,C2,h2)
                          
        elif d1!=0 and d2==0:
            Ed = Edrug1D(d1,E0,E1,C1,h1)
            
        elif d1==0 and d2==0:
            Ed = E0
        else:
            Ed = (E3*r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
                  E3*r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
                  C2**h2*E0*r1*(C1*alpha2*d1)**h1 + 
                  C1**h1*E0*r2*(C2*alpha1*d2)**h2 + 
                  C2**h2*E1*r1*(alpha2*d1**2)**h1 + 
                  C1**h1*E2*r2*(alpha1*d2**2)**h2 + 
                  E3*d2**h2*r1*(C1*alpha2*d1)**h1 + 
                  E3*d1**h1*r2*(C2*alpha1*d2)**h2 + 
                  C1**(2*h1)*C2**h2*E0*r1 + 
                  C1**h1*C2**(2*h2)*E0*r2 + 
                  C1**(2*h1)*E2*d2**h2*r1 + 
                  C2**(2*h2)*E1*d1**h1*r2 + 
                  E1*r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
                  E2*r1*(C1*d1)**h1*(alpha1*d2)**h2 +
                  C2**h2*E1*r1*(C1*d1)**h1 +
                  C1**h1*E2*r2*(C2*d2)**h2)/  \
                 (r1*(alpha1*d2)**h2*(alpha2*d1**2)**h1 + 
                  r2*(alpha2*d1)**h1*(alpha1*d2**2)**h2 + 
                  C2**h2*r1*(C1*alpha2*d1)**h1 +
                  C1**h1*r2*(C2*alpha1*d2)**h2 + 
                  C2**h2*r1*(alpha2*d1**2)**h1 + 
                  C1**h1*r2*(alpha1*d2**2)**h2 + 
                  d2**h2*r1*(C1*alpha2*d1)**h1 + 
                  d1**h1*r2*(C2*alpha1*d2)**h2 + 
                  C1**(2*h1)*C2**h2*r1 + 
                  C1**h1*C2**(2*h2)*r2 + 
                  C1**(2*h1)*d2**h2*r1 + 
                  C2**(2*h2)*d1**h1*r2 + 
                  r1*(C1*d1)**h1*(alpha1*d2)**h2 + 
                  r2*(C2*d2)**h2*(alpha2*d1)**h1 + 
                  C2**h2*r1*(C1*d1)**h1 + 
                  C1**h1*r2*(C2*d2)**h2)
                 
        return Ed.std_dev
    except:
        return np.nan
   

#@jit(nopython=True)
def Edrug2D_NDB_hill(d,E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2):
    d1 = d[0]
    d2 = d[1]
    Ed =    (E2*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2**(gamma1 + 1)*(C1**h1*r1)**gamma2 + 
             E1*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*(C2**h2*r2)**gamma1 + 
             C1**h1*E0*r1**(gamma2 + 1)*(alpha2*d1)**(gamma2*h1)*(C2**h2*r2)**gamma1 + 
             C2**h2*E0*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2)*(C1**h1*r1)**gamma2 + 
             E2*d1**h1*r1**(gamma2 + 1)*r2**gamma1*(C1**h1)**gamma2*(alpha1*d2)**(gamma1*h2) + 
             E1*d2**h2*r1**gamma2*r2**(gamma1 + 1)*(C2**h2)**gamma1*(alpha2*d1)**(gamma2*h1) + 
             E3*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r1**gamma2*r2**(gamma1 + 1)*(alpha2*d1)**(gamma2*h1) + 
             E3*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*r2**gamma1*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*C2**h2*E0*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + C1**h1*C2**h2*E0*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             C2**h2*E3*d1**h1*r1*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*E3*d2**h2*r1**(gamma2 + 1)*r2*(alpha2*d1)**(gamma2*h1) + 
             C2**h2*E1*d1**h1*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + 
             C2**h2*E1*d1**h1*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             C1**h1*E2*d2**h2*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + 
             C1**h1*E2*d2**h2*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2)/ \
            (C1**h1*r1**(gamma2 + 1)*(alpha2*d1)**(gamma2*h1)*(C2**h2*r2)**gamma1 + 
             C2**h2*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2)*(C1**h1*r1)**gamma2 + 
             alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2**(gamma1 + 1)*(C1**h1*r1)**gamma2 + 
             alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*(C2**h2*r2)**gamma1 + 
             alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r1**gamma2*r2**(gamma1 + 1)*(alpha2*d1)**(gamma2*h1) + 
             alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*r2**gamma1*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*C2**h2*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + C1**h1*C2**h2*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             C2**h2*d1**h1*r1*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2) + 
             C1**h1*d2**h2*r1**(gamma2 + 1)*r2*(alpha2*d1)**(gamma2*h1) +
             C1**h1*d2**h2*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 +
             C1**h1*d2**h2*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             C2**h2*d1**h1*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + 
             C2**h2*d1**h1*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
             d1**h1*r1**(gamma2 + 1)*r2**gamma1*(C1**h1)**gamma2*(alpha1*d2)**(gamma1*h2) + 
             d2**h2*r1**gamma2*r2**(gamma1 + 1)*(C2**h2)**gamma1*(alpha2*d1)**(gamma2*h1))
    return Ed




#For propogating error in the non-detail balance case
#assumes h1,h2,c1,c2,alpha1,alpha2,gamma1,gamma2 come in as log10()

def err_Edrug2D_NDB_hill(d,x):
    d1      = d[0]
    d2      = d[1]
    E0      = ufloat(x[0],x[1])
    E1      = ufloat(x[2],x[3])
    E2      = ufloat(x[4],x[5])
    E3      = ufloat(x[6],x[7])
    r1      = ufloat(x[8],x[9])
    r2      = ufloat(x[10],x[11])
    C1      = 10**ufloat(x[12],x[13])
    C2      = 10**ufloat(x[14],x[15])
    h1      = 10**ufloat(x[16],x[17])
    h2      = 10**ufloat(x[18],x[19])
    alpha1  = 10**ufloat(x[20],x[21])
    alpha2  = 10**ufloat(x[22],x[23])
    gamma1  = 10**ufloat(x[24],x[25])
    gamma2  = 10**ufloat(x[26],x[27])    
    try:
        if d1==0 and d2!=0:
            Ed = Edrug1D(d2,E0,E2,C2,h2)
            
        elif d1!=0 and d2==0:
            Ed = Edrug1D(d1,E0,E1,C1,h1)
      
        elif d1==0 and d2==0:
            Ed = E0
            
        else:
            Ed =    (E2*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2**(gamma1 + 1)*(C1**h1*r1)**gamma2 + 
                     E1*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*(C2**h2*r2)**gamma1 + 
                     C1**h1*E0*r1**(gamma2 + 1)*(alpha2*d1)**(gamma2*h1)*(C2**h2*r2)**gamma1 + 
                     C2**h2*E0*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2)*(C1**h1*r1)**gamma2 + 
                     E2*d1**h1*r1**(gamma2 + 1)*r2**gamma1*(C1**h1)**gamma2*(alpha1*d2)**(gamma1*h2) + 
                     E1*d2**h2*r1**gamma2*r2**(gamma1 + 1)*(C2**h2)**gamma1*(alpha2*d1)**(gamma2*h1) + 
                     E3*alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r1**gamma2*r2**(gamma1 + 1)*(alpha2*d1)**(gamma2*h1) + 
                     E3*alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*r2**gamma1*(alpha1*d2)**(gamma1*h2) + 
                     C1**h1*C2**h2*E0*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + C1**h1*C2**h2*E0*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
                     C2**h2*E3*d1**h1*r1*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2) + 
                     C1**h1*E3*d2**h2*r1**(gamma2 + 1)*r2*(alpha2*d1)**(gamma2*h1) + 
                     C2**h2*E1*d1**h1*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + 
                     C2**h2*E1*d1**h1*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
                     C1**h1*E2*d2**h2*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + 
                     C1**h1*E2*d2**h2*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2)/ \
                    (C1**h1*r1**(gamma2 + 1)*(alpha2*d1)**(gamma2*h1)*(C2**h2*r2)**gamma1 + 
                     C2**h2*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2)*(C1**h1*r1)**gamma2 + 
                     alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r2**(gamma1 + 1)*(C1**h1*r1)**gamma2 + 
                     alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*(C2**h2*r2)**gamma1 + 
                     alpha1**(gamma1*h2)*d2**(h2*(gamma1 + 1))*r1**gamma2*r2**(gamma1 + 1)*(alpha2*d1)**(gamma2*h1) + 
                     alpha2**(gamma2*h1)*d1**(h1*(gamma2 + 1))*r1**(gamma2 + 1)*r2**gamma1*(alpha1*d2)**(gamma1*h2) + 
                     C1**h1*C2**h2*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + C1**h1*C2**h2*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
                     C2**h2*d1**h1*r1*r2**(gamma1 + 1)*(alpha1*d2)**(gamma1*h2) + 
                     C1**h1*d2**h2*r1**(gamma2 + 1)*r2*(alpha2*d1)**(gamma2*h1) +
                     C1**h1*d2**h2*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 +
                     C1**h1*d2**h2*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
                     C2**h2*d1**h1*r1*r2**(gamma1 + 1)*(C2**h2)**gamma1 + 
                     C2**h2*d1**h1*r1**(gamma2 + 1)*r2*(C1**h1)**gamma2 + 
                     d1**h1*r1**(gamma2 + 1)*r2**gamma1*(C1**h1)**gamma2*(alpha1*d2)**(gamma1*h2) + 
                     d2**h2*r1**gamma2*r2**(gamma1 + 1)*(C2**h2)**gamma1*(alpha2*d1)**(gamma2*h1))
                 
        return Ed.std_dev
    except:
        return np.nan
   

def Edrug2D_NDB_hill_pw(d,E0,E1,E2,E3,C1,h1,alpha2,gamma2):
    d1 = d[0]
    d2 = d[1]
    return np.piecewise(d1,[d2==0,d2!=0],[lambda d1: Edrug1D(d1,E0,E1,C1,h1),lambda d1: Edrug1D(d1,E2,E3,C1/alpha2,h1*gamma2)])
