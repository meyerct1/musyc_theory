#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 09:57:30 2018

@author: xnmeyer
"""
import plotly.graph_objs as go
from plotly.offline import  plot
import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')  # or whatever other backend that you want
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import *
from matplotlib.collections import PolyCollection
from matplotlib.ticker import FormatStrFormatter
from matplotlib.colors import colorConverter
#from matplotlib.patches import FancyArrowPatch
from matplotlib.colors import ListedColormap
import matplotlib.cm as cm
from matplotlib import rc
from scipy import interpolate
from sklearn.metrics import r2_score
rc('text', usetex=False)
font = {'family' : 'arial',
        'weight':'normal',
        'size'   : 8}
axes = {'linewidth': 2}
rc('font', **font)
rc('axes',**axes)


from NDHillFun import Edrug1D, Edrug2D_NDB, err_Edrug2D_NDB, Edrug2D_NDB_hill, err_Edrug2D_NDB_hill
import os
import datetime

def plotDoseResponseSurf(T,d1,d2,dip,dip_sd,zero_conc=1):   
    popt3 = [ T['E0'],
              T['E1'],
              T['E2'],
              T['E3'],
              T['r1'],
              T['r2'],
              10**T['log_C1'],
              10**T['log_C2'],
              10**T['log_h1'],
              10**T['log_h2'],
              10**T['log_alpha1'],
              10**T['log_alpha2'],
              10**T['log_gamma1'],
              10**T['log_gamma2']]
        
    if T['to_save_plots']==1 and T['boundary_sampling']==0:
        DosePlots_PLY(d1,d2,dip,dip_sd,T['drug1_name'],T['drug2_name'],popt3,'NDB_hill',T['sample'],T['expt'],T['metric_name'],T['save_direc'],zero_conc=zero_conc)     
    elif T['to_save_plots']==1 and T['boundary_sampling']==1:
        DosePlots_1D(T,d1,d2,dip,dip_sd,zero_conc=zero_conc)         
    return T


def DosePlots_1D(T,d1,d2,dip,dip_sd,zero_conc=1):
    #To plot doses on log space
    t_d1 = d1.copy()
    t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10.**zero_conc

    plt.figure(figsize=(2.5,2.5));    ax = plt.subplot(111)
    plt.scatter(np.log10(t_d1[d2==0]),dip[d2==0],color='b')
    dr = np.logspace(np.log10(np.min(t_d1)),np.log10(np.max(t_d1)))
    y = Edrug1D(dr,T['E0'],T['E1'],T['C1'],T['h1'])
    plt.plot(np.log10(dr),y,'b',label=T['drug1_name'])

    plt.scatter(np.log10(t_d1[d2!=0]),dip[d2!=0],color='r')
    yp = Edrug1D(dr,T['E2'],T['E3'],T['C1']/10**T['log_alpha2'],T['h1']*10**T['log_gamma2'])
    plt.plot(np.log10(dr),yp,'r',label=T['drug1_name']+'+'+T['drug2_name'])
    #Format the plot
    #plt.ylim(ylim)
    if T['asym_ci']==1:
        plt.title(T['sample'] + ":\n" r"$\alpha$={:.2} [{:.2},{:.2}]" "\n" r"$\beta(obs)$={:.2} [{:.2},{:.2}]".format(float(T['log_alpha2']),
                  float(T['log_alpha2_ci'][0]),float(T['log_alpha2_ci'][1]),float(T['beta_obs']),float(T['beta_obs_ci'][0]),float(T['beta_obs_ci'][1])),fontsize=8)
    else:
        plt.title(T['sample'] + ":\n" r"$\alpha$={:.2}$\pm${:.2}" "\n" r"$\beta(obs)$={:.2}$\pm${:.2}".format(float(T['log_alpha2']),
                  float(T['log_alpha2_std']),float(T['beta_obs']),float(T['beta_obs_std'])),fontsize=8)
        
    plt.xlabel('log(drug)')
    plt.ylabel(T['metric_name'])
    plt.yticks(rotation=45)
    ax.xaxis.set_tick_params(pad=-1)
    ax.yaxis.set_tick_params(pad=-4)
    ax.yaxis.labelpad=-4
    ax.xaxis.labelpad=-1
    ax.legend(handletextpad=0.05,handlelength=.5,borderaxespad=0.,frameon=False,loc='lower left')
    plt.tight_layout()
    plt.savefig(T['sample']+'_'+T['drug1_name']+'_'+T['drug2_name']+'.pdf',bbox_inches='tight')
    
    
############################################################
#Function to plot the combination surface plots in plotly    
############################################################
def DosePlots_PLY(d1,d2,dip,dip_sd,drug1_name,drug2_name,popt3,which_model,sample,expt,metric_name,direc,zero_conc=1):
    #To plot doses on log space
    t_d1 = d1.copy()
    t_d2 = d2.copy()
    t_d1[t_d1==0] = min(t_d1[t_d1!=0])/10.**zero_conc
    t_d2[t_d2==0] = min(t_d2[t_d2!=0])/10.**zero_conc

    trace1 = go.Scatter3d(x=np.log10(t_d1),y=np.log10(t_d2),z=dip,mode='markers',
                        marker=dict(size=3,color ='rgb(0,0,0)',
                                    line=dict(
                                              color='rgb(0,0,0)',width=1)),
                        )    
    #Plot the fit
    conc1 = 10**np.linspace(np.log10(min(t_d1)),np.log10(max(t_d1)), 30)
    conc2 = 10**np.linspace(np.log10(min(t_d2)),np.log10(max(t_d2)), 30)
    conc3 = np.array([(c1,c2) for c1 in conc1 for c2 in conc2])
    if which_model=='NDB':
        twoD_fit = np.array([Edrug2D_NDB(d,*popt3) for d in conc3])
    elif which_model=='NDB_hill':
        twoD_fit = np.array([Edrug2D_NDB_hill(d,*popt3) for d in conc3])
        
    zl = [min(twoD_fit),max(twoD_fit)]
    twoD_fit = np.resize(twoD_fit, (len(conc1),len(conc2)))
    
    [X,Y] = np.meshgrid(np.log10(conc1), np.log10(conc2))

    trace2 = go.Surface(x=X,y=Y,z=twoD_fit.transpose(),
                      colorscale = 'Viridis',
                      opacity=.9,
                      contours  = dict( 
                                        y = dict(highlight=False,show=False,color='#444',width=1),
                                        x = dict(highlight=False,show=False,color='#444',width = 1 ),
                                        z = dict(highlight=True,show=True,highlightwidth=4,width=3,usecolormap=False)
                                        ))

                      
    
    layout = go.Layout(
                       title = sample,
                       scene = dict(
                                    xaxis=dict(    
                                               range = [np.log10(min(t_d1)),np.log10(max(t_d1))],
                                               title = 'log(' + drug1_name + ')',
                                               autorange=True,
                                               showgrid=True,
                                               zeroline=False,
                                               showline=False,
                                               ticks='',
                                               showticklabels=True,
                                               gridwidth = 5,
                                               gridcolor = 'k'
                                    ),
                                    yaxis=dict(    
                                               range = [np.log10(min(t_d1)),np.log10(max(t_d1))],
                                               title = 'log(' + drug2_name + ')',
                                               autorange=True,
                                               showgrid=True,
                                               zeroline=False,
                                               showline=False,
                                               ticks='',
                                               showticklabels=True,
                                               gridwidth = 5,
                                               gridcolor = 'k'
                                    ),
                                    zaxis=dict(
                                               range = zl,
                                               title = metric_name,
                                               autorange=True,
                                               tick0=np.min(twoD_fit),
                                               dtick=(np.max(twoD_fit)-np.min(twoD_fit))/5,
                                               showgrid=True,
                                               zeroline=True,
                                               zerolinewidth=5,
                                               showline=False,
                                               ticks='',
                                               showticklabels=True,
                                               gridwidth=5,
                                               gridcolor = 'k'
                                    )
                                    ),
                    margin=dict(
                                l=0,
                                r=0,
                                b=0,
                                t=35
                    ),
                    showlegend=False,
                    font=dict(family='Arial', size=18),
    )
    
    data = [trace1,trace2]

    for e,i in enumerate(dip_sd):
        x = np.log10(t_d1[e])*np.ones((2,))
        y = np.log10(t_d2[e])*np.ones((2,))
        z = np.array([dip[e]+i,dip[e]-i])
        trace = go.Scatter3d(
            x=x, y=y, z=z,
            mode = 'lines',
            line=dict(
                color='#1f77b4',
                width=2
            )
        )
        data.append(trace)

        
    fig = go.Figure(data=data,layout=layout)
    camera = dict(
                  up=dict(x=0,y=0,z=1),
                  center=dict(x=0,y=0,z=0),
                  eye = dict(x=1.25,y=-1.25,z=1.25))
    fig['layout'].update(scene=dict(camera=camera))
    s_idx = expt.rfind(os.sep)
    expt = expt[s_idx+1:-4]
    plot(fig,filename = direc + os.sep + '{}_{}_{}_{}_DoseResponseSurface.html'.format(datetime.datetime.now().strftime('%Y%m%d'),sample,drug1_name,drug2_name),auto_open=False)



#fname=None; zlim=(-.1,1.1); zero_conc=1;title=None
def matplotlibDoseResponseSurface(T,d1,d2,dip_rate,dip_sd,fname=None, zlim=None, zero_conc=1,title=None,d1lim=None,d2lim=None,fmt='pdf',plt_slices=True,plt_zeropln=True,plt_data=True,incl_gamma=False,figsize_surf=(3.5,3),figsize_slice=(2.75,1.5)):
    N=15;elev=20;azim=19;alpha = 0.9;path = T['save_direc'];metric_name=T['metric_name'].values[0];
    if 'fit_gamma' in T:
        if T['fit_gamma']==1:
            incl_gamma=True
    e0, e1, e2, e3, h1, h2, r1, r1r, r2, r2r, ec50_1, ec50_2, alpha1, alpha2, gamma1,gamma2,d1_min, d1_max, d2_min, d2_max, drug1_name, drug2_name, expt = get_params(T)
    
    if d1lim is not None:
        d1_min=d1lim[0];d1_max=d1lim[1]
    if d2lim is not None:
        d2_min=d2lim[0];d2_max=d2lim[1]    
        
    fig1 = plt.figure(figsize=figsize_surf,facecolor='w')
    ax = fig1.gca(projection='3d')
    #ax.set_axis_off()
    y = np.linspace(d1_min-zero_conc, d1_max ,N)
    t = np.linspace(d2_min-zero_conc, d2_max ,N)

    if incl_gamma:
        tt, yy = np.meshgrid(t, y)
        conc3 = np.array([(np.power(10.,c1),np.power(10.,c2)) for c1 in y for c2 in t])
        zz = np.array([Edrug2D_NDB_hill(d,e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2) for d in conc3])
        zz = zz.reshape(N,N)
    else:
        yy, tt = np.meshgrid(y, t)
        zz = dip(yy,tt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
    if zlim is None:
        zmin = np.min(zz)
        zmax = np.max(zz)
        if np.abs(zmin) > np.abs(zmax): zmax = np.abs(zmin)
        else: zmin = -np.abs(zmax)
    else:
        zmin = zlim[0]
        zmax = zlim[1]
        
    # Plot it once on a throwaway axis, to get cbar without alpha problems
    my_cmap_rgb = plt.get_cmap('PRGn')(np.arange(256))

    for i in range(3): # Do not include the last column!
        my_cmap_rgb[:,i] = (1 - alpha) + alpha*my_cmap_rgb[:,i]
    my_cmap = ListedColormap(my_cmap_rgb, name='my_cmap')
    surf = ax.plot_surface(yy, tt, zz, cstride=1, rstride=1, cmap = my_cmap, vmin=zmin, vmax=zmax, linewidth=0)
#    cbar_ax = fig.add_axes([0.1, 0.9, 0.6, 0.05])
    cbar = fig1.colorbar(surf,ticks=[zmin, 0, zmax],pad=-.08,fraction=.025)
    cbar.ax.set_yticklabels(['%.2f'%zmin, 0, '%.2f'%zmax])
    cbar.solids.set_rasterized(True)
    cbar.solids.set_edgecolor('face')
    cbar.outline.set_visible(False)
    
    plt.cla()
    ax.set_zlim(zmin,zmax)
    # Plot the surface for real, with appropriate alpha
    surf = ax.plot_surface(tt, yy, zz, cstride=1, rstride=1, alpha=alpha, cmap = cm.PRGn, vmin=zmin, vmax=zmax, linewidth=0,zorder=1)
    
    # colored curves on left and right
    lw = 5
    if incl_gamma:
        ax.plot(d2_min*np.ones(y.shape)-zero_conc, y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,d2_min*np.ones(y.shape)-zero_conc)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), linewidth=lw,color='b')
        ax.plot(d2_max*np.ones(y.shape), y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,d2_max*np.ones(y.shape))),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), linewidth=lw,color='b',linestyle='--')

        ax.plot(t, d1_min*np.ones(y.shape)-zero_conc, Edrug2D_NDB_hill((np.power(10.,d1_min*np.ones(t.shape)-zero_conc),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), linewidth=lw,color='r')
        ax.plot(t, d1_max*np.ones(y.shape), Edrug2D_NDB_hill((np.power(10.,d1_max*np.ones(t.shape)),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), linewidth=lw,color='r',linestyle='--')
    
    else:
        ax.plot(d2_min*np.ones(y.shape)-zero_conc, y, dip(y,d2_min-zero_conc, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), linewidth=lw,color='b')
        ax.plot(d2_max*np.ones(y.shape), y, dip(y,d2_max, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), linewidth=lw,color='b',linestyle='--')
    
        ax.plot(t, d1_min*np.ones(y.shape)-zero_conc, dip(d1_min-zero_conc,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), linewidth=lw,color='r')
        ax.plot(t, d1_max*np.ones(y.shape), dip(d1_max,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), linewidth=lw,color='r',linestyle='--')
        
    
    if incl_gamma:
        # light grey grid across surface
        for ttt in np.linspace(d2_min-zero_conc,d2_max,10):
            ax.plot(ttt*np.ones(y.shape), y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,ttt)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
            ax.plot(ttt*np.ones(y.shape), y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,ttt)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
            ax.plot(ttt*np.ones(y.shape), y, Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,ttt)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
    
        for yyy in np.linspace(d1_min-zero_conc, d1_max,10):
            ax.plot(t, yyy*np.ones(y.shape), Edrug2D_NDB_hill((np.power(10.,yyy),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
            ax.plot(t, yyy*np.ones(y.shape), Edrug2D_NDB_hill((np.power(10.,yyy),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
            ax.plot(t, yyy*np.ones(y.shape), Edrug2D_NDB_hill((np.power(10.,yyy),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2), '-k', linewidth=1, alpha=0.1)
        
    else:
        # light grey grid across surface
        for ttt in np.linspace(d2_min-zero_conc,d2_max,10):
            ax.plot(ttt*np.ones(y.shape), y, dip(y,ttt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
            ax.plot(ttt*np.ones(y.shape), y, dip(y,ttt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
            ax.plot(ttt*np.ones(y.shape), y, dip(y,ttt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
    
        for yyy in np.linspace(d1_min-zero_conc, d1_max,10):
            ax.plot(t, yyy*np.ones(y.shape), dip(yyy,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
            ax.plot(t, yyy*np.ones(y.shape), dip(yyy,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)
            ax.plot(t, yyy*np.ones(y.shape), dip(yyy,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r), '-k', linewidth=1, alpha=0.1)

    # Set the view
    ax.view_init(elev=elev, azim=azim)
    ax.set_ylabel("log(%s)[M]"%drug1_name,labelpad=-5)   
    ax.set_xlabel("log(%s)[M]"%drug2_name,labelpad=-5)
    ax.set_zlabel(metric_name,labelpad=-3)

    if plt_data:
        scat_d1 = d1.copy()
        scat_d2 = d2.copy()
        scat_dip = dip_rate.copy()
        scat_erb = dip_sd.copy()
        scat_d1 = np.log10(scat_d1)
        scat_d2 = np.log10(scat_d2)
        if d1lim is not None:
            scat_d1[scat_d1==-np.inf] = d1_min
        else:
            scat_d1[scat_d1==-np.inf] = scat_d1[scat_d1!=-np.inf].min()-zero_conc
        if d2lim is not None:
            scat_d2[scat_d2==-np.inf] = d2_min
        else:
            scat_d2[scat_d2==-np.inf] = scat_d2[scat_d2!=-np.inf].min()-zero_conc
        
        #Remove values which are outside bounds
        mask = ~np.isnan(scat_d1)
        if d1lim is not None:
            mask=(mask) & ((scat_d1>=d1_min) & (scat_d1<=d1_max))
        if d2lim is not None:
            mask=(mask) & ((scat_d2>=d2_min) & (scat_d2<=d2_max))
        scat_d1=scat_d1[mask];scat_d2=scat_d2[mask];
        scat_erb=scat_erb[mask];scat_dip=scat_dip[mask];
        
        ax.scatter(scat_d2, scat_d1, scat_dip, s=10,c='k', depthshade=False,zorder=100)
        # Plot error bars
        for _d1, _d2, _dip, _erb in zip(scat_d1, scat_d2, scat_dip, scat_erb):
            ax.plot([_d2,_d2], [_d1,_d1], [_dip-_erb, _dip+_erb], 'k-', alpha=0.3,linewidth=1)

    #format axis
    ax.yaxis.set_major_formatter(FormatStrFormatter('%.0f'))
    ax.zaxis.set_major_formatter(FormatStrFormatter('%.2f'))
    ax.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))


    ax.set_zticks([zmin,0,zmax])
    ax.xaxis.set_tick_params(pad=-4)
    ax.yaxis.set_tick_params(pad=-4)
    ax.zaxis.set_tick_params(pad=-2)


    # Plane of DIP=0
    if plt_zeropln:
        c_plane = colorConverter.to_rgba('k', alpha=0.3)
        verts = [np.array([(d2_min-zero_conc,d1_min-zero_conc), (d2_min-zero_conc,d1_max), (d2_max,d1_max), (d2_max,d1_min-zero_conc), (d2_min-zero_conc,d1_min-zero_conc)])]
        poly = PolyCollection(verts, facecolors=c_plane)
        #ax.add_collection3d(poly, zs=[max(e1,e2)], zdir='z')
        ax.add_collection3d(poly, zs=[0], zdir='z')

        # Plot intersection of surface with DIP=0 plane
        #plt.contour(tt,yy,zz,levels=[max(e1,e2)], linewidths=3, colors='k')
        plt.contour(tt,yy,zz,levels=[0], linewidths=3, colors='k')

    ax_surf=ax
    # If needed, manually set zlim
    if zlim is not None: ax.set_zlim(zlim)
    plt.tight_layout()

    # Save plot, or show it
    if fname is None: plt.show()
    else: plt.savefig(fname+'.'+fmt,pad_inches=0.,format=fmt);plt.close()
    
    #######################
    #Now plot slices
    ######################
    if plt_slices:
        if d1lim is not None:
            d1_min=d1lim[0];d1_max=d1lim[1]
        if d2lim is not None:
            d2_min=d2lim[0];d1_max=d2lim[1]  
        y = np.linspace(d1_min, d1_max ,N)
        t = np.linspace(d2_min, d2_max ,N)
        x_ax = np.linspace(np.ceil(d2_min),np.floor(d2_max),int(np.floor(d2_max)-np.ceil(d2_min)+1.))
        if len(x_ax)>3:    
            x_ax=x_ax[0:-1:np.round(len(x_ax)/3)]
        y_ax = np.linspace(np.ceil(d1_min),np.floor(d1_max),int(np.floor(d1_max)-np.ceil(d1_min)+1.))
        if len(y_ax)>3:
            y_ax=y_ax[0:-1:np.round(len(y_ax)/3)]
    
    
        if incl_gamma:
            dip2_0 = Edrug2D_NDB_hill((np.power(10.,t),np.power(10.,d1_min*np.ones(len(y)))), e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2)
            dip2_1 = Edrug2D_NDB_hill((np.power(10.,t),np.power(10.,d1_max*np.ones(len(y))))          , e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2)
            dip1_0 = Edrug2D_NDB_hill((np.power(10.,d2_min*np.ones(len(y))),np.power(10.,y)), e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2)
            dip1_1 = Edrug2D_NDB_hill((np.power(10.,d2_max*np.ones(len(y))          ),np.power(10.,y)), e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2) 
        else:
            dip1_0 = dip(y,d2_min*np.ones(len(y)), e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
            dip1_1 = dip(y,d2_max*np.ones(len(y)), e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
            dip2_0 = dip(d1_min*np.ones(len(y)),t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
            dip2_1 = dip(d1_max*np.ones(len(y)),t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)    
    
        # colored curves on left and right
        fig2 = plt.figure(figsize=figsize_slice,facecolor='w')
        lw = 3
#        ax3 = plt.subplot2grid((2,2),(0,0),colspan=2)
        ax1 = plt.subplot2grid((1,2),(0,0))
        ax2 = plt.subplot2grid((1,2),(0,1))
        ax1.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
        ax1.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
        ax2.yaxis.set_major_formatter(FormatStrFormatter('%.1f'))
        ax2.xaxis.set_major_formatter(FormatStrFormatter('%.0f'))
        
        ax1.plot(y, dip1_0, linewidth=lw,color='b',label='min('+drug2_name[0:3]+')')
        ax1.plot(y, dip1_1, linewidth=lw,color='b',linestyle='--',label='max('+drug2_name[0:3]+')')
    #    ax1.plot(y,0*y,'k--',alpha=.7)
        ax1.set_ylim(zmin-.01,zmax+.01)
        #ax1.set_xlim(y[0],y[-1])
        ax1.set_ylabel(metric_name,labelpad=-2)
        ax1.set_xlabel("log(%s)[M]"%drug1_name[0:3],labelpad=-1) 
        ax1.spines['top'].set_visible(False)
        ax1.spines['right'].set_visible(False)
        # Put a legend below current axis
    #    ax1.legend(loc='lower left',fancybox=True, shadow=False, ncol=1,handletextpad=.5)
        ax1.set_xticks(x_ax)
        ax1.set_yticks([zmin,0,zmax])
        ax1.xaxis.set_tick_params(pad=1)
        ax1.yaxis.set_tick_params(pad=1)
        
        ax2.plot(t, dip2_0, linewidth=lw,color='r',label='min('+drug1_name[0:3]+')')
        ax2.plot(t, dip2_1, linewidth=lw,color='r',linestyle='--',label='max('+drug1_name[0:3]+')')
    #    ax2.plot(t,0*t,'k--',alpha=.7)
        ax2.set_ylim(zmin-.01,zmax+.01)
        #ax2.set_xlim(y[0],y[-1])
        ax2.set_ylabel('',labelpad=-3)
        ax2.set_xlabel("log(%s)[M]"%drug2_name[0:3],labelpad=-1)
        ax2.spines['top'].set_visible(False)
        ax2.spines['right'].set_visible(False)
    #    ax2.legend(loc='lower left',fancybox=True, shadow=False, ncol=1,handletextpad=.5)
        ax2.set_xticks(y_ax)
        ax2.set_yticks([zmin,0,zmax])
        ax2.xaxis.set_tick_params(pad=1)
        ax2.yaxis.set_tick_params(pad=1)
        
        #Plot the EC50
        plt.sca(ax1)
        f = interpolate.interp1d(dip1_0,y)
        y1 = (max(dip1_0)-(max(dip1_0)-min(dip1_0))/2)
        x1 = f(y1)    
        plt.scatter(x1,y1,s=30,marker= 'o',edgecolor='b',facecolor='w',zorder=100)
        plt.plot((x1,x1),(ax1.get_ylim()),color = 'b',linestyle='--',linewidth=1)
        f = interpolate.interp1d(dip1_1,y)
        y1 = (max(dip1_1)-(max(dip1_1)-min(dip1_1))/2)
        x1 = f(y1)    
        plt.scatter(x1,y1,s=20,marker='o',color='k',zorder=100)
        plt.plot((x1,x1),(ax1.get_ylim()),color = 'k',linestyle='--',linewidth=1)
    
    
        plt.sca(ax2)    
        f = interpolate.interp1d(dip2_0,t)
        y1 = (max(dip2_0)-(max(dip2_0)-min(dip2_0))/2)
        x1 = f(y1)    
        plt.scatter(x1,y1,s=30,marker= 'o',facecolor='w',edgecolor='r',zorder=100)
        plt.plot((x1,x1),(ax2.get_ylim()),color = 'r',linestyle='--',linewidth=1)
        f = interpolate.interp1d(dip2_1,t)
        y1 = (max(dip2_1)-(max(dip2_1)-min(dip2_1))/2)
        x1 = f(y1)    
        plt.scatter(x1,y1,s=20,marker='o',color='k',zorder=100)
        plt.plot((x1,x1),(ax2.get_ylim()),color = 'k',linestyle='--',linewidth=1)
     
#        ax3.spines["top"].set_visible(False)
#        ax3.spines["right"].set_visible(False)
#        ax3.spines["bottom"].set_visible(False)
#        ax3.spines["left"].set_visible(False)
#
#        ax3.set_xlim([-1,1])
#        ax3.set_ylim([-1,1])
#        plt.sca(ax3)
#        plt.text(.5,.5,title,va='center',ha='center',fontsize=8)
        plt.tight_layout()
        plt.subplots_adjust(wspace=.1)
        
        
        if fname is None: plt.show()
        else: plt.savefig(fname+'_slices.pdf',pad_inches=0.,format='pdf');plt.close()
        
        return [fig1,fig2],[ax_surf,ax1,ax2],[tt,yy,zz],surf

    
def matplotlibDoseResponseSlices(T,fname=None,zlim=None,zero_conc=1,title=None):
    incl_gamma=False;dd=.1;gN=50;N=10;metric_name=T['metric_name'].values[0];
    if 'fit_gamma' in T:
        if T['fit_gamma']==1:
            incl_gamma=True
            
    e0, e1, e2, e3, h1, h2, r1, r1r, r2, r2r, ec50_1, ec50_2, alpha1, alpha2, gamma1,gamma2,d1_min, d1_max, d2_min, d2_max, drug1_name, drug2_name, expt = get_params(T)
    #ax.set_axis_off()
    y = np.linspace(d1_min-zero_conc, d1_max ,gN)
    t = np.linspace(d2_min-zero_conc, d2_max ,gN)
    
    y_p = np.linspace(d1_min-zero_conc, d1_max ,N)
    t_p = np.linspace(d2_min-zero_conc, d2_max ,N)

    if incl_gamma:
        tt, yy = np.meshgrid(t, y)
        conc3 = np.array([(np.power(10.,c1),np.power(10.,c2)) for c1 in y for c2 in t])
        zz = np.array([Edrug2D_NDB_hill(d,e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2) for d in conc3])
        zz = zz.reshape(gN,gN)
    else:
        yy, tt = np.meshgrid(y, t)
        zz = dip(yy,tt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
    if zlim is None:
        zmin = np.min(zz)
        zmax = np.max(zz)
        if np.abs(zmin) > np.abs(zmax): zmax = np.abs(zmin)
        else: zmin = -np.abs(zmax)
    else:
        zmin = zlim[0]
        zmax = zlim[1]
        
    # colored curves on left and right
    fig = plt.figure(figsize=(2,2))
    ax1 = plt.subplot2grid((2,10),(0,0),colspan=8)
    ax2 = plt.subplot2grid((2,10),(1,0),colspan=8)
    ax11 = plt.subplot2grid((2,10),(0,9))
    ax22 = plt.subplot2grid((2,10),(1,9))
    ax111 = plt.subplot2grid((2,10),(0,8))
    ax222 = plt.subplot2grid((2,10),(1,8))

    plt.subplots_adjust(hspace=.5,wspace=.7)
    
    cmap1 = cm.Blues(np.linspace(.25,1,N))
    cmap2 = cm.Reds(np.linspace(.25,1,N))
    dx = 1e-6
    if incl_gamma:
        # light grey grid across surface
        for j,ttt in enumerate(t_p):
            dip1_0 = Edrug2D_NDB_hill((np.power(10.,y),np.power(10.,ttt)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2)
            f = interpolate.interp1d(dip1_0,y)
            y1 = (max(dip1_0)-(max(dip1_0)-min(dip1_0))/2)
            x1 = f(y1)    
            ax1.scatter(x1,y1,s=20,marker= 'o',color=cmap1[j,:],zorder=100)
            if j == 0 or j==N-1:
                ax1.plot(y, dip1_0, color=cmap1[j,:], linewidth=1)
            #for derivative
            dy =  Edrug2D_NDB_hill((np.power(10.,x1+dx),np.power(10.,ttt)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2)-Edrug2D_NDB_hill((np.power(10.,x1-dx),np.power(10.,ttt)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2)
            slope = dy/dx
            ax1.plot(np.array([x1-dd,x1+dd]),np.array([y1-dd*slope,y1+dd*slope]),color=cmap1[j,:])
            
        for j,yyy in enumerate(y_p):
            dip1_0 = Edrug2D_NDB_hill((np.power(10.,yyy),np.power(10.,t)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2)
            f = interpolate.interp1d(dip1_0,t)
            y1 = (max(dip1_0)-(max(dip1_0)-min(dip1_0))/2)
            x1 = f(y1)    
            ax2.scatter(x1,y1,s=20,marker= 'o',color=cmap2[j,:],zorder=100)
            if j == 0 or j==N-1:
                ax2.plot(t, dip1_0, color=cmap2[j,:], linewidth=1)
            #for derivative
            dy =  Edrug2D_NDB_hill((np.power(10.,yyy),np.power(10.,x1+dx)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2)-Edrug2D_NDB_hill((np.power(10.,yyy),np.power(10.,x1-dx)),e0,e1,e2,e3,r1,r2,ec50_1,ec50_2,h1,h2,alpha1,alpha2,gamma1,gamma2)
            slope = dy/dx
            ax2.plot(np.array([x1-dd,x1+dd]),np.array([y1-dd*slope,y1+dd*slope]),color=cmap2[j,:])

        
    else:
        for j,ttt in enumerate(t_p):
            dip1_0 = dip(y,ttt, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
            f = interpolate.interp1d(dip1_0,y)
            y1 = (max(dip1_0)-(max(dip1_0)-min(dip1_0))/2)
            x1 = f(y1)    
            ax1.scatter(x1,y1,s=20,marker= 'o',color=cmap1[j,:],zorder=100)
            if j == 0 or j==N-1:
                ax1.plot(y, dip1_0, color=cmap1[j,:], linewidth=1)
            
        for j,yyy in enumerate(y_p):
            dip1_0 = dip(yyy,t, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r)
            f = interpolate.interp1d(dip1_0,t)
            y1 = (max(dip1_0)-(max(dip1_0)-min(dip1_0))/2)
            x1 = f(y1)    
            ax2.scatter(x1,y1,s=20,marker= 'o',color=cmap2[j,:],zorder=100)
            if j == 0 or j==N-1:
                ax2.plot(t, dip1_0, color=cmap2[j,:], linewidth=1)

    ax1.set_xlim(d1_min,d1_max)
    ax2.set_xlim(d2_min,d2_max)
    ax1.set_ylim(zmin,zmax)
    ax2.set_ylim(zmin,zmax)    
    ax2.spines['right'].set_visible(False)
    ax1.spines['right'].set_visible(False)
    ax2.spines['top'].set_visible(False)
    ax1.spines['top'].set_visible(False)
    ax1.set_xlabel('log('+drug1_name+')',labelpad=-1)
    ax2.set_xlabel('log('+drug2_name+')',labelpad=-1)
    ax1.set_ylabel(metric_name,labelpad=-1)
    
    ax111.set_xlim(0,1)
    ax111.set_ylim(zmin,zmax)
    ax111.set_xticks([.5])
    ax111.scatter(.5,e2,s=20,c='r',marker='D')
    ax111.spines['right'].set_visible(False)
    ax111.spines['top'].set_visible(False)
    ax111.spines['left'].set_visible(False)
    ax111.set_xticklabels(['max('+drug2_name+')'])
    ax111.set_yticks([])

    ax222.set_xlim(0,1)
    ax222.set_ylim(zmin,zmax)
    ax222.set_xticks([.5])
    ax222.scatter(.5,e1,s=20,c='b',marker='D')
    ax222.spines['right'].set_visible(False)
    ax222.spines['top'].set_visible(False)
    ax222.spines['left'].set_visible(False)
    ax222.set_xticklabels(['max('+drug1_name+')'])
    ax222.set_yticks([])

    cb1 = mpl.colorbar.ColorbarBase(ax11, cmap=mpl.cm.Blues,
                                    norm=mpl.colors.Normalize(vmin=d2_min, vmax=d2_max),
                                    orientation='vertical',drawedges=False)
    
    cb1.set_label('log('+drug2_name+')',labelpad=-1)
    cb1.set_ticks((d2_min,d2_max))
    cb1.outline.set_edgecolor(None)

    cb2 = mpl.colorbar.ColorbarBase(ax22, cmap=mpl.cm.Reds,
                                    norm=mpl.colors.Normalize(vmin=d1_min, vmax=d1_max),
                                    orientation='vertical')
    cb2.set_label('log('+drug1_name+')',labelpad=-1)
    cb2.set_ticks((d1_min,d1_max))    
    cb2.outline.set_edgecolor(None)
    
    ax22.spines['left'].set_visible(False)
    ax11.spines['left'].set_visible(False)
    ax22.spines['top'].set_visible(False)
    ax11.spines['top'].set_visible(False)
    ax22.spines['bottom'].set_visible(False)
    ax11.spines['bottom'].set_visible(False)    

    if fname is None: plt.show()
    else: plt.savefig(fname+'_slices.pdf',pad_inches=0.,format='pdf');plt.close()
    return fig, [ax1,ax2,ax11,ax22,ax111,ax222]

def get_params(df):
    #Effects
    e0 = np.float(df['E0'])
    e1 = np.float(df['E1'])
    e2 = np.float(df['E2'])
    e3 = np.float(df['E3'])
    #EC50s
    ec50_1 = np.power(10.,np.float(df['log_C1']))
    ec50_2 = np.power(10.,np.float(df['log_C2']))
    #Hill slopes
    h1 = np.float(df['h1'])
    h2 = np.float(df['h2'])
    #Transition parameters
    r1 = np.float(df['r1'])
    r2 = np.float(df['r2'])
    r1r = r1*np.power(ec50_1,h1)
    r2r = r2*np.power(ec50_2,h2)
    #synergy parameters
    alpha_1 = np.power(10.,np.float(df['log_alpha1']))
    alpha_2 = np.power(10.,np.float(df['log_alpha2']))
    
    #synergy parameters
    if 'log_gamma1' in df:
        gamma_1 = np.power(10.,np.float(df['log_gamma1']))
        gamma_2 = np.power(10.,np.float(df['log_gamma2']))
    else:
        gamma_1 = 1
        gamma_2 = 1
        
    #max and min concentrations for the two drugs    
    concentration_1_min = np.log10(np.float(df['min_conc_d1']))
    concentration_2_min = np.log10(np.float(df['min_conc_d2']))
    concentration_1_max = np.log10(np.float(df['max_conc_d1']))
    concentration_2_max = np.log10(np.float(df['max_conc_d2']))
    #Get drug names
    drug1_name = str(df['drug1_name'].values[0])
    drug2_name = str(df['drug2_name'].values[0])
    #Get experiment name
    expt = str(df['expt'].values[0])
    return e0, e1, e2, e3, h1, h2, r1, r1r, r2, r2r, ec50_1, ec50_2, alpha_1, alpha_2, gamma_1, gamma_2, concentration_1_min, concentration_1_max, concentration_2_min, concentration_2_max, drug1_name, drug2_name, expt

#2D hill equation (full model...ie no detail balance)
def dip(d1, d2, e0, e1, e2, e3, h1, h2, alpha1, alpha2, r1, r1r, r2, r2r):
    d1 = np.power(10.,d1);      d2 = np.power(10.,d2)
    h1 = 1.*h1;                 h2 = 1.*h2
    alpha1 = 1.*alpha1;         alpha2 = 1.*alpha2
    r1 = 1.*r1;                 r1r = 1.*r1r
    r2 = 1.*r2;                 r2r = 1.*r2r
    #Percent unaffected in each population
    U = r1r*r2r*(r1*(alpha2*d1)**h1 + r1r + r2*(alpha1*d2)**h2 + r2r)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
    A1 = r1*r2r*(d1**h1*r1*(alpha2*d1)**h1 + d1**h1*r1r + d1**h1*r2r + d2**h2*r2*(alpha2*d1)**h1)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
    A2 = r1r*r2*(d1**h1*r1*(alpha1*d2)**h2 + d2**h2*r1r + d2**h2*r2*(alpha1*d2)**h2 + d2**h2*r2r)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
    A12 = r1*r2*(d1**h1*r1*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r2r*(alpha1*d2)**h2 + d2**h2*r1r*(alpha2*d1)**h1 + d2**h2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1)/(d1**h1*r1**2*r2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d1**h1*r1**2*r2r*(alpha2*d1)**h1 + d1**h1*r1*r1r*r2*(alpha1*d2)**h2 + d1**h1*r1*r1r*r2r + d1**h1*r1*r2*r2r*(alpha1*d2)**h2 + d1**h1*r1*r2r**2 + d2**h2*r1*r1r*r2*(alpha2*d1)**h1 + d2**h2*r1*r2**2*(alpha1*d2)**h2*(alpha2*d1)**h1 + d2**h2*r1*r2*r2r*(alpha2*d1)**h1 + d2**h2*r1r**2*r2 + d2**h2*r1r*r2**2*(alpha1*d2)**h2 + d2**h2*r1r*r2*r2r + r1*r1r*r2r*(alpha2*d1)**h1 + r1r**2*r2r + r1r*r2*r2r*(alpha1*d2)**h2 + r1r*r2r**2)
    #Return dip rate
    return U*e0 + A1*e1 + A2*e2 + A12*e3



