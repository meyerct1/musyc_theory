#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
#Code to calculate other synergy parameters for other metrics
"""
import numpy as np
import time
from scipy.stats import linregress
from scipy.optimize import curve_fit,root
import pandas as pd
import os
import subprocess
from sklearn.metrics import r2_score
from math import pi 
import sys

from initialFit import init_fit
from NDHillFun import Edrug2D_NDB_hill

#percent=True;E_fix=None;calc_brd_zip=False;
def calcOtherSynergy(T,d1,d2,dip,dip_sd,percent=True,E_fix=None,E_bnd=None,calc_brd_zip=False,musyc_path='/home/xnmeyer/Documents/Lab/Repos/MuSyC_Code/',hill_orient=1,scale=1.):
    print('Calculating other synergy metrics...')
    cur_path = os.getcwd()
    #Extract MuSyC fits
    E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, C1, C2, alpha1, alpha2,gamma1,gamma2, d1min, d1max, d2min, d2max, drug1_name, drug2_name, expt = get_params(pd.DataFrame([T]))
    #Rescale
    E0 = E0/float(scale);E1 = E1/float(scale);E2 = E2/float(scale);E3 = E3/float(scale);dip = dip/float(scale);dip_sd = dip_sd/float(scale);
    
    if (sum(d1==0)==0) & (sum(d2==0)==0):
        DD1, DD2 = np.meshgrid(np.concatenate(([0],np.unique(d1))),np.concatenate(([0],np.unique(d2))))
    elif (sum(d1==0)==0):
        DD1, DD2 = np.meshgrid(np.concatenate(([0],np.unique(d1))),np.unique(d2))
    elif (sum(d2==0)==0):
        DD1, DD2 = np.meshgrid(np.unique(d1),np.concatenate(([0],np.unique(d2))))
    else:
        DD1, DD2 = np.meshgrid(np.unique(d1),np.unique(d2))
            
    #Surface fit
    E = Edrug2D_NDB_hill((DD1.reshape((-1,)),DD2.reshape((-1,))),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2).reshape(DD2.shape)
   
    if np.isnan(E).any():
        loewe_fit_ec50,loewe_fit_ec10,loewe_fit_ec25,loewe_fit_mxd1d2,loewe_fit_all,loewe_fit_perUnd,hsa_fit_all,hsa_fit_ec50,hsa_fit_mxd1d2,bliss_fit_all,bliss_fit_ec50,bliss_fit_mxd1d2,schindler_fit_all,schindler_fit_ec50,schindler_fit_mxd1d2 = np.nan*np.zeros(15)
    else:   
        #Effect at ec50,ec10,ec50,maxd1,d2
        E_ec50 = Edrug2D_NDB_hill((C1,C2),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
        EC10_1 = hill_1D_inv(E0-(E0-E1)*.1,E0,E1,h1,C1) #concentration for this effect
        EC10_2 = hill_1D_inv(E0-(E0-E2)*.1,E0,E2,h2,C2) #concentration for this effect
        E_ec10 = Edrug2D_NDB_hill((EC10_1,EC10_2),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
        EC25_1 = hill_1D_inv(E0-(E0-E1)*.25,E0,E1,h1,C1)
        EC25_2 = hill_1D_inv(E0-(E0-E2)*.25,E0,E2,h2,C2)
        E_ec25 = Edrug2D_NDB_hill((EC25_1,EC25_2),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
        E_mxd1d2 = Edrug2D_NDB_hill((10**d1max,10**d2max),E0,E1,E2,E3,r1,r2,C1,C2,h1,h2,alpha1,alpha2,gamma1,gamma2)
    
        loewe_fit_ec50 = -np.log10(loewe(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2))
        loewe_fit_ec10 = -np.log10(loewe(EC10_1, EC10_2, E_ec10, E0, E1, E2, h1, h2, C1, C2))
        loewe_fit_ec25 = -np.log10(loewe(EC25_1, EC25_2, E_ec25, E0, E1, E2, h1, h2, C1, C2))
        loewe_fit_mxd1d2 = -np.log10(loewe(10**d1max, 10**d2max, E_ec25, E0, E1, E2, h1, h2, C1, C2))
        loewe_fit_all = np.nansum(-np.log10(loewe(DD1, DD2, E, E0, E1, E2, h1, h2, C1, C2)))
        loewe_fit_perUnd  = float(np.sum(np.isnan(-np.log10(loewe(DD1, DD2, E, E0, E1, E2, h1, h2, C1, C2)))))/len(E.reshape((-1,)))
            
        hsa_fit_all = hsa(DD1.reshape(-1,),DD2.reshape(-1,),E.reshape(-1,))
        hsa_fit_ec50 = hsa([E0-(E0-E1)/2],[E0-(E0-E2)/2],E_ec50)
        hsa_fit_mxd1d2= hsa([hill_1D(10**d1max,E0,E1,h1,C1)], [hill_1D(10**d2max,E0,E2,h2,C2)], E_mxd1d2)
    
        if percent:
            cnt=0
            dd1 = DD1.reshape((-1,));dd2=DD2.reshape((-1,));ee=E.reshape((-1,))
            for e,(x1,x2) in enumerate(zip(dd1,dd2)):
                cnt = cnt+bliss(ee[(dd1==x1)&(dd2==0)],ee[(dd1==0)&(dd2==x2)],ee[(dd1==x1)&(dd2==x2)])[0]
            bliss_fit_all=cnt
            bliss_fit_ec50 = bliss(E0-(E0-E1)/2, E0-(E0-E2)/2, E_ec50)
            bliss_fit_mxd1d2 = bliss(hill_1D(10**d1max,E0,E1,h1,C1), hill_1D(10**d2max,E0,E2,h2,C2), E_mxd1d2)
        else:
            bliss_fit_all=np.nan
            bliss_fit_ec50=np.nan
            bliss_fit_mxd1d2=np.nan
            
        schindler_fit_ec50 = schindler(C1, C2, E_ec50, E0, E1, E2, h1, h2, C1, C2, logspace=False)
        schindler_fit_all = np.nansum(schindler(DD1, DD2, E, E0, E1, E2, h1, h2, C1, C2, logspace=False))
        schindler_fit_mxd1d2 = np.nansum(schindler(10**d1max, 10**d2max, E, E0, E1, E2, h1, h2, C1, C2, logspace=False))
    #Now let the algorithms fit the raw data
    #For algorithms to be fit using raw data. First fit the 2 hill curves using the same function as used to fit for musyc.
    E_fx = [np.nan,np.nan,np.nan,np.nan] if E_fix is None else E_fix
    E_bd = [[-np.inf,-np.inf,-np.inf,-np.inf],[np.inf,np.inf,np.inf,np.inf]] if E_bnd is None else E_bnd
    sub_T = init_fit({'boundary_sampling':0},d1,d2,dip,dip_sd,drug1_name,drug2_name,E_fx,E_bd)  
    for key in T.keys():
        if key not in sub_T.keys():
            sub_T[key] = T[key]
    if sub_T['d1_init_pval']>.05 or sub_T['d2_init_pval']>.05:
        hsa_raw_all = np.nan
    else:
        keys = ['h1','h2','C1','C2']
        for k in keys: sub_T[k] = 10**sub_T['log_'+k]  
        E0, E1, E2, E3, h1, h2, r1, r1r, r2, r2r, C1, C2, alpha1, alpha2,gamma1,gamma2, d1min, d1max, d2min, d2max, drug1_name, drug2_name, expt = get_params(pd.DataFrame([sub_T]))     
        try:
            hsa_raw_all = hsa(d1,d2,dip)
        except:
            cnt = 0
            for x1 in np.unique(d1[(d1!=0)&(d2!=0)]):
                for x2 in np.unique(d2[(d1!=0)&(d2!=0)]):
                    cnt = cnt + hsa([hill_1D(x1,E0,E1,h1,C1)],[hill_1D(x2,E0,E2,h2,C2)],np.nanmean(dip[(d1==x1)&(d2==x2)]))
            hsa_raw_all = cnt            
    if percent:
        t1 = time.time()
        try:
            zimmer_C1,zimmer_C2,zimmer_h1,zimmer_h2,zimmer_raw_a1, zimmer_raw_a2, _, zimmer_R2,zimmer_loglike = zimmer(d1,d2,dip,sigma=dip_sd,p0=[C1,C2,h1,h2,1.,1.],vector_input=True)
        except RuntimeError: #Fit failed
            zimmer_C1,zimmer_C2,zimmer_h1,zimmer_h2,zimmer_raw_a1, zimmer_raw_a2, zimmer_R2,zimmer_loglike= np.zeros(8)*np.nan
        t2 = time.time()
        time_zimmer = (t2-t1)/60.
        try:
            ci_raw,_,_,ci_C1,ci_C2,ci_h1,ci_h2=combination_index(d1,d2,dip)
            ci_raw_mean  = np.nanmean(ci_raw.reshape((-1,)))
        except ValueError: #Fit failed
            ci_raw_mean,ci_C1,ci_C2,ci_h1,ci_h2=np.zeros(5)*np.nan
        try: #If the doses don't line up can't calculate bliss from the raw data
            cnt=0
            for e,(x1,x2) in enumerate(zip(d1,d2)):
                cnt = cnt+bliss(dip[(d1==x1)&(d2==0)],dip[(d1==0)&(d2==x2)],dip[(d1==x1)&(d2==x2)])
            bliss_raw_all = cnt[0]
        except:
            if sub_T['d1_init_pval']==1. or sub_T['d2_init_pval']==1.:
                bliss_raw_all = np.nan
            else:
                cnt = 0
                for x1 in np.unique(d1[(d1!=0)&(d2!=0)]):
                    for x2 in np.unique(d2[(d1!=0)&(d2!=0)]):
                        cnt = cnt + bliss(hill_1D(x1,E0,E1,h1,C1),hill_1D(x2,E0,E2,h2,C2),np.nanmean(dip[(d1==x1)&(d2==x2)]))
                bliss_raw_all = cnt 

    else:
        zimmer_C1,zimmer_C2,zimmer_h1,zimmer_h2,zimmer_raw_a1,zimmer_raw_a2,zimmer_R2,zimmer_loglike,time_zimmer = np.zeros(9)*np.nan
        ci_raw_mean,ci_C1,ci_C2,ci_h1,ci_h2,bliss_raw_all = np.zeros(6)*np.nan

    if calc_brd_zip:
        os.chdir(musyc_path + 'SynergyCalculator')
        try:
            zip_raw,time_zip = zip_syn(d1,d2,dip)
        except ValueError:
            zip_raw,time_zip = np.nan*np.zeros(2)

        brd_R2,brd_loglike,brd_C1,brd_C2,brd_h1,brd_h2,brd_delta,brd_kappa,brd_E0,brd_E1,brd_E2,brd_E3,time_braid = braid(d1,d2,dip,dip_sd)
        os.chdir(cur_path)
    else:
        brd_R2,brd_loglike,brd_C1,brd_C2,brd_h1,brd_h2,brd_delta,brd_kappa,brd_E0,brd_E1,brd_E2,brd_E3,time_braid = np.zeros(13)*np.nan
        zip_raw,time_zip = np.nan*np.zeros(2)
        
    #Convert to percent effect
    if hill_orient==1:
        tmp = sub_T['E0']-dip 
        sub_T['E1'],sub_T['E2'],sub_T['E0'] =  sub_T['E0'] -  np.array((sub_T['E1'],sub_T['E2'],sub_T['E0']))
    else:
        tmp = dip
    
    gdpi_la_iab,gdpi_la_iba,gdpi_la_eab,gdpi_la_eba, gpdi_la_r2, gpdi_la_loglike,time_gpdi_la, gdpi_bi_iab,gdpi_bi_iba,gdpi_bi_eab,gdpi_bi_eba, gpdi_bi_r2, gpdi_bi_loglike,time_gpdi_bi = GPDI(sub_T,d1,d2,tmp,drug1_name,drug2_name,sigma=dip_sd,percent=percent)

    return [loewe_fit_ec50,loewe_fit_ec10,loewe_fit_ec25,loewe_fit_mxd1d2,loewe_fit_all,loewe_fit_perUnd,
            hsa_fit_all,hsa_fit_ec50,hsa_fit_mxd1d2,
            bliss_fit_all,bliss_fit_ec50,bliss_fit_mxd1d2,
            schindler_fit_all,schindler_fit_ec50,schindler_fit_mxd1d2,
            hsa_raw_all,
            zimmer_C1,zimmer_C2,zimmer_h1,zimmer_h2,zimmer_raw_a1, zimmer_raw_a2, zimmer_R2, zimmer_loglike,
            ci_raw_mean,ci_C1,ci_C2,ci_h1,ci_h2,
            bliss_raw_all,
            zip_raw,
            brd_R2,brd_loglike,brd_C1,brd_C2,brd_h1,brd_h2,brd_delta,brd_kappa,brd_E0,brd_E1,brd_E2,brd_E3,
            gdpi_la_iab,gdpi_la_iba,gdpi_la_eab,gdpi_la_eba,
            gdpi_bi_iab,gdpi_bi_iba,gdpi_bi_eab,gdpi_bi_eba,            
            gpdi_la_r2, gpdi_la_loglike, gpdi_bi_r2, gpdi_bi_loglike,
            time_zip,time_braid,time_gpdi_la,time_gpdi_bi,time_zimmer]    
    
#################################
# Helper functions
#################################
def get_params(T):
    #Effects
    e0 = np.float(T['E0'])
    e1 = np.float(T['E1'])
    e2 = np.float(T['E2'])
    e3 = np.float(T['E3'])
    #EC50s
    ec50_1 = np.power(10.,np.float(T['log_C1']))
    ec50_2 = np.power(10.,np.float(T['log_C2']))
    #Hill slopes
    h1 = np.float(T['h1'])
    h2 = np.float(T['h2'])
    #Transition parameters
    r1 = np.float(T['r1'])
    r2 = np.float(T['r2'])
    r1r = r1*np.power(ec50_1,h1)
    r2r = r2*np.power(ec50_2,h2)
    #synergy parameters
    alpha_1 = np.power(10.,np.float(T['log_alpha1']))
    alpha_2 = np.power(10.,np.float(T['log_alpha2']))
    
    #synergy parameters
    if np.in1d('fit_gamma',T.columns)[0]:
        gamma_1 = np.power(10.,np.float(T['log_gamma1']))
        gamma_2 = np.power(10.,np.float(T['log_gamma2']))
    else:
        gamma_1 = 1.
        gamma_2 = 1.
    #max and min concentrations for the two drugs    
    concentration_1_min = np.log10(np.float(T['min_conc_d1']))
    concentration_2_min = np.log10(np.float(T['min_conc_d2']))
    concentration_1_max = np.log10(np.float(T['max_conc_d1']))
    concentration_2_max = np.log10(np.float(T['max_conc_d2']))
    #Get drug names
    drug1_name = str(T['drug1_name'].values[0])
    drug2_name = str(T['drug2_name'].values[0])
    #Get experiment name
    expt = str(T['expt'].values[0])
    return e0, e1, e2, e3, h1, h2, r1, r1r, r2, r2r, ec50_1, ec50_2, alpha_1, alpha_2, gamma_1, gamma_2, concentration_1_min, concentration_1_max, concentration_2_min, concentration_2_max, drug1_name, drug2_name, expt

def hill_1D(d, E0, E1, h, ec50, logspace=False):
    """
    1D hill equation. Returns effect at given dose d.
    """
    if logspace:
        ec50 = np.power(10.,ec50)
        d = np.power(10.,d)
    return E1 + (E0-E1) / (1. + (d/ec50)**h)

def hill_1D_inv(E, E0, E1, h, ec50, logspace=False):
    """
    Inversion of 1D hill equation. Returns dose that achieves effect e.
    """
#    if (e < min(e0,e1)) or (e > max(e0,e1)): return np.nan
    if logspace: ec50 = np.power(10., ec50)
    d = np.power((E0-E1)/(E-E1)-1.,1./h)*ec50
    if logspace: return np.log10(d)
    else: return d
    
def u_hill(d1, d2, E1, E2, h1, h2, C1, C2):
    """
    From "Theory of synergistic effects: Hill-type response surfaces as 'null-interaction' models for mixtures" - Michael Schindler
    
    E - u_hill = 0 : Additive
    E - u_hill > 0 : Synergistic
    E - u_hill < 0 : Antagonistic
    """
    m1 = (d1/C1)
    m2 = (d2/C2)
    
    y = (h1*m1 + h2*m2) / (m1+m2)
    u_max = (E1*m1 + E2*m2) / (m1+m2)
    power = np.power(m1+m2, y)
    
    return u_max * power / (1. + power)

def zimmer_effective_dose(d1, d2, C1, C2, h1, h2, a12, a21, logspace=False):
    """
    From Zimmer's effective dose model
    May return NaNs if a12 or a21 are too negative
    
    Returns the effective dose surface value
    """
    if logspace:
        d1 = np.power(10., d1)
        d2 = np.power(10., d2)
        C1 = np.power(10., C1)
        C2 = np.power(10., C2)
        
    A = d2 + C2*(a21+1) + d2*a12
    B = d2*C1 + C1*C2 + a12*d2*C1 - d1*(d2+C2*(a21+1))
    C = -d1*(d2*C1 + C1*C2)

    d1p = (-B + np.sqrt(np.power(B,2.) - 4*A*C)) / (2.*A)
    d2p = d2 / (1. + a21 / (1. + C1/d1p))
    
    # Check a12 and a21 to make sure they are in a nice defined range
    return hill_1D(d1p, 1., 0., h1, C1) * hill_1D(d2p, 1., 0., h2, C2)

def rates(ec50, h, logspace=False, r1=1.):
    """
    U -> A, at rate r1*d**h
    A -> U, at rate r1r
    The dose d at which U=A=1/2 (half maximal effect) is called ec50
    
    At equilibrium
    r1*ec50**h = r1r
    r1r/r1 = ec50**h
    Assumes r1 = 1.
    returns r1=1, r1r=ec50**h
    """
    if logspace: ec50 = np.power(10.,ec50)
    r1r = np.power(ec50, h)
    return r1, r1r
    
def get_ec50(r1, r1r, h, logspace=False):
    """
    Returns ec50
    """
    ec50 = np.power(r1r/r1, 1./h)
    if logspace: return np.log10(ec50)
    return ec50
    
    
def fit_hill_ci(d, E):
    """
    Fits a dose response curve with assuming that effect scales from 1
    to 0, using the median-effect equation from Chou-Talalay.
    
    d must be in linear space
    
    Requires 0 < e < 1: returns None if this is not satisfied
    
    returns 1, 0, h, ec50 (in linear space)
    """
    E = E.copy()
    E[(E < 0) | (E > 1)]=np.nan
    fU = E
    fA = 1-E

    C2 = linregress(np.log(d),np.log(fA/fU))
    h = C2.slope
    ec50 = np.exp(-C2.intercept/h)
    
    return 1., 0., h, ec50
    
def get_example_params(E0=1., E1=0., E2=0., E3=0., alpha1=1.,      \
                       alpha2=1., gamma1=1.,gamma2=1.,d1min=-8., d2min=-8., d1max=-3., \
                       d2max=-3., C1=None, C2=None, h1=1., h2=1.,  \
                       r1=1., r2=1.):
    """
    Generates example parameters.
    Returns E0, E1, E2, E3, alpha1, alpha2, d1min, d1max, d2min, d2max, C1, C2, h1, h2, r1, r1r, r2, r2r
    """
    if C1 is None: C1 = (d1min + d1max)/2.
    if C2 is None: C2 = (d2min + d2max)/2.
    r1,r1r = rates(C1, h1, logspace=True, r1=r1)
    r2,r2r = rates(C2, h2, logspace=True, r1=r2)
    return E0, E1, E2, E3, alpha1, alpha2, gamma1, gamma2, d1min, d1max, d2min, d2max, C1, C2, h1, h2, r1, r1r, r2, r2r
    

#################################
# Synergy metrics
#################################
def combination_index(d1,d2,E):
    #Remove the values > 1
    #pv_sub.reset_index()
    pv_sub = pd.DataFrame([])
    pv_sub['per_via']=E
    pv_sub['drug1.conc']=d1
    pv_sub['drug2.conc']=d2
    #Remove the values > 1
    pv_sub = pv_sub[pv_sub['per_via']<1];
    #pv_sub.reset_index()
    d1_single = pv_sub[pv_sub['drug2.conc']==0]
    d2_single = pv_sub[pv_sub['drug1.conc']==0]
    d1_t = d1_single['drug1.conc'].values
    d2_t = d2_single['drug2.conc'].values
    fA1 = 1- d1_single['per_via'].values
    fA2 = 1- d2_single['per_via'].values
    fA1 = fA1[d1_t!=0]
    d1_t = d1_t[d1_t!=0]
    fA2 = fA2[d2_t!=0]
    d2_t = d2_t[d2_t!=0]
    
    #Fit the data to a line
    C1 = linregress(np.log(d1_t),np.log(fA1/(1-fA1)))
    C2 = linregress(np.log(d2_t),np.log(fA2/(1-fA2)))
    m1 = C1.slope
    m2 = C2.slope
    Dm1 = np.exp(C1.intercept/(-m1))
    Dm2 = np.exp(C2.intercept/(-m2))
    
    #Calculate the CI
    def d1_ci(fA):
        return (fA/(1-fA))**(1/m1)*Dm1
    def d2_ci(fA):
        return (fA/(1-fA))**(1/m2)*Dm2
    
    #The effect of the combination and the respective drug concentrations
    combin_effect = 1-pv_sub['per_via'][np.logical_and(pv_sub['drug1.conc']!=0 ,pv_sub['drug2.conc']!=0)].values
    d1_full = pv_sub['drug1.conc'][np.logical_and(pv_sub['drug1.conc']!=0 ,pv_sub['drug2.conc']!=0)].values
    d2_full = pv_sub['drug2.conc'][np.logical_and(pv_sub['drug1.conc']!=0 ,pv_sub['drug2.conc']!=0)].values
    CI = np.zeros((len(combin_effect),1));
    for e,eff in enumerate(combin_effect):
        CI[e] = d1_full[e]/d1_ci(eff) + d2_full[e]/d2_ci(eff)
    return CI,d1_full,d2_full,Dm1,Dm2,m1,m2



def loewe(d1, d2, E, E0, E1, E2, h1, h2, C1, C2, logspace=False):
    """
    Loewe = 1 : Additive
    Loewe > 1 : Antagonistic
    Loewe < 1 : Synergistic

    To get CI, use h and ec50 from fit_hill_ci, and use e0=1 and emax=0
    """
    if logspace:
        d1 = np.power(10., d1)
        d2 = np.power(10., d2)
        C1 = np.power(10., C1)
        C2 = np.power(10., C2)
    
    D1C = hill_1D_inv(E, E0, E1, h1, C1) # concentration of drug 1 alone which gets effect E
    D2C = hill_1D_inv(E, E0, E2, h2, C2) # concentration of drug 2 alone which gets effect E
    return d1/D1C + d2/D2C

def bliss(e_drug1_alone, e_drug2_alone, e_combination):
    """
    Bliss = 0 : Additive
    Bliss > 0 : Synergistic
    Bliss < 0 : Antagonistic 
    
    Unreliable behavior unless 0 <= e <= 1 for all arguments.
    """
    return e_drug1_alone*e_drug2_alone - e_combination
    
def hsa(e_drug1_alone, e_drug2_alone, e_combination, decreasing=True):
    """
    HSA = 0 : Additive
    HSA > 0 : Synergistic
    HSA < 0 : Antagonistic 
    
    If decreasing=True, assumes that the stronger effect is a lower value
    If decreasing=False, assumes that the stronger effect is a higher value
    """
    if len(e_drug1_alone)==1:
        if decreasing: return min(e_drug1_alone, e_drug2_alone) - e_combination
        else: return e_combination - max(e_drug1_alone, e_drug2_alone)
    else: 
        DD1=e_drug1_alone
        DD2=e_drug2_alone
#        mask = (DD2!=0.)&(DD1!=0.)
#        DD1=DD1[mask]
#        DD2=DD2[mask]
#        e_combination=e_combination[mask]
        if decreasing:
            emx = 0.
            for i in range(len(DD1)):
                if len(min((e_combination[(DD1==DD1[i])&(DD2==0)],e_combination[(DD1==0)&(DD2==DD2[i])])))>0:
                    emx = emx + min((e_combination[(DD1==DD1[i])&(DD2==0)],e_combination[(DD1==0)&(DD2==DD2[i])]))-e_combination[i]
            if emx==0.:emx=np.nan
            return emx
        else:
            emx = 0.
            for i in range(len(DD1)):
                if len(min((e_combination[(DD1==DD1[i])&(DD2==0)],e_combination[(DD1==0)&(DD2==DD2[i])])))>0:
                    emx = emx - max((e_combination[(DD1==DD1[i])&(DD2==0)],e_combination[(DD1==0)&(DD2==DD2[i])]))+e_combination[i]
            if emx==0.:emx=np.nan
            return emx        
def schindler(d1, d2, E, E0, E1, E2, h1, h2, C1, C2, logspace=False):
    """
    From "Theory of synergistic effects: Hill-type response surfaces as 'null-interaction' models for mixtures" - Michael Schindler
    
    schin = 0 : Additive
    schin > 0 : Synergistic
    schin < 0 : Antagonistic
    """
    if logspace:
        d1 = np.power(10., d1)
        d2 = np.power(10., d2)
        C1 = np.power(10., C1)
        C2 = np.power(10., C2)
    return (E0-E) - u_hill(d1, d2, E0-E1, E0-E2, h1, h2, C1, C2)

def zimmer(DD1, DD2, E, logspace=False, sigma=None, vector_input=False,p0=None):
    """
    Returns a12, a21, C1, C2, h1, and h2
    """
    if logspace:
        DD1 = np.power(DD1,10.)
        DD2 = np.power(DD2,10.)
    
    if not vector_input:
        d1 = DD1.reshape((-1,))
        d2 = DD2.reshape((-1,))
        E = E.reshape((-1,))
        
    else:
        d1 = DD1
        d2 = DD2
        E = E
    if p0 is None:
        p0=[np.power(10.,-1),np.power(10.,-1),1,1,1,1]

    xdata = np.vstack((d1,d2))
        
    f = lambda d, C1, C2, h1, h2, a12, a21: zimmer_effective_dose(d[0], d[1], C1, C2, h1, h2, a12, a21, logspace=False)
    popt1, pcov = curve_fit(f, xdata, E, p0=p0,maxfev=int(1e5))
    C1, C2, h1, h2, a12, a21 = popt1
    perr = np.sqrt(np.diag(pcov))
    try:
        R2 = r2_score(E,f(xdata,*popt1))
    except ValueError:
        R2 = np.nan
    log_likelihood =  sum(np.log(1/(sigma*np.sqrt(2*pi)))) - sum(((E-f(xdata,*popt1))/sigma)**2)/2
    if np.isnan(log_likelihood):
        log_likelihood = -10e10
    
    return C1, C2, h1, h2, a12, a21, perr, R2, -log_likelihood   


def zip_syn(d1,d2,dip,drug1_name='drug1',drug2_name='drug2',drug1_units='M',drug2_units='M',sample='sample'):
    df = pd.DataFrame({'conc_r':d1,'conc_c':d2,'response':dip})
    df['drug_row'] = drug1_name
    df['drug_col'] = drug2_name
    df['conc_r_unit'] = drug1_units
    df['conc_c_unit'] = drug2_units
    df['cell_line_name'] = sample
    df['block_id'] = 1.
    np.random.seed(int(np.round(time.time())))    
    process = np.random.randint(0,100000000)    
    while True:
        if os.path.exists('./zip/test_data_'+str(process)+'.csv'):
            process = np.random.randint(0,100000000)    
        else:
            df.to_csv('./zip/test_data_'+str(process)+'.csv',index=False)
            break
   
    t1 = time.time()
    subprocess.call(["./zip/runR.sh",str(process)])
    t2 = time.time()
    zip_time = (t2-t1)/60.
    if os.path.exists('./zip/delta_score_'+str(process)+'.csv'):
        t = pd.read_csv('./zip/delta_score_'+str(process)+'.csv')
        os.remove('./zip/delta_score_'+str(process)+'.csv')
        os.remove('./zip/test_data_'+str(process)+'.csv')
        return t['x'].values[0],zip_time
    else:
        return np.nan,np.nan
   


def zip_syn_old(DD1,DD2,E,logspace=False,drug1_name='drug1',drug2_name='drug2',vector_input=False):
    if logspace:
        DD1 = np.power(10.,DD1)
        DD2 = np.power(10.,DD2)
        
    if vector_input:
        x1 = np.unique(DD1)
        x2 = np.unique(DD2)   
        xx1,xx2 = np.meshgrid(x1,x2)
        E_tmp = np.zeros((len(x1),len(x2)))
        for e1,i in enumerate(x1):
            for e2,j in enumerate(x2):
                E_tmp[e1,e2] = np.mean(E[(DD1==i)&(DD2==j)])
                
                
        E = E_tmp
    
    if np.isnan(E).any():
        return np.nan,np.nan
    #Need three tables to pass to script in Delta_calculation.R
    #Plate_mat is the treated matrix
    #conc_range is the doses for the drugs
    #pair.list is meta data for the drugs
    #See https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4759128/ for more info and examples
    plate_mat  = 1-pd.DataFrame(E).T
    start_time = time.time()#keep track of the time
    np.random.seed(int(np.round(start_time)))    
    process = np.random.randint(0,100000000)    
    while True:
        if os.path.exists('./zip/plate_mat_'+str(process)+'.csv'):
            process = np.random.randint(0,100000000)    
        else:
            plate_mat.to_csv('./zip/plate_mat_'+str(process)+'.csv',index=False)
            break
        
    conc_range = pd.DataFrame([])
    if vector_input:
        conc_range['drug1name']= xx1[0,:]
        conc_range['drug2name']= xx2[:,0]  
    else:    
        conc_range['drug1name'] = DD1[0,:]
        conc_range['drug2name'] = DD2[:,0]
    conc_range.to_csv('./zip/conc_range_'+str(process)+'.csv',index=False)    

    pd.DataFrame([{'index':1,'drug1':drug1_name,'drug2':drug2_name,'cell.line':'cellline','plate':1}]).to_csv('./zip/pair_list_'+str(process)+'.csv')
    t1 = time.time()
    subprocess.call(["./zip/runR.sh",str(process)])
    t2 = time.time()
    zip_time = (t2-t1)/60.
    if os.path.exists('./zip/delta_score_'+str(process)+'.csv'):
        t = pd.read_csv('./zip/delta_score_'+str(process)+'.csv')
        os.remove('./zip/delta_score_'+str(process)+'.csv')
        os.remove('./zip/plate_mat_'+str(process)+'.csv')
        os.remove('./zip/pair_list_'+str(process)+'.csv')
        os.remove('./zip/conc_range_'+str(process)+'.csv')

        return t['x'].values[0],zip_time
    else:
        return np.nan,np.nan

def braid(d1,d2,e,sigma,logspace=False):
    if logspace:
        d1 = np.power(d1,10.)
        d2 = np.power(d2,10.)

    #https://www.nature.com/articles/srep25523
    #write effect in a dataframe
    df = pd.DataFrame()
    df['eff']=e
    df['d1.conc']=d1
    df['d2.conc']=d2
    start_time = time.time()#keep track of the time
    np.random.seed(int(np.round(start_time))) 
    process = np.random.randint(0,100000000)    
    while True:
        if os.path.exists('braid/effect_'+str(process)+'.csv'):
            process = np.random.randint(0,100000000)    
        else:             
            df.to_csv('braid/effect_'+str(process)+'.csv',index=False)
            break
    t1 = time.time()
    subprocess.call(["./braid/runR.sh",str(process)])
    t2 = time.time()
    time_braid = (t2-t1)/60.
    if os.path.exists('braid/braid_parms_' + str(process) + '.csv'):
        t = pd.read_csv('braid/braid_parms_' + str(process) + '.csv',index_col=0)
        brd_C1,brd_C2,brd_h1,brd_h2,brd_delta,brd_kappa,brd_E0,brd_E1,brd_E2,brd_E3 = t['x'].values
        os.remove('braid/braid_parms_' + str(process) + '.csv')
        os.remove('braid/calc_kappa_' + str(process) + '.csv')
        os.remove('braid/effect_'+str(process)+'.csv')        
        popt1 = [brd_C1,brd_C2,brd_h1,brd_h2,brd_delta,brd_kappa,brd_E0,brd_E1,brd_E2,brd_E3]
        def f(d,C1,C2,h1,h2,delta,kappa,E0,E1,E2,E3):
            d1 = d[0]
            d2 = d[1]
            DB = (((E2-E0)/(E3-E0))*(d2/C2)**h2)/(1+(1-(E2-E0)/(E3-E0))*(d2/C2)**h2)
            DA = (((E1-E0)/(E3-E0))*(d1/C1)**h1)/(1+(1-(E1-E0)/(E3-E0))*(d1/C1)**h1)
            DAB = DA**(1/(delta*np.sqrt(h1*h2)))+DB**(1/(delta*np.sqrt(h1*h2)))+kappa*np.sqrt(DA**(1/(delta*np.sqrt(h1*h2)))*DB**(1/(delta*np.sqrt(h1*h2))))
            E = E0+(E3-E0)/(1+DAB**(-delta*np.sqrt(h1*h2)))
            return E
            
        e_fit = f((d1,d2),*popt1)
        brd_R2 = r2_score(e,e_fit)
        log_likelihood =  sum(np.log(1/(sigma*np.sqrt(2*pi)))) - sum(((e-e_fit)/sigma)**2)/2
        if np.isnan(log_likelihood):
            log_likelihood = -10e10
        return  brd_R2,-log_likelihood,brd_C1,brd_C2,brd_h1,brd_h2,brd_delta,brd_kappa,brd_E0,brd_E1,brd_E2,brd_E3,time_braid
    else:
        return np.zeros(13)*np.nan
    
    
    
    
#Fit GPDI model
#sigma=dip_sd;percent=True
def GPDI(sub_T,d1,d2,dip,drug1_name,drug2_name,sigma=None,percent=True):
    keys = ['h1','h2','C1','C2']
    for k in keys: sub_T[k] = 10**sub_T['log_'+k]  
    p0 = [sub_T['E1'],sub_T['C1'],sub_T['h1'],sub_T['E2'],sub_T['C2'],sub_T['h2']]
    #Fit BI
    xdata = np.vstack((d1,d2))
    #Don't fit HintAB
    t1 = time.time()
    IAB_LA,IBA_LA,EIAB_LA,EIBA_LA,R2_LA,log_likelihood_LA =np.nan*np.zeros(6)
    print('Fixing LA GDPI')
#    try: 
#        f = lambda d,IAB,IBA,EIAB,EIBA: LA_GPDI(d[0], d[1],dip,[p0[0],p0[1],p0[2],p0[3],p0[4],p0[5],IAB,IBA,EIAB,EIBA,1,1])
#        popt1, pcov = curve_fit(f, xdata, dip, p0=[0.,0.,sub_T['C1'],sub_T['C2']],maxfev=int(1e4),bounds=((-1,-1,min(d1),min(d2)),(np.inf,np.inf,max(d1),max(d2))))
#        try:
#            R2_LA = r2_score(dip,f(xdata,*popt1))
#        except ValueError:
#            R2_LA = np.nan
#        log_likelihood_LA =  sum(np.log(1/(sigma*np.sqrt(2*pi)))) - sum(((dip-f(xdata,*popt1))/sigma)**2)/2
#        if np.isnan(log_likelihood_LA):
#            log_likelihood_LA = -10e10
#        IAB_LA,IBA_LA,EIAB_LA,EIBA_LA = popt1 
#    except ValueError or RuntimeError:
#        IAB_LA,IBA_LA,EIAB_LA,EIBA_LA,R2_LA,log_likelihood_LA =np.nan*np.zeros(6)
    t2 = time.time()
    time_LA = (t2-t1)/60.
    t1 = time.time()
    IAB_BI, IBA_BI, EIAB_BI, EIBA_BI, R2_BI, log_likelihood_BI = np.nan*np.zeros(6)
    print('Fixing LA Bliss')
#    if percent:
#        try:
#            f = lambda d, IAB,IBA,EIAB,EIBA: BI_GPDI(d[0],d[1],[p0[0],p0[1],p0[2],p0[3],p0[4],p0[5],IAB,IBA,EIAB,EIBA,1,1])
#            popt1, pcov = curve_fit(f, xdata, dip, p0=[0,0,sub_T['C1'],sub_T['C2']],maxfev=int(1e4),bounds=((-1,-1,min(d1),min(d2)),(np.inf,np.inf,max(d1),max(d2))))
#            try:
#                R2_BI = r2_score(dip,f(xdata,*popt1))
#            except ValueError:
#                R2_BI = np.nan
#            log_likelihood_BI =  sum(np.log(1/(sigma*np.sqrt(2*pi)))) - sum(((dip-f(xdata,*popt1))/sigma)**2)/2
#            if np.isnan(log_likelihood_LA):
#                log_likelihood_BI = -10e10
#            IAB_BI,IBA_BI,EIAB_BI,EIBA_BI = popt1 
#        except ValueError or RuntimeError:
#            IAB_BI, IBA_BI, EIAB_BI, EIBA_BI, R2_BI, log_likelihood_BI = np.nan*np.zeros(6)
#    else:
#        IAB_BI, IBA_BI, EIAB_BI, EIBA_BI, R2_BI, log_likelihood_BI = np.nan*np.zeros(6)
    t2 = time.time()
    time_BI = (t2-t1)/60.
    
    return IAB_LA, IBA_LA, EIAB_LA, EIBA_LA, R2_LA, -log_likelihood_LA, time_LA, IAB_BI, IBA_BI, EIAB_BI, EIBA_BI, R2_BI, -log_likelihood_BI, time_BI

#GPDI model
def EA(d1,d2,E1,C1,h1,E2,C2,h2,IAB,IBA,EIAB,EIBA,hIAB,hIBA):
    return E1*d1**h1 / ((C1*(1+d2**hIAB/(EIAB**hIAB+d2**hIAB)*IAB))**h1+d1**h1)
def EB(d1,d2,E1,C1,h1,E2,C2,h2,IAB,IBA,EIAB,EIBA,hIAB,hIBA):
    return E2*d2**h2 / ((C2*(1+d1**hIBA/(EIBA**hIBA+d1**hIBA)*IBA))**h2+d2**h2)
def LA_GPDI(d1,d2,dip,p):
    #Parameters
    E1 = p[0];C1 = p[1];h1 = p[2]
    E2 = p[3];C2 = p[4];h2 = p[5]
    IAB = p[6]; IBA = p[7]; EIAB = p[8]; EIBA = p[9]
    hIAB = p[10]; hIBA = p[11]
    res = np.zeros(len(d1))*np.nan
    for i in range(len(d1)):
        if (d1[i]==0) & (d2[i]==0):
            res[i] = 0
        else:
            LA = lambda E: abs(d1[i]/(C1*(1.+(IAB*d2[i]**hIAB)/(EIAB**hIAB+d2[i]**hIAB))*((E/(E1-E))**(1./h1))) + 
                               d2[i]/(C2*(1.+(IBA*d1[i]**hIBA)/(EIBA**hIBA+d1[i]**hIBA))*((E/(E2-E))**(1./h2))) - 1)
            res[i] = root(LA,dip[i]).x
    return res
def BI_GPDI(d1,d2,p):
    #Parameters
    E1 = p[0];C1 = p[1];h1 = p[2]
    E2 = p[3];C2 = p[4];h2 = p[5]
    IAB = p[6]; IBA = p[7]; EIAB = p[8]; EIBA = p[9]
    hIAB = p[10]; hIBA = p[11]
    x1 = EA(d1,d2,E1,C1,h1,E2,C2,h2,IAB,IBA,EIAB,EIBA,hIAB,hIBA)
    x2 = EB(d1,d2,E1,C1,h1,E2,C2,h2,IAB,IBA,EIAB,EIBA,hIAB,hIBA)
    return x1+x2-x1*x2
    

