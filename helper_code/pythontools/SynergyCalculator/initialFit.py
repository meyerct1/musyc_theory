#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 09:57:31 2018

@author: xnmeyer
"""
import numpy as np
import scipy.stats
import scipy.optimize
##############################################################################
############## Functions to run the initial 1D hill fits      ################
##############################################################################

def init_fit(T,d1,d2,dip,dip_sd,drug1_name,drug2_name,E_fx,E_bd):
    #Cannot have a zero standard deviation.  A result from Darren's code on occasion.  Just set to minimal std.
    dip_sd[dip_sd==0] = min(dip_sd[dip_sd!=0])  
    #Try and fit the single dose response curves....
    p1 = 1; p2 = 1; #Store p-value of fit
    if T['boundary_sampling']==1:
        popt1, p1 , perr1 = fit_drc(d1[d2==0], dip[d2==0],E_fx=E_fx[0:2],E_bd=[E_bd[0][0:2],E_bd[1][0:2]])
        popt2, p2, perr2 = popt1,p1,perr1
        
    elif sum(d1==0)==0 and sum(d2==0)==0: #Neither drug has zero conditions
        popt1 = np.zeros((4,))*np.NAN; perr1 = np.zeros((2,))*np.NAN
        popt2 = np.zeros((4,))*np.NAN; perr2 = np.zeros((2,))*np.NAN
        #print(drug1_name + ' has no zero condition...skipping single dose fit.')
        #print(drug2_name + ' has no zero condition...skipping single dose fit.')
    elif sum(d1==0)==0:
        #print(drug1_name + ' has no zero condition...skipping single dose fit.')
        #Fit the single drug dose response curve
        popt1, p1 , perr1 = fit_drc(d1[d2==0], dip[d2==0],E_fx=E_fx[0:2],E_bd=[E_bd[0][0:2],E_bd[1][0:2]])
        popt2 = np.zeros((4,))*np.NAN; perr2 = np.zeros((4,))*np.NAN
    elif sum(d2==0)==0:
        #print(drug2_name + ' has no zero condition...skipping single dose fit.')
        popt1 = np.zeros((4,))*np.NAN; perr1 = np.zeros((4,))*np.NAN
        popt2, p2 , perr2 = fit_drc(d2[d1==0], dip[d1==0],E_fx=E_fx[0:3:2],E_bd=[E_bd[0][0:3:2],E_bd[1][0:3:2]])        
    else:
        popt1, p1 , perr1 = fit_drc(d1[d2==0], dip[d2==0],E_fx=E_fx[0:2],E_bd=[E_bd[0][0:2],E_bd[1][0:2]])
        popt2, p2 , perr2 = fit_drc(d2[d1==0], dip[d1==0],E_fx=E_fx[0:3:2],E_bd=[E_bd[0][0:3:2],E_bd[1][0:3:2]])

    #If fit failed make approximations:
    if np.isnan(popt1).any():
        #print(drug1_name + ' single fit failed.  Will make approximations based on data')  
        if np.isnan(E_fx).any():
            popt1 = np.array([0.,
                              np.mean(dip[np.where(d1-d2 == max(d1-d2))]),
                              np.mean(dip[np.where(d1+d2 == min(d1+d2))]),
                              np.log10(max(d1)) - (np.log10(max(d1))-np.log10(min(d1[d1!=0])))/2])
        else: popt1 = np.array([0.,E_fx[1],E_fx[0],np.log10(max(d1))-np.log10(min(d1[d1!=0]))])     
    #else: print(drug1_name + ' single fit succeeded with p-val:' + '%.2e'%p1)              
    if np.isnan(popt2).any():
        #print(drug2_name + ' single fit failed. Will make approximations based on data')
        if np.isnan(E_fx).any():
            popt2 = np.array([0.,
                              np.mean(dip[np.where(d2-d1 == max(d2-d1))]),
                              np.mean(dip[np.where(d1+d2 == min(d1+d2))]),
                              np.log10(max(d2)) - (np.log10(max(d2))-np.log10(min(d2[d2!=0])))/2])  
        else: popt2 = np.array([0.,E_fx[1],E_fx[0],np.log10(max(d2))-np.log10(min(d2[d2!=0]))])        
    #else: print(drug2_name + ' single fit succeeded with p-val:' + '%.2e'%p2)              
    
    #If hessian resulted in a singluar matrix which results in undefined uncertainty.  Make approximations:
    if np.isnan(perr1).any():
        #print(drug1_name + ' negative coraviariance.  Will make approximations for uncertainty based on data')           
        if np.isnan(E_fx).any():
            perr1 = np.array([0.25,
                  np.mean(dip_sd[np.where(d1-d2 == max(d1-d2))]),
                  np.mean(dip_sd[np.where(d1+d2 == min(d1+d2))]),
                  1])
        else: perr1 = np.array([0.25,0,0,1])
    if np.isnan(perr2).any():
        #print(drug2_name + ' negative coraviariance.  Will make approximations for uncertainty based on data')                            
        if np.isnan(E_fx).any():
            perr2 = np.array([0.25,
                  np.mean(dip_sd[np.where(d2-d1 == max(d2-d1))]),
                  np.mean(dip_sd[np.where(d1+d2 == min(d1+d2))]),
                  1])
        else: perr2 = np.array([0.25,0,0,1])
        
    #initial guesses for parameters to be fit initially using the lm method in the curve fit function
    #based on the fits resulting from the single dose response curves
    E0      = 0.5*(popt1[2]+popt2[2]);           E0_std      = 0.5*(perr1[2]+perr2[2]);
    E1      = popt1[1];                          E1_std      = perr1[1];
    if T['boundary_sampling']==1:
        #Estimate E2
        if np.isnan(E_fx[2]):
            E2      = np.average(dip[(d1==min(d1))&(d2==max(d2))])
            E2_std  = np.average(dip_sd[(d1==min(d1))&(d2==max(d2))])
        else:
            E2 = E_fx[2];
            E2_std = 0.
        if E2<E_bd[0][2]: E2=E_bd[0][2]
        if E2>E_bd[1][2]: E2=E_bd[1][2]
    else:
        E2      = popt2[1];                          E2_std      = perr2[1];    
        
    if np.isnan(E_fx[3]):
        #Find dose farthest from the origin
        t_d1 = np.copy(d1);t_d2=np.copy(d2);
        t_d1 = t_d1/max(d1);t_d2=t_d2/max(d2) #Rescale so that different units do not matter
        tmp = np.sqrt(t_d1**2+t_d2**2)
        if T['boundary_sampling']==0:
            E3      = np.average(dip[tmp.argsort()[-4:]],weights=[.5,.25,.125,.125]);  
        else:
            E3   = dip[tmp.argsort()[-1]]                        
        E3_std  = max(E1_std,E2_std); 
    else:
        E3      = E_fx[3];                          
        E3_std  = 0;
        
    if E3<E_bd[0][3]: E3=E_bd[0][3]
    if E3>E_bd[1][3]: E3=E_bd[1][3]
    
    beta         = np.nan
    beta_std     = np.nan
    
    #Other single dose response patterns
    log_C1  = popt1[3];                          log_C1_std  = perr1[3]
    log_C2  = popt2[3];                          log_C2_std  = perr2[3]
    log_h1  = popt1[0];                          log_h1_std  = perr1[0]
    log_h2  = popt2[0];                          log_h2_std  = perr2[0]
    
    #Store fit p-values
    T['d1_init_pval'] = p1
    T['d2_init_pval'] = p2

    #Store parameters and return dict
    T.update({'E0':E0,'E1':E1,'E2':E2,'E3':E3,'log_C1':log_C1,'log_C2':log_C2,'log_h1':log_h1,'log_h2':log_h2,
              'E0_std':E0_std,'E1_std':E1_std,'E2_std':E2_std,'E3_std':E3_std,
              'beta':beta,'beta_std':beta_std,
              'log_C1_std':log_C1_std,'log_C2_std':log_C2_std,'log_h1_std':log_h1_std,'log_h2_std':log_h2_std,
              'log_alpha1':0,'log_alpha1_std':2,'log_alpha2':0,'log_alpha2_std':2,
              'r1':100.,'r2':100.,'r1_std':0.,'r2_std':0.,
              'log_gamma1':0,'log_gamma1_std':.1,'log_gamma2':0,'log_gamma2_std':.1})
    
    return T


    
##############################################################################
#Functions from Alex's pyDRC library used for initial fits to the 1D hill eqn#
##############################################################################
def ll4(x, b, c, d, e):
    """
    Four parameter log-logistic function ("Hill curve")

     - b: log10(Hill slope)
     - c: Emax
     - d: E0
     - e: log10_EC50
     """
    return c+(d-c)/(1+(np.power(10,np.power(10,b)*(np.log10(x)-e))))

def ll4_inv(E,b,c,d,e):
    return np.power(10,(np.log10((d-c)/(E-c)-1)/np.power(10,b)+e))

#Fitting function
#doses = d1[d2==0]; dip_rates = dip[d2==0]; dip_std_errs=dip_sd[d2==0]
#doses = d2[d1==0]; dip_rates = dip[d1==0]; dip_std_errs=dip_sd[d1==0]
#null_rejection_threshold=0.05;E0_fix=None;Emx_fix=None;
def fit_drc(doses, dip_rates, dip_std_errs=None, 
            null_rejection_threshold=0.05,E_fx=None,E_bd=None):
    #Remove any nans
    dip_rate_nans = np.isnan(dip_rates)
    if np.any(dip_rate_nans):
        doses = doses[~dip_rate_nans]
        dip_rates = dip_rates[~dip_rate_nans]
        if dip_std_errs is not None:
            dip_std_errs = dip_std_errs[~dip_rate_nans]
            
    #Declare intial arrays
    popt = np.zeros((4,))*np.nan
    perr = np.zeros((4,))*np.nan
    
    if np.isnan(E_fx).any(): #If min/max drug effects are not fixed fit a 4 parameter hill curve
        curve_initial_guess = list(ll4_initials(doses, dip_rates))               
        if 10**curve_initial_guess[3]>max(doses): curve_initial_guess[3]=np.log10(max(doses))#If the EC50 is greater than the maximum dose
        if 10**curve_initial_guess[3]<min(doses[doses!=0]): curve_initial_guess[3]=np.log10(min(doses[doses!=0])) #If the EC50 is less than the minimum dose
                       
        hill_fn = ll4
        bounds = [(-2,E_bd[0][1],E_bd[0][0],-np.inf),(2,E_bd[1][1],E_bd[1][0],np.inf)]
        if curve_initial_guess[1]<E_bd[0][1]: curve_initial_guess[1]=E_bd[0][1]#If the Emx is less than the lower bound
        if curve_initial_guess[1]>E_bd[1][1]: curve_initial_guess[1]=E_bd[1][1]#If the Emx is greater than the upper bound
        if curve_initial_guess[2]<E_bd[0][0]: curve_initial_guess[2]=E_bd[0][0]#If the E0 is less than the lower bound
        if curve_initial_guess[2]>E_bd[1][0]: curve_initial_guess[2]=E_bd[1][0]#If the E0 is greater than the upper bound
        if curve_initial_guess[0]>2: curve_initial_guess[0]=2 #If the log(h) is greater than the upper bound
        if curve_initial_guess[0]<-2: curve_initial_guess[0]=-2 #If the log(h) is greater than the upper bound

                
    else: #Fit a 2 parameter hill curve (hill slope and ec50)
        curve_initial_guess = list(ll2_initials(doses,dip_rates,E_fx[0],E_fx[1]))
        if 10**curve_initial_guess[1]>max(doses): curve_initial_guess[1]=np.log10(max(doses))#If the EC50 is greater than the maximum dose
        if 10**curve_initial_guess[1]<min(doses): curve_initial_guess[1]=np.log10(min(doses[doses!=0])) #If the EC50 is less than the minimum dose
        if curve_initial_guess[0]>2: curve_initial_guess[0]=2 #If the log(h) is greater than the upper bound
        if curve_initial_guess[0]<-2: curve_initial_guess[0]=-2 #If the log(h) is greater than the upper bound
                   
        hill_fn = lambda x,b,e: ll4(x, b, E_fx[1], E_fx[0], e)     
        bounds = [(-2,-np.inf),(2,np.inf)]

        
#    print bounds
#    print curve_initial_guess
    try:
        popt, pcov = scipy.optimize.curve_fit(hill_fn,
                                              doses,
                                              dip_rates,
                                              p0=curve_initial_guess,
                                              sigma=dip_std_errs,
                                              maxfev=100000,
                                              bounds=bounds,
                                              )
        #Calculate uncertainty
        if np.isnan(E_fx).any():
            perr = np.sqrt(np.diag(pcov)) if not np.isnan(pcov).any() else np.zeros((4,)) * np.NAN
        else:
            if np.isnan(pcov).any():
                perr = np.zeros((4,))*np.NAN
            else:
                perr = np.sqrt(np.diag(pcov)) 
                perr = [perr[0],0.,0.,perr[1]]
            popt = [popt[0],E_fx[1],E_fx[0],popt[1]]   
 
    except RuntimeError:
        pass
        
    p = 1#p-value
    #Calculate the pvalue
    if not np.isnan(popt).any():
        # DIP rate fit
        dip_rate_fit_curve = ll4(doses, *popt)
        # F test vs flat linear "no effect" fit
        ssq_model = ((dip_rate_fit_curve - dip_rates) ** 2).sum()
        ssq_null = ((np.mean(dip_rates) - dip_rates) ** 2).sum()
        if np.isnan(E_fx).any():
            df = len(doses) - 4
        else:
            df = len(doses)-2
        f_ratio = (ssq_null-ssq_model)/(ssq_model/df)
        p = 1 - scipy.stats.f.cdf(f_ratio, 1, df)
        if p > null_rejection_threshold:
            popt = np.zeros((4,))*np.nan
            perr = np.zeros((4,))*np.nan
    else:
        popt = np.zeros((4,))*np.nan
        perr =  np.zeros((4,))*np.nan
    
    return popt, p, perr

#####To look at fit
#import matplotlib.pyplot as plt
#plt.figure()
#d = doses.copy()
#d[d==0]=min(d[d!=0])/10
#plt.scatter(np.log10(d),dip_rates)
#plt.plot(np.log10(d),hill_fn(doses,*curve_initial_guess))
#plt.scatter(np.log10(d),hill_fn(d,*popt))

# Functions for finding initial parameter estimates for curve fitting
def ll4_initials(x, y):
    c_val, d_val = _find_cd_ll4(y,x)
    b_val, e_val = _find_be_ll4(x, y, c_val, d_val)
    return b_val, c_val, d_val, e_val
def ll2_initials(x, y,E0_fix,Emx_fix):
    b_val, e_val = _find_be_ll4(x, y, Emx_fix, E0_fix)
    return b_val, e_val

def _response_transform(y, c_val, d_val):
    return np.log10((d_val-c_val)/(y-c_val)-1)


def _find_be_ll4(d, dip_rates, c_val, d_val, slope_scaling_factor=1.):
    x = d.copy()
    y = dip_rates.copy()
    x[x==0]=min(x[x!=0])/100. #Approximate zero dose
    y[y>c_val]=c_val  #Fix effects out side of max and min bounds
    y[y<d_val]=d_val
    #Transform the dose response curve to a line
    y = _response_transform(y,c_val,d_val)
    tmp = x.copy()
    x = np.log10((x[(~np.isnan(y)) & (~np.isinf(y))]))
    y = y[(~np.isnan(y)) & (~np.isinf(y))]
    if len(x)>2:
        slope, intercept, r_value, p_value, std_err = scipy.stats.linregress(x,y)
        b_val = np.log10(slope_scaling_factor * slope)
        e_val = -intercept
        if np.isnan(e_val) or np.isinf(e_val):
            e_val = np.min(x)+2
        if np.isnan(b_val) or np.isinf(b_val):
            b_val = 0.
    else:
        b_val = 0.
        e_val = np.mean(np.log10(tmp))
    return b_val, e_val


def _find_cd_ll4(y,x, scale=0.):
    e0 = np.mean(y[x==min(x)])
    emx = np.mean(y[x==max(x)])
    return emx, e0
