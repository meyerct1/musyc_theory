#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 12:08:51 2019

@author: meyerct6
"""

import os
import pandas as pd
from doseResponseSurfPlot import DosePlots_PLY
from gatherData import subset_data

def plot_PLY(sub_T,expt=None,flip_d1d2=False,zero_conc=1):
    if expt is None:
        expt        = sub_T['expt']

    drug1_name  = sub_T['drug1_name']
    drug2_name  = sub_T['drug2_name']
    sample      = sub_T['sample']
    data        = pd.read_table(expt, delimiter=',')        
    data['drug1'] = data['drug1'].str.lower()
    data['drug2'] = data['drug2'].str.lower()
    data['sample'] = data['sample'].str.upper()
    sub_T['r1'] = 100.
    sub_T['r2'] = 100.
    d1,d2,dip,dip_sd = subset_data(data,drug1_name,drug2_name,sample)
    dip_sd[:] = 0
    sub_T['save_direc'] = os.getcwd()
    #Flip d1 and d2
    if flip_d1d2 is True:
        d1,d2 = d2,d1
        drug1_name,drug2_name=drug2_name,drug1_name
        sub_T['E1'],sub_T['E2'],sub_T['log_C1'],sub_T['log_C2'],sub_T['log_h1'],sub_T['log_h2'],sub_T['log_alpha1'],sub_T['log_alpha2']=sub_T['E2'],sub_T['E1'],sub_T['log_C2'],sub_T['log_C1'],sub_T['log_h2'],sub_T['log_h1'],sub_T['log_alpha2'],sub_T['log_alpha1']
        for i in ['C1','C2','h1','h2']:
            sub_T[i] = 10**sub_T['log_'+i]
            
    popt3 = [sub_T['E0'],sub_T['E1'],sub_T['E2'],sub_T['E3'],sub_T['r1'],sub_T['r2'],10**sub_T['log_C1'],10**sub_T['log_C2'],10**sub_T['log_h1'],10**sub_T['log_h2'],10**sub_T['log_alpha1'],10**sub_T['log_alpha2']]
    DosePlots_PLY(d1,d2,dip,dip_sd,sub_T['drug1_name'],sub_T['drug2_name'],popt3,'NDB',sub_T['sample'],sub_T['expt'],sub_T['metric_name'],sub_T['save_direc'],zero_conc=zero_conc)     

    


